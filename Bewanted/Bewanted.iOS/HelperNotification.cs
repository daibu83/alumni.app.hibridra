using UIKit;
using Foundation;
using System;
using System.IO;

namespace C2Forms.iOS
{
    public class HelperNotification
    {
        //////////////////////////////////////
        public static bool ExistLocalNotification(string id)
        {
            
            var documents = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
            //var cache = Path.Combine(documents, "..", "Library", "Caches");
            string tmp = Path.Combine(documents, "..", "tmp") + "/noti_" + id;
            try {
                UILocalNotification noti = (UILocalNotification)NSKeyedUnarchiver.UnarchiveFile(tmp);
                UIApplication.SharedApplication.CancelLocalNotification(noti);
                NSError err;
                NSFileManager.DefaultManager.Remove(tmp,out err);
                
            } catch (Exception e) { }
            
            
            return false;

        }
        //////////////////////////////////////
    }

}