﻿using System;
using UIKit;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;
using C2Forms;
using C2Forms.iOS;

[assembly: ExportRenderer(typeof(C2ContentPage), typeof(C2PageRenderer))]
namespace C2Forms.iOS
{
    class C2PageRenderer : PageRenderer
    {
        //-----------------------------------------------------

        protected override void OnElementChanged(VisualElementChangedEventArgs e)
        {
            base.OnElementChanged(e);
            if (ViewController != null)
            {
                //C2ContentPage con = (C2ContentPage)this.Element;
                
                ViewController.Title = "PRUEBAS";

                if (ViewController.NavigationController != null)
                    ViewController.NavigationController.Title = "PRUEBAS2";

                if (ViewController.NavigationItem != null)
                {
                    ViewController.NavigationItem.Title = "PRUEBAS3";
                    ViewController.NavigationItem.Prompt = "PRUEBAS4";
                }
            }
        }

        //-----------------------------------------------------

    }
}