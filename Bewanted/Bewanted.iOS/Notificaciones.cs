using System;
using Foundation;
using UIKit;
using C2Forms.iOS;
using Bewanted;
using System.Threading;
using System.IO;

[assembly: Xamarin.Forms.Dependency(typeof(Notificaciones))]
namespace C2Forms.iOS
{
    public class Notificaciones : INotificaciones
    {

        public void contadorIcono(int contador)
        {
            UIApplication.SharedApplication.ApplicationIconBadgeNumber = contador;
        }
        private async void logintoken()
        {

            try
            {
                await HelperService.Login_request();

            }
            catch (Exception e)
            {

            }

        }

        public void iniciaServicio()
        {
            var oldDeviceToken = NSUserDefaults.StandardUserDefaults.StringForKey("PushDeviceToken");

            
            if (UIDevice.CurrentDevice.CheckSystemVersion(8, 0))
            {

                UIApplication.SharedApplication.RegisterForRemoteNotifications();
            }
            else
            {
                UIRemoteNotificationType notificationTypes = UIRemoteNotificationType.Alert | UIRemoteNotificationType.Badge | UIRemoteNotificationType.Sound;
                UIApplication.SharedApplication.RegisterForRemoteNotificationTypes(notificationTypes);

            }

        }
        public void paraServicio()
        {
            Bewanted.AppDelegate._device_token = "";
            logintoken();

        }

    }
}
