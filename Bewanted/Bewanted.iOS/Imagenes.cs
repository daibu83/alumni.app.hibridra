using System;
using Foundation;
using UIKit;
using C2Forms.iOS;
using Bewanted;
using System.Threading;
using System.IO;
using System.Threading.Tasks;
using System.Net.Http;

[assembly: Xamarin.Forms.Dependency(typeof(Imagenes))]
namespace C2Forms.iOS
{

    public class Imagenes : IImages
    {
        public async Task<UIImage> LoadImage(string imageUrl)
        {
            var httpClient = new HttpClient();

            Task<byte[]> contentsTask = httpClient.GetByteArrayAsync(imageUrl);

            // await! control returns to the caller and the task continues to run on another thread
            var contents = await contentsTask;

            // load from bytes
            return UIImage.LoadFromData(NSData.FromArray(contents));
        }

        public async Task<byte[]> ImageFromUrl(string text)
        {

            byte[] image_data = null;

            UIImage image = await LoadImage(text);
            if (image != null)
            {
                UIImage scaled_image = null;

                if (image.Size.Width > 200.0f)
                {
                    try
                    {
                        CoreGraphics.CGSize new_size = new CoreGraphics.CGSize();
                        new_size.Width = 200.0f;
                        new_size.Height = image.Size.Height * new_size.Width / image.Size.Width;
                        UIGraphics.BeginImageContext(new_size);
                        image.Draw(new CoreGraphics.CGRect(0, 0, new_size.Width, new_size.Height));
                        scaled_image = UIGraphics.GetImageFromCurrentImageContext();
                        UIGraphics.EndImageContext();
                    }
                    catch (Exception)
                    {
                        scaled_image = image;
                    }
                }
                else
                {
                    scaled_image = image;
                }

                if (scaled_image != null)
                {
                    NSData data = scaled_image.AsJPEG(0.8f);
                    if (data != null)
                    {
                        image_data = new byte[data.Length];
                        System.Runtime.InteropServices.Marshal.Copy(data.Bytes, image_data, 0, Convert.ToInt32(data.Length));
                    }
                }
            }
            return image_data;
        }
    }
}
