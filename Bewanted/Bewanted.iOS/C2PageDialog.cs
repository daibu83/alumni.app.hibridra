using System;
using UIKit;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;
using C2Forms;
using C2Forms.iOS;
using CoreFoundation;

[assembly: ExportRenderer(typeof(C2PageDialog), typeof(C2PageDialogRenderer))]
namespace C2Forms.iOS
{
    class C2PageDialogRenderer : PageRenderer
    {

        static async void OnCloseRequested(object sender, C2PageDialog.CloseModalRequestedEventArgs e)
        {
            var page = (C2PageDialog)sender;

            var viewController = PlatformMethods.GetRenderer(page).ViewController;

            if (viewController != null && !viewController.IsBeingDismissed)
            {
         
                DispatchQueue.MainQueue.DispatchAfter(DispatchTime.Now, async () => {
                    e.ClosingPageTask = viewController.DismissViewControllerAsync(true);
                    await e.ClosingPageTask;
                    PlatformMethods.DisposeModelAndChildrenRenderers(page);
                });
            }
        }

        protected override void OnElementChanged(VisualElementChangedEventArgs e)
        {
            base.OnElementChanged(e);

            // UI settings
            this.ModalPresentationStyle = UIModalPresentationStyle.OverCurrentContext;
            this.ModalTransitionStyle = UIModalTransitionStyle.CoverVertical;

            // Close event
            if (e.OldElement as C2PageDialog != null)
            {
                var hostPage = (C2PageDialog)e.OldElement;
                hostPage.CloseModalRequested -= OnCloseRequested;
            }

            if (e.NewElement as C2PageDialog != null)
            {
                var hostPage = (C2PageDialog)e.NewElement;
                hostPage.CloseModalRequested += OnCloseRequested;
            }

            

        }

        

        public override void ViewDidLayoutSubviews()
        {
            base.ViewDidLayoutSubviews();

            // Set size manualy because page is rendered outside of Visual tree
            base.ViewDidLayoutSubviews();
            SetElementSize(new Xamarin.Forms.Size(View.Bounds.Width, View.Bounds.Height));
        }

        

    }


}