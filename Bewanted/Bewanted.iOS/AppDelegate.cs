﻿using Foundation;
using UIKit;
using C2Forms.iOS;
using System;
using CoreFoundation;
using System.IO;
using System.Threading.Tasks;

namespace Bewanted.iOS
{
    // The UIApplicationDelegate for the application. This class is responsible for launching the 
    // User Interface of the application, as well as listening (and optionally responding) to 
    // application events from iOS.
    [Register("AppDelegate")]
    public partial class AppDelegate : global::Xamarin.Forms.Platform.iOS.FormsApplicationDelegate
    {

        public static UIViewController viewController;
        private UIWindow window;
        
        //
        // This method is invoked when the application has loaded and is ready to run. In this 
        // method you should instantiate the window, load the UI into it and then make the window
        // visible.
        //
        // You have 17 seconds to return from this method, or iOS will terminate your application.
        //
        public override bool FinishedLaunching(UIApplication app, NSDictionary options)
        {
            PickImageServiceIOS.app = app;
            GAServiceImp.Initialize();
            global::Xamarin.Forms.Forms.Init();
            viewController = new UIViewController();
            window = new UIWindow(UIScreen.MainScreen.Bounds);

            if (UIDevice.CurrentDevice.CheckSystemVersion(8, 0))
            {
                var settings = UIUserNotificationSettings.GetSettingsForTypes(UIUserNotificationType.Alert | UIUserNotificationType.Badge | UIUserNotificationType.Sound, null);
                UIApplication.SharedApplication.RegisterUserNotificationSettings(settings);

                if (UIApplication.SharedApplication.CurrentUserNotificationSettings.Types != settings.Types)
                {
                    Bewanted.AppDelegate._user_notify = false;
                }


            }

            //var settings = UIUserNotificationSettings.GetSettingsForTypes(UIUserNotificationType.Alert | UIUserNotificationType.Badge | UIUserNotificationType.Sound, null);
            //UIApplication.SharedApplication.RegisterUserNotificationSettings(settings);

            //if (UIApplication.SharedApplication.CurrentUserNotificationSettings.Types != settings.Types)
            //{
            //    Bewanted.AppDelegate._user_notify = false;
            //}
            window.RootViewController = viewController;
            window.MakeKeyAndVisible();
            // check for a notification
            if (options != null)
            {
                // check for a local notification
                if (options.ContainsKey(UIApplication.LaunchOptionsLocalNotificationKey))
                {
                    var localNotification = options[UIApplication.LaunchOptionsLocalNotificationKey] as UILocalNotification;
                    if (localNotification != null)
                    {
                        UIAlertController okayAlertController = UIAlertController.Create(localNotification.AlertAction, localNotification.AlertBody, UIAlertControllerStyle.Alert);
                        okayAlertController.AddAction(UIAlertAction.Create("OK", UIAlertActionStyle.Default, null));

                        viewController.PresentViewController(okayAlertController, true, null);
                        
                    }
                }
            }
            
            LoadApplication(new App(App.Platform.iOS));
            // Handling Push notification when app is closed if App was opened by Push Notification...
            if (options != null && options.Keys != null && options.ContainsKey(new NSString("UIApplicationLaunchOptionsRemoteNotificationKey")))
            {
                NSDictionary UIApplicationLaunchOptionsRemoteNotificationKey = options.ObjectForKey(new NSString("UIApplicationLaunchOptionsRemoteNotificationKey")) as NSDictionary;
                processNotification(UIApplicationLaunchOptionsRemoteNotificationKey, false);
            }
            return base.FinishedLaunching(app, options);
        }

        public override void OnActivated(UIApplication application)
        {
            // Call the 'ActivateApp' method to log an app event for use
            // in analytics and advertising reporting. This is optional
            Facebook.CoreKit.AppEvents.ActivateApp();
            Facebook.CoreKit.AppEvents.Flush();
        }

        public override bool OpenUrl(UIApplication application, NSUrl url, string sourceApplication, NSObject annotation)
        {
            Console.WriteLine(url);
            /* now store the url somewhere in the app’s context. The url is in the url NSUrl object. The data is in url.Host if the link as a scheme as superduperapp://something_interesting */
            return true;
        }

        public static void ExisteNotificacion(string id) {
            UILocalNotification[] notifications = UIApplication.SharedApplication.ScheduledLocalNotifications;

            foreach (UILocalNotification pnotification in notifications)
            {
                var puserInfo = pnotification.UserInfo;
                if (puserInfo != null)
                {
                    if (puserInfo.ValueForKey(new NSString("TaskID")).ToString() == "BeWanted")
                    {
                        if (puserInfo.ValueForKey(new NSString("NotifyID")).ToString() == id)
                        {
                            //Ya esta notificada-Cancelar anterior y actualizar
                            UIApplication.SharedApplication.CancelLocalNotification(pnotification);
                        }


                    }

                }

            }
        }
        public override void ReceivedLocalNotification(UIApplication application, UILocalNotification notification)
        {

        }


        private async void logintoken()
        {

            try
            {
                await HelperService.Login_request();

            }
            catch (Exception e)
            {

            }

        }

        public override void RegisteredForRemoteNotifications(UIApplication application, NSData deviceToken)
        {
            // Get current device token
            string DeviceToken = deviceToken.Description;
            if (!string.IsNullOrWhiteSpace(DeviceToken))
            {
                DeviceToken = DeviceToken.Trim('<').Trim('>');
            }

            // Get previous device token
            string oldDeviceToken = NSUserDefaults.StandardUserDefaults.StringForKey("PushDeviceToken");

            // Has the token changed?
            if (string.IsNullOrEmpty(oldDeviceToken) || !oldDeviceToken.Equals(DeviceToken))
            {
                //TODO: Put your own logic here to notify your server that the device token has changed/been created!
                
            }
            // Save new device token 
            NSUserDefaults.StandardUserDefaults.SetString(DeviceToken, "PushDeviceToken");
            Bewanted.AppDelegate._device_token = DeviceToken;
            //new UIAlertView("Registrado Servicio de notificaciones", DeviceToken, null, "OK", null).Show();
          
            logintoken();
        }

        // We've received a notification, yay!
        public override void ReceivedRemoteNotification(UIApplication application, NSDictionary userInfo)
        {
            Console.WriteLine("PushData received " + userInfo.ToString());
            processNotification(userInfo, false);
        }


        
        public override void FailedToRegisterForRemoteNotifications(UIApplication application, NSError error)
        {
            new UIAlertView("Error registrando notificaciones. Servicio no disponible", error.LocalizedDescription, null, "OK", null).Show();
        }

        void processNotification(NSDictionary options, bool fromFinishedLaunching)
        {

            //Check to see if the dictionary has the aps key.  This is the notification payload you would have sent
            if (null != options && options.ContainsKey(new NSString("aps")))
            {
                //Get the aps dictionary
                NSDictionary aps = options.ObjectForKey(new NSString("aps")) as NSDictionary;

                string alert = string.Empty;
                //string id = string.Empty;
                
                string sound = string.Empty;
                string titleStr = string.Empty;
                nint badge = -1;

                //Extract the alert text
                //NOTE: If you're using the simple alert by just specifying "  aps:{alert:"alert msg here"}  "
                //      this will work fine.  But if you're using a complex alert with Localization keys, etc., your "alert" object from the aps dictionary
                //      will be another NSDictionary... Basically the json gets dumped right into a NSDictionary, so keep that in mind
                if (aps.ContainsKey(new NSString("alert")))
                    alert = (aps[new NSString("alert")] as NSString).ToString();

                //Extract the sound string
                if (aps.ContainsKey(new NSString("sound")))
                    sound = (aps[new NSString("sound")] as NSString).ToString();

                //Extract the badge
                if (aps.ContainsKey(new NSString("badge")))
                {
                    string badgeStr = (aps[new NSString("badge")] as NSObject).ToString();
                    nint.TryParse(badgeStr, out badge);
                    if (badge > 0)
                        UIApplication.SharedApplication.ApplicationIconBadgeNumber = badge;
                }

                if (null != options && options.ContainsKey(new NSString("contador")))
                {
                    string badgeStr = (options[new NSString("contador")] as NSObject).ToString();
                    nint.TryParse(badgeStr, out badge);
                  

                    if (options.ContainsKey(new NSString("contenido")))
                        alert = (options[new NSString("contenido")] as NSString).ToString();

                    //if (options.ContainsKey(new NSString("id")))
                    //  id = (options[new NSString("id")] as NSString).ToString();-
                    if (badge >= 0)
                        UIApplication.SharedApplication.ApplicationIconBadgeNumber = badge;


                    //if (UIApplication.SharedApplication.ApplicationState != UIApplicationState.Active)
                    //{
                    //    SendLocalNotification("Bewanted", alert, "", "", "1", "", 0);
                    //}

                    Bewanted.AppDelegate._refreshcont = true;
                  

                }

                if (UIApplication.SharedApplication.ApplicationState == UIApplicationState.Active)
                {
                    UIAlertView avAlert = new UIAlertView("Bewanted", alert, null, "OK", null);
                    avAlert.Show();
                }

            }

            //You can also get the custom key/value pairs you may have sent in your aps (outside of the aps payload in the json)
            // This could be something like the ID of a new message that a user has seen, so you'd find the ID here and then skip displaying
            // the usual screen that shows up when the app is started, and go right to viewing the message, or something like that.
            //if (null != options && options.ContainsKey(new NSString("title")))
            //{
            //    //launchWithCustomKeyValue = (options[new NSString("customKeyHere")] as NSString).ToString();

            //    //You could do something with your customData that was passed in here
            //}
        }

        public void SendLocalNotification(string title, string description, string descripcion_extendida, string sumary, string id, string fecha, int iconID)
        {
            //   throw new NotImplementedException();
            // create the notification
            var notification = new UILocalNotification();
            //int bewantednotifies;
            // set the fire date (the date time in which it will fire)
            //notification.FireDate = NSDate.FromTimeIntervalSinceNow(5);

            // configure the alert

            //notification.AlertTitle = title;
            notification.AlertBody = description;
            //notification.AlertLaunchImage = "icon.png";
            // modify the badge
            //notification.ApplicationIconBadgeNumber = notification.ApplicationIconBadgeNumber + 1;

            // set the sound to be the default sound
            //notification.SoundName = UILocalNotification.DefaultSoundName;
            var keys = new object[] { "TaskID", "NotifyID" };
            var values = new object[] { "BeWanted", id };
            var dict = NSDictionary.FromObjectsAndKeys(values, keys);
            //var userInfo = NSDictionary.FromObjectAndKey(new NSString("TaskID"), new NSString("BeWanted"));
            var userInfo = dict;
            notification.UserInfo = userInfo;

     
            if (HelperNotification.ExistLocalNotification(id) == false)
            {

            }
            //notification.ApplicationIconBadgeNumber = bewantednotifies + 1;
            notification.SoundName = UILocalNotification.DefaultSoundName;
            
            
            UIApplication.SharedApplication.PresentLocalNotificationNow(notification);

            var documents = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
            string tmp = Path.Combine(documents, "..", "tmp") + "/noti_" + id;
            NSKeyedArchiver.ArchiveRootObjectToFile(notification, tmp);




        }
    }
}

