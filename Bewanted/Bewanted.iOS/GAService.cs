﻿using System;
using System.Collections.Generic;
using System.Text;
using Google.Analytics;
using Bewanted.iOS;

[assembly: Xamarin.Forms.Dependency(typeof(GAServiceIOS))]
namespace Bewanted.iOS
{
    public class GAServiceImp
    {
        private static string TrackingId = "UA-41165542-7";
        private static ITracker GATracker;

        //-----------------------------------------------------

        public static ITracker GetTracker()
        {
            return GAServiceImp.GATracker;
        }

        //-----------------------------------------------------

        public static void Initialize()
        {
            Gai.SharedInstance.DispatchInterval = 10;
            Gai.SharedInstance.TrackUncaughtExceptions = true;
            GAServiceImp.GATracker = Gai.SharedInstance.GetTracker(GAServiceImp.TrackingId);
        }

        //-----------------------------------------------------
    }

    //-----------------------------------------------------

    public class GAServiceIOS : IGAService
    {
        public void Track_screen(String screen_name)
        {
            ITracker tracker = GAServiceImp.GetTracker();
            tracker.Set(GaiConstants.ScreenName, screen_name);
            tracker.Send(DictionaryBuilder.CreateScreenView().Build());
        }
    }
}
