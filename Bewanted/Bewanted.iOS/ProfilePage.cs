
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;
using Bewanted;
using C2Forms.iOS;
using CoreFoundation;

[assembly: ExportRenderer(typeof(ProfilePage), typeof(ProfilePageRenderer))]
namespace C2Forms.iOS
{
    class ProfilePageRenderer : PageRenderer
    {
        protected override void OnElementChanged(VisualElementChangedEventArgs e)
        {
            base.OnElementChanged(e);

            if (e.OldElement as ProfilePage != null)
            {
                var hostPage = (ProfilePage)e.OldElement;
                hostPage.DisplayPageModalRequested -= OnDisplayPageModalRequested;
            }

            if (e.NewElement as ProfilePage != null)
            {
                var hostPage = (ProfilePage)e.NewElement;
                hostPage.DisplayPageModalRequested += OnDisplayPageModalRequested;
            }
        }

        void OnDisplayPageModalRequested(object sender, ProfilePage.DisplayPageModalRequestedEventArgs e)
        {
            e.PageToDisplay.Parent = this.Element;
            IVisualElementRenderer renderer = PlatformMethods.GetRenderer(e.PageToDisplay);
            if (renderer == null)
            {
                renderer = RendererFactory.GetRenderer(e.PageToDisplay);
                PlatformMethods.SetRenderer(e.PageToDisplay, renderer);
            }

            DispatchQueue.MainQueue.DispatchAfter(DispatchTime.Now, async () =>
            {
                e.DisplayingPageTask = this.PresentViewControllerAsync(renderer.ViewController, true);
            });
        }
    }
}

