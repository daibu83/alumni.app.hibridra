using System;
using Android.Content;
using Android.Gms.Analytics;
using Bewanted.Droid;

[assembly: Xamarin.Forms.Dependency(typeof(GAServiceAndroid))]
namespace Bewanted.Droid
{
    public class GAServiceImp
    {
        private static string TrackingId = "UA-41165542-7";
        private static GoogleAnalytics GAInstance;
        private static Tracker GATracker;
        private static GAServiceImp thisRef;

        //-----------------------------------------------------

        private GAServiceImp()
        {

        }

        //-----------------------------------------------------

        public static GAServiceImp GetGASInstance()
        {
            if (GAServiceImp.thisRef == null)
                GAServiceImp.thisRef = new GAServiceImp();
            return GAServiceImp.thisRef;
        }

        //-----------------------------------------------------

        public static Tracker GetTracker()
        {
            return GAServiceImp.GATracker;
        }

        //-----------------------------------------------------

        public void Initialize(Context AppContext)
        {
            GAServiceImp.GAInstance = GoogleAnalytics.GetInstance(AppContext.ApplicationContext);
            GAServiceImp.GAInstance.SetLocalDispatchPeriod(10);
            GAServiceImp.GATracker = GAServiceImp.GAInstance.NewTracker(TrackingId);
            GAServiceImp.GATracker.EnableExceptionReporting(true);
            GAServiceImp.GATracker.EnableAdvertisingIdCollection(true);
            GAServiceImp.GATracker.EnableAutoActivityTracking(false);
        }

        //-----------------------------------------------------
    }

    //-----------------------------------------------------

    public class GAServiceAndroid : IGAService
    {
        public void Track_screen(String screen_name)
        {
            Tracker tracker = GAServiceImp.GetTracker();
            tracker.SetScreenName(screen_name);
            tracker.Send(new HitBuilders.ScreenViewBuilder().Build());
        }

        //-----------------------------------------------------

        /*
        public void TrackEvent(String GAEventCategory, String EventToTrack)
        {
            HitBuilders.EventBuilder builder = new HitBuilders.EventBuilder();
            builder.SetCategory(GAEventCategory);
            builder.SetAction(EventToTrack);
            builder.SetLabel("AppEvent");
            GATracker.Send(builder.Build());
        }*/

        //-----------------------------------------------------

        /*
        public void TrackException(String ExceptionMessageToTrack, Boolean isFatalException)
        {
            HitBuilders.ExceptionBuilder builder = new HitBuilders.ExceptionBuilder();
            builder.SetDescription(ExceptionMessageToTrack);
            builder.SetFatal(isFatalException);
            GATracker.Send(builder.Build());
        }*/
    }
}

