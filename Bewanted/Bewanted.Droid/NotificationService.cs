using System;
using Android.App;
using Android.Content;
using Android.OS;
using System.Threading;
using Android.Util;
using System.Collections.Generic;

namespace Bewanted.Droid
{
    
    [Service(Enabled = true)]
    public class NotificationService : Service
    {
        
        static readonly string TAG = "X:" + typeof(NotificationService).Name;
        //static readonly int TimerWait = 60000*15;
        static readonly int TimerWait = 60000;
        static Timer _timer;
        long _user_id;
        string _device_id;
        string _access_token;
        bool _notify;

        public override void OnCreate()
        {
            base.OnCreate();
        }

        public  void inicia() {
            _timer = new Timer(o => {DoWork();}, null, 0,TimerWait);
        }
        

        public override void OnTaskRemoved(Intent rootIntent)
        {
            if (this._notify) {
                base.OnTaskRemoved(rootIntent);
                Intent restartService = new Intent(this, typeof(NotificationService));
                restartService.PutExtra("user_id", this._user_id);
                restartService.PutExtra("device_id", this._device_id);
                AlarmManager mgr = (AlarmManager)this.GetSystemService(Context.AlarmService);
                PendingIntent pi = PendingIntent.GetService(this, 1, restartService, PendingIntentFlags.OneShot);
                mgr.Set(AlarmType.ElapsedRealtime, SystemClock.ElapsedRealtime() + 1000, pi);
            }
        }

        public override StartCommandResult OnStartCommand(Android.Content.Intent intent, StartCommandFlags flags, int startId)
        {
            //this._user_id = intent.GetLongExtra("user_id", 0);
            //Log.Debug("aqui","{0}", this._user_id);
            //this._device_id = intent.GetStringExtra("device_id");
            //Log.Debug("aqui ", "{0}", this._device_id);
            Log.Info("NotificationService", "NotificationService started");
            HelperService._token_expire_date = new DateTime(1970, 1, 1);
            try {
                List<string> data = new List<string>();
                HelperService.Read_data(data);
                if (data.Count >= 2)
                {
                    //email = data[0];
                    //password = data[1];
                    this._user_id = Convert.ToInt64(data[2]);
                    this._device_id = data[3];
                    this._notify = true;
                    if (data[4]== "N"){
                        //No desea recibir notificaciones-No arrancar el servicio
                        this._notify = false;
                        OnDestroy();
                    }
                }

            } catch (Exception e)
            {
                
            }
            
            inicia();
            return StartCommandResult.Sticky;
        }


        public override IBinder OnBind(Intent intent)
        {
            return null;
        }

        private void DoWork()
        {
            Log.Debug(TAG, "Hello  Obt access. {0}", DateTime.UtcNow);
            HelperService.i_Access_token();
            this._access_token = HelperService._access_token;
            Log.Debug(TAG, "Hello  Fin Obt access. {0}", DateTime.UtcNow);


            Log.Debug(TAG, "Hello  from Service. {0}", DateTime.UtcNow);
            //Comprueba mensajes y ofertas y muestra notificacion si es necesario
            HelperService.notificacionOfertasMensajes(this._access_token, this._user_id, this._device_id);
            Log.Debug(TAG, "Hello 2 from Service. {0}", DateTime.UtcNow);
            //Consulta notificaciones personalizadas
            HelperService.Update_notificaciones(this._access_token,this._user_id,this._device_id);
            Log.Debug(TAG, "Hello 3 from Service. {0}", DateTime.UtcNow);
        }

        public override void OnDestroy()
        {
            
            base.OnDestroy();
            Log.Info("EXIT", "ondestroy!");
            _timer.Dispose();
            _timer = null;
            Log.Debug(TAG, "Ooooops! Service destroyed at {0}.", DateTime.UtcNow);
        }

    }
}