using Bewanted;
using Android.App;
using Bewanted.Droid;
using Xamarin.Forms.Platform.Android;
using Xamarin.Forms;
using Android.Graphics.Drawables;
using Android.Text;
using Android.Text.Style;
using C2Forms;

//Para formatear cabecera bloqueada en dialogs
[assembly: ExportRenderer(typeof(PersonalPage), typeof(CustomNavigationRenderer))]
namespace Bewanted.Droid
{
    public class CustomNavigationRenderer : PageRenderer
    {
        protected override void OnElementChanged(ElementChangedEventArgs<Page> e)
        {
            base.OnElementChanged(e);
            var actionBar = ((Activity)Context).ActionBar;
        }

        protected override void OnLayout(bool changed, int l, int t, int r, int b)
        {
            base.OnLayout(changed, l, t, r, b);
            var actionBar = ((Activity)Context).ActionBar;
            
            int android_api = DependencyService.Get<IDeviceService>().AndroidAPILevel();
            if (actionBar.Title == "Datos personales ")
            {
                actionBar.SetBackgroundDrawable(new ColorDrawable(Color.FromHex("#192728").ToAndroid()));
                
                var st = new SpannableString("Datos personales ");
                st.SetSpan(new ForegroundColorSpan(Color.FromHex("#5E6262").ToAndroid()), 0, st.Length(), SpanTypes.ExclusiveExclusive);
                //#5E6262
                actionBar.TitleFormatted = st;
                actionBar.SetHomeAsUpIndicator(Resource.Drawable.icbacklight);
            }

        }
    }
}
