using System.ComponentModel;
using System.Linq;
using Android.Graphics;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;
using C2Forms;
using C2Forms.Droid;
using Android.App;
using System.Threading.Tasks;
using System.Net.Http;
using Android.Graphics.Drawables;

[assembly: ExportRenderer(typeof(C2Image), typeof(C2ImageRenderer))]
namespace C2Forms.Droid
{
    public class C2ImageRenderer : ImageRenderer
    {


        private bool _isDecoded;

        private async Task<Bitmap> GetImageBitmapFromUrl(string url)
        {
            Bitmap imageBitmap = null;

            try
            {
                var uri = Android.Net.Uri.Parse(url);
                HttpClient http_client;
                HttpResponseMessage http_response = null;
                http_client = new HttpClient();
                http_response = await http_client.GetAsync(url);
                if (http_response.IsSuccessStatusCode)
                {
                    var imageBytes = await http_response.Content.ReadAsByteArrayAsync();
                    if (imageBytes != null && imageBytes.Length > 0)
                    {

                        try { imageBitmap = BitmapFactory.DecodeByteArray(imageBytes, 0, imageBytes.Length); } catch { } 

                                            }
                }
            }
            catch (System.Net.WebException e)
            {
                
            }

            return imageBitmap;
        }

        protected async override void OnElementPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            base.OnElementPropertyChanged(sender, e);

            var largeImage = (C2Image)Element;
            Android.Widget.ImageView img = Control;
            //if (Element.Width > 0 && Element.Height > 0 && e.PropertyName=="IsLoading") { }
            //else { return; }


            if (e.PropertyName == Image.IsLoadingProperty.PropertyName
            && !this.Element.IsLoading && this.Control!= null && Element.Width > 0 && Element.Height > 0)
            {
            }
            else { return; }

            if (largeImage.Uri == null) return;
            if (largeImage.Uri == "") return;
            var options = new BitmapFactory.Options { InJustDecodeBounds = true };

            
            

            //The with and height of the elements (XLargeImage) will be used to decode the image
            var width = (int)Element.Width;
            var height = (int)Element.Height;
            options.InSampleSize = CalculateInSampleSize(options, width, height);

            options.InJustDecodeBounds = false;
            //var uri=Android.Net.Uri.Parse(largeImage.Uri);

            Android.Graphics.Bitmap bitmap = await GetImageBitmapFromUrl(largeImage.Uri);


            //Bitmap bitmap = img.GetDrawingCache(false);


            Android.Graphics.Bitmap scaled_bitmap;
            if (bitmap != null) {
                Control.SetImageBitmap(null);
                if (bitmap.Width > 100)
                {
                    int new_width = 100;
                    int new_height = 100;
                    //int new_height = bitmap.Height * new_width / bitmap.Width;
                    scaled_bitmap = Android.Graphics.Bitmap.CreateScaledBitmap(bitmap, new_width, new_height, false);
                }
                else
                {
                    scaled_bitmap = bitmap;
                }

                System.IO.MemoryStream jpg_stream = new System.IO.MemoryStream();
                scaled_bitmap.Compress(Android.Graphics.Bitmap.CompressFormat.Jpeg, 70, jpg_stream);


                //BitmapFactory.Options options = new BitmapFactory.Options();
                //options.inSampleSize = 2; //decrease decoded image 
                //Bitmap bitmap = BitmapFactory.decodeStream(is, null, options);
                //bitmap.compress(Bitmap.CompressFormat.JPEG, 70, fos);

                //var bitmap = BitmapFactory.DecodeStream(largeImage,null,options);

                //Set the bitmap to the native control
                Control.SetImageBitmap(scaled_bitmap);
                bitmap = null;
                scaled_bitmap = null;
                //_isDecoded = true;
            }
            
        }
        public int CalculateInSampleSize(BitmapFactory.Options options, int reqWidth, int reqHeight)
        {
            // Raw height and width of image
            float height = options.OutHeight;
            float width = options.OutWidth;
            var inSampleSize = 1D;

            if (!(height > reqHeight) && !(width > reqWidth)) return (int)inSampleSize;
            var halfHeight = (int)(height / 2);
            var halfWidth = (int)(width / 2);

            // Calculate a inSampleSize that is a power of 2 - the decoder will use a value that is a power of two anyway.
            while ((halfHeight / inSampleSize > reqHeight) && (halfWidth / inSampleSize > reqWidth))
            {
                inSampleSize *= 2;
            }

            return (int)inSampleSize;
        }

       

}
}