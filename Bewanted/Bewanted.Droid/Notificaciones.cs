
using System;
using C2Forms.Droid;
using Android.App;
using Bewanted.Droid;
using Android.Content;
using Android.Gms.Common;
using Bewanted;
using Android.Gms.Gcm.Iid;
using Android.Gms.Gcm;
using System.Threading.Tasks;

[assembly: Xamarin.Forms.Dependency(typeof(Notificaciones))]
namespace C2Forms.Droid
{
    public class Notificaciones : INotificaciones
    {
        public void contadorIcono(int contador) {
            
            
        }

        public void iniciaServicio()
        {
            if (IsPlayServicesAvailable())
            {
                Intent intent = new Intent(Application.Context, typeof(RegistrationIntentService));
                Application.Context.StartService(intent);

            }
        }

        public static async void pararPUSH() {
            await System.Threading.Tasks.Task.Run(() =>
            {
                var instanceID = InstanceID.GetInstance(Application.Context);
                string token;
                //version de bewanted anterior

                token = instanceID.GetToken(
                    "991195049208", GoogleCloudMessaging.InstanceIdScope, null);

                //version de bewanted nueva
                //token = instanceID.GetToken(
                //   "1077756587223", GoogleCloudMessaging.InstanceIdScope, null);
                instanceID.DeleteToken(token, GoogleCloudMessaging.InstanceIdScope);
                instanceID.DeleteInstanceID();
                AppDelegate._device_token = "";
            });
        }

        public void paraServicio()
        {
            pararPUSH();
        }

        public bool IsPlayServicesAvailable()
        {
            int resultCode = GoogleApiAvailability.Instance.IsGooglePlayServicesAvailable(Application.Context);
            if (resultCode != ConnectionResult.Success)
            {
                if (GoogleApiAvailability.Instance.IsUserResolvableError(resultCode))
                    AppDelegate._googleservices = GoogleApiAvailability.Instance.GetErrorString(resultCode);
                else
                {
                    AppDelegate._googleservices = "KO";

                }
                return false;
            }
            else
            {
                AppDelegate._googleservices = "OK";
                return true;
            }
        }

    }
}

