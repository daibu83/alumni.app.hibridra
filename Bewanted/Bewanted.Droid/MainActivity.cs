﻿using Android.App;
using Android.OS;
using Android.Content.PM;
using C2Forms.Droid;
using Android.Gms.Common;
using Android.Content;
using System;
using Android.Preferences;
using Android.Support.V4.Content;
using System.Collections.Generic;
using Xamarin.Facebook.AppEvents;
using Xamarin.Facebook;

namespace Bewanted.Droid
{
    //-----------------------------------------------------
    //android:host="www.myurl.com"
    //          android:pathPrefix="/openmyapp"
    //        android:scheme="http" >
    //[IntentFilter(new[] { Intent.ActionView }, Categories = new[] { Intent.CategoryDefault, Intent.CategoryBrowsable }, DataScheme = "http", DataHost = "jadispro.com", DataPathPrefix = "/openapp/")]
    [Activity(Label = "Bewanted", Icon = "@drawable/icon",  ConfigurationChanges = ConfigChanges.ScreenSize | ConfigChanges.Orientation, ScreenOrientation = ScreenOrientation.Portrait)]
    //[IntentFilter(new[] { Intent.ActionView }, Categories = new[] { Intent.CategoryDefault, Intent.CategoryBrowsable }, DataScheme = "bewanted", DataHost = "openapp.com")]
    public class MainActivity : global::Xamarin.Forms.Platform.Android.FormsApplicationActivity
    {
        //-----------------------------------------------------
        const int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;
        const string TAG = "MainActivity";

        BroadcastReceiver mRegistrationBroadcastReceiver;

        protected override void OnActivityResult(int requestCode, Result resultCode, Android.Content.Intent data)
        {
            base.OnActivityResult(requestCode, resultCode, data);
            if (requestCode == PickImageServiceAndroid.PICK_IMAGE_REQUEST_CODE)
            {
                if (resultCode == Result.Ok)
                    PickImageServiceAndroid.Load_image(data);
                else
                    PickImageServiceAndroid.Load_image(null);
            }
        }

        //-----------------------------------------------------
        private async void logintoken()
        {
            
            try
            {
                await HelperService.Login_request();

            }
            catch (Exception e)
            {

            }

        }

        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);
            PickImageServiceAndroid.activity = this;
            GAServiceImp.GetGASInstance().Initialize(this);
            global::Xamarin.Forms.Forms.Init(this, bundle);
            
            mRegistrationBroadcastReceiver = new BroadcastReceiver();
            mRegistrationBroadcastReceiver.Receive += (sender, e) => {
                var sharedPreferences = PreferenceManager.GetDefaultSharedPreferences((Context)sender);
                var sentToken = sharedPreferences.GetBoolean(com.Bewanted.QuickstartPreferences.SENT_TOKEN_TO_SERVER, false);
                logintoken();
                //mInformationTextView.Text = sentToken ? GetString(Resource.String.gcm_send_message) : GetString(Resource.String.token_error_message);
            };

            FacebookSdk.SdkInitialize(Xamarin.Forms.Forms.Context);
            AppEventsLogger.ActivateApp(this);
            LoadApplication(new App(App.Platform.Android));
        }
        
        

          protected override void OnSaveInstanceState(Bundle outState)
          {
        
              // always call the base implementation!
              base.OnSaveInstanceState(outState);
          }

        protected override void OnStart()
        {
            // Handle when your app starts
            //ReStartSchedule();
            base.OnStart();
            Xamarin.Facebook.AppEvents.AppEventsLogger.ActivateApp(Xamarin.Forms.Forms.Context);
        }

        protected override void OnResume()
        {
            base.OnResume();
            Xamarin.Facebook.AppEvents.AppEventsLogger.ActivateApp(Xamarin.Forms.Forms.Context);

            LocalBroadcastManager.GetInstance(this).RegisterReceiver(mRegistrationBroadcastReceiver,
                new IntentFilter(com.Bewanted.QuickstartPreferences.REGISTRATION_COMPLETE));
        }

        protected override void OnPause()
        {
            LocalBroadcastManager.GetInstance(this).UnregisterReceiver(mRegistrationBroadcastReceiver);
            base.OnPause();
        }

        protected override void OnDestroy()
        {
          
            base.OnDestroy();

        }

        
        

        class BroadcastReceiver : Android.Content.BroadcastReceiver
        {
            public EventHandler<BroadcastEventArgs> Receive { get; set; }

            public override void OnReceive(Context context, Intent intent)
            {
                if (Receive != null)
                    Receive(context, new BroadcastEventArgs(intent));
            }
        }

        class BroadcastEventArgs : EventArgs
        {
            public Intent Intent { get; private set; }

            public BroadcastEventArgs(Intent intent)
            {
                Intent = intent;
            }
        }

    }
}

