using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Util;


namespace Bewanted.Droid
{
    [BroadcastReceiver]
    [IntentFilter(new[] { Intent.ActionBootCompleted },
    Categories = new[] { "android.intent.category.HOME" })]
    public class OnBootRestartService : BroadcastReceiver
    {
        public override void OnReceive(Context context, Intent intent)
        {
            //  Conte.startService(new Intent(context, YourService.class));
            //Intent intent = new Intent(Application.Context, typeof(NotificationService));
            Log.Info("Broadcast", "System Boot!!");
            //Application.Context.StartService(new Intent(Application.Context, typeof(NotificationService)));
            Intent rebootintent = new Intent(Application.Context, typeof(NotificationService));
            Application.Context.StartService(rebootintent);
        }

        
    }
}