using System;
using System.Collections.Generic;
using Android.App;
using Android.Content;
using Android.OS;
using System.Threading.Tasks;
using Android.Graphics;
using C2Core;
using System.IO;

namespace Bewanted.Droid
{
    //Funciones traidas al servicio y que tienen su correspondiente copia o parecida en Portable o Interfaces
    public class HelperService
    {
        public static uint _user_new_offers;
        public static long _user_unreaded_messages;
        public static DateTime _token_expire_date;
        public static string _access_token;

        public static void Read_data(List<string> data)
        {
            try
            {
                Stream stream = Application.Context.OpenFileInput("cache_data");
                BinaryReader reader = new BinaryReader(stream);
                int count = reader.ReadInt32();
                for (int i = 0; i < count; ++i)
                {
                    string str;
                    str = reader.ReadString();
                    data.Add(str);
                }

                reader.Close();
                stream.Close();
            }
            catch (Exception e)
            {
            }
        }

        public static async Task<ServerResponse> i_Access_token()
        {
            ServerResponse response;
            if (_token_expire_date > DateTime.Now)
            {
                response.type = ResponseType.OK_SUCCESS;
                response.message = "";
            }
            else
            {
                response = await i_Authenticate();
            }

            return response;
        }

        private static DateTime i_Date_from_unix_time(long seconds_from_epoch)
        {
            return new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc).AddSeconds(seconds_from_epoch);
        }

        public static async Task<ServerResponse> i_Authenticate()
        {
            ServerResponse<AuthJSON> auth_response;
            ServerResponse response;

            auth_response = await Server.Authentication_request();

            if (auth_response.type == ResponseType.OK_SUCCESS)
            {
                _access_token = auth_response.result_object.access_token;
                _token_expire_date = i_Date_from_unix_time(auth_response.result_object.expires - 3600);
                response.type = ResponseType.OK_SUCCESS;
                response.message = "";
            }
            else if (auth_response.type == ResponseType.SERVER_ERROR)
            {
                response.type = ResponseType.AUTHENTICATION_ERROR;
                response.message = auth_response.message;
            }
            else if (auth_response.type == ResponseType.SERVICE_ERROR)
            {
                response.type = ResponseType.AUTHENTICATION_ERROR;
                response.message = auth_response.message;
            }

            else if (auth_response.type == ResponseType.URL_ERROR)
            {
                response.type = ResponseType.AUTHENTICATION_ERROR;
                response.message = auth_response.message;
            }
            else
            {
                response.type = auth_response.type;
                response.message = auth_response.message;
            }

            return response;
        }


        public static void i_Get_common_request_params(long user_id, string access_token, string device_id, ref List<string> key_list, ref List<string> value_list)
        {
            long date;
            string version;
            string device_type;
            string md5;
            key_list = new List<string>();
            value_list = new List<string>();
            date = Server.i_Date_to_unix_milliseconds(DateTime.Now);
            version = Server._APP_VERSION;
            device_type = "android";
            md5 = MD5.GetHashString(date.ToString() + device_id + Server._SUFFIX_MD5);
            key_list.Add("date");
            value_list.Add(date.ToString());
            key_list.Add("version");
            value_list.Add(version);
            key_list.Add("device_id");
            value_list.Add(device_id);
            key_list.Add("device_type");
            value_list.Add(device_type);
            key_list.Add("student_id");
            value_list.Add(user_id.ToString());
            key_list.Add("md5");
            value_list.Add(md5);
            key_list.Add("access_token");
            value_list.Add(access_token);
        }


        public static async Task<ServerResponse<LoginJSON>> Login_request(string access_token, string email, string password)
        {
            long user_id;
            List<string> key_list = null;
            List<string> value_list = null;
            user_id = 0;
            Server.i_Get_common_request_params(user_id, access_token, ref key_list, ref value_list);
            key_list.Add("email");
            value_list.Add(email);
            key_list.Add("password");
            value_list.Add(password);
            key_list.Add("device_token");
            value_list.Add(AppDelegate._device_token);
            return await Server.i_Get_server_data<LoginJSON>(Server._URL_BASE + Server._LOGIN_SERVICE, key_list, value_list);
        }

        
    }
}