using Android.App;
using Android.Content;
using Android.OS;
using Android.Gms.Gcm;
using Android.Util;
using Android.Graphics;
using Bewanted.Droid;
using Bewanted;

namespace com.Bewanted
{
    [Service(Exported = false), IntentFilter(new[] { "com.google.android.c2dm.intent.RECEIVE" })]
    public class MyGcmListenerService : GcmListenerService
    {
        public override void OnMessageReceived(string from, Bundle data)
        {
            
            string description = data.GetString("message");
            string descripcion_extendida = data.GetString("messageExtend");
            string sumary = data.GetString("sumary");
            string id = data.GetString("id");
            string title = data.GetString("title");
            
            Log.Debug("MyGcmListenerService", "From:    " + from);
            Log.Debug("MyGcmListenerService", "Message: " + description);
            SendNotification(title,description,descripcion_extendida,sumary,id);
        }

        void SendNotification(string title, string description, string descripcion_extendida, string sumary, string id)
        {
           /* var intent = new Intent(this, typeof(MainActivity));
            intent.AddFlags(ActivityFlags.ClearTop);
            var pendingIntent = PendingIntent.GetActivity(this, 0, intent, PendingIntentFlags.OneShot);

            var notificationBuilder = new Notification.Builder(this)
                .SetSmallIcon(Resource.Drawable.ic_stat_ic_notification)
                .SetContentTitle("GCM Message")
                .SetContentText(message)
                .SetAutoCancel(true)
                .SetContentIntent(pendingIntent);

            var notificationManager = (NotificationManager)GetSystemService(Context.NotificationService);
            notificationManager.Notify(0, notificationBuilder.Build());*/




            Intent intent = new Intent(Application.Context, typeof(MainActivity));
            int notificationId = 0;
            if (id != "" && id != null) { notificationId = System.Convert.ToInt32(id); }
            PendingIntent pIntent = PendingIntent.GetActivity(Application.Context, notificationId, intent, PendingIntentFlags.CancelCurrent);

            Notification.BigTextStyle textStyle = new Notification.BigTextStyle();

            if (descripcion_extendida != "" && descripcion_extendida != null)
            {
                textStyle.BigText(descripcion_extendida);
            }
            else { textStyle.BigText(description); }

            if (title != "" && title != null)
            {

            }
            else {
                title = "bewanted";
            }

            if (sumary != "" && sumary != null) { textStyle.SetSummaryText(sumary); }

            Notification.Builder builder = new Notification.Builder(Application.Context)
                .SetContentTitle(title)
                .SetContentText(description)
                .SetContentIntent(pIntent)
                .SetDefaults(NotificationDefaults.Sound)
                .SetAutoCancel(true)
                .SetSmallIcon(Resource.Drawable.icon_notify);

            if (descripcion_extendida != "" && descripcion_extendida != null)
            {
                builder.SetStyle(textStyle);
                Bitmap bitmap = BitmapFactory.DecodeResource(Application.Context.Resources, Resource.Drawable.icon);
                builder.SetLargeIcon(bitmap);
            }

            if (Build.VERSION.SdkInt >= BuildVersionCodes.Lollipop)
            {
                int whiteColorValue = Color.White;
                builder.SetColor(whiteColorValue);
                builder.SetPriority(2);
            }
            Notification notification = builder.Build();

            //para evitar con largeIcon que aparezcan los dos-se sustituye el small por el icon a mostrar en la notificacion NO Extendida
            notification.ContentView.SetImageViewResource(Android.Resource.Id.Icon, Resource.Drawable.icon);

            //notification.Flags |= NotificationFlags.OnlyAlertOnce;

            NotificationManager notificationManager =
            Application.Context.GetSystemService(Context.NotificationService) as NotificationManager;


            notificationManager.Notify(notificationId, notification);
            
            AppDelegate._refreshcont = true;
            

        }
    }
}