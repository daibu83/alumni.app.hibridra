using System;
using Android.App;
using Android.Content;
using Android.Util;
using Android.Gms.Gcm;
using Android.Gms.Gcm.Iid;
using Android.Preferences;

namespace Bewanted.Droid
{
    [Service(Exported = false)]
    class RegistrationIntentService : IntentService
    {
        static object locker = new object();

        public RegistrationIntentService() : base("RegistrationIntentService") { }

        protected override void OnHandleIntent(Intent intent)
        {
            var sharedPreferences = PreferenceManager.GetDefaultSharedPreferences(this);
            try
            {
                Log.Info("RegistrationIntentService", "Calling InstanceID.GetToken");
                lock (locker)
                {
                    var instanceID = InstanceID.GetInstance(this);
                    string token;
                    token = instanceID.GetToken(
                        "991195049208", GoogleCloudMessaging.InstanceIdScope, null);
                    //token = instanceID.GetToken(
                    //   "1077756587223", GoogleCloudMessaging.InstanceIdScope, null);
                    if (AppDelegate._user_notify == true)
                    {

#if DEBUG
        instanceID.DeleteToken(token, GoogleCloudMessaging.InstanceIdScope);
        instanceID.DeleteInstanceID();
        token = instanceID.GetToken("991195049208", GoogleCloudMessaging.InstanceIdScope, null);
        //token = instanceID.GetToken("1077756587223", GoogleCloudMessaging.InstanceIdScope, null);
#endif
                        Log.Info("RegistrationIntentService", "GCM Registration Token: " + token);
                    }
                    else
                    {
                        instanceID.DeleteToken(token, GoogleCloudMessaging.InstanceIdScope);
                        instanceID.DeleteInstanceID();
                        Log.Info("UnRegistrationIntentService", "GCM UNRegistration Token: " + token);
                        token = "";
                    }
                    
                    SendRegistrationToAppServer(token);
                    Subscribe(token);
                    sharedPreferences.Edit().PutBoolean(com.Bewanted.QuickstartPreferences.SENT_TOKEN_TO_SERVER, true).Apply();
                }
            }
            catch (Exception e)
            {
                Log.Debug("RegistrationIntentService", "Failed to get a registration token");
                sharedPreferences.Edit().PutBoolean(com.Bewanted.QuickstartPreferences.SENT_TOKEN_TO_SERVER, false).Apply();
                return;
            }
            var registrationComplete = new Intent(com.Bewanted.QuickstartPreferences.REGISTRATION_COMPLETE);
            Android.Support.V4.Content.LocalBroadcastManager.GetInstance(this).SendBroadcast(registrationComplete);
        }

        void SendRegistrationToAppServer(string token)
        {
            // Add custom implementation here as needed.
            //llamar a login 3 con el token
            AppDelegate._device_token = token;
            

        }

        void Subscribe(string token)
        {
            var pubSub = GcmPubSub.GetInstance(this);
            pubSub.Subscribe(token, "/topics/global", null);
        }
    }
}