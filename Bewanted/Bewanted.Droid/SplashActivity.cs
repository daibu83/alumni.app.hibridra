using System.Threading.Tasks;
using Android.App;
using Android.Content;
using Android.Support.V7.App;
using Android.Content.PM;

namespace Bewanted.Droid
{
    [Activity(Theme = "@style/MyTheme.Splash", MainLauncher = true, NoHistory = true, ConfigurationChanges = ConfigChanges.ScreenSize | ConfigChanges.Orientation, ScreenOrientation = ScreenOrientation.Portrait)]
    public class SplashActivity : AppCompatActivity
    {
        //-----------------------------------------------------

            /*
        public override void OnCreate(Bundle savedInstanceState, PersistableBundle persistentState)
        {
            base.OnCreate(savedInstanceState, persistentState);
        }*/

        //-----------------------------------------------------

        protected override void OnResume()
        {
            base.OnResume();

            Task startupWork = new Task(() => 
            {
                Task.Delay(1000); // Simulate a bit of startup work.

            });

            startupWork.ContinueWith(t =>
            {
                StartActivity(new Intent(Application.Context, typeof(MainActivity)));
            }, TaskScheduler.FromCurrentSynchronizationContext());

            startupWork.Start();
        }
    }
}


