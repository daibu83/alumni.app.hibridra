using System;
using Android.Runtime;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;
using C2Forms;
using C2Forms.Droid;
using Android.Graphics.Drawables;

[assembly: ExportRenderer(typeof(C2Badge2), typeof(C2Badge2Renderer))]
namespace C2Forms.Droid
{
    class C2Badge2Renderer : FrameRenderer
    {
        protected override void OnElementChanged(ElementChangedEventArgs<Frame> e)
        {
            base.OnElementChanged(e);
            C2Badge2 extFrame = e.NewElement as C2Badge2;
            if (extFrame != null) {
                GradientDrawable gd = new Android.Graphics.Drawables.GradientDrawable();
               // gd.SetColor(extFrame.BorderColor.ToAndroid());
                gd.SetCornerRadius(extFrame.Radious);
                gd.SetStroke(extFrame.BorderWidth, extFrame.BorderColor.ToAndroid());

                SetBackgroundDrawable(gd);
            }


            ////var extFrame = e.NewElement as CR.ExtendedFrame;

            ////var gd = new Android.Graphics.Drawables.GradientDrawable();

            ////gd.SetColor(extFrame.BackgroundColor.ToAndroid());

            ////if (extFrame.HasUnevenBorder)
            ////{
            ////    gd.SetCornerRadii(extFrame.CornerBorderRadius);
            ////}
            ////else
            ////{
            ////    gd.SetCornerRadius((float)extFrame.Radius);
            ////}

            ////gd.SetStroke(extFrame.BorderWidth, extFrame.BorderColor.ToAndroid());

            ////SetBackgroundDrawable(gd);

        }

        protected override void OnElementPropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            base.OnElementPropertyChanged(sender, e);


            
            if (Element != null)
            {
                C2Badge2 extFrame = (C2Badge2)Element;
                GradientDrawable gd = new Android.Graphics.Drawables.GradientDrawable();
                // gd.SetColor(extFrame.BorderColor.ToAndroid());
                gd.SetCornerRadius(extFrame.Radious);
                gd.SetStroke(extFrame.BorderWidth, extFrame.BorderColor.ToAndroid());

                SetBackgroundDrawable(gd);
            }


            


        }

    }
}