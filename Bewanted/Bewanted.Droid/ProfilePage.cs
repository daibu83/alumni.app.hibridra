using Bewanted;
using Android.App;
using Bewanted.Droid;
using Xamarin.Forms.Platform.Android;
using Xamarin.Forms;
using Android.Graphics.Drawables;
using Android.Text;
using Android.Text.Style;
using C2Forms;

//Para formatear cabecera bloqueada en dialogs
[assembly: ExportRenderer(typeof(ProfilePage), typeof(ProfilePageRenderer))]
namespace Bewanted.Droid
{
    public class ProfilePageRenderer : PageRenderer
    {
        protected override void OnElementChanged(ElementChangedEventArgs<Page> e)
        {
            base.OnElementChanged(e);
            var actionBar = ((Activity)Context).ActionBar;
        }

        protected override void OnLayout(bool changed, int l, int t, int r, int b)
        {
            base.OnLayout(changed, l, t, r, b);
            var actionBar = ((Activity)Context).ActionBar;
                    
            int android_api = DependencyService.Get<IDeviceService>().AndroidAPILevel();
            if (actionBar.Title == "Editar perfil ")
            {
                actionBar.SetBackgroundDrawable(new ColorDrawable(Color.FromHex("#192728").ToAndroid()));
                var st = new SpannableString("Editar perfil ");
                st.SetSpan(new ForegroundColorSpan(Color.FromHex("#5E6262").ToAndroid()), 0, st.Length(), SpanTypes.ExclusiveExclusive);
                //#5E6262
                actionBar.TitleFormatted = st;
                if (android_api >= 1 && android_api <= 20)
                {
                    actionBar.SetHomeAsUpIndicator(Resource.Drawable.menu_marginpop);
                }
                else
                {
                    actionBar.SetHomeAsUpIndicator(Resource.Drawable.menupop);
                }

            }
            else {
                if (actionBar.Title == "Editar perfil  ") {
                    //private static readonly Color _NAVIGATION_BAR = Color.FromHex("3A6360");
                    //private static readonly Color _NAVIGATION_BAR_TEXT = Color.White;
                    actionBar.SetBackgroundDrawable(new ColorDrawable(Color.FromHex("#3A6360").ToAndroid()));
                    var st = new SpannableString("Editar perfil");
                    st.SetSpan(new ForegroundColorSpan(Color.White.ToAndroid()), 0, st.Length(), SpanTypes.ExclusiveExclusive);
                    //#5E6262
                    actionBar.TitleFormatted = st;
                    if (android_api >= 1 && android_api <= 20)
                    {
                        actionBar.SetHomeAsUpIndicator(Resource.Drawable.menu_margin);
                    }
                    else
                    {
                        actionBar.SetHomeAsUpIndicator(Resource.Drawable.menu);
                    }
                }
                    
            }
            

        }
    }
}
