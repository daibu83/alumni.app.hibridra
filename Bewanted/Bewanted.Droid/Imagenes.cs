
using System;
using C2Forms.Droid;
using Android.App;
using Bewanted.Droid;
using Android.Content;
using Android.Gms.Common;
using Bewanted;
using Android.Gms.Gcm.Iid;
using Android.Gms.Gcm;
using System.Threading.Tasks;
using Android.Graphics;
using System.Net.Http;

[assembly: Xamarin.Forms.Dependency(typeof(Imagenes))]
namespace C2Forms.Droid
{
    public class Imagenes : IImages
    {
        public async Task<byte[]> ImageFromUrl(string text)
        {
            byte[] image_data = null;
            Bitmap imageBitmap = null;

            try
            {
                var uri = Android.Net.Uri.Parse(text);
                HttpClient http_client;
                HttpResponseMessage http_response = null;
                http_client = new HttpClient();
                http_response = await http_client.GetAsync(text);
                if (http_response.IsSuccessStatusCode)
                {
                    var imageBytes = await http_response.Content.ReadAsByteArrayAsync();
                    if (imageBytes != null && imageBytes.Length > 0)
                    {

                        try {
                            imageBitmap = BitmapFactory.DecodeByteArray(imageBytes, 0, imageBytes.Length);
                            Android.Graphics.Bitmap scaled_bitmap;
                            if (imageBitmap.Width > 200)
                            {
                                int new_width = 200;
                                int new_height = imageBitmap.Height * new_width / imageBitmap.Width;
                                scaled_bitmap = Android.Graphics.Bitmap.CreateScaledBitmap(imageBitmap, new_width, new_height, false);
                            }
                            else
                            {
                                scaled_bitmap = imageBitmap;
                            }

                            System.IO.MemoryStream jpg_stream = new System.IO.MemoryStream();
                            scaled_bitmap.Compress(Android.Graphics.Bitmap.CompressFormat.Jpeg, 90, jpg_stream);
                            image_data = jpg_stream.ToArray();



                        } catch { }
            

                    }
                }
            }
            catch (System.Net.WebException e)
            {

            }
            return image_data;

        }
    }
}

