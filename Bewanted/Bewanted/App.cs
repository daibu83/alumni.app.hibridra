﻿using System;
using System.Collections.Generic;
using Xamarin.Forms;
using C2Core;
using C2Forms;

namespace Bewanted
{
    public class App : Application
    {
        public enum Platform
        {
            Android,
            iOS
        };

        public Platform _platform;
        public AppDelegate _delegate;
        public int _picking_state;

        
         
        public App(Platform platform)
        {
            this._delegate = null;
            this._platform = platform;
            this._picking_state = 0;
            if (platform == Platform.iOS)
                this.MainPage = new NavigationPage(new PreviousPage(platform));
            else
                this.MainPage = new NavigationPage(new PreviousPage(platform));
        }

        //-----------------------------------------------------

        ~App()
        {
            C2Assert.Is_null(this._delegate);
            if (this._delegate != null) {
                C2Assert.No_null(this._delegate);
                this._delegate.End_app();
                this._delegate.Dispose();
                this._delegate = null;
            }
                
        }

        //-----------------------------------------------------

        private async void i_Start()
        {
            C2Assert.Error(this == Application.Current);
            bool ok;
            try
            {
                if (this._delegate == null)
                {


                    _delegate = new AppDelegate(this);
                    
                    var cultureInfo = DependencyService.Get<ICultureInfo>();
                    if (cultureInfo.CurrentUICulture.TwoLetterISOLanguageName.Contains("es") || cultureInfo.CurrentUICulture.TwoLetterISOLanguageName.Contains("mx"))
                    {
                        if (cultureInfo.CurrentUICulture.TwoLetterISOLanguageName.Contains("es"))
                        {
                            Server._CULTURE_COUNTRY_API = "es";
                            Server._LOCALE_API = "/es";
                        }
                        if (cultureInfo.CurrentUICulture.TwoLetterISOLanguageName.Contains("mx"))
                        {
                            Server._CULTURE_COUNTRY_API = "mx";
                            Server._LOCALE_API = "/mx";
                        }
                    }
                    else
                    {
                        Server._CULTURE_COUNTRY_API = "es";
                        Server._LOCALE_API = "/es";
                    }
                    cultureInfo.CurrentUICulture = new System.Globalization.CultureInfo("es-ES");
                    
                    string email = null;
                    string password = null;
                    if (this._platform == Platform.Android)
                    {
                        List<string> data = new List<string>();
                        DependencyService.Get<IFileService>().Read_data(data);
                        if (data.Count >= 2)
                        {
                            email = data[0];
                            password = data[1];
                        }
                    }
                    else
                    {
                        if (Application.Current.Properties.ContainsKey("email") == true)
                        {
                            email = Convert.ToString(Application.Current.Properties["email"]);
                        }
                        if (Application.Current.Properties.ContainsKey("password") == true)
                            password = Convert.ToString(Application.Current.Properties["password"]);
                    }

                    ok = await _delegate.Start_app(email, password);

                }
                
            }
            catch (System.Exception)
            {

            }
        }

        //-----------------------------------------------------

        


        protected override void OnStart()
        {
            base.OnStart();
            Application.Current.Properties["CODE"] = "qwe5567";
            Application.Current.SavePropertiesAsync();
            this.i_Start();
        }

        //-----------------------------------------------------

        protected override void OnSleep()
        {
            base.OnSleep();
        }

        
//-----------------------------------------------------


        protected override void OnResume()
        {
            base.OnResume();

        }

        //-----------------------------------------------------


    }
}
