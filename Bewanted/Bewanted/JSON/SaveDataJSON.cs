﻿using System.Runtime.Serialization;

namespace Bewanted
{
    [DataContract]
    public class SaveDataJSON
    {

        [DataMember(Name = "student_id")]
        public int student_id;

        [DataMember(Name = "language_id")]
        public int language_id;
        
        [DataMember(Name = "id")]
        public int id;

        [DataMember(Name = "Details")]
        public string details;

        [DataMember(Name = "response")]
        public string response;

        [DataMember(Name = "url")]
        public string url;

        [DataMember(Name = "result")]
        public bool result;

        [DataMember(Name = "error")]
        public bool error;
    }
}
