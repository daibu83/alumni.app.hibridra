﻿using System.Runtime.Serialization;

namespace Bewanted
{
    [DataContract]
    public class ErrorJSON
    {
        [DataMember(Name = "code")]
        public int code;

        [DataMember(Name = "message")]
        public string message;

        [DataMember(Name = "description")]
        public string description;
    }

    //-----------------------------------------------------

    [DataContract]
    public class ServerErrorJSON
    {
        [DataMember(Name = "error")]
        public ErrorJSON error;
    }
}
