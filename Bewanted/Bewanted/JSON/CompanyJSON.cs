﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Bewanted
{
    [DataContract]
    public class CompanyImageSocialJSON
    {
        [DataMember(Name = "ico_class")]
        public string image;
    }

    [DataContract]
    public class CompanySocialJSON
    {
        [DataMember(Name = "url")]
        public string url;

        [DataMember(Name = "social_sites")]
        public CompanyImageSocialJSON icono;
    }

    //-----------------------------------------------------

    [DataContract]
    public class CompanySectorJSON
    {
        [DataMember(Name = "id")]
        public int id;

        [DataMember(Name = "name")]
        public string name;
    }

    //-----------------------------------------------------



    [DataContract]
    public class CompanyJSON
    {
        [DataMember(Name = "id")]
        public int id;


        [DataMember(Name = "company_id")]
        public int orderid;

        [DataMember(Name = "name")]
        public string name;

        [DataMember(Name = "address")]
        public string address;

        [DataMember(Name = "zip_code")]
        public string zip_code;

        
        [DataMember(Name = "city_id")]
        public string city_id;

        

        [DataMember(Name = "state_id")]
        public string state_id;

        [DataMember(Name = "country_id")]
        public string country_id;

        

        [DataMember(Name = "url")]
        public string url;

        

        [DataMember(Name = "description_list")]
        public string description;

        [DataMember(Name = "img_small")]
        public string image_source;

        
        [DataMember(Name = "sectors")]
        public List<CompanySectorJSON> sectors;

        
        [DataMember(Name = "country_def")]
        public CountryJSON country;

        
        [DataMember(Name = "state_def")]
        public StateJSON state;

        
        [DataMember(Name = "city_def")]
        public CityJSON city;

        [DataMember(Name = "social_sites")]
        public List<CompanySocialJSON> social_sites;
    }

    

    //-----------------------------------------------------

    [DataContract]
    public class CompaniesJSON
    {
        [DataMember(Name = "total")]
        public int total;

        [DataMember(Name = "data")]
        public List<CompanyJSON> items;

        [DataMember(Name = "error")]
        public bool error;
    }

    
}


