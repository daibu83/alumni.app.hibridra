﻿using System.Runtime.Serialization;

namespace Bewanted
{
    [DataContract]
    public class AuthJSON
    {
        [DataMember(Name = "access_token")]
        public string access_token;

        [DataMember(Name = "token_type")]
        public string token_type;

        [DataMember(Name = "expires")]
        public long expires;

        [DataMember(Name = "expires_in")]
        public long expires_in;
    }
}