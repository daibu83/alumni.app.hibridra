﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Bewanted
{
    [DataContract]
    public class CertificateJSON
    {
        [DataMember(Name = "id")]
        public int id;

        [DataMember(Name = "value")]
        public string value;
    }
      
}
