﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Bewanted
{
    [DataContract]
    public class UniversityJSON
    {
        [DataMember(Name = "id")]
        public int id;

        //[DataMember(Name = "value")]
        //public string value;
        [DataMember(Name = "country_id")]
        public int country_id;
        [DataMember(Name = "country")]
        public CountryJSON country;
        [DataMember(Name = "state_id")]
        public int state_id;
        [DataMember(Name = "state")]
        public StateJSON state;

        [DataMember(Name = "name")]
        public string name;

        //[DataMember(Name = "label")]
        //public string label;
    }

    
}
