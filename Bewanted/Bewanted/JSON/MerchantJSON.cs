﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Bewanted
{
    
    //-----------------------------------------------------

    [DataContract]
    public class MerchantJobLocationJSON
    {
        [DataMember(Name = "id")]
        public int id;

        [DataMember(Name = "country_id")]
        public int country_id;

        [DataMember(Name = "country")]
        public string country;

        [DataMember(Name = "state_id")]
        public int state_id;

        [DataMember(Name = "state")]
        public string state;
    }

    //-----------------------------------------------------
    [DataContract]
    public class MerchantSectorPivotJSON
    {
        [DataMember(Name = "sector_id")]
        public int id;


    }

    [DataContract]
    public class MerchantSectorJSON
    {
        [DataMember(Name = "pivot")]
        public MerchantSectorPivotJSON pivot;

        [DataMember(Name = "sector")]
        public string name;
    }

    //-----------------------------------------------------
    [DataContract]
    public class MerchantPositionPivotJSON
    {
        [DataMember(Name = "position_id")]
        public int id;


    }

    [DataContract]
    public class MerchantPositionJSON
    {
        [DataMember(Name = "pivot")]
        public MerchantPositionPivotJSON pivot;


        [DataMember(Name = "position")]
        public string name;
    }

    //-----------------------------------------------------

    [DataContract]
    public class MerchantDataJSON
    {
        [DataMember(Name = "student_data")]
        public StudentDataJSON student_data;

        [DataMember(Name = "student_joblocations")]
        public List<MerchantJobLocationJSON> locations;

        [DataMember(Name = "searching")]
        public string searching;

        [DataMember(Name = "where")]
        public string where;

        [DataMember(Name = "hours")]
        public string hours;

        [DataMember(Name = "positions")]
        public string desc_positions;

        [DataMember(Name = "sectors")]
        public string desc_sectors;

        [DataMember(Name = "availability")]
        public string availability;
        
        [DataMember(Name = "student_sectors")]
        public List<MerchantSectorJSON> sectors;

        [DataMember(Name = "student_positions")]
        public List<MerchantPositionJSON> positions;
    }

    //-----------------------------------------------------

    [DataContract]
    public class MerchantJSON
    {
        [DataMember(Name = "result")]
        public MerchantDataJSON data;

        [DataMember(Name = "error")]
        public bool error;
    }
}
