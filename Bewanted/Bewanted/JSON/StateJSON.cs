﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Bewanted
{
    [DataContract]
    public class StateJSON
    {
        [DataMember(Name = "id")]
        public int id;
        
        [DataMember(Name = "name")]
        public string label;
    }
}
