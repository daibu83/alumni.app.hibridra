﻿using System.Runtime.Serialization;

namespace Bewanted
{
    
    //-----------------------------------------------------

    [DataContract]
    public class VoluntaryDataJSON
    {
        [DataMember(Name = "id")]
        public int id;

        [DataMember(Name = "ong")]
        public string ong;

        //[DataMember(Name = "position")]
        //public PositionJSON position;
        [DataMember(Name = "started_at")]
        public string started_at;

        [DataMember(Name = "ystart")]
        public int year_start;

        [DataMember(Name = "mstart")]
        public int month_start;

        [DataMember(Name = "finished_at")]
        public string finished_at;

        [DataMember(Name = "yend")]
        public int year_end;

        [DataMember(Name = "mend")]
        public int month_end;

        [DataMember(Name = "country_id")]
        public int country_id;

        [DataMember(Name = "state_id")]
        public int state_id;

        [DataMember(Name = "state")]
        public StateJSON state;
    }

    //-----------------------------------------------------

    [DataContract]
    public class VoluntaryJSON
    {
        [DataMember(Name = "result")]
        public VoluntaryDataJSON data;

        [DataMember(Name = "error")]
        public bool error;
    }
}



