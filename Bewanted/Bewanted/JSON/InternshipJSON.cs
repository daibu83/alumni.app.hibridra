﻿using System.Runtime.Serialization;

namespace Bewanted
{
    

    //-----------------------------------------------------

    [DataContract]
    public class InternshipDataJSON
    {
        [DataMember(Name = "id")]
        public int id;
        
        [DataMember(Name = "country_id")]
        public int country_id;

        [DataMember(Name = "country")]
        public CountryJSON country;

        [DataMember(Name = "description")]
        public string description;

        [DataMember(Name = "year")]
        public int year;

        

        
    }

    //-----------------------------------------------------

    [DataContract]
    public class InternshipJSON
    {
        [DataMember(Name = "result")]
        public InternshipDataJSON data;

        [DataMember(Name = "error")]
        public bool error;
    }
}

