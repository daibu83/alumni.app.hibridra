﻿using System.Runtime.Serialization;

namespace Bewanted
{
    [DataContract]
    public class LanguageDescriptionJSON
    {
        [DataMember(Name = "id")]
        public int id;

        [DataMember(Name = "name")]
        public string name;
    }

    //-----------------------------------------------------

    [DataContract]
    public class LanguageLevelJSON
    {
        [DataMember(Name = "id")]
        public int id;

        [DataMember(Name = "name")]
        public string name;
    }

    //-----------------------------------------------------

    [DataContract]
    public class LanguageCertificateJSON
    {
        [DataMember(Name = "id")]
        public int id;

        [DataMember(Name = "name")]
        public string name;
    }

    //-----------------------------------------------------

    [DataContract]
    public class LanguageDataJSON
    {
        
        [DataMember(Name = "language_id")]
        public string language_id;
        [DataMember(Name = "language")]
        public LanguageDescriptionJSON language;
        [DataMember(Name = "language_level_id")]
        public string language_level_id;
        [DataMember(Name = "language_level")]
        public LanguageLevelJSON level;
        [DataMember(Name = "certificate_id")]
        public string certificate_id;
        [DataMember(Name = "certificate")]
        public LanguageCertificateJSON certificate;
    }

    //-----------------------------------------------------

    [DataContract]
    public class LanguageJSON
    {
        [DataMember(Name = "result")]
        public LanguageDataJSON data;

        [DataMember(Name = "error")]
        public bool error;
    }
}

