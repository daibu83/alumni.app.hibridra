﻿using System.Runtime.Serialization;

namespace Bewanted
{
    
    
    [DataContract]
    public class RegisterJSON
    {
        [DataMember(Name = "student_id")]
        public int id;
            
        [DataMember(Name = "error")]
        public bool error;
    }
}
