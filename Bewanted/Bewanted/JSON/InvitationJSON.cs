﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Bewanted
{
    //student_id":3,"job_offer_id":167,"job_offers_status_id":2,"comments":"","company_id":21266,"created_at":"2014-11-06 11:10:20","updated_at":"2017-01-10 08:12:36","deleted":"0","id":38,"notify":false}
    [DataContract]
    public class InvitationJSON
    {
        [DataMember(Name = "student_id")]
        public string student_id;

        [DataMember(Name = "job_offer_id")]
        public string job_offer_id;

        [DataMember(Name = "job_offer_status_id")]
        public int job_offer_status_id;

        [DataMember(Name = "company_id")]
        public string company_id;

        [DataMember(Name = "created_at")]
        public string created_at;
                
        [DataMember(Name = "id")]
        public string offer_id;
        
    }

    //-----------------------------------------------------

    
}
