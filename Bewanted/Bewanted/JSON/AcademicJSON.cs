﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Bewanted
{
    //-----------------------------------------------------

    [DataContract]
    public class AcademicStudyJSON
    {
        [DataMember(Name = "id")]
        public int id;

        [DataMember(Name = "name")]
        public string name;

        [DataMember(Name = "academic_level")]
        public string academic_level;

        [DataMember(Name = "study_level_NAME")]
        public string study_level_name;

        [DataMember(Name = "study_level_id")]
        public string study_level_id;
    }

    //-----------------------------------------------------

    [DataContract]
    public class AcademicStateJSON
    {
        [DataMember(Name = "id")]
        public int id;

        [DataMember(Name = "name")]
        public string name;
    }

    //-----------------------------------------------------
    [DataContract]
    public class AcademicSubjectPivotJSON
    {
        [DataMember(Name = "qualification")]
        public float qualification;
    }

    [DataContract]
    public class AcademicSubjectJSON
    {
        [DataMember(Name = "id")]
        public int id;

        [DataMember(Name = "name")]
        public string name;

        //[DataMember(Name = "qualification")]
        //public float qualification;

        [DataMember(Name = "pivot")]
        public AcademicSubjectPivotJSON pivot;
    }

    //-----------------------------------------------------

    [DataContract]
    public class AcademicDataJSON
    {
        [DataMember(Name = "id")]
        public int id;
                
        
        [DataMember(Name = "university_id")]
        public string university_id;

        [DataMember(Name = "university")]
        public UniversityJSON university;

        [DataMember(Name = "group_study_id")]
        public int group_id;

        [DataMember(Name = "study_id")]
        public string study_id;

        [DataMember(Name = "study")]
        public AcademicStudyJSON study;

        [DataMember(Name = "started_at")]
        public string started_at;

        [DataMember(Name = "ystart")]
        public int year_start;

        [DataMember(Name = "mstart")]
        public int month_start;

        [DataMember(Name = "finished_at")]
        public string finished_at;

        [DataMember(Name = "yend")]
        public int year_end;

        [DataMember(Name = "mend")]
        public int month_end;

        [DataMember(Name = "average_calification")]
        public float calification;

        [DataMember(Name = "subjects")]
        public List<AcademicSubjectJSON> subjects;
    }

    //-----------------------------------------------------

    [DataContract]
    public class AcademicJSON
    {
        [DataMember(Name = "result")]
        public AcademicDataJSON data;

        [DataMember(Name = "error")]
        public bool error;
    }
}
