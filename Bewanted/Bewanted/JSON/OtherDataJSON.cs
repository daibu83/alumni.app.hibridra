﻿using System.Runtime.Serialization;

namespace Bewanted
{
    [DataContract]
    public class OtherDataDataJSON
    {
        [DataMember(Name = "other_data")]
        public string text;
    }

    //-----------------------------------------------------

    [DataContract]
    public class OtherDataJSON
    {
        [DataMember(Name = "result")]
        public OtherDataDataJSON data;

        [DataMember(Name = "error")]
        public bool error;
    }
}



