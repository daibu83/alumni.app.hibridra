﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Bewanted
{
    [DataContract]
    public class SocialPivotJSON
    {
        [DataMember(Name = "url")]
        public string url;
    }

    [DataContract]
    public class SocialJSON
    {
        [DataMember(Name = "id")]
        public int id;

        [DataMember(Name = "name")]
        public string name;

        [DataMember(Name = "img_ico")]
        public string image;

        [DataMember(Name = "pivot")]
        public SocialPivotJSON pivot;
    }
  
}
