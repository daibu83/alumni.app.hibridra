﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Bewanted
{
    [DataContract]
    public class CityJSON
    {
        [DataMember(Name = "id")]
        public int id;

        [DataMember(Name = "value")]
        public string value;

        [DataMember(Name = "name")]
        public string label;
    }

   
}
