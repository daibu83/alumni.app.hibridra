﻿
using System.Runtime.Serialization;

namespace Bewanted
{


    [DataContract]
    public class CheckEmailJSON
    {
        [DataMember(Name = "available")]
        public int available;

        [DataMember(Name = "error")]
        public bool error;
    }
}

