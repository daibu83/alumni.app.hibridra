﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Bewanted
{
    [DataContract]
    public class CompanyDetailSocialJSON
    {
        [DataMember(Name = "url")]
        public string url;

        [DataMember(Name = "image")]
        public string image;
    }

    //-----------------------------------------------------

    [DataContract]
    public class CompanyDetailSectorJSON
    {
        [DataMember(Name = "name")]
        public string name;
    }

    //-----------------------------------------------------

    [DataContract]
    public class CompanyDetailStateJSON
    {
        [DataMember(Name = "name")]
        public string name;
    }

    //-----------------------------------------------------

    [DataContract]
    public class CompanyDetailCountryJSON
    {
        [DataMember(Name = "name")]
        public string name;
    }

    //-----------------------------------------------------

    [DataContract]
    public class CompanyDetailDataJSON
    {
        [DataMember(Name = "id")]
        public int id;

        [DataMember(Name = "name")]
        public string name;

        [DataMember(Name = "url")]
        public string url;

        [DataMember(Name = "address")]
        public string address;

        [DataMember(Name = "zip_code")]
        public string zip_code;

        [DataMember(Name = "description")]
        public string description;

        [DataMember(Name = "img_small")]
        public string image;

        [DataMember(Name = "social_sites")]
        public List<CompanyDetailSocialJSON> social_sites;

        [DataMember(Name = "sectors")]
        public List<CompanyDetailSectorJSON> sectors;

        [DataMember(Name = "state")]
        public CompanyDetailStateJSON state;

        [DataMember(Name = "country")]
        public CompanyDetailCountryJSON country;
    }

    

    //-----------------------------------------------------

    [DataContract]
    public class CompanyDetailJSON
    {
        [DataMember(Name = "result")]
        public CompanyDetailDataJSON data;

        [DataMember(Name = "error")]
        public bool error;
    }

    //-----------------------------------------------------

}
