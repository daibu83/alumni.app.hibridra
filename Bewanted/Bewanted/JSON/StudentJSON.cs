﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Bewanted
{

    

    [DataContract]
    public class StudentLocationJSON
    {
        [DataMember(Name = "country_id")]
        public string country_id;
        [DataMember(Name = "country")]
        public CountryJSON country;
        [DataMember(Name = "state_id")]
        public string state_id;
        [DataMember(Name = "state")]
        public StateJSON state;
        [DataMember(Name = "city_id")]
        public string city_id;
        [DataMember(Name = "city")]
        public CityJSON city;
        

    }

    //-----------------------------------------------------

    
    
    [DataContract]
    public class StudentDataJSON
    {

        //student id
        [DataMember(Name = "id")]
        public int id;

        //percentaje
        [DataMember(Name = "profile_percentage")]
        public int percentaje;

        //personal data
        [DataMember(Name = "first_name")]
        public string name;
        
        [DataMember(Name = "last_name")]
        public string last_name;

        [DataMember(Name = "telephone")]
        public string phone;

        [DataMember(Name = "email")]
        public string email;

        [DataMember(Name = "img_profile")]
        public string image_url;

        [DataMember(Name = "nationality")]
        public int nationality_id;

        [DataMember(Name = "completed_registration")]
        public int completed_registration;

        [DataMember(Name = "birth_date")]
        public string birth_date;

        //settings
        [DataMember(Name = "wants_offers")]
        public string public_profile;

        [DataMember(Name = "newsletter")]
        public string newsletter;
        
        [DataMember(Name = "notify")]
        public string notify;

        
        
        //redes sociales
                
        [DataMember(Name = "social_networks")]
        public List<SocialJSON> sites;

        //informacion de Merchant

        [DataMember(Name = "first_employ")]
        public string first_job;

        [DataMember(Name = "trainee")]
        public string scholarship;

        [DataMember(Name = "non_profit")]
        public string ong;

        [DataMember(Name = "unpaid_job")]
        public string updaid;

        [DataMember(Name = "any_country")]
        public string any_country;

        [DataMember(Name = "any_state")]
        public string any_state;

        [DataMember(Name = "availability")]
        public string availability;

        [DataMember(Name = "morning")]
        public string morning;

        [DataMember(Name = "afternoon")]
        public string afternoon;

        [DataMember(Name = "all_day")]
        public string all_day;

        [DataMember(Name = "any_position")]
        public string any_position;

        [DataMember(Name = "any_sector")]
        public string any_sector;

        //OTROS DATOS
        [DataMember(Name = "other_data")]
        public string data;


        [DataMember(Name = "location")]
        public StudentLocationJSON location;

        
    }


    //-----------------------------------------------------


}
