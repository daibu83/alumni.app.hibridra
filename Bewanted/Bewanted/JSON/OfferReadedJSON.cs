﻿using System.Runtime.Serialization;

namespace Bewanted
{
    

    //-----------------------------------------------------

    [DataContract]
    public class OfferReadedJSON
    {
        [DataMember(Name = "newInvitationChanges")]
        public int offers_not_readed;

        [DataMember(Name = "total")]
        public int total;
               

        [DataMember(Name = "error")]
        public bool error;
    }
}
