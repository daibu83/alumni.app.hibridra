﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Bewanted
{
    

    //-----------------------------------------------------

    [DataContract]
    public class CVOtherJSON
    {
        [DataMember(Name = "student_id")]
        public int id;

        [DataMember(Name = "other_data")]
        public string data;
    }

    //-----------------------------------------------------

    [DataContract]
    public class CVDataJSON
    {
        [DataMember(Name = "personal_data")]
        public StudentDataJSON personal;

        [DataMember(Name = "academic_data")]
        public List<AcademicDataJSON> academic;

        [DataMember(Name = "merchant")]
        public MerchantDataJSON merchant;

        [DataMember(Name = "languages")]
        public List<LanguageDataJSON> languages;

        [DataMember(Name = "internships")]
        public List<InternshipDataJSON> internships;

        [DataMember(Name = "experience")]
        public List<ExperienceDataJSON> experience;

        [DataMember(Name = "voluntary")]
        public List<VoluntaryDataJSON> voluntary;

        [DataMember(Name = "other_data")]
        public string other;
    }

    //-----------------------------------------------------

    [DataContract]
    public class CVJSON
    {
        [DataMember(Name = "result")]
        public CVDataJSON data;

        [DataMember(Name = "error")]
        public bool error;
    }
}
