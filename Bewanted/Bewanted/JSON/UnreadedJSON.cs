﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Bewanted
{
    [DataContract]
    public class UnreadedDataJSON
    {
        [DataMember(Name = "messages_unread_number")]
        public long unreaded_messages;

        [DataMember(Name = "messages_number")]
        public long total_messages;
    }

    //-----------------------------------------------------

    [DataContract]
    public class UnreadedJSON
    {
        [DataMember(Name = "data")]
        public List<UnreadedDataJSON> data;

        [DataMember(Name = "error")]
        public bool error;
    }
}
