﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Bewanted
{
    [DataContract]
    public class PersonalStateJSON
    {
        [DataMember(Name = "id")]
        public int id;

        [DataMember(Name = "name")]
        public string name;
    }

    //-----------------------------------------------------

    [DataContract]
    public class PersonalCityJSON
    {
        [DataMember(Name = "id")]
        public int id;

        [DataMember(Name = "name")]
        public string name;
    }

    //-----------------------------------------------------

    [DataContract]
    public class PersonalLocationJSON
    {
        [DataMember(Name = "country_id")]
        public int country_id;

        [DataMember(Name = "state")]
        public PersonalStateJSON state;

        [DataMember(Name = "city")]
        public PersonalCityJSON city;
    }

    //-----------------------------------------------------

    [DataContract]
    public class PersonalSocialJSON
    {
        [DataMember(Name = "id")]
        public int id;

        [DataMember(Name = "url")]
        public string url;

        [DataMember(Name = "image")]
        public string image;

        [DataMember(Name = "title")]
        public string title;
    }

    //-----------------------------------------------------

    [DataContract]
    public class PersonalDataJSON
    {
        [DataMember(Name = "student_id")]
        public long id;

        [DataMember(Name = "first_name")]
        public string name;

        [DataMember(Name = "last_name")]
        public string last_name;

        [DataMember(Name = "telephone")]
        public string phone;

        [DataMember(Name = "img_profile")]
        public string image_url;

        [DataMember(Name = "nationality")]
        public int nationality_id;

        [DataMember(Name = "location")]
        public PersonalLocationJSON location;

        [DataMember(Name = "birth_date")]
        public string birth_date;

        [DataMember(Name = "social_sites")]
        public List<PersonalSocialJSON> sites;
    }

    //-----------------------------------------------------

    [DataContract]
    public class PersonalJSON
    {
        [DataMember(Name = "result")]
        public PersonalDataJSON data;

        [DataMember(Name = "error")]
        public bool error;
    }
}
