﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Bewanted
{
    [DataContract]
    public class OfferJSON
    {
        [DataMember(Name = "company_id")]
        public int company_id;

        [DataMember(Name = "company_name")]
        public string company_name;

        [DataMember(Name = "img_small")]
        public string company_image;

        [DataMember(Name = "position")]
        public string position;

        [DataMember(Name = "contract_kind_name")]
        public string contract;

        [DataMember(Name = "date_start")]
        public string date_start;

        [DataMember(Name = "date_end")]
        public string date_end;

        [DataMember(Name = "city_name")]
        public string city;

        [DataMember(Name = "country_name")]
        public string country;

        [DataMember(Name = "salary")]
        public string salary;

        [DataMember(Name = "name_offer")]
        public string name;

        [DataMember(Name = "job_offer_id")]
        public int job_offer_id;

        [DataMember(Name = "job_offers_status_id")]
        public int job_offer_status_id;

        //job_offer_status_name
        [DataMember(Name = "name")]
        public string status_name;

        
        [DataMember(Name = "id")]
        public int invitation_id;



        [DataMember(Name = "messages_unread_number")]
        public uint unreaded_messages;

        [DataMember(Name = "messages_number")]
        public uint total_messages;

        //"student_id":1,"job_offer_id":285,"job_offers_status_id":1,"comments":"","company_id":7,"created_at":"2016-12-30 09:20:31","updated_at":"2016-12-30 09:20:31","deleted":"0","id":240577,"name_offer":"Offer name","messages_number":0,"messages_unread_number":0,

    }

    //-----------------------------------------------------

    [DataContract]
    public class OffersJSON
    {
        [DataMember(Name = "data")]
        public List<OfferJSON> items;

        [DataMember(Name = "error")]
        public bool error;
    }
}
