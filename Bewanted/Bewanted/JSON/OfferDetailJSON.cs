﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Bewanted
{
    
    [DataContract]
    public class OfferKillerAnswerDataJSON
    {
        [DataMember(Name = "answer")]
        public string response;

        
    }
    [DataContract]
    public class OfferKillerDataJSON
    {
        [DataMember(Name = "id")]
        public int id;

        [DataMember(Name = "name")]
        public string name;

        [DataMember(Name = "answers")]
        public OfferKillerAnswerDataJSON answers;
    }

    //-----------------------------------------------------

    [DataContract]
    public class OfferMessageDataJSON
    {
        [DataMember(Name = "id")]
        public int id;

        [DataMember(Name = "company_user_id")]
        public int company_user_id;
        [DataMember(Name = "company_user")]
        public OfferUserDataJSON user;

        //[DataMember(Name = "email")]
        //public string email;

        //[DataMember(Name = "subject")]
        //public string subject;

        [DataMember(Name = "message")]
        public string message;

        [DataMember(Name = "updated_at")]
        public string updated_at;

        [DataMember(Name = "created_at")]
        public string created_at;

        [DataMember(Name = "read")]
        public string readed;

        [DataMember(Name = "transmitter")]
        public string from;

        [DataMember(Name = "invitation_id")]
        public string invitation_id;
    }

    //-----------------------------------------------------

    [DataContract]
    public class OfferCompanyDataJSON
    {
        [DataMember(Name = "id")]
        public int id;

        [DataMember(Name = "name")]
        public string name;

        [DataMember(Name = "url")]
        public string url;
    }

    //-----------------------------------------------------

    [DataContract]
    public class OfferUserDataJSON
    {
        [DataMember(Name = "id")]
        public int id;

        [DataMember(Name = "first_name")]
        public string first_name;

        [DataMember(Name = "last_name")]
        public string last_name;

        [DataMember(Name = "email")]
        public string email;
    }

    //-----------------------------------------------------
    [DataContract]
    public class OfferMsgInfoDataJSON
    {

        [DataMember(Name = "messages_number")]
        public int messages_number;

        [DataMember(Name = "messages_unread_number")]
        public int messages_unread_number;

        [DataMember(Name = "messages")]
        public List<OfferMessageDataJSON> messages;
    }

    [DataContract]
    public class OfferMsgDataJSON
    {
        [DataMember(Name = "data")]
        public List<OfferMsgInfoDataJSON> data;

        [DataMember(Name = "numConversations")]
        public int numConversations;
    }


    [DataContract]
    public class OfferDetailDataJSON
    {
        [DataMember(Name = "id")]
        public int id;

        [DataMember(Name = "name")]
        public string name;

        [DataMember(Name = "position")]
        public string position;

        [DataMember(Name = "started_at")]
        public string date_start;

        [DataMember(Name = "finished_at")]
        public string date_end;

        [DataMember(Name = "contract_kind_id")]
        public int contract_kind_id;

        [DataMember(Name = "vacants")]
        public string vacants;
        
        

        [DataMember(Name = "company_description")]
        public string company_description;

        [DataMember(Name = "applicant_description")]
        public string applicant_description;

        [DataMember(Name = "offer_description")]
        public string offer_description;

        [DataMember(Name = "salary")]
        public string salary;

        [DataMember(Name = "visible_salary")]
        public string visible_salary;

        [DataMember(Name = "hour_id")]
        public int hour_id;

        [DataMember(Name = "country_id")]
        public int country_id;

        [DataMember(Name = "country_name")]
        public string country_name;

        [DataMember(Name = "killer_questions")]
        public List<OfferKillerDataJSON> killer_questions;

        [DataMember(Name = "messages")]
        public OfferMsgDataJSON messages;

        [DataMember(Name = "company_id")]
        public int company_id;

        [DataMember(Name = "company")]
        public OfferCompanyDataJSON company;

        [DataMember(Name = "company_user_id")]
        public int company_user_id;
        [DataMember(Name = "company_user")]
        public OfferUserDataJSON user;
    }

    //-----------------------------------------------------

    [DataContract]
    public class OfferDetailJSON
    {
        [DataMember(Name = "result")]
        public OfferDetailDataJSON data;

        [DataMember(Name = "error")]
        public bool error;
    }
}
