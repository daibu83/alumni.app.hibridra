﻿using System;
using System.Collections.Generic;
using Xamarin.Forms;
using C2Forms;

namespace Bewanted
{
    //-----------------------------------------------------

    public class MessageLayout : StackLayout
    {
        private Label _name_label;
        private Label _date_label;
        private Label _text_label;

        //-----------------------------------------------------

        public MessageLayout() : base()
        {
            StackLayout header_layout;
            Label empty_label;
            this.Spacing = AppStyle.Layout_stack_spacing;
            this.Padding = new Thickness(AppStyle.Content_box_padding_horizontal, AppStyle.Content_box_padding_vertical);
            this.BackgroundColor = C2Style.Control_background;
            header_layout = new StackLayout();
            header_layout.Orientation = StackOrientation.Horizontal;
            this._name_label = new Label { Style = AppStyle.Content_label_strong };
            this._name_label.HorizontalOptions = LayoutOptions.Start;
            this._name_label.FontSize = C2Style.Entry_font_size;
            this._name_label.FontAttributes = FontAttributes.Bold;
            empty_label = new Label();
            empty_label.HorizontalOptions = LayoutOptions.FillAndExpand;
            this._date_label = new Label { Style = AppStyle.Content_label };
            this._date_label.HorizontalOptions = LayoutOptions.End;
            this._date_label.FontSize = C2Style.Entry_font_size;
            this._text_label = new Label { Style = AppStyle.Content_label };
            header_layout.Children.Add(this._name_label);
            header_layout.Children.Add(empty_label);
            header_layout.Children.Add(this._date_label);
            this.Children.Add(header_layout);
            this.Children.Add(this._text_label);
        }

        //-----------------------------------------------------

        public void Set_data(string name, string date, string text)
        {
            this._name_label.Text = name;
            this._date_label.Text = date;
            this._text_label.Text = text;
        }

        //-----------------------------------------------------
    }

    //-----------------------------------------------------

    public class MessagesPage : ContentPage
    {
        private AppDelegate _delegate;
        private int _offer_id;
        private int _invitation_id;
        private int _company_user_id;
        private string _message_destiny;
        private NewMessagePage _new_message_page;
        private StackLayout _layout;
        private StackLayout _messages_layout;
        private StackLayout _waiting_layout;
        private OfferPage _offer_page;
        //-----------------------------------------------------

        public MessagesPage(AppDelegate _delegate, OfferPage offer_page) : base()
        {
            Device.OnPlatform(Android: () => NavigationPage.SetTitleIcon(this, "no_image.png"));
            this.Title = Local.txtMessages00;
            this.Style = AppStyle.Page;
            this._delegate = _delegate;
            this._offer_id = int.MinValue;
            this._invitation_id = int.MinValue;
            this._company_user_id = int.MinValue;
            this._message_destiny = null;
            this._new_message_page = null;
            this._layout = new StackLayout();
            this._layout.VerticalOptions = LayoutOptions.Start;
            this._layout.Spacing = AppStyle.Layout_stack_long_spacing;
            this._messages_layout = new StackLayout();
            this._messages_layout.Spacing = AppStyle.Layout_stack_spacing;
            this._layout.Children.Add(this._messages_layout);

            {
                StackLayout buttons_layout;
                C2Button new_message_button;
                buttons_layout = new StackLayout();
                buttons_layout.Padding = new Thickness(AppStyle.Button_padding_horizontal, AppStyle.Button_padding_vertical);
                buttons_layout.Spacing = AppStyle.Layout_stack_long_spacing;
                new_message_button = new C2Button { Text = Local.txtMessages01 };
                new_message_button.Style = AppStyle.Button4_style;
                new_message_button.Clicked += i_On_new_message;
                buttons_layout.Children.Add(new_message_button);
                this._layout.Children.Add(buttons_layout);
            }

            this._waiting_layout = UserPage.Waiting_layout();
            this._offer_page = offer_page;

            Content = new ScrollView
            {
                Content = this._layout
            };
        }

        //-----------------------------------------------------

        protected override async void OnAppearing()
        {
            if (this._new_message_page != null)
            {
                NewMessagePage new_message_page = this._new_message_page;
                this._new_message_page = null;
                new_message_page.Set_data(this._invitation_id,this._company_user_id, this._message_destiny, this);
                await this.Navigation.PushAsync(new_message_page);
            }
        }

        //-----------------------------------------------------

        private void i_Set_waiting_layout()
        {
            ScrollView scroll = (ScrollView)this.Content;
            scroll.Content = this._waiting_layout;
        }

        //-----------------------------------------------------

        private void i_Set_layout()
        {
            ScrollView scroll = (ScrollView)this.Content;
            scroll.Content = this._layout;
        }

        //-----------------------------------------------------

        public async void Update_messages()
        {
            ServerResponse<OfferDetailDataJSON> response;
            this.i_Set_waiting_layout();
            response = await this._delegate.On_offer_detail(this._offer_id,this._invitation_id);
            if (response.type == ResponseType.OK_SUCCESS)
            {
                this._offer_page.Update_messages(response.result_object.messages);
                this.i_Set_data(response.result_object.messages.data[0].messages);
            }
            else
            {
                Alert.Show(response, this);
            }

            this.i_Set_layout();
        }

        //-----------------------------------------------------

        private async void i_Set_data(List<OfferMessageDataJSON> messages)
        {
            string from = "";
            ServerResponse<SaveDataJSON> response = await this._delegate.On_offer_mark_messages_as_readed(this._invitation_id);
            if (response.type == ResponseType.OK_SUCCESS)
            {
                //marcados ok
            }
            int mensajes = 0;
            if (messages != null) {
                mensajes= messages.Count;
                for (int i = 0; i < mensajes; ++i)
                {
                    MessageLayout message_layout = null;
                    OfferMessageDataJSON message;

                    if (this._messages_layout.Children.Count > i)
                    {
                        message_layout = (MessageLayout)this._messages_layout.Children[i];
                    }
                    else
                    {
                        message_layout = new MessageLayout();
                        this._messages_layout.Children.Add(message_layout);
                    }

                    message = messages[i];
                    if (message.from == "c")
                    {
                        from = message.user.first_name + " " + message.user.last_name;
                    }
                    else {
                        from = _delegate._user_name + " " + _delegate._user_last_name;
                    }
                    message_layout.Set_data(from, Util.Date_dd_MM_yy(message.created_at), message.message);
                }
            }
            

            while (this._messages_layout.Children.Count > mensajes)
                this._messages_layout.Children.RemoveAt(mensajes);
        }

        //-----------------------------------------------------

        public void Set_data(int offer_id,int invitation_id,int company_user_id, string message_destiny, List<OfferMessageDataJSON> messages, NewMessagePage new_message_page)
        {
            this._delegate.GA_track_screen(Local.txtGA13);
            this._offer_id = offer_id;
            this._invitation_id = invitation_id;
            this._company_user_id = company_user_id;
            this._message_destiny = message_destiny;
            this._new_message_page = new_message_page;
            this.i_Set_data(messages);
        }

        //-----------------------------------------------------

        private async void i_On_new_message(object sender, EventArgs e)
        {
            NewMessagePage new_message_page = new NewMessagePage(this._delegate);
            new_message_page.Set_data(this._invitation_id,this._company_user_id, this._message_destiny, this);
            await this.Navigation.PushAsync(new_message_page);
        }

        //-----------------------------------------------------


    }
}
