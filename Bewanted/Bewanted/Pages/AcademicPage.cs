﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Xamarin.Forms;
using C2Core;
using C2Forms;

namespace Bewanted
{
    public class SubjectRow : C2TableRow
    {
        private C2AssistedEntry _subject;
        private C2NumberEntry _qualification;

        //-----------------------------------------------------

        public SubjectRow(C2AssistedEntry subject, C2NumberEntry qualification)
        {
            this._subject = subject;
            this._qualification = qualification;
        }

        //-----------------------------------------------------

        public override View Cell(int index)
        {
            C2Assert.Error(index >= 0 && index <= 1);
            if (index == 0)
                return this._subject;
            else if (index == 1)
                return this._qualification;
            else
                return null;
        }

        //-----------------------------------------------------

        public override bool Validate()
        {
            bool ok = true;
            ok = ok & this._subject.Validate();
            ok = ok & this._qualification.Validate();
            return ok;
        }

        //-----------------------------------------------------

        public override Object Data()
        {
            AcademicSubjectJSON subject = new AcademicSubjectJSON();
            subject.id = this._subject.CurrentId;
            subject.name = this._subject.Text;
            subject.pivot = new AcademicSubjectPivotJSON();
            subject.pivot.qualification = this._qualification.Value;
            return subject;
        }
    }

    //-----------------------------------------------------

    public class AcademicPage : ContentPage
    {
        private AppDelegate _delegate;
        private int _academic_id;
        private StackLayout _layout;
        private StackLayout _waiting_layout;
        private CountryState _country_state;
        private C2AssistedEntry _university;
        private C2Picker _group_study;
        private C2Picker _level_study;
        private C2AssistedEntry _study;
        private IntervalLayout _interval_layout;
        private C2NumberEntry _average;
        private C2Table _subject_table;
        private C2Button _delete_button;

        //-----------------------------------------------------

        public AcademicPage(AppDelegate _delegate) : base()
        {
            //NavigationPage.SetTitleIcon(this, "no_image.png");
            Device.OnPlatform(Android: () => NavigationPage.SetTitleIcon(this, "no_image.png"));
            this.Title = Local.txtAcademic00;
            this.Style = AppStyle.Page;
            this._delegate = _delegate;
            this._academic_id = int.MinValue;
            this._layout = new StackLayout();
            this._layout.VerticalOptions = LayoutOptions.Start;
            this._layout.Spacing = AppStyle.Layout_stack_long_spacing;
            this._layout.Padding = new Thickness(0.0, AppStyle.Layout_padding_vertical);

            {
                StackLayout location_layout;
                location_layout = new StackLayout();
                location_layout.Spacing = AppStyle.Layout_stack_spacing;
                this._country_state = new CountryState(_delegate, this, Local.txtAcademic01, Local.txtAcademic02, false, this.i_On_country_changed, this.i_On_state_changed);
                location_layout.Children.Add(this._country_state._country);
                location_layout.Children.Add(this._country_state._state);
                this._layout.Children.Add(location_layout);
            }

            {
                StackLayout study_layout;
                study_layout = new StackLayout();
                study_layout.Spacing = AppStyle.Layout_stack_spacing;
                this._university = new C2AssistedEntry(Local.txtAcademic03, true, true, this.i_On_university_assist_required, this.i_On_university_changed);
                this._group_study = new C2Picker(Local.txtAcademic14, this.i_On_group_changed);
                this._level_study = new C2Picker(Local.txtAcademic21, this.i_On_level_changed);
                this._study = new C2AssistedEntry(Local.txtAcademic04, true, true, this.i_On_study_assist_required, this.i_On_study_changed);
                study_layout.Children.Add(this._university);
                study_layout.Children.Add(this._group_study);
                study_layout.Children.Add(this._level_study);
                study_layout.Children.Add(this._study);
                this._layout.Children.Add(study_layout);
            }

            {
                this._interval_layout = new IntervalLayout(Local.txtAcademic09);
                this._layout.Children.Add(this._interval_layout);
            }

            {
                /*StackLayout calification_layout;
                //Label label;
                calification_layout = new StackLayout();
                calification_layout.Orientation = StackOrientation.Horizontal;
                calification_layout.Spacing = AppStyle.Layout_stack_long_spacing;*/
                // calification_layout.Padding = new Thickness(0, 0, AppStyle.Layout_inner_padding_horizontal, 0);
                this._average = new C2NumberEntry(Local.txtAcademic10, 0.0f, 10.0f, 2, false,null);
                this._average.Placeholder = this._average.Placeholder + " " + Local.txtAcademic19;
                this._average.Add_constant(Local.txtAcademic13, 11.0f);
                this._average.HorizontalOptions = LayoutOptions.FillAndExpand;
                /*label = new Label { Text = Local.txtAcademic19, Style = AppStyle.Label };
                label.FontSize = Device.GetNamedSize(NamedSize.Small, typeof(Label));
                label.VerticalOptions = LayoutOptions.Center;*/
                //calification_layout.Children.Add(this._average);
                //calification_layout.Children.Add(label);
                //this._layout.Children.Add(calification_layout);
                this._layout.Children.Add(this._average);
            }

            {
                List<C2TableColumn> columns = new List<C2TableColumn>();
                columns.Add(new C2TableColumn(6.5));
                columns.Add(new C2TableColumn(1.5));
                this._subject_table = new C2Table(columns, "add2.png", "delete.png", this.i_New_subject);
                this._layout.Children.Add(this._subject_table);
            }

            {
                ButtonsLayout buttons_layout;
                buttons_layout = new ButtonsLayout(this, out this._delete_button, this.i_On_save, this.i_On_delete);
                this._layout.Children.Add(buttons_layout);
            }

            this._waiting_layout = UserPage.Waiting_layout();

            Content = new ScrollView
            {
                Content = this._waiting_layout
            };
        }

        //-----------------------------------------------------

        private SubjectRow i_Subject_row(AcademicSubjectJSON subjectJSON)
        {
            C2AssistedEntry subject;
            C2NumberEntry qualification;
            subject = new C2AssistedEntry(Local.txtAcademic15, true, true, this.i_On_subject_assist_required, null);
            subject.Placeholder = Local.txtAcademic15;
            qualification = new C2NumberEntry(Local.txtAcademic16, 0.0f, 10.0f, 2, true,Keyboard.Numeric);
            qualification.Placeholder = Local.txtAcademic16;

            if (subjectJSON != null)
            {
                subject.Set_text_and_id(subjectJSON.name, subjectJSON.id);
                qualification.Value = subjectJSON.pivot.qualification;
            }

            return new SubjectRow(subject, qualification);
        }

        //-----------------------------------------------------

        private C2TableRow i_New_subject()
        {
            return i_Subject_row(null);
        }

        //-----------------------------------------------------

        private void i_Set_waiting_layout()
        {
            ScrollView scroll = (ScrollView)this.Content;
            scroll.Content = this._waiting_layout;
        }

        //-----------------------------------------------------

        private void i_Set_layout()
        {
            ScrollView scroll = (ScrollView)this.Content;
            scroll.Content = this._layout;
        }

        //-----------------------------------------------------

        private async void i_Load_groups()
        {
            this._group_study.Set_waiting_state();
            if (this._group_study.Count == 0)
            {
                ServerResponse<List<GroupStudyJSON>> response = await this._delegate.On_groups_study();
                if (response.type == ResponseType.OK_SUCCESS)
                {
                    for (int i = 0; i < response.result_object.Count; ++i)
                        this._group_study.Add(response.result_object[i].name, response.result_object[i].id);
                }
                else
                {
                    this._group_study.Clear(true);
                    Alert.Show(response, this);
                }
            }
            this._group_study.Unset_waiting_state();
        }

        private async void i_Load_levels()
        {
            this._level_study.Set_waiting_state();
            if (this._level_study.Count == 0)
            {
                ServerResponse<List<LevelJSON>> response = await this._delegate.On_levels_study();
                if (response.type == ResponseType.OK_SUCCESS)
                {
                    for (int i = 0; i < response.result_object.Count; ++i)
                        this._level_study.Add(response.result_object[i].name, response.result_object[i].id);
                }
                else
                {
                    this._level_study.Clear(true);
                    Alert.Show(response, this);
                }
            }
            this._level_study.Unset_waiting_state();
        }
        //-----------------------------------------------------

        private void i_OnAppearing()
        {
            this.i_Set_waiting_layout();
            this._country_state.Load_countries();
            this.i_Load_groups();
            this.i_Load_levels();
        }

        //-----------------------------------------------------
        //public async void Set_data(AcademicDataJSON academic)
        public void Set_data(AcademicDataJSON academic)
        {
            this._delegate.GA_track_screen(Local.txtGA01);
            this.i_OnAppearing();

            if (academic != null)
            {
                this._academic_id = academic.id;
                this._country_state._country.CurrentId = academic.university.country_id;
                this._country_state._state.Set_text_and_id(academic.university.state.label, academic.university.state.id);
                this._university.Set_text_and_id(academic.university.name, academic.university.id);
                this._group_study.CurrentId = academic.group_id;
                if (academic.study.study_level_id != "") {
                    this._level_study.CurrentId = Convert.ToInt32(academic.study.study_level_id);
                }
                
                this._study.Set_text_and_id(academic.study.name, academic.study.id);
                this._interval_layout.Set_data(academic.month_start, academic.year_start, academic.month_end, academic.year_end);
                this._average.Value = academic.calification;

                this._subject_table.Clear();
                for (int i = 0; i < academic.subjects.Count; ++i)
                {
                    AcademicSubjectJSON subjectJSON = academic.subjects[i];
                    SubjectRow subject = this.i_Subject_row(subjectJSON);
                    this._subject_table.Add_row(subject);
                }

                this._delete_button.IsVisible = true;
                this.i_Set_layout();
                AppDelegate._is_academic_data = true;
                //ServerResponse<AcademicDataJSON> response;
                //response = await this._delegate.On_academic_data(academic.id);
                //if (response.type == ResponseType.OK_SUCCESS)
                //{
                //    this._academic_id = academic.id;
                //    this._country_state._country.CurrentId = response.result_object.country_id;
                //    this._country_state._state.Set_text_and_id(response.result_object.state.name, response.result_object.state.id);
                //    this._university.Set_text_and_id(response.result_object.university.name, response.result_object.university.id);
                //    this._group_study.CurrentId = response.result_object.group_id;
                //    this._study.Set_text_and_id(response.result_object.study.name, response.result_object.study.id);
                //    this._interval_layout.Set_data(response.result_object.month_start, response.result_object.year_start, response.result_object.month_end, response.result_object.year_end);
                //    this._average.Value = response.result_object.calification;

                //    this._subject_table.Clear();
                //    for (int i = 0; i < response.result_object.subjects.Count; ++i)
                //    {
                //        AcademicSubjectJSON subjectJSON = response.result_object.subjects[i];
                //        SubjectRow subject = this.i_Subject_row(subjectJSON);
                //        this._subject_table.Add_row(subject);
                //    }

                //    this._delete_button.IsVisible = true;
                //    this.i_Set_layout();
                //    AppDelegate._is_academic_data = true;
                //}
                //else
                //{
                //    Alert.Show(response, this);
                //    await this.Navigation.PopAsync();
                //}
            }
            else
            {
                this._academic_id = int.MinValue;
                this._country_state.Clear();
                this._university.Clear();
                this._group_study.Clear(false);
                this._level_study.Clear(false);
                this._study.Clear();
                this._interval_layout.Clear();
                this._average.Clear();
                this._subject_table.Clear();
                this._delete_button.IsVisible = false;
                this.i_Set_layout();
                AppDelegate._is_academic_data = false;
            }
        }

        //-----------------------------------------------------
        
        private void i_On_country_changed(C2Picker picker)
        {
            this.i_On_state_changed(0);
        }

        //-----------------------------------------------------

        private void i_On_state_changed(Int32 value)
        {
            this._university.Clear();
            this.i_On_university_changed(0);
        }

        //-----------------------------------------------------

        private void i_On_university_changed(Int32 value)
        {
            this._group_study.Clear(false);
            this.i_On_group_changed(null);
            
        }

        //-----------------------------------------------------

        private void i_On_group_changed(C2Picker picker)
        {
            this._level_study.Clear(false);
            this.i_On_level_changed(null);
        }

        private void i_On_level_changed(C2Picker picker)
        {
            //this._study.Clear();
            //this.i_On_study_changed(0);
        }

        //-----------------------------------------------------

        private void i_On_study_changed(Int32 value)
        {
            this._interval_layout.Clear();
            this._average.Clear();
            this._subject_table.Clear();
        }

        //-----------------------------------------------------

        private async Task<bool> i_On_university_assist_required(C2AssistedEntry sender, string search_text)
        {
            ServerResponse<List<UniversityJSON>> response;
            C2Assert.Error(sender == this._university);
            response = await this._delegate.On_universities(this._country_state._country.CurrentId, this._country_state._state.CurrentId, search_text);

            if (response.type == ResponseType.OK_SUCCESS)
            {
                for (int i = 0; i < response.result_object.Count; ++i)
                    sender.Add_assisted_item(response.result_object[i].name, response.result_object[i].id);
                return true;
            }
            else
            {
                Alert.Show(response, this);
                return false;
            }
        }

        //-----------------------------------------------------

        private async Task<bool> i_On_study_assist_required(C2AssistedEntry sender, string search_text)
        {
            ServerResponse<List<StudyJSON>> response;
            C2Assert.Error(sender == this._study);

            if (this._university.CurrentId != int.MinValue) {
                response = await this._delegate.On_studies(this._university.CurrentId, this._group_study.CurrentId, search_text);

                if (response.type == ResponseType.OK_SUCCESS)
                {
                    for (int i = 0; i < response.result_object.Count; ++i)
                        sender.Add_assisted_item(response.result_object[i].name, response.result_object[i].id);
                    return true;
                }
                else
                {
                    Alert.Show(response, this);
                    return false;
                }
            }
            else{
                return true;
            }
            
        }

        //-----------------------------------------------------

        private async Task<bool> i_On_subject_assist_required(C2AssistedEntry sender, string search_text)
        {
            ServerResponse<List<SubjectJSON>> response;
            if (this._study.CurrentId != int.MinValue)
            {
                response = await this._delegate.On_subjects(this._study.CurrentId, search_text);

                if (response.type == ResponseType.OK_SUCCESS)
                {
                    for (int i = 0; i < response.result_object.Count; ++i)
                        sender.Add_assisted_item(response.result_object[i].name, response.result_object[i].id);
                    return true;
                }
                else
                {
                    Alert.Show(response, this);
                    return false;
                }
            }
            else {
                return true;
            }
            
        }

        //-----------------------------------------------------

        private async void i_On_save(object sender, EventArgs e)
        {
            bool ok = true;

            ((Button)sender).IsEnabled = false;
            ok = ok & this._country_state.Validate();
            ok = ok & this._university.Validate();
            ok = ok & this._group_study.Validate();
            ok = ok & this._level_study.Validate();
            ok = ok & this._study.Validate();
            ok = ok & this._average.Validate();
            ok = ok & this._interval_layout.Validate();
            ok = ok & this._subject_table.Validate();

            if (ok == true)
            {
                AcademicDataJSON academic = new AcademicDataJSON();
                
                academic.university = new UniversityJSON();
                academic.study = new AcademicStudyJSON();
                academic.id = this._academic_id;
                academic.university.country_id = this._country_state._country.CurrentId;
                academic.university.state_id = this._country_state._state.CurrentId;
                academic.university.id = this._university.CurrentId;
                academic.university.name = this._university.Text;
                academic.group_id = this._group_study.CurrentId;
                academic.study.study_level_id = this._level_study.CurrentId.ToString();
                academic.study.id = this._study.CurrentId;
                academic.study.name = this._study.Text;
                academic.calification = this._average.Value;
                this._interval_layout.Get_data(out academic.month_start, out academic.year_start, out academic.month_end, out academic.year_end);
                academic.subjects = this._subject_table.CurrentData<AcademicSubjectJSON>();


                ServerResponse<SaveDataJSON> response;
                response = await this._delegate.On_academic_data_save(academic);
                if (response.type == ResponseType.OK_SUCCESS)
                {
                    if (response.result_object.result == true || response.result_object.id !=int.MinValue)
                    {
                        await this.Navigation.PopAsync();
                    }
                    else
                    {
                        await this.DisplayAlert("Developer Error", null, "Ok");
                    }
                }
                else
                {
                    Alert.Show(response, this);
                }
            }

            ((Button)sender).IsEnabled = true;
        }

        //-----------------------------------------------------

        private async void i_On_delete(object sender, EventArgs e)
        {
            if (await this.DisplayAlert(null, Local.txtAcademic20, Local.txtButtons05, Local.txtButtons06) == true)
            {
                ServerResponse<SaveDataJSON> response;
                response = await this._delegate.On_academic_data_delete(this._academic_id);
                if (response.type == ResponseType.OK_SUCCESS)
                {
                    if (response.result_object.result == true || response.result_object.details== "Deleted resource")
                    {
                        await this.Navigation.PopAsync();
                    }
                    else
                    {
                        await this.DisplayAlert("Developer Error", null, "Ok");
                    }
                }
                else
                {
                    Alert.Show(response, this);
                }
            }
        }

        //-----------------------------------------------------


    }
}