﻿using System;
using Xamarin.Forms;
using C2Forms;

namespace Bewanted
{
    class OtherPage : ContentPage
    {
        private AppDelegate _delegate;
        private StackLayout _layout;
        private StackLayout _waiting_layout;
        private C2Editor _editor;

        //-----------------------------------------------------

        public OtherPage(AppDelegate _delegate) : base()
        {
            Device.OnPlatform(Android: () => NavigationPage.SetTitleIcon(this, "no_image.png"));
            this.Title = Local.txtOther00;
            this.Style = AppStyle.Page;
            this._delegate = _delegate;
            this._layout = new StackLayout();
            this._layout.VerticalOptions = LayoutOptions.Start;
            this._layout.Spacing = AppStyle.Layout_stack_long_spacing;
            this._layout.Padding = new Thickness(0.0, AppStyle.Layout_padding_vertical);

            {
                this._editor = new C2Editor(Local.txtOther01, 0, 1000, 20,null);
                this._layout.Children.Add(this._editor);
            }

            {
                ButtonsLayout buttons_layout;
                C2Button clean_text_button;
                buttons_layout = new ButtonsLayout(this, this.i_On_save);
                clean_text_button = new C2Button { Text = Local.txtOther02 };
                clean_text_button.Style = AppStyle.Button4_style;
                clean_text_button.Clicked += i_On_clean_text;
                buttons_layout.Children.Add(clean_text_button);
                this._layout.Children.Add(buttons_layout);
            }

            this._waiting_layout = UserPage.Waiting_layout();

            Content = new ScrollView
            {
                Content = this._waiting_layout
            };
        }

        //-----------------------------------------------------

        private void i_Set_waiting_layout()
        {
            ScrollView scroll = (ScrollView)this.Content;
            scroll.Content = this._waiting_layout;
        }

        //-----------------------------------------------------

        private void i_Set_layout()
        {
            ScrollView scroll = (ScrollView)this.Content;
            scroll.Content = this._layout;
        }

        //-----------------------------------------------------

        private void i_OnAppearing()
        {
            this.i_Set_waiting_layout();
        }

        //-----------------------------------------------------

        public void Set_data(string other)
        {
            this.i_OnAppearing();
            this._delegate.GA_track_screen(Local.txtGA16);
            this._editor.Text = other;
            this.i_Set_layout();
            //ServerResponse<StudentDataJSON> response;
            //response = await this._delegate.On_personal_data();
            //if (response.type == ResponseType.OK_SUCCESS)
            //{
            //    this._editor.Text = response.result_object.data;
            //    this.i_Set_layout();
            //}
            //else
            //{
            //    Alert.Show(response, this);
            //    await this.Navigation.PopAsync();
            //}
        }

        //-----------------------------------------------------

        private async void i_On_save(object sender, EventArgs e)
        {
            StudentDataJSON other = new StudentDataJSON();
            ((Button)sender).IsEnabled = false;
            other.data = this._editor.Text;
        
            ServerResponse<SaveDataJSON> response;
            response = await this._delegate.On_other_data_save(other);
            if (response.type == ResponseType.OK_SUCCESS)
            {
                if (response.result_object.result == true || response.result_object.id!=int.MinValue)
                {
                    await this.Navigation.PopAsync();
                }
                else
                {
                    await this.DisplayAlert("Developer Error", null, "Ok");
                }
            }
            else
            {
                Alert.Show(response, this);
            }

            ((Button)sender).IsEnabled = true;
        }

        //-----------------------------------------------------

        private void i_On_clean_text(object sender, EventArgs e)
        {
            this._editor.Text = "";
        }

        //-----------------------------------------------------


    }
}
