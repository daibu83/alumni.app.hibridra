﻿using System;
using Xamarin.Forms;
using C2Forms;

namespace Bewanted
{
    class HowtoPage : ContentPage
    {
        private AppDelegate _delegate;
        private string _email;
        private string _password;

        //-----------------------------------------------------

        private StackLayout i_Image_layout(string image_src, string title, string text)
        {
            StackLayout layout;
            StackLayout text_layout;
            Image image;
            C2Label title_label;
            C2Label text_label;
            layout = new StackLayout();
            layout.Orientation = StackOrientation.Horizontal;
            layout.Spacing = 20.0;
            layout.Padding = new Thickness(0.0, 10.0, 0.0, 10.0);
            text_layout = new StackLayout();
            text_layout.Spacing = 0.0;
            image = new Image { Source = image_src, Aspect = Aspect.AspectFit };
            image.WidthRequest = 96.0;
            image.HeightRequest = 96.0;
            image.VerticalOptions = LayoutOptions.Center;
            title_label = new C2Label(1) { Text = title, Style = AppStyle.Content_label_strong, FontAttributes = FontAttributes.Bold };
            title_label.FontSize = C2Style.Label_font_size;
            title_label.HorizontalOptions = LayoutOptions.FillAndExpand;
            title_label.VerticalOptions = LayoutOptions.FillAndExpand;
            text_label = new C2Label(5) { Text = text, Style = AppStyle.Content_label };
            text_label.FontSize = C2Style.Label_font_size;
            text_label.HorizontalOptions = LayoutOptions.FillAndExpand;
            text_label.VerticalOptions = LayoutOptions.FillAndExpand;
            text_layout.Children.Add(title_label);
            text_layout.Children.Add(text_label);
            layout.Children.Add(image);
            layout.Children.Add(text_layout);
            return layout;
        }

        //-----------------------------------------------------

        public HowtoPage(AppDelegate _delegate, string email, string password) : base()
        {
            StackLayout layout;
            Device.OnPlatform(Android: () => NavigationPage.SetTitleIcon(this, "no_image.png"));
            if (email != null)
                this.Title = Local.txtHowto12;
            else
                this.Title = Local.txtHowto01;

            this.Style = AppStyle.Page;
            this.BackgroundColor = C2Style.Control_background;
            this._delegate = _delegate;
            this._delegate.GA_track_screen(Local.txtGA06);
            this._email = email;
            this._password = password;
            layout = new StackLayout();
            layout.VerticalOptions = LayoutOptions.Start;
            layout.Spacing = Device.OnPlatform(2 * AppStyle.Layout_stack_long_spacing, 0, 0);
            layout.Padding = new Thickness(20.0, 10.0, 20.0, 10.0);
            
            {
                StackLayout image_layout;
                image_layout = this.i_Image_layout("pencil.png", Local.txtHowto04, Local.txtHowto05);
                layout.Children.Add(image_layout);
            }

            {
                StackLayout image_layout;
                image_layout = this.i_Image_layout("search.png", Local.txtHowto06, Local.txtHowto07);
                layout.Children.Add(image_layout);
            }

            {
                StackLayout image_layout;
                image_layout = this.i_Image_layout("msg.png", Local.txtHowto08, Local.txtHowto09);
                layout.Children.Add(image_layout);
            }

            {
                StackLayout image_layout;
                image_layout = this.i_Image_layout("horm.png", Local.txtHowto10, Local.txtHowto11);
                layout.Children.Add(image_layout);
            }

            if (email != null)
            {
                StackLayout buttons_layout;
                C2Button ok_button;
                buttons_layout = new StackLayout();
                buttons_layout.Padding = new Thickness(0.0, AppStyle.Button_padding_vertical);
                buttons_layout.Spacing = AppStyle.Layout_stack_long_spacing;
                ok_button = new C2Button { Text = Local.txtHowto13 };
                ok_button.Style = AppStyle.Button1_style;
                ok_button.Clicked += i_On_ok;
                buttons_layout.Children.Add(ok_button);
                layout.Children.Add(buttons_layout);
            }

            layout.ForceLayout();

            Content = new ScrollView
            {
                Content = layout
            };
        }

        //-----------------------------------------------------

        private void i_On_ok(object sender, EventArgs e)
        {
            
            this._delegate.User_current_page = UserCurrentPage.kProfile;
            this._delegate.On_login2(this._email, this._password);
        }

        //-----------------------------------------------------


    }
}
