﻿using Xamarin.Forms;

namespace Bewanted
{
    class LegalPage : ContentPage
    {
        //-----------------------------------------------------

        public LegalPage(AppDelegate _delegate, string legal_url) : base()
        {
            WebView view;
            Device.OnPlatform(Android: () => NavigationPage.SetTitleIcon(this, "no_image.png"));
            _delegate.GA_track_screen(Local.txtGA10);
            this.Title = Local.txtLegal00;
            this.Style = AppStyle.Page;
            view = new WebView { Source = legal_url };
            Content = view;
        }

        //-----------------------------------------------------


    }
}
