﻿using System;
using Xamarin.Forms;
using C2Forms;

namespace Bewanted
{
    public class UserPage : MasterDetailPage
    {
        private AppDelegate _delegate;
        private static Image image;
        //-----------------------------------------------------

        public UserPage(AppDelegate _delegate, MenuPage master, NavigationPage detail) : base()
        {
            this._delegate = _delegate;
            //this.MasterBehavior = MasterBehavior.SplitOnPortrait;
            this.MasterBehavior = MasterBehavior.Default;
            this.Master = master;
            this.Detail = detail;
            this.IsPresentedChanged += this.i_On_presented_changed;
        }

        //-----------------------------------------------------

        public void Change_detail_page(NavigationPage detail)
        {
            this.Detail = detail;
        }

        //-----------------------------------------------------

        public void On_detail_apearing()
        {
            this.Detail.ForceLayout();
        }

        //-----------------------------------------------------


        public static ContentView Right_arrow_view(Layout content_layout, EventHandler func_hander,Boolean blnVerimg,String mensajes)
        {
            ContentView view;
            StackLayout layout;
            ContentView empty_view;
            C2Badge _badge1;
            //Image image;
            image = new Image();
            view = new ContentView();
            view.Style = C2Style.Control_style;

            layout = new StackLayout();
            layout.Orientation = StackOrientation.Horizontal;
            layout.Padding = new Thickness(10);
            layout.VerticalOptions = LayoutOptions.Center;
            layout.HorizontalOptions = LayoutOptions.FillAndExpand;
            
            Grid msg;
            msg = new Grid
            {
                //VerticalOptions = LayoutOptions.Center,
                //HorizontalOptions = LayoutOptions.FillAndExpand,
                RowDefinitions =
                {
                new RowDefinition { Height =new GridLength(1, GridUnitType.Star)},

                },
                ColumnDefinitions =
                {
                    new ColumnDefinition { Width = new GridLength(8, GridUnitType.Star) },
                    new ColumnDefinition { Width = new GridLength(2, GridUnitType.Star) }
                }
            };





            //this._message_view.Content = layout;


            layout.Children.Add(content_layout);
            ////msg.Children.Add(layout, 0, 0);
            //empty_view = new ContentView();
            //empty_view.HorizontalOptions = LayoutOptions.FillAndExpand;
            ////msg.Children.Add(empty_view, 1, 0);
            //layout.Children.Add(empty_view);

            msg.Children.Add(layout, 0, 0);
            layout = new StackLayout();
            //layout.Spacing = 5;
            layout.Orientation = StackOrientation.Vertical;
            layout.Padding = new Thickness(0, 0, 15, 0);
            //layout.BackgroundColor = Color.Blue;
            layout.VerticalOptions = LayoutOptions.FillAndExpand;
            layout.HorizontalOptions = LayoutOptions.EndAndExpand;

            if (mensajes != null)
            {
                if (mensajes != "")
                {
                    double size;
                    double font_size;
                    font_size = Device.GetNamedSize(NamedSize.Small, typeof(Label));
                    size = 1.6 * font_size;
                    _badge1 = new C2Badge(size, size, font_size);
                    _badge1.BoxColor = AppStyle.Mensajes_panel_label1;
                    _badge1.HorizontalOptions = LayoutOptions.End;
                    _badge1.VerticalOptions = LayoutOptions.CenterAndExpand;
                    _badge1.Text = mensajes;
                    
                    layout.Children.Add(_badge1);

                }
                else {
                    image = new Image { Source = "no_image.png" };
                    image.VerticalOptions = LayoutOptions.CenterAndExpand;
                    image.HorizontalOptions = LayoutOptions.End;
                    image.HeightRequest = 31;

                    layout.Children.Add(image);
                }


            }
            else {
                //msg.Children.Add(layout, 0, 0);
                if (blnVerimg) { image = new Image { Source = "arrow_right.png" }; } else { image = new Image { Source = "no_image.png" }; }

                image.VerticalOptions = LayoutOptions.CenterAndExpand;
                image.HorizontalOptions = LayoutOptions.End;
                image.HeightRequest = 31;

                layout.Children.Add(image);
            }
            
            
            msg.Children.Add(layout, 1,0);
            //view.Content = layout;
            view.Content = msg;
            if (func_hander != null)
            {
                TapGestureRecognizer gesture;
                gesture = new TapGestureRecognizer();
                gesture.Tapped += func_hander;
                view.GestureRecognizers.Add(gesture);
            }
            
            
            
            return view;
        }

        //-----------------------------------------------------

        public static Layout Right_arrow_layout(ContentView view, Boolean blnVerimg, String mensajes)
        {

            Grid gridlayout = (Grid)view.Content;
                
            if (blnVerimg)
            {
                StackLayout layoutimg = (StackLayout)gridlayout.Children[1];
                Image imagen = (Image)layoutimg.Children[0];
                imagen.Source = "arrow_right.png";
                
            }
            else
            {
                StackLayout layoutimg = (StackLayout)gridlayout.Children[1];
                Image imagen = (Image)layoutimg.Children[0];
                imagen.Source = "no_image.png";
            }
            StackLayout layout = (StackLayout)gridlayout.Children[0];
            return (Layout)layout.Children[0];
        }

        //-----------------------------------------------------

        public static StackLayout Waiting_layout()
        {
            
            StackLayout layout;
            ActivityIndicator indicator;
            
            layout = new StackLayout();
            layout.Padding = new Thickness(0.0, 10.0, 0.0, 10.0);
            indicator = new ActivityIndicator();
            indicator.IsRunning = true;
            layout.Children.Add(indicator);
            return layout;
        }

        //-----------------------------------------------------

        private void i_On_presented_changed(object sender, EventArgs e)
        {
            if (this.IsPresented == true)
                this._delegate.GA_track_screen(Local.txtGA11);
        }
    }
}