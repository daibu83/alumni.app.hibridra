﻿using Xamarin.Forms;

namespace Bewanted
{
    public class PreviousPage : ContentPage
    {
        public PreviousPage(App.Platform platform)
        {
            NavigationPage.SetHasNavigationBar(this, false);
            this.BackgroundColor = AppStyle.Splash_background;

            if (platform == App.Platform.iOS)
            {
                Image image;
                image = new Image { Source = "splash_image.png" };
                image.Aspect = Aspect.AspectFill;
                image.VerticalOptions = LayoutOptions.Center;
                image.HorizontalOptions = LayoutOptions.Center;
                this.Content = image;
            }
            else
            {
                ActivityIndicator indicator;
                indicator = new ActivityIndicator();
                indicator.IsRunning = true;
                indicator.Color = Color.White;
                indicator.WidthRequest = 64;
                indicator.HeightRequest = 64;
                indicator.VerticalOptions = LayoutOptions.Center;
                indicator.HorizontalOptions = LayoutOptions.Center;
                this.Content = indicator;
            }
        }
    }
}
