﻿using System;
using System.Threading.Tasks;
using Xamarin.Forms;
using C2Forms;
using System.Collections.Generic;

namespace Bewanted
{
    class LanguagePage : ContentPage
    {
        private AppDelegate _delegate;
        private string _language_id;
        private StackLayout _layout;
        private StackLayout _waiting_layout;
        private C2Picker _language;
        private C2Picker _level;
        //private C2AssistedEntry _certificate;
        private C2TextEntry _certificate;
        private C2Button _delete_button;

        //-----------------------------------------------------

        public LanguagePage(AppDelegate _delegate) : base()
        {
            Device.OnPlatform(Android: () => NavigationPage.SetTitleIcon(this, "no_image.png"));
            this.Title = Local.txtLanguage00;
            this.Style = AppStyle.Page;
            this._delegate = _delegate;
            this._language_id = "";
            this._layout = new StackLayout();
            this._layout.VerticalOptions = LayoutOptions.Start;
            this._layout.Padding = new Thickness(0.0, AppStyle.Layout_padding_vertical);
            this._layout.Spacing = AppStyle.Layout_stack_long_spacing;
            this._language = new C2Picker(Local.txtLanguage01, this.i_On_language_changed);
            this._level = new C2Picker(Local.txtLanguage02, null);
            //this._certificate = new C2AssistedEntry(Local.txtLanguage03, false, true, this.i_On_certificate_assist_requiered, null);
            this._certificate = new C2TextEntry(Local.txtLanguage03, 0, 255,null);
            this._layout.Children.Add(this._language);
            this._layout.Children.Add(this._level);
            this._layout.Children.Add(this._certificate);

            {
                ButtonsLayout buttons_layout;
                buttons_layout = new ButtonsLayout(this, out this._delete_button, this.i_On_save, this.i_On_delete);
                this._layout.Children.Add(buttons_layout);
            }

            this._waiting_layout = UserPage.Waiting_layout();

            Content = new ScrollView
            {
                Content = this._waiting_layout
            };
        }

        //-----------------------------------------------------

        private async Task<List<LanguageItemJSON>> i_Load_language_items()
        {
            ServerResponse<List<LanguageItemJSON>> response = await this._delegate.On_language_items();
            if (response.type == ResponseType.OK_SUCCESS)
            {
                return response.result_object;
            }
            else
            {
                Alert.Show(response, this);
                return null;
            }
        }

        //-----------------------------------------------------

        private async Task<List<LevelJSON>> i_Load_levels()
        {
            ServerResponse<List<LevelJSON>> response = await this._delegate.On_levels();
            if (response.type == ResponseType.OK_SUCCESS)
            {
                return response.result_object;
            }
            else
            {
                Alert.Show(response, this);
                return null;
            }
        }

        //-----------------------------------------------------

        private void i_Set_waiting_layout()
        {
            ScrollView scroll = (ScrollView)this.Content;
            scroll.Content = this._waiting_layout;
        }

        //-----------------------------------------------------

        private void i_Set_layout()
        {
            ScrollView scroll = (ScrollView)this.Content;
            scroll.Content = this._layout;
        }

        //-----------------------------------------------------

        private async void i_OnAppearing()
        {
            this.i_Set_waiting_layout();

            if (this._language.Count == 0)
            {
                List<LanguageItemJSON> language;
                this._language.Set_waiting_state();
                language = await this.i_Load_language_items();
                if (language != null) {
                    for (int i = 0; i < language.Count; ++i)
                        this._language.Add(language[i].name, language[i].id);
                }
                this._language.Unset_waiting_state();
            }

            if (this._level.Count == 0)
            {
                List<LevelJSON> levels;
                this._level.Set_waiting_state();
                levels = await this.i_Load_levels();
                if (levels != null)
                {
                    for (int i = 0; i < levels.Count; ++i)
                        this._level.Add(levels[i].name, levels[i].id);
                }
                this._level.Unset_waiting_state();
            }

            Device.OnPlatform(iOS: () => this.i_Set_layout());
        }

        //-----------------------------------------------------

        public async void Set_data(LanguageDataJSON language)
        {
            this._delegate.GA_track_screen(Local.txtGA09);
            this.i_OnAppearing();

            if (language != null)
            {
                ServerResponse<LanguageJSON> response;
                response = await this._delegate.On_language_data(language.language_id);
                if (response.type == ResponseType.OK_SUCCESS)
                {
                    this._language_id = language.language_id;
                    this._language.CurrentId = response.result_object.data.language.id;
                    this._level.CurrentId = response.result_object.data.level.id;

                    if (response.result_object.data.certificate != null)
                    {
                        //this._certificate.Set_text_and_id(response.result_object.data.certificate.name, response.result_object.data.certificate.id);
                        this._certificate.Text = response.result_object.data.certificate.name;
                    }

                    this._delete_button.IsVisible = true;
                    this.i_Set_layout();
                }
                else
                {
                    Alert.Show(response, this);
                    await this.Navigation.PopAsync();
                }
            }
            else
            {
                this._language_id = "";
                this._language.Clear(false);
                this._level.Clear(false);
                this._certificate.Clear();
                this._delete_button.IsVisible = false;
                this.i_Set_layout();
            }
        }

        //-----------------------------------------------------

        private void i_On_language_changed(C2Picker picker)
        {
            this._level.Clear(false);
            this._certificate.Clear();
        }

        //-----------------------------------------------------

        private async Task<bool> i_On_certificate_assist_requiered(C2AssistedEntry sender, string search_text)
        {
            ServerResponse<List<CertificateJSON>> response;
            response = await this._delegate.On_certificates(this._language.CurrentId, search_text);

            if (response.type == ResponseType.OK_SUCCESS)
            {
                for (int i = 0; i < response.result_object.Count; ++i)
                    sender.Add_assisted_item(response.result_object[i].value, response.result_object[i].id);
                return true;
            }
            else
            {
                Alert.Show(response, this);
                return false;
            }
        }

        //-----------------------------------------------------

        private async void i_On_save(object sender, EventArgs e)
        {
            bool ok = true;
            ((Button)sender).IsEnabled = false;
            ok = ok & this._language.Validate();
            ok = ok & this._level.Validate();
            ok = ok & this._certificate.Validate();

            if (ok == true)
            {
                LanguageDataJSON language = new LanguageDataJSON();
                language.language = new LanguageDescriptionJSON();
                language.level = new LanguageLevelJSON();
                language.certificate = new LanguageCertificateJSON();
                language.language.id = this._language.CurrentId;
                language.level.id = this._level.CurrentId;
                language.certificate.id = int.MinValue;// this._certificate.CurrentId;
                language.certificate.name = this._certificate.Text;

                ServerResponse<SaveDataJSON> response;
                response = await this._delegate.On_language_data_save(language);
                if (response.type == ResponseType.OK_SUCCESS)
                {
                    if (response.result_object.result == true || response.result_object.language_id != int.MinValue)
                    {
                        await this.Navigation.PopAsync();
                    }
                    else
                    {
                        await this.DisplayAlert("Developer Error", null, "Ok");
                    }
                }
                else
                {
                    Alert.Show(response, this);
                }
            }

            ((Button)sender).IsEnabled = true;
        }

        //-----------------------------------------------------

        private async void i_On_delete(object sender, EventArgs e)
        {
            if (await this.DisplayAlert(null, Local.txtLanguage08, Local.txtButtons05, Local.txtButtons06) == true)
            {
                ServerResponse<SaveDataJSON> response;
                response = await this._delegate.On_language_data_delete(this._language_id);
                if (response.type == ResponseType.OK_SUCCESS)
                {
                    if (response.result_object.result == true || response.result_object.details == "Deleted resource")
                    {
                        await this.Navigation.PopAsync();
                    }
                    else
                    {
                        await this.DisplayAlert("Developer Error", null, "Ok");
                    }
                }
                else
                {
                    Alert.Show(response, this);
                }
            }
        }

        //-----------------------------------------------------


    }
}
