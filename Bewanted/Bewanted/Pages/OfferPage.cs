﻿using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;
using C2Forms;
using System.Globalization;

namespace Bewanted
{
    public class OfferPage : ContentPage
    {
        private AppDelegate _delegate;
        private int _offer_id;
        private int _invitation_id;
        private int _company_user_id;
        private string _message_destiny;
        private List<OfferMessageDataJSON> _messages;
        private List<OfferKillerDataJSON> _killer_questions;
        private ContentView _message_view;
        private C2Badge _message_badge;
        private Image _minus_image;
        private StackLayout _layout;
        private StackLayout _waiting_layout;
        private ImageLayout _offer_layout;
        private C2LabelHtml _offer_description_label;
        private C2LabelHtml _company_description_label;
        private C2LabelHtml _applicant_description_label;
        //private Label _user_label;
        private C2LabelHtml _position_label;
        private C2LabelHtml _more_label;
        private C2Button _accept_button;
        private C2Button _decline_button;
        private C2Button _delete_button;
        private C2Button _message_button;
        //private MessagesPage _messages_page;
        //private NewMessagePage _new_message_page;

        //-----------------------------------------------------

        private StackLayout i_Detail_layout(string title, out C2LabelHtml text_label)
        {
            StackLayout layout;
            Label title_label;
            layout = new StackLayout();
            layout.Spacing = AppStyle.Layout_stack_spacing;
            title_label = new Label { Text = title, Style = AppStyle.Content_label_strong };
            title_label.FontSize = C2Style.Entry_font_size;
            title_label.FontAttributes = FontAttributes.Bold;
            text_label = new C2LabelHtml { Text = title, Style = AppStyle.Content_label };
            layout.Children.Add(title_label);
            layout.Children.Add(text_label);
            return layout;
        }

        //-----------------------------------------------------

        private ContentView i_Message_bar_view()
        {
            //StackLayout layout;
            Label label;
            TapGestureRecognizer gesture;
            this._message_view = new ContentView();
            this._message_view.Style = AppStyle.Filter_background;
            //this._message_view.Padding = new Thickness(AppStyle.Filter_padding_horizontal, AppStyle.Filter_padding_vertical, 2.0 * AppStyle.Filter_padding_horizontal, AppStyle.Filter_padding_vertical);
            this._message_view.Padding = new Thickness(5, AppStyle.Filter_padding_vertical, 10, AppStyle.Filter_padding_vertical);
            //layout = new StackLayout();
            //layout.Orientation = StackOrientation.Horizontal;
            //layout.HorizontalOptions = LayoutOptions.FillAndExpand;
            //layout.Spacing = 5.0;
            label = new Label { Text = Local.txtOffer28 };
            label.Style = AppStyle.Filter_label;
            label.HorizontalOptions = LayoutOptions.Start;
            label.VerticalOptions = LayoutOptions.Center;
            double font_size = Device.GetNamedSize(NamedSize.Micro, typeof(Label));
            double size = 1.6 * font_size;
            this._message_badge = new C2Badge(size,0, font_size);
            this._message_badge.HorizontalOptions = LayoutOptions.Center;
            this._message_badge.VerticalOptions = LayoutOptions.Center;
            this._message_badge.Text = "5";
            this._message_badge.BorderColor = AppStyle.Filter_text;
            this._message_badge.BoxColor = AppStyle.Filter_text;
            this._message_badge.TextColor = AppStyle.Filter_background_color;

            this._minus_image = new Image { Source = "arrow_right.png" };
            this._minus_image.WidthRequest = 10;
            //this._minus_image.TranslationX = 20;
            this._minus_image.HorizontalOptions = LayoutOptions.End;
            
            gesture = new TapGestureRecognizer();
            gesture.Tapped += this.i_On_messages;
            
            this._message_view.GestureRecognizers.Add(gesture);
            //layout.Children.Add(this._message_badge);
            //layout.Children.Add(label);
            //layout.Children.Add(this._minus_image);

            Grid msg;
            msg = new Grid
            {
                //VerticalOptions = LayoutOptions.FillAndExpand,
                RowDefinitions =
                {
                    new RowDefinition { Height =new GridLength(1, GridUnitType.Star)},
                    //new RowDefinition { Height = GridLength.Auto },
                    
                },
                ColumnDefinitions =
                {
                    //new ColumnDefinition { Width = new GridLength(1, GridUnitType.Auto) },
                    new ColumnDefinition { Width = new GridLength(size, GridUnitType.Absolute) },
                    new ColumnDefinition { Width = new GridLength(9, GridUnitType.Star) },
                    new ColumnDefinition { Width = new GridLength(1, GridUnitType.Star) }
                }
                
        };
            msg.Children.Add(this._message_badge,0,0);
            msg.Children.Add(label, 1, 0);
            msg.Children.Add(this._minus_image, 2, 0);


            //this._message_view.Content = layout;
            this._message_view.Content = msg;
            return this._message_view;
        }

        //-----------------------------------------------------
        protected override void OnAppearing()
        {
            base.OnAppearing();
            this.Title = Local.txtOffer00;

        }
        public OfferPage(AppDelegate _delegate) : base()
        {
            Device.OnPlatform(Android: () => NavigationPage.SetTitleIcon(this, "no_image.png"));
            NavigationPage.SetBackButtonTitle(this, "");
            this.Title = Local.txtOffer00;
            this.Style = AppStyle.Page;
            this._delegate = _delegate;
            this._offer_id = int.MinValue;
            this._invitation_id = int.MinValue;
            this._company_user_id = int.MinValue;
            this._message_destiny = null;
            this._messages = null;
            this._killer_questions = null;
            this._layout = new StackLayout();
            this._layout.VerticalOptions = LayoutOptions.Start;
            this._layout.Spacing = AppStyle.Layout_stack_spacing;

            {
                ContentView view;
                view = this.i_Message_bar_view();
                this._layout.Children.Add(view);
            }

            {
                ContentView view;
                view = new ContentView { Style = C2Style.Control_style };
                view.Padding = new Thickness(AppStyle.Content_box_padding_horizontal, AppStyle.Content_box_padding_vertical);
                this._offer_layout = new ImageLayout(AppStyle.Image_size,false,false);
                view.Content = this._offer_layout;
                this._layout.Children.Add(view);
            }

            {
                ContentView view;
                StackLayout layout;
                view = new ContentView { Style = C2Style.Control_style };
                view.Padding = new Thickness(AppStyle.Content_box_padding_horizontal, AppStyle.Content_box_padding_vertical);
                layout = new StackLayout();
                layout.Spacing = 2.0 * AppStyle.Layout_stack_long_spacing;

                {
                    StackLayout offer_description_layout;
                    offer_description_layout = this.i_Detail_layout(Local.txtOffer01, out this._offer_description_label);
                    layout.Children.Add(offer_description_layout);
                }

                {
                    StackLayout company_description_layout;
                    company_description_layout = this.i_Detail_layout(Local.txtOffer18, out this._company_description_label);
                    layout.Children.Add(company_description_layout);
                }

                {
                    StackLayout applicant_description_layout;
                    applicant_description_layout = this.i_Detail_layout(Local.txtOffer19, out this._applicant_description_label);
                    layout.Children.Add(applicant_description_layout);
                }

                /*
                {
                    StackLayout user_layout;
                    user_layout = this.i_Detail_layout(Local.txtOffer02, out this._user_label);
                    layout.Children.Add(user_layout);
                }*/

                {
                    StackLayout position_layout;
                    position_layout = this.i_Detail_layout(Local.txtOffer03, out this._position_label);
                    layout.Children.Add(position_layout);
                }

                {
                    StackLayout more_layout;
                    more_layout = this.i_Detail_layout(Local.txtOffer11, out this._more_label);
                    layout.Children.Add(more_layout);
                }

                view.Content = layout;
                this._layout.Children.Add(view);
            }

            {
                StackLayout buttons_layout;
                buttons_layout = new StackLayout();
                buttons_layout.Padding = new Thickness(AppStyle.Button_padding_horizontal, AppStyle.Button_padding_vertical);
                buttons_layout.Spacing = AppStyle.Button_padding_vertical;
                this._accept_button = new C2Button { Text = Local.txtOffer12 };
                this._accept_button.Style = AppStyle.Button2_style;
                this._accept_button.Clicked += i_On_accept;
                this._decline_button = new C2Button { Text = Local.txtOffer13 };
                this._decline_button.Style = AppStyle.Button3_style;
                this._decline_button.Clicked += i_On_decline;
                this._delete_button = new C2Button { Text = Local.txtOffer14 };
                this._delete_button.Style = AppStyle.Button3_style;
                this._delete_button.Clicked += i_On_delete;
                this._message_button = new C2Button { Text = Local.txtOffer15 };
                this._message_button.Style = AppStyle.Button4_style;
                this._message_button.Clicked += i_On_new_message;
                buttons_layout.Children.Add(this._accept_button);
                buttons_layout.Children.Add(this._decline_button);
                buttons_layout.Children.Add(this._delete_button);
                buttons_layout.Children.Add(this._message_button);
                this._layout.Children.Add(buttons_layout);
            }

            this._waiting_layout = UserPage.Waiting_layout();
            //this._messages_page = null;
            //this._new_message_page = null;

            Content = new ScrollView
            {
                Content = this._waiting_layout
            };
        }

        //-----------------------------------------------------

        private void i_Set_waiting_layout()
        {
            ScrollView scroll = (ScrollView)this.Content;
            scroll.Content = this._waiting_layout;
        }

        //-----------------------------------------------------

        private void i_Set_layout()
        {
            ScrollView scroll = (ScrollView)this.Content;
            scroll.Content = this._layout;
        }

        //-----------------------------------------------------

        private string i_contract_kind(int contract_kind_id)
        {
            switch (contract_kind_id)
            {
                case 1:
                    return Local.txtOffer20;
                case 2:
                    return Local.txtOffer21;
                case 3:
                    return Local.txtOffer22;
                case 5:
                    return Local.txtOffer23;
            }

            return "";
        }

        //-----------------------------------------------------

        private string i_hour(int hour_id)
        {
            switch (hour_id)
            {
                case 1:
                    return Local.txtOffer09;
                case 2:
                    return Local.txtOffer07;
                case 3:
                    return Local.txtOffer08;
            }

            return "";
        }

        //-----------------------------------------------------
        
        public async void Set_data(OfferJSON offer)
        {
            ServerResponse<OfferDetailDataJSON> response;
            this._delegate.GA_track_screen(Local.txtGA15);
            this.i_Set_waiting_layout();
            response = await this._delegate.On_offer_detail(offer.job_offer_id, offer.invitation_id);
            if (response.type == ResponseType.OK_SUCCESS)
            {
                StringBuilder stream = new StringBuilder();
                string date = Util.Date_range(offer.date_start, offer.date_end);
                string user_name = response.result_object.user.first_name + " " + response.result_object.user.last_name;

                this._offer_id = offer.job_offer_id;
                this._invitation_id = offer.invitation_id;
                this._company_user_id = response.result_object.company_user_id;
                this._message_destiny = user_name;
                if (response.result_object.messages.data.Count > 0 && response.result_object.messages.data[0].messages!=null)
                {
                    this._messages = response.result_object.messages.data[0].messages;
                }
                else {
                    this._messages = null;
                }
                

                if (offer.job_offer_status_id == 1)
                    this._killer_questions = response.result_object.killer_questions;
                else
                    this._killer_questions = null;

                //this._offer_layout.Set_data(offer.company_image, offer.name, offer.company_name, date, Util.Location(offer.city, offer.country),null, offer.status_name);
                
                this._offer_layout.Set_data(offer.company_image, offer.name, offer.company_name, null, null, null, offer.status_name);
                //string msg = String.Format(Resources.GetString(Resource.String.mensagem).ToString(), cliente.Nome.ToUpper());
                //lblNome.TextFormatted = Html.FromHtml(msg); 
                
                if (response.result_object.offer_description != null)
                {
                    this._offer_description_label.Text = response.result_object.offer_description;
                }
                else
                {
                    this._offer_description_label.Text = "";
                }

                if (response.result_object.company_description != null)
                {
                    this._company_description_label.Text = response.result_object.company_description;
                }
                else {
                    this._company_description_label.Text = "";
                }


                if (response.result_object.applicant_description != null)
                {
                    this._applicant_description_label.Text = response.result_object.applicant_description;
                }
                else
                {
                    this._applicant_description_label.Text = "";
                }

                
                //this._user_label.Text = user_name + "\n" + response.result_object.data.user.email;
                this._position_label.Text = response.result_object.position;

                stream.Append(Local.txtOffer04);
                stream.Append(": ");
                stream.Append(response.result_object.vacants);
                stream.Append("\n");
                stream.Append(Local.txtOffer05);
                stream.Append(": ");
                stream.Append(this.i_contract_kind(response.result_object.contract_kind_id));
                stream.Append("\n");
                stream.Append(Local.txtOffer06);
                stream.Append(": ");
                stream.Append(this.i_hour(response.result_object.hour_id));

                if (response.result_object.visible_salary=="1")
                {
                    stream.Append("\n");
                    stream.Append(Local.txtOffer17);
                    stream.Append(": ");
                    stream.Append(response.result_object.salary);
                }

                this._more_label.Text = stream.ToString();
                this._accept_button.IsVisible = (offer.job_offer_status_id == 1);
                this._decline_button.IsVisible = (offer.job_offer_status_id == 1);
                this._message_view.IsVisible = (offer.job_offer_status_id == 3);
                this._delete_button.IsVisible = (offer.job_offer_status_id == 3);
                this._message_button.IsVisible = (offer.job_offer_status_id == 3);
                if (response.result_object.messages.data.Count > 0) {
                    if (response.result_object.messages.data[0].messages_unread_number == 0)
                    {
                        this._message_badge.IsVisible = false;
                        ///    this._minus_image.TranslationX = 45;
                    }
                    else
                    {
                        this._message_badge.Text = response.result_object.messages.data[0].messages_unread_number.ToString();
                        this._message_badge.IsVisible = true;
                        // this._minus_image.TranslationX = 20;
                    }
                }
                

/*                if (this.Parent != null && this.Parent.GetType() == typeof(OfferMessagePage))
                {
                    OfferMessagePage offer_message = (OfferMessagePage)this.Parent;
                    offer_message.Set_messages_data(offer.id, user_name, offer.unreaded_messages, response.result_object.data.messages);
                }*/

                this.i_Set_layout();
            }
            else
            {
                Alert.Show(response, this);
                await this.Navigation.PopAsync();
            }
        }


        
        //-----------------------------------------------------

        private async void i_On_accept(object sender, EventArgs e)
        {
            if (this._killer_questions != null && this._killer_questions.Count > 0)
            {
                KillerPage killer_page = new KillerPage(this._delegate);
                killer_page.Set_data(this._offer_id, this._killer_questions);
                await this.Navigation.PushAsync(killer_page);
                
            }
            else
            {
                
                ServerResponse<SaveDataJSON> response;
                response = await this._delegate.On_offer_accept(this._offer_id, null);
                if (response.type == ResponseType.OK_SUCCESS)
                {
                    if (response.result_object.result == true || response.result_object.id!=int.MinValue)
                    {
                        this._delegate.User_current_filter = UserCurrentFilter.kNews;
                        await this.DisplayAlert(Local.txtOffer24, null, Local.txtOffer25);
                        await this.Navigation.PopAsync();
                    }
                    else
                    {
                        await this.DisplayAlert("Developer Error", null, "Ok");
                    }
                }
                else
                {
                    Alert.Show(response, this);
                }
            }
        }

        //-----------------------------------------------------

        private async void i_On_decline(object sender, EventArgs e)
        {
            ServerResponse<SaveDataJSON> response;
            response = await this._delegate.On_offer_decline(this._offer_id);
            if (response.type == ResponseType.OK_SUCCESS)
            {
                if (response.result_object.result == true || response.result_object.id!=int.MinValue)
                {
                    this._delegate.User_current_filter = UserCurrentFilter.kNews;
                    await this.DisplayAlert(Local.txtOffer26, null, Local.txtOffer25);
                    await this.Navigation.PopAsync();
                }
                else
                {
                    await this.DisplayAlert("Developer Error", null, "Ok");
                }
            }
            else
            {
                Alert.Show(response, this);
            }
        }

        //-----------------------------------------------------

        private async void i_On_delete(object sender, EventArgs e)
        {
            ServerResponse<SaveDataJSON> response;
            response = await this._delegate.On_offer_delete(this._offer_id);
            if (response.type == ResponseType.OK_SUCCESS)
            {
                if (response.result_object.result == true || response.result_object.id != int.MinValue)
                {
                    this._delegate.User_current_filter = UserCurrentFilter.kAccepted;
                    await this.DisplayAlert(Local.txtOffer27, null, Local.txtOffer25);
                    await this.Navigation.PopAsync();
                }
                else
                {
                    await this.DisplayAlert("Developer Error", null, "Ok");
                }
            }
            else
            {
                Alert.Show(response, this);
            }
        }

        //-----------------------------------------------------

        public void Update_messages(OfferMsgDataJSON messages)
        {
            this._messages = messages.data[0].messages;
        }

        //-----------------------------------------------------

        private async void i_On_messages(object sender, EventArgs e)
        {
            MessagesPage messages_page = new MessagesPage(this._delegate, this);
            messages_page.Set_data(this._offer_id,this._invitation_id,this._company_user_id, this._message_destiny, this._messages, null);
            this._message_badge.IsVisible = false;
            await this.Navigation.PushAsync(messages_page);
        }

        //-----------------------------------------------------

        private async void i_On_new_message(object sender, EventArgs e)
        {
            NewMessagePage new_message_page = new NewMessagePage(this._delegate);
            MessagesPage messages_page = new MessagesPage(this._delegate, this);
            messages_page.Set_data(this._offer_id, this._invitation_id, this._company_user_id, this._message_destiny, this._messages, new_message_page);
            this._message_badge.IsVisible = false;
            await this.Navigation.PushAsync(messages_page);
        }

        //-----------------------------------------------------


    }
}
