﻿using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;
using C2Forms;

namespace Bewanted
{
    public class CompanyPage : ContentPage
    {
        private AppDelegate _delegate;
        private StackLayout _layout;
        private StackLayout _waiting_layout;
        private ImageLayout _company_layout;
        private Label _description_label;
        private Label _address_label;

        //-----------------------------------------------------

        private StackLayout i_Detail_layout(string title, out Label text_label)
        {
            StackLayout layout;
            Label title_label;
            layout = new StackLayout();
            layout.Spacing = AppStyle.Layout_stack_spacing;
            title_label = new Label { Text = title, Style = AppStyle.Content_label_strong };
            title_label.FontSize = C2Style.Entry_font_size;
            title_label.FontAttributes = FontAttributes.Bold;
            text_label = new Label { Text = title, Style = AppStyle.Content_label };
            layout.Children.Add(title_label);
            layout.Children.Add(text_label);
            return layout;
        }

        //-----------------------------------------------------

        public CompanyPage(AppDelegate _delegate) : base()
        {
            Device.OnPlatform(Android: () => NavigationPage.SetTitleIcon(this, "no_image.png"));
            this.Title = Local.txtCompany00;
            this.Style = AppStyle.Page;
            this._delegate = _delegate;
            this._layout = new StackLayout();
            this._layout.VerticalOptions = LayoutOptions.Start;
            this._layout.Spacing = AppStyle.Layout_stack_spacing;
            this._layout.Padding = new Thickness(0.0, 0.0, 0.0, 5 * AppStyle.Layout_stack_long_spacing);

            {
                ContentView view;
                view = new ContentView { Style = C2Style.Control_style };
                view.Padding = new Thickness(AppStyle.Content_box_padding_horizontal, AppStyle.Content_box_padding_vertical);
                this._company_layout = new ImageLayout(AppStyle.Image_size,false,false);
                view.Content = this._company_layout;
                this._layout.Children.Add(view);
            }

            {
                ContentView view;
                StackLayout layout;
                view = new ContentView { Style = C2Style.Control_style };
                view.Padding = new Thickness(AppStyle.Content_box_padding_horizontal, AppStyle.Content_box_padding_vertical);
                layout = new StackLayout();
                layout.Spacing = AppStyle.Layout_stack_long_spacing;

                {
                    StackLayout description_layout;
                    description_layout = this.i_Detail_layout(Local.txtCompany06, out this._description_label);
                    layout.Children.Add(description_layout);
                }

                {
                    StackLayout address_layout;
                    address_layout = this.i_Detail_layout(Local.txtCompany07, out this._address_label);
                    layout.Children.Add(address_layout);
                }

                view.Content = layout;
                this._layout.Children.Add(view);
            }

            this._waiting_layout = UserPage.Waiting_layout();

            Content = new ScrollView
            {
                Content = this._waiting_layout
            };
        }

        //-----------------------------------------------------

        private void i_Set_waiting_layout()
        {
            ScrollView scroll = (ScrollView)this.Content;
            scroll.Content = this._waiting_layout;
        }

        //-----------------------------------------------------

        private void i_Set_layout()
        {
            ScrollView scroll = (ScrollView)this.Content;
            scroll.Content = this._layout;
        }

        //-----------------------------------------------------

        public async void Set_data(int company_id)
        {
            ServerResponse<CompanyJSON> response;
            this._delegate.GA_track_screen(Local.txtGA03);
            this.i_Set_waiting_layout();
            response = await this._delegate.On_company_description(company_id);
            if (response.type == ResponseType.OK_SUCCESS)
            {
                StringBuilder stream = new StringBuilder();
                List<string> icons = new List<string>();
                List<string> urls = new List<string>();
                string sector = null;

                if (response.result_object.sectors != null && response.result_object.sectors.Count > 0 && response.result_object.sectors[0] != null)
                    sector = response.result_object.sectors[0].name;

                if (response.result_object.social_sites != null)
                {
                    for (int i = 0; i < response.result_object.social_sites.Count; ++i) {
                        icons.Add(Server._URL_IMAGES_COMPANIES_API_SOCIAL + response.result_object.social_sites[i].icono.image + ".png");
                        //icons.Add(response.result_object.social_sites[i].icono.image + ".png");
                        urls.Add(response.result_object.social_sites[i].url);
                    }
                        


                }
                //Server._URL_IMAGES_COMPANIES_API + company.image_source+"_logo.jpg";
                //this._company_layout.Set_data(Server._URL_IMAGES_COMPANIES_API+ response.result_object.image_source + "_logo.jpg", response.result_object.name, sector, null, null, response.result_object.url,null);
                this._company_layout.Set_data(response.result_object.image_source, response.result_object.name, sector, null, null, response.result_object.url, null);
                this._company_layout.Set_icons(icons,urls);
                this._description_label.Text = response.result_object.description;

                stream.Append(response.result_object.address);
                stream.Append("\n");
                stream.Append(response.result_object.zip_code);
                stream.Append(" ");

                if (response.result_object.state != null)
                    stream.Append(response.result_object.state.label);

                stream.Append(", ");

                if (response.result_object.country != null)
                    stream.Append(response.result_object.country.name);

                this._address_label.Text = stream.ToString();
                this.i_Set_layout();
            }
            else
            {
                Alert.Show(response, this);
                await this.Navigation.PopAsync();
            }
        }

        //-----------------------------------------------------
        public void Set_data_Obj(CompanyJSON company_id)
        {
            
            this._delegate.GA_track_screen(Local.txtGA03);
            this.i_Set_waiting_layout();
            StringBuilder stream = new StringBuilder();
            List<string> icons = new List<string>();
            List<string> urls = new List<string>();
            string sector = null;

            if (company_id.sectors != null && company_id.sectors.Count > 0 && company_id.sectors[0] != null)
                sector = company_id.sectors[0].name;

            if (company_id.social_sites != null)
            {
                for (int i = 0; i < company_id.social_sites.Count; ++i)
                {
                    //Server._URL_IMAGES_COMPANIES_API + company_id.social_sites[i].image+".png";
                    icons.Add(Server._URL_IMAGES_COMPANIES_API_SOCIAL + company_id.social_sites[i].icono.image + ".png");
                    urls.Add(company_id.social_sites[i].url);
                }



            }
            //Server._URL_IMAGES_COMPANIES_API + company_id.image_source+"_logo.jpg";
            //this._company_layout.Set_data(Server._URL_IMAGES_COMPANIES_API + company_id.image_source + "_logo.jpg", company_id.name, sector, null, null, company_id.url, null);
            this._company_layout.Set_data(company_id.image_source, company_id.name, sector, null, null, company_id.url, null);
            this._company_layout.Set_icons(icons, urls);
            this._description_label.Text = company_id.description;

            stream.Append(company_id.address);
            stream.Append("\n");
            stream.Append(company_id.zip_code);
            stream.Append(" ");

            if (company_id.state != null)
                stream.Append(company_id.state.label);

            stream.Append(", ");

            if (company_id.country != null)
                stream.Append(company_id.country.name);

            this._address_label.Text = stream.ToString();
            this.i_Set_layout();
        }

    }
}
