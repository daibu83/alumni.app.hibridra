﻿using System;
using System.Threading.Tasks;
using Xamarin.Forms;
using C2Forms;
using System.Collections.Generic;

namespace Bewanted
{
    class ExperiencePage : ContentPage
    {
        private AppDelegate _delegate;
        private int _experience_id;
        private StackLayout _layout;
        private StackLayout _waiting_layout;
        //private C2AssistedEntry _company;
        private C2TextEntry _company;
        private C2AssistedEntry _position;
        private IntervalLayout _interval_layout;
        private CountryState _country_state;
        private C2Button _delete_button;

        //-----------------------------------------------------

        public ExperiencePage(AppDelegate _delegate) : base()
        {
            Device.OnPlatform(Android: () => NavigationPage.SetTitleIcon(this, "no_image.png"));
            this.Title = Local.txtExperience00;
            this.Style = AppStyle.Page;
            this._delegate = _delegate;
            this._experience_id = int.MinValue;
            this._layout = new StackLayout();
            this._layout.VerticalOptions = LayoutOptions.Start;
            this._layout.Spacing = AppStyle.Layout_stack_long_spacing;
            this._layout.Padding = new Thickness(0.0, AppStyle.Layout_padding_vertical);

            {
                StackLayout company_layout;
                company_layout = new StackLayout();
                company_layout.Spacing = AppStyle.Layout_stack_spacing;
                //this._company = new C2AssistedEntry(Local.txtExperience01, true, true, this.i_On_company_assist_required, null);
                this._company = new C2TextEntry(Local.txtExperience01, 3, 400, null);
                this._position = new C2AssistedEntry(Local.txtExperience02, true, true, this.i_On_position_assist_required, null);
                company_layout.Children.Add(this._company);
                company_layout.Children.Add(this._position);
                this._layout.Children.Add(company_layout);
            }

            {
                this._interval_layout = new IntervalLayout(Local.txtExperience03);
                this._layout.Children.Add(this._interval_layout);
            }

            {
                StackLayout location_layout;
                location_layout = new StackLayout();
                location_layout.Spacing = AppStyle.Layout_stack_spacing;
                this._country_state = new CountryState(_delegate, this, Local.txtExperience04, Local.txtExperience05, false, null, null);
                location_layout.Children.Add(this._country_state._country);
                location_layout.Children.Add(this._country_state._state);
                this._layout.Children.Add(location_layout);
            }

            {
                ButtonsLayout buttons_layout;
                buttons_layout = new ButtonsLayout(this, out this._delete_button, this.i_On_save, this.i_On_delete);
                this._layout.Children.Add(buttons_layout);
            }

            this._waiting_layout = UserPage.Waiting_layout();

            Content = new ScrollView
            {
                Content = this._waiting_layout
            };
        }

        //-----------------------------------------------------

        private void i_Set_waiting_layout()
        {
            ScrollView scroll = (ScrollView)this.Content;
            scroll.Content = this._waiting_layout;
        }

        //-----------------------------------------------------

        private void i_Set_layout()
        {
            ScrollView scroll = (ScrollView)this.Content;
            scroll.Content = this._layout;
        }

        //-----------------------------------------------------

        private void i_OnAppearing()
        {
            this.i_Set_waiting_layout();
            this._country_state.Load_countries();
        }

        //-----------------------------------------------------

        public void Set_data(ExperienceDataJSON experience)
        {
            this._delegate.GA_track_screen(Local.txtGA05);
            this.i_OnAppearing();

            if (experience != null)
            {
                this._experience_id = experience.id;
                this._company.Text = experience.company.name;
                //this._company.Set_text_and_id(response.result_object.data.company.name, response.result_object.data.company.id);
                this._position.Set_text_and_id(experience.position.name, experience.position.id);
                this._interval_layout.Set_data(experience.month_start, experience.year_start, experience.month_end, experience.year_end);
                this._country_state._country.CurrentId = experience.country_id;
                this._country_state._state.Set_text_and_id(experience.state.label, experience.state.id);
                this._delete_button.IsVisible = true;
                this.i_Set_layout();

                //ServerResponse<ExperienceDataJSON> response;
                //response = await this._delegate.On_experience_data(experience.id);
                //if (response.type == ResponseType.OK_SUCCESS)
                //{
                //    this._experience_id = experience.id;
                //    this._company.Text = response.result_object.company.name;
                //    //this._company.Set_text_and_id(response.result_object.data.company.name, response.result_object.data.company.id);
                //    this._position.Set_text_and_id(response.result_object.position.name, response.result_object.position.id);
                //    this._interval_layout.Set_data(response.result_object.month_start, response.result_object.year_start, response.result_object.month_end, response.result_object.year_end);
                //    this._country_state._country.CurrentId = response.result_object.country_id;
                //    this._country_state._state.Set_text_and_id(response.result_object.state.label, response.result_object.state.id);
                //    this._delete_button.IsVisible = true;
                //    this.i_Set_layout();
                //}
                //else
                //{
                //    Alert.Show(response, this);
                //    await this.Navigation.PopAsync();
                //}
            }
            else
            {
                this._experience_id = int.MinValue;
                this._company.Clear();
                this._position.Clear();
                this._interval_layout.Clear();
                this._country_state.Clear();
                this._delete_button.IsVisible = false;
                this.i_Set_layout();
            }
        }

        //-----------------------------------------------------

        //private async Task<bool> i_On_company_assist_required(C2AssistedEntry sender, string search_text)
        //{
        //    ServerResponse<CompaniesSearchJSON> response;
        //    response = await this._delegate.On_companies_search(search_text);

        //    if (response.type == ResponseType.OK_SUCCESS)
        //    {
        //        for (int i = 0; i < response.result_object.items.Count; ++i)
        //            sender.Add_assisted_item(response.result_object.items[i].value, response.result_object.items[i].id);
        //        return true;
        //    }
        //    else
        //    {
        //        Alert.Show(response, this);
        //        return false;
        //    }
        //}

        //-----------------------------------------------------

        private async Task<bool> i_On_position_assist_required(C2AssistedEntry sender, string search_text)
        {
            ServerResponse<List<PositionJSON>> response;
            response = await this._delegate.On_positions(search_text);

            if (response.type == ResponseType.OK_SUCCESS)
            {
                for (int i = 0; i < response.result_object.Count; ++i)
                    sender.Add_assisted_item(response.result_object[i].name, response.result_object[i].id);
                return true;
            }
            else
            {
                Alert.Show(response, this);
                return false;
            }
        }

        //-----------------------------------------------------

        private async void i_On_save(object sender, EventArgs e)
        {
            bool ok = true;
            ((Button)sender).IsEnabled = false;
            ok = ok & this._company.Validate();
            ok = ok & this._position.Validate();
            ok = ok & this._interval_layout.Validate();
            ok = ok & this._country_state.Validate();

            if (ok == true)
            {
                ExperienceDataJSON experience = new ExperienceDataJSON();
                experience.company = new CompanyJSON();
                experience.position = new PositionJSON();
                experience.state = new StateJSON();
                experience.id = this._experience_id;
                //experience.company.id = this._company.CurrentId;
                experience.company.name = this._company.Text;
                experience.position.id = this._position.CurrentId;
                experience.position.name = this._position.Text;
                this._interval_layout.Get_data(out experience.month_start, out experience.year_start, out experience.month_end, out experience.year_end);
                experience.country_id = this._country_state._country.CurrentId;
                experience.state.id = this._country_state._state.CurrentId;

                ServerResponse<SaveDataJSON> response;
                response = await this._delegate.On_experience_data_save(experience);
                if (response.type == ResponseType.OK_SUCCESS)
                {
                    if (response.result_object.result == true || response.result_object.id !=int.MinValue)
                    {
                        await this.Navigation.PopAsync();
                    }
                    else
                    {
                        await this.DisplayAlert("Developer Error", null, "Ok");
                    }
                }
                else
                {
                    Alert.Show(response, this);
                }
            }

            ((Button)sender).IsEnabled = true;
        }

        //-----------------------------------------------------

        private async void i_On_delete(object sender, EventArgs e)
        {
            if (await this.DisplayAlert(null, Local.txtExperience13, Local.txtButtons05, Local.txtButtons06) == true)
            {
                ServerResponse<SaveDataJSON> response;
                response = await this._delegate.On_experience_data_delete(this._experience_id);
                if (response.type == ResponseType.OK_SUCCESS)
                {
                    if (response.result_object.result == true || response.result_object.details == "Deleted resource")
                    {
                        await this.Navigation.PopAsync();
                    }
                    else
                    {
                        await this.DisplayAlert("Developer Error", null, "Ok");
                    }
                }
                else
                {
                    Alert.Show(response, this);
                }
            }
        }

        //-----------------------------------------------------


    }
}
