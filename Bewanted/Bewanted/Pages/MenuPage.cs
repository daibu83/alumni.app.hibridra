﻿using System;
using System.Collections.Generic;
using Xamarin.Forms;
using C2Forms;
using System.IO;

namespace Bewanted
{
    //-----------------------------------------------------

    public class MenuItem : ContentView
    {
        public string _image_source;
        public string _image_source_dark;
        public Image _image;
        public Label _label;
        public C2Badge _badge1;
        public C2Badge _badge2;

        //-----------------------------------------------------

        public MenuItem(bool with_framed_labels, string text, string image_source, LayoutOptions vertical_options, EventHandler handler, List<MenuItem> items,double wsize) : base()
        {
            StackLayout layout;
            TapGestureRecognizer gesture;
            this.VerticalOptions = vertical_options;
            this.Style = AppStyle.Menu_background;
            layout = new StackLayout();
            layout.Orientation = StackOrientation.Horizontal;
            //layout.Spacing = AppStyle.Menu_padding_horizontal;
            layout.Padding = new Thickness(AppStyle.Menu_padding_horizontal, AppStyle.Menu_padding_vertical);
            this._image_source = image_source + ".png";
            this._image_source_dark = image_source + "_dark.png";
            this._image = new Image { Aspect = Aspect.AspectFit };
            this._image.WidthRequest = AppStyle.Menu_image_size;
            this._image.HeightRequest = AppStyle.Menu_image_size;
            this._image.Source = this._image_source_dark;
            this._label = new Label { Text = text, Style = AppStyle.Menu_label };
            this._label.VerticalOptions = LayoutOptions.Center;
            gesture = new TapGestureRecognizer();
            gesture.Tapped += handler;
            this.GestureRecognizers.Add(gesture);
            layout.Children.Add(this._image);
            layout.Children.Add(this._label);

            if (with_framed_labels == true)
            {
                StackLayout frames_layout;
                double size;
                double font_size;
                frames_layout = new StackLayout();
                frames_layout.Orientation = StackOrientation.Horizontal;
                if (wsize > 0)
                {
                    frames_layout.Spacing = AppStyle.Layout_stack_spacing;
                }
                else {
                    frames_layout.Spacing = AppStyle.Layout_stack_long_spacing;
                }
                
                font_size = Device.GetNamedSize(NamedSize.Micro, typeof(Label));
                size = 1.8 * font_size;
                this._badge1 = new C2Badge(size,0, font_size);
                this._badge1.Padding = new Thickness(0.5, 0, 0.5, 0);
                this._badge1.BoxColor = AppStyle.Menu_framed_label1;
                this._badge1.GestureRecognizers.Add(gesture);
                this._badge1.HorizontalOptions = LayoutOptions.Center;
                this._badge1.VerticalOptions = LayoutOptions.Center;

                this._badge2 = new C2Badge(size,wsize, font_size);
                this._badge2.Padding = new Thickness(3, 0, 2, 0);
                this._badge2.BoxColor = AppStyle.Menu_framed_label2;
                this._badge2.GestureRecognizers.Add(gesture);
                this._badge2.HorizontalOptions = LayoutOptions.Center;
                this._badge2.VerticalOptions = LayoutOptions.Center;
                frames_layout.HorizontalOptions = LayoutOptions.End;
                frames_layout.VerticalOptions = LayoutOptions.Center;
                frames_layout.Children.Add(this._badge1);
                frames_layout.Children.Add(this._badge2);
                layout.Children.Add(frames_layout);
            }

            this.Content = layout;
            items.Add(this);
        }
    }

    //-----------------------------------------------------

    public class MenuPage : ContentPage
    {
        private AppDelegate _delegate;
        private StackLayout _menu_layout;
        private Label _lbl_user_name;
        private Label _lbl_user_last_name;
        private Image _user_image;
        private List<MenuItem> _menu_items;
        
        //-----------------------------------------------------

        private StackLayout i_User_header_layout()
        {
            StackLayout layout = new StackLayout();
            layout.Style = AppStyle.Menu_background_strong;
            layout.Orientation = StackOrientation.Horizontal;
            layout.Spacing = AppStyle.Menu_padding_horizontal;
            layout.Padding = new Thickness(AppStyle.Menu_padding_horizontal, AppStyle.Menu_padding_vertical);
            this._user_image = new Image { Aspect = Aspect.AspectFit };
            this._user_image.WidthRequest = AppStyle.Image_size;
            this._user_image.HeightRequest = AppStyle.Image_size;
            layout.Children.Add(this._user_image);

            {
                StackLayout layout1;
                layout1 = new StackLayout();
                layout1.VerticalOptions = LayoutOptions.Center;
                this._lbl_user_name = new Label { Style = AppStyle.Menu_label_strong };
                this._lbl_user_last_name = new Label { Style = AppStyle.Menu_label };
                layout1.Children.Add(this._lbl_user_name);
                layout1.Children.Add(this._lbl_user_last_name);
                layout.Children.Add(layout1);
            }

            TapGestureRecognizer gesture;
            gesture = new TapGestureRecognizer();
            gesture.Tapped += this.i_On_user_data_tap;
            layout.GestureRecognizers.Add(gesture);
            return layout;
        }

        //-----------------------------------------------------

        public void lineaMenuIOS() {
            double font_size = Device.GetNamedSize(NamedSize.Micro, typeof(Label));
            double size = font_size;
            double wsize= 1.7* size;
            this._menu_layout.Children.Add(new MenuItem(true, Local.txtProfile00, "user", LayoutOptions.Start, this.i_On_item_tap, this._menu_items, wsize));

        }

        public void lineaMenuAndroid() {
            double font_size = Device.GetNamedSize(NamedSize.Micro, typeof(Label));
            double size = 1.8 * font_size;
            double wsize = 1.1 * size;
            this._menu_layout.Children.Add(new MenuItem(true, Local.txtProfile00, "user", LayoutOptions.Start, this.i_On_item_tap, this._menu_items, wsize));
        }



        public MenuPage(AppDelegate _delegate, UserCurrentPage user_current_page) : base()
        {
            
            this.Title = "Master page";

            int android_api = DependencyService.Get<IDeviceService>().AndroidAPILevel();
            if (android_api >= 1 && android_api <= 20)
            {
                this.Icon = "menu_margin.png";
            }
            else
            {
                this.Icon = "menu.png";
            }

            this._delegate = _delegate;
            this._lbl_user_name = null;
            this._lbl_user_last_name = null;
            this._user_image = null;
            this._menu_items = new List<MenuItem>();
            this._menu_layout = new StackLayout();
            this._menu_layout.Style = AppStyle.Menu_background;
            this._menu_layout.Spacing = AppStyle.Layout_stack_spacing;
            this._menu_layout.Children.Add(i_User_header_layout());
            //wsize = 1.5 * size;
            

            

            this._menu_layout.Children.Add(new MenuItem(true, Local.txtPanel00, "inbox", LayoutOptions.Start, this.i_On_item_tap, this._menu_items, 0));
            Device.OnPlatform(iOS: () => lineaMenuIOS(), Android: () => lineaMenuAndroid());
            
            this._menu_layout.Children.Add(new MenuItem(false, Local.txtCompany00, "company", LayoutOptions.Start, this.i_On_item_tap, this._menu_items,0));
            this._menu_layout.Children.Add(new MenuItem(false, Local.txtHowto00, "gear", LayoutOptions.Start, this.i_On_item_tap, this._menu_items,0));

            {
                ContentView empty_view;
                empty_view = new ContentView();
                empty_view.VerticalOptions = LayoutOptions.FillAndExpand;
                this._menu_layout.Children.Add(empty_view);
            }

            this._menu_layout.Children.Add(new MenuItem(false, Local.txtSettings00, "settings", LayoutOptions.End, this.i_On_item_tap, this._menu_items,0));
            this.i_Select_menu_item(user_current_page);

            Content = new ScrollView
            {
                Content = this._menu_layout
            };
        }

        //-----------------------------------------------------

        public void Set_user_data(string user_name, string user_last_name, string user_image)
        {
            this._lbl_user_name.Text = user_name;
            this._lbl_user_last_name.Text = user_last_name;
            this._user_image.Source = user_image;
            

        }

        //-----------------------------------------------------

        public void Set_new_offers(uint num_new_offers)
        {
            MenuItem item = this._menu_items[0];
            if (num_new_offers > 0)
            {
                item._badge1.Text = num_new_offers.ToString();
                item._badge1.IsVisible = true;
            }
            else
            {
                item._badge1.IsVisible = false;
            }
        }

        //-----------------------------------------------------

        public async void Set_percentage()
        {
        
            
            ServerResponse<StudentDataJSON> response;
            MenuItem item = this._menu_items[1];
            response = await this._delegate.On_profile_percentaje();
            if (response.type == ResponseType.OK_SUCCESS) {
                int porcentaje =(int) response.result_object.percentaje;
                
                if (AppDelegate._is_personal_data_complete == false)
                {
                    //incompleto
                    item._badge2.BoxColor = AppStyle.percentage_color;
                    item._badge2.Text = Local.txtPanel13;
                    AppDelegate._is_perfil_data_min = 2;
                    AppDelegate._showperfilcompleto = true;
                }
                else if (AppDelegate._is_academic_data == true && AppDelegate._is_merchant_data == true)
                {
                    item._badge2.BoxColor = AppStyle.percentage_color;
                    item._badge2.Text = response.result_object.percentaje.ToString() + "%" + " " + Local.txtPanel12;
                    if (AppDelegate._is_perfil_data_min == 2)
                    {
                        AppDelegate._showperfilcompleto = true;
                    }
                    else
                    {
                        AppDelegate._showperfilcompleto = false;
                    }
                    AppDelegate._is_perfil_data_min = 1;
                }
                else
                {
                    item._badge2.BoxColor = AppStyle.percentage_color;
                    item._badge2.Text = Local.txtPanel13;
                    AppDelegate._is_perfil_data_min = 2;
                    AppDelegate._showperfilcompleto = true;
                }
                
                item._badge2.IsVisible = true;
            }
            else {
                item._badge2.IsVisible = false;
            }
                
        }


        
        //-----------------------------------------------------

        public void Set_num_unreaded_messages(long num_messages)
        {
            MenuItem item = this._menu_items[0];
            if (num_messages > 0)
            {
                if (num_messages == 1) {
                    item._badge2.Text = num_messages.ToString() + " " + Local.txtPanel18;
                }else {
                    item._badge2.Text = num_messages.ToString() + " " + Local.txtPanel11;
                }
                
                item._badge2.IsVisible = true;
            }
            else
            {
                item._badge2.IsVisible = false;
            }
        }

        //-----------------------------------------------------

        private void i_Select_menu_item(UserCurrentPage user_current_page)
        {
            for (int i = 0; i < 5; ++i)
            {
                MenuItem item = (MenuItem)this._menu_items[i];
                if ((UserCurrentPage)i == user_current_page)
                {
                    item.Style = AppStyle.Menu_background_strong;
                    item._image.Source = item._image_source;
                    item._label.Style = AppStyle.Menu_label_strong;
                }
                else
                {
                    item.Style = AppStyle.Menu_background;
                    item._image.Source = item._image_source_dark;
                    item._label.Style = AppStyle.Menu_label;
                }
            }
        }

        //-----------------------------------------------------

        private void i_On_item_tap(object sender, EventArgs e)
        {
            if(this._delegate.Control_Carga(this)) return;
            UserCurrentPage new_user_current_page = UserCurrentPage.kPanel;
            for (int i = 0; i < 5; ++i)
            {
                if (this._menu_items[i] == sender || this._menu_items[i]._badge1 == sender || this._menu_items[i]._badge2 == sender)
                {
                    new_user_current_page = (UserCurrentPage)i;
                    break;
                }
            }

            this.i_Select_menu_item(new_user_current_page);
            this._delegate.On_user_current_page_change(new_user_current_page, false);
        }

        //-----------------------------------------------------

        private void i_On_user_data_tap(object sender, EventArgs e)
        {
            if (this._delegate.Control_Carga(this)) return;
            this.Set_percentage();
            this._delegate.On_user_current_page_change(UserCurrentPage.kProfile, true);
        }



        


}
}

