﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using Xamarin.Forms;
using C2Core;
using C2Forms;
using System.Threading;

namespace Bewanted
{
    public class CompanyList
    {
        public List<CompanyJSON> items;
        public bool is_at_end;
    }

    //-----------------------------------------------------

    public class CompaniesPage : ContentPage
    {

        private const int _NUM_PAGES_PER_INDEX = 1;
        private const int _NUM_ITEMS_PER_PAGE = 5;
        private AppDelegate _delegate;
        private bool _is_filter_layout_on;
        private int _current_filter_id;
        private string _current_filter_text = "";
        private int _current_page_index;
        private bool _is_last_page;
        private StackLayout _filter_bar_layout;
        private Label _filter_label;
        private Image _filter_image_down;
        private Image _filter_image_up;
        private StackLayout _waiting_layout;
        private StackLayout _empty_layout;
        private StackLayout _companies_layout;
        private C2ListView _company_view;
        private SearchBar searchbar;
        private SearchBar searchbarVacio;

        private ContentView _loading_more_view;
        private StackLayout _filters_layout;
        private ScrollView _filters_view;
        private StackLayout _content_layout;
        private bool _waiting_for_companies;
        private ObservableCollection<ImageItemEmpresa> _company_list;
        private List<int> _sector_ids;
        private bool _is_first_preview;
        private CompanyPage _company_page;
        private CompanyList list;
        private C2Picker _country;
        //-----------------------------------------------------

        private void i_Set_filter_icon(bool is_filter_layout_on)
        {
            if (is_filter_layout_on == true)
            {
                this._filter_image_down.IsVisible = false;
                this._filter_image_up.IsVisible = true;
            }
            else
            {
                this._filter_image_down.IsVisible = true;
                this._filter_image_up.IsVisible = false;
            }
        }


        //-----------------------------------------------------

        private string i_Filter_text(int current_filter_id)
        {
            if (current_filter_id == 0)
            {
                return Local.txtCompany05;
            }
            else
            {
                for (int i = 0; i < this._sector_ids.Count; ++i)
                {
                    if (this._sector_ids[i] == current_filter_id)
                    {
                        ContentView view = (ContentView)this._filters_layout.Children[i];
                        Label label = (Label)view.Content;
                        return label.Text;
                    }
                }
            }

            return Local.txtCompany05;
        }

        //-----------------------------------------------------

        private ContentView i_Filter_bar_view(bool is_filter_layout_on, int current_filter_id)
        {
            ContentView view;
            Label label;
            TapGestureRecognizer gesture;
            view = new ContentView();
            view.Style = AppStyle.Filter_background;
            view.Padding = new Thickness(AppStyle.Filter_padding_horizontal, AppStyle.Filter_padding_vertical, 2.0 * AppStyle.Filter_padding_horizontal, AppStyle.Filter_padding_vertical);
            this._filter_bar_layout = new StackLayout();
            this._filter_bar_layout.Orientation = StackOrientation.Horizontal;
            this._filter_bar_layout.Spacing = 0.0;
            this._filter_label = new Label { Text = i_Filter_text(current_filter_id) };
            this._filter_label.Style = AppStyle.Filter_label_strong;
            this._filter_label.HorizontalOptions = LayoutOptions.FillAndExpand;
            label = new Label { Text = Local.txtCompany01 + " " };
            label.Style = AppStyle.Filter_label;
            label.HorizontalOptions = LayoutOptions.Start;
            this._filter_image_down = new Image { Source = "arrow_down.png" };
            this._filter_image_down.HorizontalOptions = LayoutOptions.End;
            this._filter_image_down.WidthRequest = AppStyle.Filter_image_size;
            this._filter_image_down.HeightRequest = AppStyle.Filter_image_size;
            this._filter_image_up = new Image { Source = "arrow_up.png" };
            this._filter_image_up.HorizontalOptions = LayoutOptions.End;
            this._filter_image_up.WidthRequest = AppStyle.Filter_image_size;
            this._filter_image_up.HeightRequest = AppStyle.Filter_image_size;
            gesture = new TapGestureRecognizer();
            gesture.Tapped += this.i_On_filter;
            view.GestureRecognizers.Add(gesture);
            this._filter_bar_layout.Children.Add(label);
            this._filter_bar_layout.Children.Add(this._filter_label);
            this._filter_bar_layout.Children.Add(this._filter_image_down);
            this._filter_bar_layout.Children.Add(this._filter_image_up);
            this.i_Set_filter_icon(is_filter_layout_on);
            view.Content = this._filter_bar_layout;
            return view;
        }

        
        private StackLayout i_Companies_layout()
        {
            C2Assert.No_null(this._company_list);
            C2Assert.Error(this._company_list.Count > 0);
            if (this._companies_layout == null)
            {
                ActivityIndicator indicator;
                this._companies_layout = new StackLayout();
                this._companies_layout.VerticalOptions = LayoutOptions.FillAndExpand;
                this._companies_layout.Spacing = 0.0;
                this._company_view = new C2ListView();
                this._company_view.ItemTemplate = new DataTemplate(typeof(Bewanted.ImageCellEmpresa));
                this._company_view.ItemsSource = this._company_list;
                this._company_view.VerticalOptions = LayoutOptions.FillAndExpand;
                this._company_view.HasUnevenRows = true;
                this._company_view.ItemAppearing += this.i_On_item_apearing;
                this._company_view.ItemTapped += this.i_On_item_tap;
                this._loading_more_view = new ContentView();
                this._loading_more_view.Padding = new Thickness(10);
                this._loading_more_view.BackgroundColor = C2Style.Control_background;
                indicator = new ActivityIndicator { WidthRequest = 24, HeightRequest = 24 };
                indicator.IsRunning = true;
                this._loading_more_view.Content = indicator;
                this._loading_more_view.IsVisible = false;

             
                {
                    StackLayout layout = new StackLayout();
                    layout.Spacing = 0.0;
                    layout.HorizontalOptions = LayoutOptions.Fill;
                    this.searchbar = new SearchBar()
                    {
                        Placeholder = "Buscar",
                        PlaceholderColor=Color.FromHex("#BAC5C9")
                    };
                    this.searchbar.TextColor = Color.Black;
                    this.searchbar.TextChanged += this.EntryOnTextChanged;
                    this.searchbar.SearchButtonPressed += (sender, e) =>
                    {
                        Interlocked.Exchange(ref this.throttleCts, new CancellationTokenSource()).Cancel();

                        Task.Delay(TimeSpan.FromMilliseconds(1500), this.throttleCts.Token) // throttle time
                            .ContinueWith(

                                delegate { this.HandleString(searchbar.Text); },
                                CancellationToken.None,
                                TaskContinuationOptions.OnlyOnRanToCompletion,
                                TaskScheduler.FromCurrentSynchronizationContext());
                    };


                    layout.Children.Add(searchbar);
                    this._companies_layout.Children.Add(layout);
                }
                
                
                
                this._companies_layout.Children.Add(this._company_view);
                this._companies_layout.Children.Add(_loading_more_view);
            }

            return this._companies_layout;
        }

        private CancellationTokenSource throttleCts = new CancellationTokenSource();

        private void EntryOnTextChanged(object sender, TextChangedEventArgs args)
        {
            Interlocked.Exchange(ref this.throttleCts, new CancellationTokenSource()).Cancel();

            Task.Delay(TimeSpan.FromMilliseconds(2000), this.throttleCts.Token) // throttle time
                .ContinueWith(

                    delegate {

                        try
                        {
                            this.HandleString(searchbar.Text);
                        }
                        catch (Exception) { }
                        

                        
                    },
                    CancellationToken.None,
                    TaskContinuationOptions.OnlyOnRanToCompletion,
                    TaskScheduler.FromCurrentSynchronizationContext());


        }


        private void Entry2OnTextChanged(object sender, TextChangedEventArgs args)
        {
            Interlocked.Exchange(ref this.throttleCts, new CancellationTokenSource()).Cancel();

            Task.Delay(TimeSpan.FromMilliseconds(2000), this.throttleCts.Token) // throttle time
                .ContinueWith(

                    delegate {
                        try
                        {
                            this.HandleString(searchbarVacio.Text);
                        }
                        catch (Exception) { }
                        },
                    CancellationToken.None,
                    TaskContinuationOptions.OnlyOnRanToCompletion,
                    TaskScheduler.FromCurrentSynchronizationContext());


        }
        //
        private void HandleString(string str)
        {
            // .. if CanExecute(...) Execute(...)
            // this.Label.Text = str.Length.ToString();
            filter_Search_text(str);
        }



        private void filter_Search_text(string str)
        {
            if (this._current_filter_text == str || AppDelegate._is_textfilter == true || this._waiting_for_companies ==true)
            {
                //no buscar-son iguales
                //o se esta realizando busqueda
                if (this._waiting_for_companies == true) {
                    return;
                }
            }
            else
            {
                this._current_filter_text = str;
                this._is_filter_layout_on = false;
                this._current_page_index = 0;
                this._is_last_page = false;
                AppDelegate._is_textfilter = true;
                i_Update_companies(this._is_filter_layout_on, this._current_filter_id);

            }
            //if (this._waiting_for_companies == false) {
                
            //}

            if (searchbarVacio != null)
            {
                // si se ha mostrado alguna vez el mensaje de vacio-Act la busqueda para que muestre lo mismo que la actual
                if (searchbarVacio.Text != str)
                {
                    searchbarVacio.Text = str;
                }
            }
            if (searchbar != null)
            {
                if (searchbar.Text != str)
                {
                    searchbar.Text = str;
                }
            }


        }

        //-----------------------------------------------------

        private StackLayout i_Empty_layout()
        {
            if (this._empty_layout == null)
            {
                this._empty_layout = new StackLayout();
                this._empty_layout.HorizontalOptions = LayoutOptions.Fill;
                this._empty_layout.Padding = new Thickness(0.0, 0.0, 0.0, 0.0);
                this._empty_layout.Spacing = 0.0;

                {
                    StackLayout layout = new StackLayout();
                    layout.Spacing = 0.0;
                    layout.HorizontalOptions = LayoutOptions.Fill;
                    if (this._current_filter_text != "")
                    {
                        this.searchbarVacio = new SearchBar()
                        {
                            Text = this._current_filter_text,
                        };
                    }
                    else
                    {
                        this.searchbarVacio = new SearchBar()
                        {
                            
                            Placeholder = "Buscar",
                            PlaceholderColor = Color.FromHex("#BAC5C9")
                        };
                    }

                    searchbarVacio.TextColor = Color.Black;
                    this.searchbarVacio.TextChanged += this.Entry2OnTextChanged;
                
                    searchbarVacio.SearchButtonPressed += (sender, e) =>
                    {
                        //filter_Search_text(searchbarVacio.Text);
                        Interlocked.Exchange(ref this.throttleCts, new CancellationTokenSource()).Cancel();

                        Task.Delay(TimeSpan.FromMilliseconds(1500), this.throttleCts.Token) // throttle time
                            .ContinueWith(

                                delegate { this.HandleString(searchbarVacio.Text); },
                                CancellationToken.None,
                                TaskContinuationOptions.OnlyOnRanToCompletion,
                                TaskScheduler.FromCurrentSynchronizationContext());
                    };
                    layout.Children.Add(searchbarVacio);
                    this._empty_layout.Children.Add(layout);
                    
                }

                {
                    StackLayout layout = new StackLayout();
                    Image image;
                    layout.Padding = new Thickness(0.0, 10.0, 0.0, 0.0);
                    layout.Spacing = 0.0;
                    layout.HorizontalOptions = LayoutOptions.Center;
                    image = new Image { Source = "no_offer.png" };

                    
                    layout.Children.Add(image);
                    
                    this._empty_layout.Children.Add(layout);
                }

                {
                    StackLayout layout = new StackLayout();
                    Label title;
                    Label label1;
                    layout.Spacing = 0.0;
                    layout.HorizontalOptions = LayoutOptions.Center;
                    layout.Padding = new Thickness(0.0, 20.0, 0.0, 0.0);
                    title = new Label { Text = Local.txtCompany03, Style = AppStyle.Label };
                    title.HorizontalOptions = LayoutOptions.Center;
                    title.FontAttributes = FontAttributes.Bold;
                    label1 = new Label { Text = Local.txtCompany04, Style = AppStyle.Label };
                    label1.HorizontalOptions = LayoutOptions.Center;
                    layout.Children.Add(title);
                    layout.Children.Add(label1);
                    this._empty_layout.Children.Add(layout);
                }
            }

            return this._empty_layout;
        }

        //-----------------------------------------------------

        private ContentView i_Filter_button(string text, EventHandler handler)
        {
            ContentView view;
            Label label;
            TapGestureRecognizer gesture;
            view = new ContentView();
            view.Padding = new Thickness(0, C2Style.Entry_padding_vertical, C2Style.Entry_padding_horizontal, C2Style.Entry_padding_vertical);
            label = new Label { Text = text };
            label.FontSize = C2Style.Label_font_size;
            label.TextColor = C2Style.Control_text;
            label.HorizontalOptions = LayoutOptions.FillAndExpand;
            label.VerticalOptions = LayoutOptions.Center;
            label.TranslationX = 2.0 * C2Style.Entry_padding_horizontal;
            gesture = new TapGestureRecognizer();
            gesture.Tapped += handler;
            view.GestureRecognizers.Add(gesture);
            view.Content = label;
            return view;
        }

        //-----------------------------------------------------

        private async Task<bool> i_Create_filters_view()
        {
            if (this._filters_view == null)
            {
                this._filters_view = new ScrollView();
                this._filters_layout = new StackLayout();
                this._filters_layout.Spacing = AppStyle.Layout_stack_spacing;
                this._sector_ids = new List<int>();
                this._filters_layout.Children.Add(i_Filter_button(Local.txtCompany05, this.i_On_filter_selected));
                this._sector_ids.Add(0);

                ServerResponse<List<SectorJSON>> sectors_json = await this._delegate.On_sectors("");
                if (sectors_json.type == ResponseType.OK_SUCCESS)
                {
                    for (int i = 0; i < sectors_json.result_object.Count; ++i)
                    {
                        SectorJSON sector = sectors_json.result_object[i];
                        this._filters_layout.Children.Add(i_Filter_button(sector.name, this.i_On_filter_selected));
                        this._sector_ids.Add(sector.id);
                    }
                }
                else
                {
                    Alert.Show(sectors_json, this);
                }

                this._filters_view.Content = this._filters_layout;
            }

            return true;
        }

        //-----------------------------------------------------

        private ScrollView i_Filter_view(int current_filter_id)
        {
            C2Assert.No_null(this._filters_layout);
            C2Assert.Error(this._filters_layout.Children.Count == this._sector_ids.Count);
            for (int i = 0; i < this._filters_layout.Children.Count; ++i)
            {
                ContentView view = (ContentView)this._filters_layout.Children[i];
                if (this._sector_ids[i] == current_filter_id)
                    view.BackgroundColor = AppStyle.Page_background;
                else
                    view.BackgroundColor = C2Style.Control_background;
            }

            return this._filters_view;
        }

        //-----------------------------------------------------

        private View i_Client_view(bool is_filter_layout_on, int current_filter_id)
        {
            this.i_Set_filter_icon(is_filter_layout_on);
            this._filter_label.Text = i_Filter_text(current_filter_id);

            if (is_filter_layout_on == true)
            {
                return this.i_Filter_view(current_filter_id);
            }
            else
            {
                if (this._waiting_for_companies == true)
                {
                    if (this._waiting_layout == null)
                        this._waiting_layout = UserPage.Waiting_layout();
                    return this._waiting_layout;
                }
                else
                {
                    // AppDelegate._is_textfilter == true
                    if (this._company_list != null && this._company_list.Count > 0)
                    {
                        return this.i_Companies_layout();
                    }
                    else
                    {
                        return this.i_Empty_layout();
                    }


                }
            }
        }

        //-----------------------------------------------------

        private void i_Change_client_view(bool is_filter_layout_on, int current_filter_id)
        {
            View view;
            View content;
            C2Assert.Error(this._content_layout.Children.Count == 2);
            view = this.i_Client_view(is_filter_layout_on, current_filter_id);
            content = this._content_layout.Children[1];
            if (content != view)
            {
                
                this._content_layout.Children.RemoveAt(1);
                GC.Collect();
                this._content_layout.Children.Add(view);
                try
                {
                    if (AppDelegate._is_textfilter == true && this._waiting_for_companies == false)
                    {
                        if (this._company_list.Count > 0)
                        {
                            try
                            {
                                this.searchbar.Focus();

                            }
                            catch (Exception) { }

                        }
                        else
                        {
                            try
                            {
                                this.searchbarVacio.Focus();
                            }
                            catch (Exception) { }
                        }
                        AppDelegate._is_textfilter = false;
                    }
                }
                catch (Exception) { }


            }
        }

        //-----------------------------------------------------

        private async Task<CompanyList> i_Company_list(int current_filter_id, int current_page_index, string search_text)
        {
            CompanyList list = new CompanyList();
            list.items = new List<CompanyJSON>();
            list.is_at_end = false;
            int start_page = (current_page_index * _NUM_PAGES_PER_INDEX) + 1;

            for (int i = start_page; i < start_page + _NUM_PAGES_PER_INDEX; ++i)
            {
                ServerResponse<CompaniesJSON> companies_json;
                companies_json = await this._delegate.On_company_list(current_filter_id, i, search_text);
                if (companies_json.type == ResponseType.OK_SUCCESS)
                {
                    if (companies_json.result_object.items.Count > 0)
                    {
                        for (int j = 0; j < companies_json.result_object.items.Count; ++j) {
                            list.items.Add(companies_json.result_object.items[j]);
                        }
                        if (companies_json.result_object.items.Count < _NUM_ITEMS_PER_PAGE)
                        {
                            list.is_at_end = true;
                            break;
                        }
                    }
                    else
                    {
                        list.is_at_end = true;
                        break;
                    }
                }
                else
                {
                    Alert.Show(companies_json, this);
                }
            }
            this._waiting_for_companies = false;
            return list;
        }

        //-----------------------------------------------------

        private async Task<bool> i_Add_companies(List<CompanyJSON> items)
        {
            for (int i = 0; i < items.Count; ++i)
            {
                CompanyJSON company = items[i];
                ImageItemEmpresa item = new ImageItemEmpresa();
                //item.Image =Server._URL_IMAGES_COMPANIES_API + company.image_source+"_logo.jpg";
                //item.Image = company.image_source;
                //switch (Device.OS)
                //{
                //    case TargetPlatform.Android:
                //        item.ImageUrl = await DependencyService.Get<IImages>().ImageFromUrl(company.image_source);

                //        break;
                //    default:
                //        item.Image = company.image_source;
                //        break;
                //}
                item.ImageUrl = await DependencyService.Get<IImages>().ImageFromUrl(company.image_source);

                if (String.IsNullOrEmpty(company.name) == true)
                    item.Title = "";
                else
                    item.Title = company.name;

                {
                    string city = null;
                    string country = null;

                    if (company.city != null && company.city.id != 0)
                        city = company.city.label;

                    //if (company.country != null && company.country.id != 0)
                    //{
                    //    country = company.country.name;
                    //}
                    if (Convert.ToInt32(company.country_id) != 0)
                    {
                        this._country.CurrentId = Convert.ToInt32(company.country_id);
                        country = this._country.CurrentText;
                    }

                    item.Detail1 = Util.Location(city, country);
                }

                if (company.sectors != null)
                {
                    if (company.sectors.Count > 0 && company.sectors[0] != null)
                        item.Detail2 = company.sectors[0].name;
                    else
                        item.Detail2 = "";
                }

                item._index = this._company_list.Count;
                item._id = company.orderid;
                this._company_list.Add(item);
            }
            return true;
        }

        //-----------------------------------------------------

        private async void i_Update_companies(bool is_filter_layout_on, int current_filter_id)
        {
            C2Assert.Error(is_filter_layout_on == false);
            //CompanyList list;
            bool continuar;
            this._waiting_for_companies = true;
            this._delegate._blncargando = true;
            this._company_list.Clear();

            if (this._company_view != null)
            {
                this._company_list = new ObservableCollection<ImageItemEmpresa>();
                this._company_view.ItemsSource = this._company_list;
            }

            this.i_Change_client_view(is_filter_layout_on, current_filter_id);
            this.list = await this.i_Company_list(current_filter_id, this._current_page_index, this._current_filter_text);
            continuar= await this.i_Add_companies(this.list.items);
            this._is_last_page =this.list.is_at_end;

            if (this._is_last_page == false)
                this._current_page_index += 1;

            
            this.i_Change_client_view(is_filter_layout_on, current_filter_id);
            this._delegate._blncargando = false;
        }

        //-----------------------------------------------------

        public CompaniesPage(AppDelegate _delegate) : base()
        {
            this.Style = AppStyle.Page;
            this._delegate = _delegate;
            this._is_filter_layout_on = false;
            this._current_filter_id = 0;
            this._current_page_index = 0;
            this._is_last_page = false;
            this.Title = Local.txtCompany00;
            Device.OnPlatform(Android: () => NavigationPage.SetTitleIcon(this, "no_image.png"));
            this._filter_bar_layout = null;
            this._filter_label = null;
            this._filter_image_down = null;
            this._filter_image_up = null;
            this._waiting_layout = null;
            this._empty_layout = null;
            this._companies_layout = null;
            this._company_view = null;
            this._loading_more_view = null;
            this._filters_layout = null;
            this._filters_view = null;
            this._content_layout = null;
            this._waiting_for_companies = true;
            this._company_list = new ObservableCollection<ImageItemEmpresa>();
            this._sector_ids = null;
            this._is_first_preview = true;
            this._company_page = null;
            this._content_layout = new StackLayout();
            this._content_layout.Spacing = 0.0;
            this._content_layout.Children.Add(i_Filter_bar_view(this._is_filter_layout_on, this._current_filter_id));
            this._content_layout.Children.Add(i_Client_view(this._is_filter_layout_on, this._current_filter_id));
            this.Content = this._content_layout;
        }

        //-----------------------------------------------------
        private void i_delet_companies()
        {
            for (int i = 0; i < _company_list.Count - 1; ++i)
            {
                _company_list[i].Image = null;
            }
            _company_list.Clear();
            _company_list = null;
            GC.Collect();
        }
        protected override void OnDisappearing()
        {
           // i_delet_companies();
            base.OnDisappearing();
           // GC.Collect();
        }
        protected async override void OnAppearing()
        {
            this._delegate.GA_track_screen(Local.txtGA02);

            if (this._company_view != null)
                this._company_view.IsEnabled = true;

            if (this._is_first_preview == true)
            {
                if (await this.i_Create_filters_view() == true) {
                    this._country = new C2Picker("", null);
                    if (this._country.Count == 0)
                    {
                        this._country.Set_waiting_state();
                        ServerResponse<List<CountryJSON>> response = await this._delegate.On_countries();
                        if (response.type == ResponseType.OK_SUCCESS)
                        {
                            for (int i = 0; i < response.result_object.Count; ++i)
                                this._country.Add(response.result_object[i].name, response.result_object[i].id);
                        }
                    }
                    this._country.Unset_waiting_state();
                    this.i_Update_companies(this._is_filter_layout_on, this._current_filter_id);
                }
                    
            }



        }

        //-----------------------------------------------------

        private void i_On_filter(object sender, EventArgs e)
        {
            if (this._delegate.Control_Carga(this)) return;
            this._is_filter_layout_on = !this._is_filter_layout_on;
            this.i_Change_client_view(this._is_filter_layout_on, this._current_filter_id);
        }

        //-----------------------------------------------------

        private async void i_On_item_apearing(object sender, ItemVisibilityEventArgs e)
        {
            bool continuar;
            if (this._is_last_page == false && this._loading_more_view.IsVisible == false)
            {
                ImageItemEmpresa item = (ImageItemEmpresa)e.Item;
                if (item._index >= this._company_list.Count - _NUM_ITEMS_PER_PAGE)
                {
                    this._loading_more_view.IsVisible = true;

                    this.list = await this.i_Company_list(this._current_filter_id, this._current_page_index, this._current_filter_text);

                    continuar = await this.i_Add_companies(this.list.items);
                    this._is_last_page = this.list.is_at_end;

                    /* if (this._delegate._app._platform == App.Platform.iOS)
                     {
                         this._company_view.ItemsSource = null;
                         this._company_view.ItemsSource = this._company_list;
                     }*/

                    if (this._is_last_page == false)
                        this._current_page_index += 1;

                    this._loading_more_view.IsVisible = false;
                }
            }
        }

        //-----------------------------------------------------

        private async void i_On_item_tap(object sender, ItemTappedEventArgs e)
        {
            if (this._delegate.Control_Carga(this)) return;
            ImageItemEmpresa item = (ImageItemEmpresa)e.Item;
            int j;
            this._company_view.IsEnabled = false;
            this._company_page = new CompanyPage(this._delegate);
            this._is_first_preview = false;
            //_company_page.Set_data(item._id);
            ((ListView)sender).SelectedItem = null;
            await this.Navigation.PushAsync(this._company_page);
            
            
            for (j = 0; j < this.list.items.Count; ++j) {
                if (this.list.items[j].orderid== item._id) {
                    break;
                }
            }
            //CompanyJSON company;
            C2Assert.Error(j <= this.list.items.Count);

            //if (j < this.list.items.Count)
            //    company = this.list.items[j];
            //else
            //    company = null;

            //if (company == null)
            //{
            //    _company_page.Set_data(item._id);
            //}
            //else {
            //    _company_page.Set_data_Obj(company);
            //}

            _company_page.Set_data(item._id);
            this.list.items.Clear();

        }

        //-----------------------------------------------------

        private void i_On_filter_selected(object sender, EventArgs e)
        {
            if (this._delegate.Control_Carga(this)) return;
            C2Assert.No_null(this._filters_layout);
            C2Assert.Error(this._filters_layout.Children.Count == this._sector_ids.Count);
            this._current_filter_id = 0;

            for (int i = 0; i < this._filters_layout.Children.Count; ++i)
            {
                if (this._filters_layout.Children[i] == sender)
                {
                    this._current_filter_id = this._sector_ids[i];
                    break;
                }
            }

            this._is_filter_layout_on = false;
            this._current_page_index = 0;
            this._is_last_page = false;
            i_Update_companies(this._is_filter_layout_on, this._current_filter_id);
        }

        //-----------------------------------------------------

    }
}
