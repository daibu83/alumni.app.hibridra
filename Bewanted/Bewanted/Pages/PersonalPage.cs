﻿using System;
using System.Collections.Generic;
using Xamarin.Forms;
using C2Forms;
using System.Threading.Tasks;

namespace Bewanted
{
    public class SocialLine
    {
        public int _id;
        public C2TextEntry _url;
    }

    public class SocialGrid : Grid
    {
        private List<SocialLine> _socials;
        private AppDelegate _delegate;
        private PersonalPage _page;

        //-----------------------------------------------------

        public SocialGrid(AppDelegate _delegate, PersonalPage page) : base()
        {
            this._socials = new List<SocialLine>();
            this._delegate = _delegate;
            this._page = page;
            this.ColumnSpacing = 0.0;
            this.RowSpacing = C2Style.Table_column_spacing;
            this.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(0.5, GridUnitType.Star) });
            this.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(3.7, GridUnitType.Star) });
        }

        //-----------------------------------------------------

        public void AddLine(int id, string image_source, string name, string url)
        {
            int index = this._socials.Count;
            string ico_code;
            SocialLine social;
            //C2ControlImage image;
            C2HyperLinkLabelAwesome image;
            social = new SocialLine();
            social._id = id;
            ico_code = Util.icoAwesomeCode(image_source.ToUpper());
            image = new C2HyperLinkLabelAwesome { Text = ico_code};
            image.WidthRequest = 24;
            image.HeightRequest = 24;
            
            
            
            image.HorizontalOptions = LayoutOptions.Center;
            image.VerticalOptions = LayoutOptions.Center;
            image.FontSize = Device.GetNamedSize(NamedSize.Large, typeof(Label));
            //image.TextColor = AppStyle.Splash_background;
            image.TextColor = Util.icoAwesomeColor(image_source.ToUpper());
            //image.BackgroundColor = Color.Transparent;
            image.Subject = "enlace";

            
            
            //image.BackgroundColor = Util.icoAwesomeBackColor(icons[i].ToUpper());

            //image = new C2ControlImage(Server._URL_IMAGES_API_SOCIAL+image_source, 24.0);

            social._url = new C2TextEntry(name, 0, 512,Keyboard.Url);
            social._url.Text = url;
            //this.RowDefinitions.Add(new RowDefinition { Height = GridLength.Auto });
            this.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1, GridUnitType.Star) });
            this.Children.Add(image, 0, index);
            this.Children.Add(social._url, 1, index);
            this._socials.Add(social);
        }

        
        //-----------------------------------------------------

        public void Clear()
        {
            this._socials.Clear();
            this.Children.Clear();
        }

        //-----------------------------------------------------

        public bool Validate()
        {
            bool ok = true;
            for (int i = 0; i < this._socials.Count; ++i)
                ok = ok & this._socials[i]._url.Validate();
            return ok;
        }

        //-----------------------------------------------------

        public List<PersonalSocialJSON> CurrentData()
        {
            List<PersonalSocialJSON> socials = new List<PersonalSocialJSON>();
            for (int i = 0; i < this._socials.Count; ++i)
            {
                PersonalSocialJSON social = new PersonalSocialJSON();
                social.id = this._socials[i]._id;
                social.url = this._socials[i]._url.Text;                
                socials.Add(social);
            }
            return socials;
        }
    }

    //-----------------------------------------------------

    public class PersonalPage : ContentPage, ModalDialog
    {
        PopupLayout _popup = new PopupLayout();

        private ScrollView scroll;
        private AppDelegate _delegate;
        private ContentView view_msg;
        private ContentView view_popup_msg;
        private StackLayout _layout;
        private StackLayout _waiting_layout;
        private Image _profile_image;    
        private UserData _user_data;
        private SocialGrid _social_grid;
        private char[] _image64;
        private byte[] _imageBytes64;
        private C2PageDialog objModalPage;
        #region IModalHost implementation

        public Task DisplayPageModal(Page page)
        {
            var displayEvent = DisplayPageModalRequested;

            Task completion = null;
            if (displayEvent != null)
            {
                var eventArgs = new DisplayPageModalRequestedEventArgs(page);
                displayEvent(this, eventArgs);
                completion = eventArgs.DisplayingPageTask;
            }

            // If there is not task, just create a new completed one
            return completion ?? Task.FromResult<object>(null);
        }
        #endregion


        public event EventHandler<DisplayPageModalRequestedEventArgs> DisplayPageModalRequested;

        public sealed class DisplayPageModalRequestedEventArgs : EventArgs
        {
            public Task DisplayingPageTask { get; set; }

            public Page PageToDisplay { get; }

            public DisplayPageModalRequestedEventArgs(Page modalPage)
            {
                PageToDisplay = modalPage;
            }
        }
        //-----------------------------------------------------

        private ContentView i_Profile_image_view(string text, string image_source, EventHandler handler)
        {
            ContentView view;
            StackLayout layout;
            StackLayout text_layout;
            Label label;
            Label label2;
            TapGestureRecognizer gesture;
            view = new ContentView();
            view.Style = C2Style.Control_style;
            layout = new StackLayout();
            layout.Orientation = StackOrientation.Horizontal;
            layout.Padding = new Thickness(AppStyle.Content_box_padding_horizontal, AppStyle.Content_box_padding_vertical);
            layout.Spacing = AppStyle.Content_box_padding_horizontal;
            this._profile_image = new Image { Source = image_source };
            this._profile_image.WidthRequest = AppStyle.Image_size;
            this._profile_image.HeightRequest = AppStyle.Image_size;
            this._profile_image.Aspect = Aspect.AspectFit;
            text_layout = new StackLayout();
            text_layout.Spacing = 0.0;
            label = new Label { Text = text };
            label.Style = AppStyle.Label;
            label.VerticalOptions = LayoutOptions.Center;
            label2 = new Label { Text = Local.txtPersonal07 };
            label2.Style = AppStyle.Label;
            label2.VerticalOptions = LayoutOptions.Center;
            label2.FontSize = Device.GetNamedSize(NamedSize.Micro, typeof(Label));
            label2.FontAttributes = FontAttributes.Italic;
            text_layout.Children.Add(label);
            text_layout.Children.Add(label2);
            text_layout.VerticalOptions = LayoutOptions.Center;
            layout.Children.Add(this._profile_image);
            layout.Children.Add(text_layout);
            gesture = new TapGestureRecognizer();
            gesture.Tapped += handler;
            view.GestureRecognizers.Add(gesture);
            view.Content = layout;
            return view;
        }

        //mod001:mensaje de inicio de rellenado de datos
        private ContentView i_Personal_msg_view()
        {
            
            StackLayout layout;
            Image image;
            Label label;
            
            this.view_msg = new ContentView();
            this.view_msg.Style = AppStyle.Msg_label_background;
            
            this.view_msg.Padding = new Thickness(10, 12, 18, 13);

            layout = new StackLayout();
            layout.Orientation = StackOrientation.Horizontal;
            layout.Spacing = 0.0;
            image = new Image { Source = "profile.png" };
            image.WidthRequest = 18;
            image.TranslationX = 3;


            label = new Label();
            var fs = new FormattedString();
            fs.Spans.Add(new Span { Text = Local.txtPersonal08, ForegroundColor = AppStyle.Label_white_text, FontSize = C2Style.Label_font_size });
            fs.Spans.Add(new Span { Text = Local.txtPersonal09, ForegroundColor = AppStyle.Label_white_text, FontSize = C2Style.Label_font_size, FontAttributes = FontAttributes.Bold });
            fs.Spans.Add(new Span { Text = Local.txtPersonal10, ForegroundColor = AppStyle.Label_white_text, FontSize = C2Style.Label_font_size });
            label.FormattedText = fs;
            label.TranslationX = 18;
            layout.Children.Add(image);
            layout.Children.Add(label);
            //layout.HeightRequest = layout.Height;
            //layout.WidthRequest = layout.Width;
            this.view_msg.Content = layout;
            return this.view_msg;
        }

        private ContentView i_Personal_msg_view_popup()
        {

            C2Dialog layout;

            this.view_popup_msg = new ContentView();
            this.view_popup_msg.BackgroundColor = Color.White;
            //this.view_popup_msg.Padding = new Thickness(10, 5, 10, 5);
            var fstitle = new FormattedString();
            var fs = new FormattedString();
            if (AppDelegate._is_personal_data_complete == false)
            {
                //mostrar mensaje de complatar CV
                fstitle.Spans.Add(new Span { Text = Local.txtPersonal13, FontSize = C2Style.Label_font_size_title, FontAttributes = FontAttributes.Bold });
                fs.Spans.Add(new Span { Text = Local.txtPersonal14, FontSize = C2Style.Label_font_size_popmsg });
            }
            else {
                if (AppDelegate._showpersonalcompleto == true)
                {
                    //mostrar mensaje de enhorabuena
                    fstitle.Spans.Add(new Span { Text = Local.txtPersonal11, FontSize = C2Style.Label_font_size_title, FontAttributes = FontAttributes.Bold });
                    fs.Spans.Add(new Span { Text = Local.txtPersonal12, FontSize = C2Style.Label_font_size_popmsg });
                }
                else {
                    if (AppDelegate._is_perfil_data_min != 1)
                    {
                        //mostrar mensaje de complatar CV
                        fstitle.Spans.Add(new Span { Text = Local.txtPersonal13, FontSize = C2Style.Label_font_size_title, FontAttributes = FontAttributes.Bold });
                        fs.Spans.Add(new Span { Text = Local.txtPersonal14, FontSize = C2Style.Label_font_size_popmsg });
                    }
                    else {
                        fstitle.Spans.Add(new Span { Text = Local.txtPersonal15, FontSize = C2Style.Label_font_size_title, FontAttributes = FontAttributes.Bold });
                        fs.Spans.Add(new Span { Text = Local.txtPersonal16, FontSize = C2Style.Label_font_size_popmsg });
                    }
                    
                }

                    
            }
            
            //view_popup_msg.Opacity = 0.25;
            
            
            
            layout = new C2Dialog(fstitle, fs,true,false, i_On_close);
            layout.Orientation = StackOrientation.Vertical;
            layout.Spacing = 10.0;

            this.view_popup_msg.Content = layout;
            //this.view_popup_msg.HeightRequest = Height / 3;
            this.view_popup_msg.WidthRequest = Width - 30;
            
            return this.view_popup_msg;
        }

        //-----------------------------------------------------

        public PersonalPage(AppDelegate _delegate) : base()
        {
            
            Device.OnPlatform(Android: () => NavigationPage.SetTitleIcon(this, "no_image.png"));
            this.Title = Local.txtPersonal00;
            this.Style = AppStyle.Page;
            this._delegate = _delegate;
            this._layout = new StackLayout();
            this._layout.VerticalOptions = LayoutOptions.Start;

            
            if (AppDelegate._is_personal_data_complete == false) {
                this._layout.Children.Add(i_Personal_msg_view());
            }

            

            {
                ContentView image_view;
                image_view = this.i_Profile_image_view(Local.txtPersonal01, "user_image.png", this.i_On_profile_image_click);
                image_view.Padding = new Thickness(0, 13, 0, 13);
                this._layout.Children.Add(image_view);
            }

            {
                StackLayout data_layout;
                data_layout = new StackLayout();
                data_layout.Spacing = AppStyle.Layout_stack_spacing;
                this._user_data = new UserData(_delegate, this,true);
                this._user_data.Add_to_layout(data_layout);
                this._layout.Children.Add(data_layout);
            }

            { 
                this._social_grid = new SocialGrid(this._delegate, this);
                this._layout.Children.Add(this._social_grid);
            }

            {
                ButtonsLayout buttons_layout;
                buttons_layout = new ButtonsLayout(this, this.i_On_save);
                this._layout.Children.Add(buttons_layout);
            }

           

            this._image64 = null;
            this._waiting_layout = UserPage.Waiting_layout();


            
            Content = new ScrollView
            {
                Content = this._waiting_layout
            };
            

        }


        private StackLayout CreatePageContent_Page2()
        {
            
            View _popup;
            _popup = i_Personal_msg_view_popup();
            var backdrop = new RelativeLayout
            {
                GestureRecognizers = { new TapGestureRecognizer() }
            };

            backdrop.Children.Add(_popup,
                Constraint.RelativeToParent(p => (Width / 2) - (_popup.WidthRequest / 2)),
                Constraint.RelativeToParent(p => (Height / 2) - (_popup.HeightRequest / 2)),
                Constraint.RelativeToParent(p => _popup.WidthRequest),
                Constraint.RelativeToParent(p => _popup.HeightRequest));


            
            StackLayout objStackLayout = new StackLayout()
            {
                
                HorizontalOptions = LayoutOptions.FillAndExpand,
                VerticalOptions = LayoutOptions.End,
                HeightRequest = Height,
                WidthRequest = Width
            };

            objStackLayout.Children.Add(backdrop);
            
            return objStackLayout;
            
        }

        //-----------------------------------------------------

        private void i_Set_waiting_layout()
        {
            ScrollView scroll = (ScrollView)this.Content;
            scroll.Content = this._waiting_layout;
        }

        //-----------------------------------------------------

        private void i_Set_layout()
        {
            ScrollView scroll = (ScrollView)this.Content;
            scroll.Content = this._layout;
        }

        //-----------------------------------------------------

        protected override void OnDisappearing()
        {

        
            base.OnDisappearing();
        }

        private void i_OnAppearing()
        {
            this.i_Set_waiting_layout();
            this._user_data.Load_pickers_data();
        }

        //-----------------------------------------------------

        public async void Set_data(string profile_image)
        {
            this.i_OnAppearing();
            this._delegate.GA_track_screen(Local.txtGA18);

            ServerResponse<StudentDataJSON> response;
            response = await this._delegate.On_personal_data();

            if (response.type == ResponseType.OK_SUCCESS)
            {
                this._profile_image.Source = profile_image;
                this._image64 = null;
                this._user_data._name.Text = response.result_object.name;
                this._user_data._last_name.Text = response.result_object.last_name;
                this._user_data._phone.Text = response.result_object.phone;
                if (response.result_object.nationality_id > 0)
                {
                    this._user_data._nationality.CurrentId = response.result_object.nationality_id;
                }
                if (response.result_object.location != null)
                {
                    if (response.result_object.location.country != null) {
                        this._user_data._country_state._country.CurrentId = response.result_object.location.country.id;
                    }
                    if (response.result_object.location.state != null)
                    {
                        if (response.result_object.location.state.id > 0)
                        {
                            this._user_data._country_state._state.Set_text_and_id(response.result_object.location.state.label, response.result_object.location.state.id);
                        }
                    }
                    if (response.result_object.location.city != null) {
                        if (response.result_object.location.city.id > 0)
                        {
                            this._user_data._city.Set_text_and_id(response.result_object.location.city.label, response.result_object.location.city.id);
                        }
                    }
                }
                
                if (response.result_object.birth_date!="0000-00-00"){
                    this._user_data._birthdate.yyyy_mm_dd = response.result_object.birth_date;
                }
                
                this._social_grid.Clear();
                ServerResponse<List<SocialJSON>> responsesocial;
                responsesocial = await this._delegate.On_social();
                bool encontrado;
                if (responsesocial.type == ResponseType.OK_SUCCESS) {
                    for (int i = 0; i < responsesocial.result_object.Count; ++i)

                    {
                        SocialJSON social_ = responsesocial.result_object[i];

                        if (response.result_object.sites != null)
                        {
                            encontrado = false;
                            for (int j = 0; j < response.result_object.sites.Count; ++j)
                            {
                                SocialJSON social = response.result_object.sites[j];
                                if (social.id == social_.id)
                                {
                                    this._social_grid.AddLine(i + 1, social_.image, social.name, social.pivot.url);
                                    encontrado = true;
                                }
                            }
                            if (encontrado == false) {
                                this._social_grid.AddLine(i + 1, social_.image, "", "");
                            }
                            
                        }
                        else {
                            this._social_grid.AddLine(i + 1, social_.image, "", "");
                        }
                        
                        
                    }
                }

                
                

                this.i_Set_layout();
            }
            else
            {
                Alert.Show(response, this);
                await this.Navigation.PopAsync();
            }
        }

        //-----------------------------------------------------

        private void i_On_profile_image_click(object sender, EventArgs e)
        {
            this._delegate._app._picking_state = 1;
                
            DependencyService.Get<IPickImageService>().Pick_image(Local.txtPersonal02, this.i_On_image_pick);
        }

        //-----------------------------------------------------

        private void i_On_image_pick(byte[] data)
        {
            if (data != null)
            {
                this._profile_image.Source = ImageSource.FromStream(() => new System.IO.MemoryStream(data));
                this._imageBytes64 = data;
                int size = (int)(Math.Ceiling((double)data.Length / 3) * 4);
                this._image64 = new char[size];
                System.Convert.ToBase64CharArray(data, 0, data.Length, this._image64, 0);
            }
        }

        //-----------------------------------------------------
        private void muestraPopup() {
            this.Title = "Datos personales ";
            this.scroll = (ScrollView)this.Content;
            this.scroll.Content = this._layout;
            _popup.Content = this.scroll;
            Content = _popup;

            var frame = new Frame
            {
                WidthRequest = Width - 80,
                Padding = new Thickness(15, 20, 15, 20),
                Content = i_Personal_msg_view_popup(),
                OutlineColor = Color.Silver,
                BackgroundColor = Color.White,
                VerticalOptions = LayoutOptions.CenterAndExpand,
                HorizontalOptions = LayoutOptions.CenterAndExpand
            };

            var PopUp = new StackLayout
            {
                VerticalOptions = LayoutOptions.CenterAndExpand,
                HorizontalOptions = LayoutOptions.CenterAndExpand,
                WidthRequest = Width,
                // Important, the Popup hast to have a size to be showed
                HeightRequest = Height,
                BackgroundColor = new Color(0, 0, 0, 0.7),
            };

            PopUp.Children.Add(frame);
            NavigationPage.SetHasBackButton(this, false);
            _delegate.Disable_Menu();
            _popup.ShowPopup(PopUp);
            

        }

        private async void i_On_save(object sender, EventArgs e)
        {
            this.IsEnabled = false;
            bool ok = true;
            bool response = false;
            ((Button)sender).IsEnabled = false;
            ok = ok & this._user_data.ValidatePersonales();
            ok = ok & this._social_grid.Validate();

            if (ok == true)
            {
                PersonalDataJSON personal = new PersonalDataJSON();
                personal.location = new PersonalLocationJSON();
                personal.location.state = new PersonalStateJSON();
                personal.location.city = new PersonalCityJSON();
                personal.name = this._user_data._name.Text;
                personal.last_name = this._user_data._last_name.Text;
                personal.phone = this._user_data._phone.Text;
                personal.nationality_id = this._user_data._nationality.CurrentId;
                personal.location.country_id = this._user_data._country_state._country.CurrentId;
                personal.location.state.id = this._user_data._country_state._state.CurrentId;
                personal.location.city.id = this._user_data._city.CurrentId;
                personal.birth_date = this._user_data._birthdate.yyyy_mm_dd;
                personal.sites = this._social_grid.CurrentData();

                //_imageBytes64
                //response = await this._delegate.On_personal_data_save(personal, this._image64);
                response = await this._delegate.On_personal_data_save(personal, this._imageBytes64);
                if (response == true)
                {
                    await _delegate.datos_personales_completos();
                    objModalPage = new C2PageDialog(this.Height, this.Width, i_Personal_msg_view_popup());
                    Device.OnPlatform(iOS: () => DisplayPageModal(objModalPage), Android: () => muestraPopup());
                }
                else 
                {
                    await this.DisplayAlert(Local.txtPersonal08, Local.txtPersonal09, Local.txtAlertOk);
                }
                
            }
            this.IsEnabled = true;
            ((Button)sender).IsEnabled = true;

        }

        private void restableceMenu() {
            
            this.Title = "Datos personales";
            NavigationPage.SetHasBackButton(this, true);
            _delegate.Enable_Menu();
            _popup.DismissPopup();
            Content = this.scroll;
            this.Navigation.PopAsync();
        }

        private void i_On_close(object sender, EventArgs e)
        {
            
            
            Device.OnPlatform(iOS: () => objModalPage.Close(), Android: () => restableceMenu());
            if (this.view_msg != null)
            {
                if (this.view_msg.IsVisible)
                {
                    this.view_msg.FadeTo(0, 6000, Easing.CubicIn);
                    this.i_Remove_msg_layout_children();
                }
            }
            
            ///tras aceptar llevarlo a Editar perfil
            Device.OnPlatform(iOS: () => this.Navigation.PopAsync());


        }
        private void i_Remove_msg_layout_children()
        {
            ScrollView scroll = (ScrollView)this.Content;
            StackLayout layout = (StackLayout)scroll.Content;
            try
            {
                layout.Children.RemoveAt(layout.Children.IndexOf(this.view_msg));
            }
            catch
            {
            }
        }
        //-----------------------------------------------------
        protected override bool OnBackButtonPressed()
        {
            // If you want to continue going back
            
            if (_popup.IsPopupActive) {
                return true;
            }
            else
            {
                
                base.OnBackButtonPressed();
                return false;
            }
        }

    }
}
