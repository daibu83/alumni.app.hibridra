﻿using System;
using System.Collections.Generic;
using Xamarin.Forms;
using C2Forms;

namespace Bewanted
{
    public class LoginPage : ContentPage
    {
        private AppDelegate _delegate;
        private CreateAccountPage _create_account_page;
        private C2MailEntry _email;
        private C2PasswordEntry _password;
        C2Button enter;
        C2Button create;

        //-----------------------------------------------------

        public LoginPage(AppDelegate _delegate) : base()
        {
            StackLayout layout;
            NavigationPage.SetHasNavigationBar(this, false);
            this.Title = "Login";
            this.Style = AppStyle.Page;
            this._delegate = _delegate;
            this._create_account_page = null;
            layout = new StackLayout();
            layout.VerticalOptions = LayoutOptions.Start;
            layout.Spacing = AppStyle.Layout_stack_spacing;

            {
                Image image;
                image = new Image { Aspect = Aspect.AspectFit, Source = "login_image.png" };
                this._email = new C2MailEntry(Local.txtEmail);
                this._password = new C2PasswordEntry(Local.txtPass, 6, 50);
                layout.Children.Add(image);
                layout.Children.Add(this._email);
                layout.Children.Add(this._password);
            }

            {
                StackLayout inner_layout;
                inner_layout = new StackLayout();
                inner_layout.Padding = new Thickness(AppStyle.Button_padding_horizontal, AppStyle.Button_padding_vertical);
                inner_layout.Spacing = AppStyle.Button_padding_vertical;

                {
                    StackLayout text_layout;
                    Label label;
                    Label forget_label;
                    TapGestureRecognizer gesture;
                    text_layout = new StackLayout();
                    text_layout.Orientation = StackOrientation.Horizontal;
                    text_layout.HorizontalOptions = LayoutOptions.Start;
                    label = new Label { Text = Local.txtLogin00, Style = AppStyle.Label };
                    forget_label = new Label { Text = Local.txtLogin01, Style = AppStyle.Label_link };
                    gesture = new TapGestureRecognizer();
                    gesture.Tapped += i_OnForgetPassword;
                    forget_label.GestureRecognizers.Add(gesture);
                    text_layout.Children.Add(label);
                    text_layout.Children.Add(forget_label);
                    inner_layout.Children.Add(text_layout);
                }

                {
                    //C2Button enter;
                    this.enter = new C2Button { Text = Local.txtLogin02 };
                    this.enter.Style = AppStyle.Button1_style;
                    this.enter.Clicked += i_OnEnter;
                    inner_layout.Children.Add(this.enter);
                }

                {
                    // C2Button create;
                    this.create = new C2Button { Text = Local.txtCreate03 };
                    this.create.Style = AppStyle.Button2_style;
                    this.create.Clicked += i_OnCreate;
                    inner_layout.Children.Add(this.create);
                }

                layout.Children.Add(inner_layout);
            }

            Content = new ScrollView
            {
                Content = layout
            };
        }

        //-----------------------------------------------------

        ~LoginPage()
        {
            this._email = null;
            this._password = null;
            this._delegate = null;
        }

        //-----------------------------------------------------

        public void SetData(string email, string password)
        {
            this._delegate.GA_track_screen(Local.txtGA00);

            if (string.IsNullOrEmpty(email) == false)
                this._email.Text = email;

            if (string.IsNullOrEmpty(password) == false)
                this._password.Text = password;
        }

        //-----------------------------------------------------

        private async void i_OnEnter(object sender, EventArgs e)
        {
            
            this.IsEnabled = false;
            bool ok = true;
            this.enter.IsEnabled = false;
            this.create.IsEnabled = false;
            ok = ok & this._email.Validate();
            ok = ok & this._password.Validate();

            if (ok == true)
            {
                if (this._delegate._app._platform == App.Platform.Android)
                {
                    List<string> data = new List<string>();
                    data.Add(this._email.Text);
                    data.Add(this._password.Text);
                    DependencyService.Get<IFileService>().Save_data(data);
                }
                else
                {
                    Application.Current.Properties["email"] = this._email.Text;
                    Application.Current.Properties["password"] = this._password.Text;
                    await Application.Current.SavePropertiesAsync();
                }

                await _delegate.On_login(this._email.Text, this._password.Text, this);
                this.IsEnabled = true;
                this.enter.IsEnabled = true;
                this.create.IsEnabled = true;

            }
            else
            {
                this.IsEnabled = true;
                this.enter.IsEnabled = true;
                this.create.IsEnabled = true;
            }
        }

        //-----------------------------------------------------

        private async void i_OnCreate(object sender, EventArgs e)
        {
            this.create.IsEnabled = false;
            this.enter.IsEnabled = false;
            if (this._create_account_page == null)
                this._create_account_page = new CreateAccountPage(this._delegate);
            await this.Navigation.PushAsync(this._create_account_page);
            this.create.IsEnabled = true;
            this.enter.IsEnabled = true;
        }

        //-----------------------------------------------------

        private async void i_OnForgetPassword(object sender, EventArgs e)
        {
            RecoverPassPage page;
            page = new RecoverPassPage(this._delegate);
            await this.Navigation.PushAsync(page);
        }

        //-----------------------------------------------------


    }
}
