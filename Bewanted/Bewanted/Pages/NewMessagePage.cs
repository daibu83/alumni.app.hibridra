﻿using System;
using Xamarin.Forms;
using C2Forms;

namespace Bewanted
{
    public class NewMessagePage : ContentPage
    {
        private AppDelegate _delegate;
        private MessagesPage _messages_page;
        private int _invitation_id;
        private int _company_user_id;
        private string _destiny;
        private C2ControlLabel _destiny_label;
        //private C2Editor _subject;
        private C2Editor _message;

        //-----------------------------------------------------

        public NewMessagePage(AppDelegate _delegate) : base()
        {
            StackLayout layout;
            Device.OnPlatform(Android: () => NavigationPage.SetTitleIcon(this, "no_image.png"));
            this.Title = Local.txtMessages01;
            this.Style = AppStyle.Page;
            this._delegate = _delegate;
            this._messages_page = null;
            this._invitation_id = int.MinValue;
            this._company_user_id = int.MinValue;
            this._destiny = null;
            layout = new StackLayout();
            layout.Padding = new Thickness(0.0, AppStyle.Layout_padding_vertical);
            layout.VerticalOptions = LayoutOptions.Start;
            layout.Spacing = AppStyle.Layout_stack_spacing;
            this._destiny_label = new C2ControlLabel();
            this._destiny_label.TextColor = C2Style.Control_text;
            layout.Children.Add(this._destiny_label);

            //if (this._delegate._app._platform == App.Platform.iOS)
            //{
            //    Label label = new Label { Text = Local.txtMessages03 };
            //    label.Style = AppStyle.Label_header;
            //    label.TranslationX = C2Style.Entry_padding_horizontal;
            //    label.VerticalOptions = LayoutOptions.Center;
            //    layout.Children.Add(label);
            //}

            //this._subject = new C2Editor(Local.txtMessages03, 5, 100, 3,null);
            //layout.Children.Add(this._subject);

            if (this._delegate._app._platform == App.Platform.iOS)
            {
                Label label = new Label { Text = Local.txtMessages04 };
                label.Style = AppStyle.Label_header;
                label.TranslationX = C2Style.Entry_padding_horizontal;
                label.VerticalOptions = LayoutOptions.Center;
                layout.Children.Add(label);
            }

            this._message = new C2Editor(Local.txtMessages04, 5, 500, 12,null);
            layout.Children.Add(this._message);

            {
                StackLayout buttons_layout;
                C2Button new_button;
                //C2Button cancel_button;
                buttons_layout = new StackLayout();
                buttons_layout.Padding = new Thickness(AppStyle.Button_padding_horizontal, AppStyle.Button_padding_vertical);
                buttons_layout.Spacing = AppStyle.Layout_stack_long_spacing;
                new_button = new C2Button { Text = Local.txtMessages05 };
                new_button.Style = AppStyle.Button1_style;
                new_button.Clicked += i_On_send;
                /*cancel_button = new C2Button { Text = Local.txtMessages06 };
                cancel_button.Style = AppStyle.Button3_style;
                cancel_button.Clicked += i_On_cancel;*/
                buttons_layout.Children.Add(new_button);
                //buttons_layout.Children.Add(cancel_button);
                layout.Children.Add(buttons_layout);
            }

            Content = new ScrollView
            {
                Content = layout
            };
        }

        //-----------------------------------------------------

        public void Set_data(int invitation_id,int company_user_id, string destiny, MessagesPage messages_page)
        {
            this._delegate.GA_track_screen(Local.txtGA14);
            this._invitation_id = invitation_id;
            this._company_user_id = company_user_id;
            this._destiny = destiny;
            this._messages_page = messages_page;
            this._destiny_label.Text = Local.txtMessages02 + ": " + destiny;
            //this._subject.Clear();
            this._message.Clear();
        }

        //-----------------------------------------------------

        private async void i_On_send(object sender, EventArgs e)
        {
            bool ok = true;

            //ok = ok & this._subject.Validate();
            ok = ok & this._message.Validate();

            if (ok == true)
            {
                ServerResponse<SaveDataJSON> response;
                response = await this._delegate.On_offer_message_save(this._invitation_id, this._company_user_id, this._message.Text);
                if (response.type == ResponseType.OK_SUCCESS)
                {
                    if (response.result_object.result == true || response.result_object.response == "Saved data")
                    {
                        await this.DisplayAlert(Local.txtMessages07, null, Local.txtMessages08);
                        await this.Navigation.PopAsync();
                        this._messages_page.Update_messages();
                    }
                    else
                    {
                        await this.DisplayAlert("Developer Error", null, "Ok");
                    }
                }
                else
                {
                    Alert.Show(response, this);
                }
            }
        }

        //-----------------------------------------------------

            /*
        private async void i_On_cancel(object sender, EventArgs e)
        {
            if (await this.DisplayAlert(Local.txtButtons07, Local.txtButtons08, Local.txtButtons05, Local.txtButtons06) == true)
                await this.Navigation.PopAsync();
        }*/

        //-----------------------------------------------------

    }
}