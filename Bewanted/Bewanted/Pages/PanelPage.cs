﻿using System;
using System.Collections.Generic;
using Xamarin.Forms;
using C2Core;
using C2Forms;
using System.Globalization;
using System.Collections.ObjectModel;
using System.Threading.Tasks;

namespace Bewanted
{
    public class OfferList
    {
        public List<OfferJSON> items;
        public bool is_at_end;
    }


    public class PanelPage : ContentPage
    {
        private AppDelegate _delegate;
        private StackLayout _filter_bar_layout;
        private Label _filter_label;
        private Image _filter_image_down;
        private Image _filter_image_up;
        private StackLayout _waiting_layout;
        private StackLayout _empty_layout;
        private StackLayout _offers_layout;
        private StackLayout _filters_layout;
        private StackLayout _content_layout;
        private bool _waiting_for_offers;
        //private List<OfferJSON> _offer_list;
        private OfferPage _offer_page;
        private bool _is_first_appearing;
        private bool _enable_offer_tap;



        //private const int _NUM_PAGES_PER_INDEX = 1;
        //private const int _NUM_ITEMS_PER_PAGE = 5;
        //private int _current_page_index;
        //private bool _is_last_page;

        private C2ListView _offers_view;
        //private ContentView _loading_more_view;
        private ObservableCollection<ImageItem> _offers_list;
        private OfferList list;
        





        //-----------------------------------------------------

        private void i_Set_filter_icon(UserCurrentView user_current_view)
        {
            switch (user_current_view)
            {
                case UserCurrentView.kOffers:
                    this._filter_image_down.IsVisible = true;
                    this._filter_image_up.IsVisible = false;
                    break;
                case UserCurrentView.kFilters:
                    this._filter_image_down.IsVisible = false;
                    this._filter_image_up.IsVisible = true;
                    break;
            }
        }

        //-----------------------------------------------------

        private string i_Filter_text(UserCurrentFilter user_current_filter)
        {
            switch (user_current_filter)
            {
                case UserCurrentFilter.kAll:
                    return Local.txtPanel02;
                case UserCurrentFilter.kNews:
                    return Local.txtPanel03;
                case UserCurrentFilter.kAccepted:
                    return Local.txtPanel04;
                case UserCurrentFilter.kRejected:
                    return Local.txtPanel05;
                case UserCurrentFilter.kDiscarted:
                    return Local.txtPanel06;
                default:
                    C2Assert.Error(false);
                    return null;
            }
        }

        //-----------------------------------------------------

        private ContentView i_Filter_bar_view(UserCurrentView user_current_view, UserCurrentFilter user_current_filter)
        {
            ContentView view;
            Label label;
            TapGestureRecognizer gesture;
            view = new ContentView();
            view.Style = AppStyle.Filter_background;
            view.Padding = new Thickness(AppStyle.Filter_padding_horizontal, AppStyle.Filter_padding_vertical, 2.0 * AppStyle.Filter_padding_horizontal, AppStyle.Filter_padding_vertical);
            this._filter_bar_layout = new StackLayout();
            this._filter_bar_layout.Orientation = StackOrientation.Horizontal;
            this._filter_bar_layout.Spacing = 0.0;
            this._filter_label = new Label { Text = i_Filter_text(user_current_filter) };
            this._filter_label.Style = AppStyle.Filter_label_strong;
            this._filter_label.HorizontalOptions = LayoutOptions.FillAndExpand;
            label = new Label { Text = Local.txtPanel01 + "  " };
            label.Style = AppStyle.Filter_label;
            label.HorizontalOptions = LayoutOptions.Start;
            this._filter_image_down = new Image { Source = "arrow_down.png" };
            this._filter_image_down.HorizontalOptions = LayoutOptions.End;
            this._filter_image_down.WidthRequest = AppStyle.Filter_image_size;
            this._filter_image_down.HeightRequest = AppStyle.Filter_image_size;
            this._filter_image_up = new Image { Source = "arrow_up.png" };
            this._filter_image_up.HorizontalOptions = LayoutOptions.End;
            this._filter_image_up.WidthRequest = AppStyle.Filter_image_size;
            this._filter_image_up.HeightRequest = AppStyle.Filter_image_size;
            gesture = new TapGestureRecognizer();
            gesture.Tapped += this.i_On_filter;
            view.GestureRecognizers.Add(gesture);
            this._filter_bar_layout.Children.Add(label);
            this._filter_bar_layout.Children.Add(this._filter_label);
            this._filter_bar_layout.Children.Add(this._filter_image_down);
            this._filter_bar_layout.Children.Add(this._filter_image_up);
            this.i_Set_filter_icon(user_current_view);
            view.Content = this._filter_bar_layout;
            return view;
        }

        //-----------------------------------------------------

        private StackLayout i_Offers_layout(UserCurrentFilter user_current_filter)
        {
            //uint num_unreaded_messages = 0;

            C2Assert.No_null(this.list.items);
            C2Assert.Error(this.list.items.Count > 0);

            if (this._offers_layout == null)
            {
                //ActivityIndicator indicator;
                this._offers_layout = new StackLayout();
                this._offers_layout.Spacing = AppStyle.Layout_stack_spacing;
                this._offers_layout.VerticalOptions = LayoutOptions.FillAndExpand;
                this._offers_view = new C2ListView();
                this._offers_view.ItemTemplate = new DataTemplate(typeof(ImageCell));
                this._offers_view.ItemsSource = this._offers_list;
                this._offers_view.VerticalOptions = LayoutOptions.FillAndExpand;
                this._offers_view.HasUnevenRows = true;
                this._offers_view.ItemTapped += this.i_On_offer_detail;


                //this._loading_more_view = new ContentView();
                //this._loading_more_view.Padding = new Thickness(10);
                //this._loading_more_view.BackgroundColor = C2Style.Control_background;
                //indicator = new ActivityIndicator { WidthRequest = 24, HeightRequest = 24 };
                //indicator.IsRunning = true;
                //this._loading_more_view.Content = indicator;
                //this._loading_more_view.IsVisible = false;

                this._offers_layout.Children.Add(this._offers_view);
                //this._offers_layout.Children.Add(_loading_more_view);
            }


            ////////while (this._offers_layout.Children.Count > 0)
            ////////    this._offers_layout.Children.RemoveAt(0);

            ////////for (int i = 0; i < this._offer_list.Count; ++i)
            ////////{
            ////////    ImageLayout offer_layout = null;
            ////////    OfferJSON offer = null;
            ////////    string date = null;
            ////////    offer = this._offer_list[i];
            ////////    num_unreaded_messages += offer.unreaded_messages;
            ////////    date = Util.Date_range(offer.date_start, offer.date_end);
            ////////    String noLeidos;
            ////////    if (offer.unreaded_messages > 0) { noLeidos = offer.unreaded_messages.ToString("G", CultureInfo.CurrentCulture); }
            ////////    else { noLeidos = ""; }

                
            ////////    ContentView view;
            ////////    offer_layout = new ImageLayout(AppStyle.Image_size, false,true);
             
            ////////    view = UserPage.Right_arrow_view(offer_layout, this.i_On_offer_detail, false, noLeidos);
            ////////    this._offers_layout.Children.Add(view);

            ////////    offer_layout.Set_data(offer.company_image, offer.name, offer.company_name, null, null, null, offer.job_offer_status_id.ToString());
            ////////    //offer_layout.set_Noleidos_text_estado(noLeidos);


            ////////}

            

            return this._offers_layout;
        }

        



        //-----------------------------------------------------

        private StackLayout i_Empty_layout()
        {
            if (this._empty_layout == null)
            {
                this._empty_layout = new StackLayout();
                this._empty_layout.HorizontalOptions = LayoutOptions.Center;
                this._empty_layout.Padding = new Thickness(0.0, 20.0, 0.0, 0.0);
                this._empty_layout.Spacing = 0.0;

                {
                    StackLayout layout = new StackLayout();
                    Image image;
                    layout.Spacing = 0.0;
                    layout.HorizontalOptions = LayoutOptions.Center;
                    image = new Image { Source = "no_offer.png" };
                    layout.Children.Add(image);
                    this._empty_layout.Children.Add(layout);
                }

                {
                    StackLayout layout = new StackLayout();
                    Label title;
                    Label label1;
                    Label label2;
                    Label label3;
                    Label label4;

                    layout.Spacing = 0.0;
                    layout.HorizontalOptions = LayoutOptions.Center;
                    layout.Padding = new Thickness(0.0, 20.0, 0.0, 0.0);
                    title = new Label { Text = Local.txtPanel07, Style = AppStyle.Label };
                    title.HorizontalOptions = LayoutOptions.Center;
                    title.FontAttributes = FontAttributes.Bold;
                    if (AppDelegate._is_personal_data_complete == false || AppDelegate._showperfilcompleto==true )
                    {
                        label1 = new Label { Text = Local.txtPanel14, Style = AppStyle.Label };
                        label1.HorizontalOptions = LayoutOptions.Center;
                        label2 = new Label { Text = Local.txtPanel15, Style = AppStyle.Label };
                        label2.HorizontalOptions = LayoutOptions.Center;
                        label3 = new Label { Text = Local.txtPanel16, Style = AppStyle.Label };
                        label3.HorizontalOptions = LayoutOptions.Center;
                        
                    }
                    else {
                        label1 = new Label { Text = Local.txtPanel08, Style = AppStyle.Label };
                        label1.HorizontalOptions = LayoutOptions.Center;
                        label2 = new Label { Text = Local.txtPanel09, Style = AppStyle.Label };
                        label2.HorizontalOptions = LayoutOptions.Center;
                        label3 = new Label { Text = Local.txtPanel10, Style = AppStyle.Label };
                        label3.HorizontalOptions = LayoutOptions.Center;

                    }

                    label4 = new Label { Text = Local.txtPanel17, Style = AppStyle.Label };
                    label4.HorizontalOptions = LayoutOptions.Center;



                    layout.Children.Add(title);
                    layout.Children.Add(label1);
                    layout.Children.Add(label2);
                    layout.Children.Add(label3);
                    //if (AppDelegate._is_personal_data_complete == false) es la logica correcta
                    if (AppDelegate._is_personal_data_complete == false)
                    {
                        layout.Children.Add(label4);
                    }
                    this._empty_layout.Children.Add(layout);
                }
            }

            return this._empty_layout;
        }

        //-----------------------------------------------------

        private ContentView i_Filter_button(string text)
        {
            ContentView view;
            Label label;
            TapGestureRecognizer gesture;
            view = new ContentView();
            view.Padding = new Thickness(0, C2Style.Entry_padding_vertical, C2Style.Entry_padding_horizontal, C2Style.Entry_padding_vertical);
            label = new Label { Text = text };
            label.FontSize = C2Style.Label_font_size;
            label.TextColor = C2Style.Control_text;
            label.HorizontalOptions = LayoutOptions.FillAndExpand;
            label.VerticalOptions = LayoutOptions.Center;
            label.TranslationX = 2.0 * C2Style.Entry_padding_horizontal;
            gesture = new TapGestureRecognizer();
            gesture.Tapped += i_OnFilterSelected;
            view.GestureRecognizers.Add(gesture);
            view.Content = label;
            return view;
        }

        //-----------------------------------------------------

        private StackLayout i_Filter_layout(UserCurrentFilter user_current_filter)
        {
            if (this._filters_layout == null)
            {
                this._filters_layout = new StackLayout();
                this._filters_layout.Spacing = AppStyle.Layout_stack_spacing;
                this._filters_layout.Children.Add(i_Filter_button(Local.txtPanel02));
                this._filters_layout.Children.Add(i_Filter_button(Local.txtPanel03));
                this._filters_layout.Children.Add(i_Filter_button(Local.txtPanel04));
                this._filters_layout.Children.Add(i_Filter_button(Local.txtPanel05));
                this._filters_layout.Children.Add(i_Filter_button(Local.txtPanel06));
            }

            for (int i = 0; i < 5; ++i)
            {
                ContentView view = (ContentView)this._filters_layout.Children[i];
                if (i == (int)user_current_filter)
                    view.BackgroundColor = AppStyle.Page_background;
                else
                    view.BackgroundColor = C2Style.Control_background;
            }

            return this._filters_layout;
        }

        //-----------------------------------------------------

        private StackLayout i_Client_layout(UserCurrentView user_current_view, UserCurrentFilter user_current_filter)
        {
            this.i_Set_filter_icon(user_current_view);
            this._filter_label.Text = i_Filter_text(user_current_filter);

            switch (user_current_view)
            {
                case UserCurrentView.kOffers:
                    if (this._waiting_for_offers == true)
                    {
                        if (this._waiting_layout == null)
                            this._waiting_layout = UserPage.Waiting_layout();
                        return this._waiting_layout;
                    }
                    else
                    {
                        if (this._offers_list != null)
                            if (this._offers_list.Count > 0)
                                return this.i_Offers_layout(user_current_filter);
                            else
                                return this.i_Empty_layout();
                        else
                        {
                            return this.i_Empty_layout();
                        }
                        
                    }

                case UserCurrentView.kFilters:
                    return this.i_Filter_layout(user_current_filter);
                default:
                    C2Assert.Error(false);
                    return null;
            }
        }

        //-----------------------------------------------------

        private void i_Change_client_layout(UserCurrentView user_current_view, UserCurrentFilter user_current_filter)
        {
            
            View view;
            View content;
            C2Assert.Error(this._content_layout.Children.Count == 2);
            view = i_Client_layout(user_current_view, user_current_filter);

            content = this._content_layout.Children[1];
            if (content != view) {
                try
                {
                    this._content_layout.Children[1].IsVisible = false;
                    this._content_layout.Children.RemoveAt(1);
                    GC.Collect();
                    this._content_layout.Children.Add(view);
                    this._content_layout.Children[1].IsVisible = true;
                }
                catch {
                }
                
                
            }

           
            //GC.WaitForPendingFinalizers();

        }



        //-----------------------------------------------------
        



        private async void i_Update_offers(UserCurrentView user_current_view, UserCurrentFilter user_current_filter, bool show_waiting)
        {
        
            
            C2Assert.Error(user_current_view == UserCurrentView.kOffers);
            this._waiting_for_offers = show_waiting;
            this._enable_offer_tap = false;
            this._delegate._blncargando = true;
            bool continuar;
            if (this._offers_view != null)
            {
                if (this._offers_list != null) {
                    i_delet_offers();
                }
                this._offers_list = new ObservableCollection<ImageItem>();
                this._offers_view.ItemsSource = this._offers_list;
            }
            this.i_Change_client_layout(user_current_view, user_current_filter);
            this.list = await this.i_Offers_list(user_current_filter);
            continuar = await this.i_Add_offers(this.list.items);
            
           

            this._waiting_for_offers = false;
            this.i_Change_client_layout(user_current_view, user_current_filter);
            
            

            this._delegate.On_unreaded_messages();

            
            ServerResponse<OfferReadedJSON> offers_readed_json = await this._delegate.On_offer_get_not_readed();
            if (offers_readed_json.type == ResponseType.OK_SUCCESS)
            {
                uint noleidas = (uint)offers_readed_json.result_object.offers_not_readed;
                this._delegate.On_new_offers_change(noleidas);
            }
            else
            {
                Alert.Show(offers_readed_json, this);
            }
            this._delegate._blncargando = false;
        }

        private async Task<OfferList> i_Offers_list(UserCurrentFilter user_current_filter)
        {
            OfferList list = new OfferList();
            list.items = new List<OfferJSON>();
        
            ServerResponse<List<OfferJSON>> offers_json;
            offers_json = await this._delegate.On_offer_list(user_current_filter);
            if (offers_json.type == ResponseType.OK_SUCCESS)
            {
                if (offers_json.result_object.Count > 0)
                {
                    for (int j = 0; j < offers_json.result_object.Count; ++j)
                    {
                        list.items.Add(offers_json.result_object[j]);
                    }
                }
            }
            else
            {
                Alert.Show(offers_json, this);
            }
            return list;
        }

        private async Task<bool> i_Add_offers(List<OfferJSON> items)
        {
            uint num_unreaded_messages = 0;
            string date = null;
            for (int i = 0; i < items.Count; ++i)
            {
                OfferJSON oferta = items[i];
                ImageItem item = new ImageItem();
                //item.Image =Server._URL_IMAGES_COMPANIES_API + company.image_source+"_logo.jpg";
                
                num_unreaded_messages += oferta.unreaded_messages;
                date = Util.Date_range(oferta.date_start, oferta.date_end);
                String noLeidos;
                if (oferta.unreaded_messages > 0) { noLeidos = oferta.unreaded_messages.ToString("G", CultureInfo.CurrentCulture); }
                else { noLeidos = ""; }
                //try { item.Image = oferta.company_image; } catch { }

                //switch (Device.OS)
                //{
                //    case TargetPlatform.Android:
                //        item.ImageUrl =await  DependencyService.Get<IImages>().ImageFromUrl(oferta.company_image);

                //        break;
                //    default:
                //        item.Image = oferta.company_image;
                //        break;
                //}
                if (String.IsNullOrEmpty(oferta.company_image) == true)
                    item.ImageUrl = null;
                else
                    item.ImageUrl = await DependencyService.Get<IImages>().ImageFromUrl(oferta.company_image);

                

                
                //item.Image = oferta.company_image;
                if (String.IsNullOrEmpty(oferta.name) == true)
                    item.Title = "";
                else
                    item.Title = oferta.name;

                item.Detail1 = oferta.company_name;
                item.Detail2Visible = false;
                item.Detail3Visible = false;
                item.LinkLabelVisible = false;
                //item.EstadoOferta = oferta.job_offer_status_id.ToString();
                
                i_Update_text_estado(ref item, oferta.job_offer_status_id.ToString());
                item.MsgOferta = noLeidos;



                item._index = this._offers_list.Count;
                item._id = oferta.job_offer_id;
                this._offers_list.Add(item);

            }
            return true;
        }

        private void i_delet_offers()
        {
            if (this._offers_list == null) return;
            for (int i = 0; i < this._offers_list.Count-1; ++i)
            {
                this._offers_list[i].Image = null;
            }
            this._offers_list.Clear();
            this._offers_list = null;
            GC.Collect();
        }

        private void i_Update_text_estado(ref ImageItem item, string text)
        {
            if (string.IsNullOrEmpty(text) == true)
            {
                //estado.IsVisible = false;
            }
            else
            {

                switch (text.ToUpper())
                {
                    case "NUEVA INVITACIÓN":
                    case "NUEVA OFERTA":
                    case "1":
                        item.EstadoOfertaColor = AppStyle.Nueva_Oferta;
                        item.EstadoOfertaBorderColor = AppStyle.Nueva_Oferta;
                        item.EstadoOferta = "Nueva oferta";

                        break;
                    case "ACEPTADA":
                    case "3":
                        item.EstadoOfertaColor = AppStyle.Aceptada_Oferta;
                        item.EstadoOfertaBorderColor = AppStyle.Aceptada_Oferta;
                        item.EstadoOferta = "Aceptada";

                        break;
                    case "RECHAZADA":
                    case "2":
                        item.EstadoOfertaColor = AppStyle.Rechazada_Oferta;
                        item.EstadoOfertaBorderColor = AppStyle.Rechazada_Oferta;
                        item.EstadoOferta = "Rechazada";


                        break;
                    case "DESCARTADO":
                    case "DESCARTADA":
                    case "8":
                        item.EstadoOfertaColor = AppStyle.Descartada_Oferta;
                        item.EstadoOfertaBorderColor = AppStyle.Descartada_Oferta;
                        item.EstadoOferta = "Descartada";
                        break;
                    default:

                        item.EstadoOfertaColor = AppStyle.Aceptada_Oferta;
                        item.EstadoOfertaBorderColor = AppStyle.Aceptada_Oferta;
                        item.EstadoOferta = "Aceptada";

                        break;
                }


                //estado.IsVisible = true;
            }
        }
        //-----------------------------------------------------

        public PanelPage(AppDelegate _delegate, UserCurrentView user_current_view, UserCurrentFilter user_current_filter) : base()
        {
            
            this.Style = AppStyle.Page;
            this._delegate = _delegate;
            this._delegate.GA_track_screen(Local.txtGA17);
            this.Title = Local.txtPanel00;
            Device.OnPlatform(Android: () => NavigationPage.SetTitleIcon(this, "no_image.png"));
            this._filter_bar_layout = null;
            this._filter_label = null;
            this._filter_image_down = null;
            this._filter_image_up = null;
            this._waiting_layout = null;
            this._empty_layout = null;
            this._offers_layout = null;
            this._filters_layout = null;
            this._waiting_for_offers = true;
            this._offer_page = null;
            this._offers_list = new ObservableCollection<ImageItem>();

            this._content_layout = new StackLayout();
            this._content_layout.Spacing = 0.0;
            this._content_layout.Children.Add(i_Filter_bar_view(user_current_view, user_current_filter));
            this._content_layout.Children.Add(i_Client_layout(user_current_view, user_current_filter));
            this._offers_view = null;
            //this._loading_more_view = null;
            this._is_first_appearing = true;
            this._enable_offer_tap = false;
            this.Content = this._content_layout;
        }

        //-----------------------------------------------------
        protected override void OnDisappearing()
        {
            i_delet_offers();
            this._is_first_appearing = true;
            base.OnDisappearing();
            GC.Collect();
            
        }

        protected override void OnAppearing()
        {
            Title = Local.txtPanel00;
            
            this._delegate.GA_track_screen(Local.txtGA17);
            if (this._is_first_appearing == true || AppDelegate._refreshcont==true) {
                this.i_Update_offers(this._delegate.User_current_view, this._delegate.User_current_filter, this._is_first_appearing);
                if (AppDelegate._device_token != null)
                {
                    Device.OnPlatform(iOS: () => AppDelegate.notificacionOfertasMensajes());
                    AppDelegate._refreshcont = false;
                }
                this._is_first_appearing = false;
            }

            this._enable_offer_tap = true;

        }

        //-----------------------------------------------------

        private void i_On_filter(object sender, EventArgs e)
        {
            if (this._delegate.Control_Carga(this)) return;
            this._delegate.On_filter_button();
            this.i_Change_client_layout(this._delegate.User_current_view, this._delegate.User_current_filter);

        }



        
        //-----------------------------------------------------

        private void i_OnFilterSelected(object sender, EventArgs e)
        {
            if (this._delegate.Control_Carga(this)) return;
            int user_current_filter = 0;
            
            for (int i = 0; i < 5; ++i)
            {
                if (this._filters_layout.Children[i] == sender)
                {
                    user_current_filter = i;
                    break;
                }
            }

            this.i_Update_offers(UserCurrentView.kOffers, (UserCurrentFilter)user_current_filter, true);
            this._enable_offer_tap = true;
        }

        //-----------------------------------------------------

        private int i_Sender_index(object sender)
        {
            for (int i = 0; i < this._offers_layout.Children.Count; ++i)
            {
                if (this._offers_layout.Children[i] == sender)
                    return i;
            }

            C2Assert.Error(false);
            return 0;
        }

        //-----------------------------------------------------

        private async void i_On_offer_detail(object sender, ItemTappedEventArgs e)
        {
            
            //this._offers_view.IsEnabled = false;
            

            if (this._enable_offer_tap == true)
            {
                ImageItem item = (ImageItem)e.Item;
                int j;
                this._enable_offer_tap = false;
                this._offer_page = new OfferPage(this._delegate);

                ((ListView)sender).SelectedItem = null;
                await this.Navigation.PushAsync(this._offer_page);
                for (j = 0; j < this.list.items.Count; ++j)
                {
                    if (this.list.items[j].job_offer_id == item._id)
                    {
                        break;
                    }
                }

                C2Assert.Error(j <= this.list.items.Count);
                this._offer_page.Set_data(this.list.items[j]);
                this.list.items.Clear();
                this._is_first_appearing = true;
            }
      }


        

        //-----------------------------------------------------

    }
}
