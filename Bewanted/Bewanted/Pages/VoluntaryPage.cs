﻿using System;
using Xamarin.Forms;
using C2Forms;

namespace Bewanted
{
    class VoluntaryPage : ContentPage
    {
        private AppDelegate _delegate;
        private int _voluntary_id;
        private StackLayout _layout;
        private StackLayout _waiting_layout;
        private C2TextEntry _ong;
        private IntervalLayout _interval_layout;
        private CountryState _country_state;
        private C2Button _delete_button;

        //-----------------------------------------------------

        public VoluntaryPage(AppDelegate _delegate) : base()
        {
            this.Title = Local.txtVoluntary00;
            Device.OnPlatform(Android: () => NavigationPage.SetTitleIcon(this, "no_image.png"));
            this.Style = AppStyle.Page;
            this._delegate = _delegate;
            this._voluntary_id = int.MinValue;
            this._layout = new StackLayout();
            this._layout.VerticalOptions = LayoutOptions.Start;
            this._layout.Spacing = AppStyle.Layout_stack_long_spacing;
            this._layout.Padding = new Thickness(0.0, AppStyle.Layout_padding_vertical);

            {
                this._ong = new C2TextEntry(Local.txtVoluntary01, 1, 100,null);
                this._layout.Children.Add(this._ong);
            }

            {
                this._interval_layout = new IntervalLayout(Local.txtVoluntary02);
                this._layout.Children.Add(this._interval_layout);
            }

            {
                StackLayout location_layout;
                location_layout = new StackLayout();
                location_layout.Spacing = AppStyle.Layout_stack_spacing;
                this._country_state = new CountryState(_delegate, this, Local.txtVoluntary03, Local.txtVoluntary04, true, null, null);
                location_layout.Children.Add(this._country_state._country);
                location_layout.Children.Add(this._country_state._state);
                this._layout.Children.Add(location_layout);
            }

            {
                ButtonsLayout buttons_layout;
                buttons_layout = new ButtonsLayout(this, out this._delete_button, this.i_On_save, this.i_On_delete);
                this._layout.Children.Add(buttons_layout);
            }

            this._waiting_layout = UserPage.Waiting_layout();

            Content = new ScrollView
            {
                Content = this._waiting_layout
            };
        }

        //-----------------------------------------------------

        private void i_Set_waiting_layout()
        {
            ScrollView scroll = (ScrollView)this.Content;
            scroll.Content = this._waiting_layout;
        }

        //-----------------------------------------------------

        private void i_Set_layout()
        {
            ScrollView scroll = (ScrollView)this.Content;
            scroll.Content = this._layout;
        }

        //-----------------------------------------------------

        private void i_OnAppearing()
        {
            this.i_Set_waiting_layout();
            this._country_state.Load_countries();
        }

        //-----------------------------------------------------

        public  void Set_data(VoluntaryDataJSON voluntary)
        {
            this.i_OnAppearing();
            this._delegate.GA_track_screen(Local.txtGA22);

            if (voluntary != null)
            {
                this._voluntary_id = voluntary.id;
                this._ong.Text = voluntary.ong;
                this._interval_layout.Set_data(voluntary.month_start, voluntary.year_start, voluntary.month_end, voluntary.year_end);
                this._country_state._country.CurrentId = voluntary.country_id;
                this._country_state._state.Set_text_and_id(voluntary.state.label, voluntary.state.id);
                this._delete_button.IsVisible = true;
                this.i_Set_layout();
                //ServerResponse<VoluntaryDataJSON> response;
                //response = await this._delegate.On_voluntary_data(voluntary.id);
                //if (response.type == ResponseType.OK_SUCCESS)
                //{
                //    this._voluntary_id = voluntary.id;
                //    this._ong.Text = response.result_object.ong;
                //    this._interval_layout.Set_data(response.result_object.month_start, response.result_object.year_start, response.result_object.month_end, response.result_object.year_end);
                //    this._country_state._country.CurrentId = response.result_object.country_id;
                //    this._country_state._state.Set_text_and_id(response.result_object.state.label, response.result_object.state.id);
                //    this._delete_button.IsVisible = true;
                //    this.i_Set_layout();
                //}
                //else
                //{
                //    Alert.Show(response, this);
                //    await this.Navigation.PopAsync();
                //}
            }
            else
            {
                this._voluntary_id = int.MinValue;
                this._ong.Clear();
                this._interval_layout.Clear();
                this._country_state.Clear();
                this._delete_button.IsVisible = false;
                this.i_Set_layout();
            }
        }

        //-----------------------------------------------------

        private async void i_On_save(object sender, EventArgs e)
        {
            bool ok = true;
            ((Button)sender).IsEnabled = false;
            ok = ok & this._ong.Validate();
            ok = ok & this._interval_layout.Validate();
            ok = ok & this._country_state.Validate();

            if (ok == true)
            {
                VoluntaryDataJSON voluntary = new VoluntaryDataJSON();
                voluntary.state = new StateJSON();
                voluntary.id = this._voluntary_id;
                voluntary.ong = this._ong.Text;
                this._interval_layout.Get_data(out voluntary.month_start, out voluntary.year_start, out voluntary.month_end, out voluntary.year_end);
                voluntary.country_id = this._country_state._country.CurrentId;
                voluntary.state.id = this._country_state._state.CurrentId;

                ServerResponse<SaveDataJSON> response;
                response = await this._delegate.On_voluntary_data_save(voluntary);
                if (response.type == ResponseType.OK_SUCCESS)
                {
                    if (response.result_object.result == true || response.result_object.id !=int.MinValue)
                    {
                        await this.Navigation.PopAsync();
                    }
                    else
                    {
                        await this.DisplayAlert("Developer Error", null, "Ok");
                    }
                }
                else
                {
                    Alert.Show(response, this);
                }
            }

            ((Button)sender).IsEnabled = true;
        }

        //-----------------------------------------------------

        private async void i_On_delete(object sender, EventArgs e)
        {
            if (await this.DisplayAlert(null, Local.txtVoluntary06, Local.txtButtons05, Local.txtButtons06) == true)
            {
                ServerResponse<SaveDataJSON> response;
                response = await this._delegate.On_voluntary_data_delete(this._voluntary_id);
                if (response.type == ResponseType.OK_SUCCESS)
                {
                    if (response.result_object.result == true || response.result_object.details == "Deleted resource")
                    {
                        await this.Navigation.PopAsync();
                    }
                    else
                    {
                        await this.DisplayAlert("Developer Error", null, "Ok");
                    }
                }
                else
                {
                    Alert.Show(response, this);
                }
            }
        }

        //-----------------------------------------------------


    }
}
