﻿using System;
using System.Threading.Tasks;
using Xamarin.Forms;
using C2Forms;
using System.Collections.Generic;

namespace Bewanted
{
    class InternshipPage : ContentPage
    {
        private AppDelegate _delegate;
        private int _internship_id;
        private StackLayout _layout;
        private StackLayout _waiting_layout;
        private C2TextEntry _internship;
        private C2Picker _country;
        private C2YearPicker _year;
        private C2Button _delete_button;

        //-----------------------------------------------------

        public InternshipPage(AppDelegate _delegate) : base()
        {
            Device.OnPlatform(Android: () => NavigationPage.SetTitleIcon(this, "no_image.png"));
            this.Title = Local.txtInternship00;
            this.Style = AppStyle.Page;
            this._delegate = _delegate;
            this._internship_id = int.MinValue;
            this._layout = new StackLayout();
            this._layout.VerticalOptions = LayoutOptions.Start;
            this._layout.Spacing = AppStyle.Layout_stack_long_spacing;
            this._layout.Padding = new Thickness(0.0, AppStyle.Layout_padding_vertical);
            this._internship = new C2TextEntry(Local.txtInternship01, 1, 100,null);
            this._country = new C2Picker(Local.txtInternship03, null);
            this._year = new C2YearPicker(Local.txtInternship02, null, 1970, DateTime.Now.Year);
            this._layout.Children.Add(this._internship);
            this._layout.Children.Add(this._country);
            this._layout.Children.Add(this._year);

            {
                ButtonsLayout buttons_layout;
                buttons_layout = new ButtonsLayout(this, out this._delete_button, this.i_On_save, this.i_On_delete);
                this._layout.Children.Add(buttons_layout);
            }

            this._waiting_layout = UserPage.Waiting_layout();

            Content = new ScrollView
            {
                Content = this._waiting_layout
            };
        }

        //-----------------------------------------------------

        private void i_Set_waiting_layout()
        {
            ScrollView scroll = (ScrollView)this.Content;
            scroll.Content = this._waiting_layout;
        }

        //-----------------------------------------------------

        private void i_Set_layout()
        {
            ScrollView scroll = (ScrollView)this.Content;
            scroll.Content = this._layout;
        }

        //-----------------------------------------------------

        private async void i_OnAppearing()
        {
            this.i_Set_waiting_layout();

            if (this._country.Count == 0)
            {
                this._country.Set_waiting_state();
                ServerResponse<List<CountryJSON>> response = await this._delegate.On_countries();
                if (response.type == ResponseType.OK_SUCCESS)
                {
                    for (int i = 0; i < response.result_object.Count; ++i)
                        this._country.Add(response.result_object[i].name, response.result_object[i].id);
                }
            }

            this._country.Unset_waiting_state();
        }

        //-----------------------------------------------------

        public void Set_data(InternshipDataJSON internship)
        {
            this._delegate.GA_track_screen(Local.txtGA07);
            this.i_OnAppearing();

            if (internship != null)
            {
                this._internship_id = internship.id;
                this._internship.Text = internship.description;
                this._country.CurrentId = internship.country_id;
                this._year.CurrentId = internship.year;
                this._delete_button.IsVisible = true;
                this.i_Set_layout();
                //ServerResponse<InternshipDataJSON> response;
                //response = await this._delegate.On_internship_data(internship.id);
                //if (response.type == ResponseType.OK_SUCCESS)
                //{
                //    this._internship_id = internship.id;
                //    this._internship.Text = response.result_object.description;
                //    this._country.CurrentId = response.result_object.country_id;
                //    this._year.CurrentId = response.result_object.year;
                //    this._delete_button.IsVisible = true;
                //    this.i_Set_layout();
                //}
                //else
                //{
                //    Alert.Show(response, this);
                //    await this.Navigation.PopAsync();
                //}
            }
            else
            {
                this._internship_id = int.MinValue;
                this._internship.Clear();
                this._country.Clear(false);
                this._year.Clear(false);
                this._delete_button.IsVisible = false;
                this.i_Set_layout();
            }      
        }

        //-----------------------------------------------------

        private async void i_On_save(object sender, EventArgs e)
        {
            bool ok = true;

            ((Button)sender).IsEnabled = false;
            ok = ok & this._internship.Validate();
            ok = ok & this._country.Validate();
            ok = ok & this._year.Validate();

            if (ok == true)
            {
                InternshipDataJSON internship = new InternshipDataJSON();
                internship.id = this._internship_id;
                internship.country_id = this._country.CurrentId;
                internship.description = this._internship.Text;
                internship.year = this._year.CurrentId;

                ServerResponse<SaveDataJSON> response;
                response = await this._delegate.On_internship_data_save(internship);
                if (response.type == ResponseType.OK_SUCCESS)
                {
                    if (response.result_object.result == true || response.result_object.id!=int.MinValue)
                    {
                        await this.Navigation.PopAsync();
                    }
                    else
                    {
                        await this.DisplayAlert("Developer Error", null, "Ok");
                    }
                }
                else
                {
                    Alert.Show(response, this);
                }
            }

            ((Button)sender).IsEnabled = true;
        }

        //-----------------------------------------------------

        private async void i_On_delete(object sender, EventArgs e)
        {
            if (await this.DisplayAlert(null, Local.txtInternship11, Local.txtButtons05, Local.txtButtons06) == true)
            {
                ServerResponse<SaveDataJSON> response;
                response = await this._delegate.On_internship_data_delete(this._internship_id);
                if (response.type == ResponseType.OK_SUCCESS)
                {
                    if (response.result_object.result == true || response.result_object.details == "Deleted resource")
                    {
                        await this.Navigation.PopAsync();
                    }
                    else
                    {
                        await this.DisplayAlert("Developer Error", null, "Ok");
                    }
                }
                else
                {
                    Alert.Show(response, this);
                }
            }
        }

        //-----------------------------------------------------


    }
}
