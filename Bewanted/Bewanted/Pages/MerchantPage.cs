﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Xamarin.Forms;
using C2Core;
using C2Forms;

namespace Bewanted
{
    public class PositionRow : C2TableRow
    {
        private C2AssistedEntry _position;

        //-----------------------------------------------------

        public PositionRow(C2AssistedEntry position)
        {
            this._position = position;
        }

        //-----------------------------------------------------

        public override View Cell(int index)
        {
            C2Assert.Error(index == 0);
            if (index == 0)
                return this._position;
            else
                return null;
        }

        //-----------------------------------------------------

        public override bool Validate()
        {
            return this._position.Validate();
        }

        //-----------------------------------------------------

        public override Object Data()
        {
            MerchantPositionJSON position = new MerchantPositionJSON();
            position.pivot = new MerchantPositionPivotJSON();
            position.pivot.id = this._position.CurrentId;
            position.name = this._position.Text;
            return position;
        }
    }

    //-----------------------------------------------------

    public class SectorRow : C2TableRow
    {
        private C2AssistedEntry _sector;

        //-----------------------------------------------------

        public SectorRow(C2AssistedEntry sector)
        {
            this._sector = sector;
        }

        //-----------------------------------------------------

        public override View Cell(int index)
        {
            C2Assert.Error(index == 0);
            if (index == 0)
                return this._sector;
            else
                return null;
        }

        //-----------------------------------------------------

        public override bool Validate()
        {
            return this._sector.Validate();
        }

        //-----------------------------------------------------

        public override Object Data()
        {
            MerchantSectorJSON sector = new MerchantSectorJSON();
            sector.pivot.id = this._sector.CurrentId;
            sector.name = this._sector.Text;
            return sector;
        }
    }

    //-----------------------------------------------------

    public class LocationRow : C2TableRow
    {
        private CountryState _country_state;

        //-----------------------------------------------------

        public LocationRow(CountryState country_state)
        {
            this._country_state = country_state;
        }

        //-----------------------------------------------------

        public override View Cell(int index)
        {
            C2Assert.Error(index >= 0 && index <= 1);
            if (index == 0)
                return this._country_state._country;
            else if (index == 1)
                return this._country_state._state;
            else
                return null;
        }

        //-----------------------------------------------------

        public override bool Validate()
        {
            return this._country_state.Validate();
        }

        //-----------------------------------------------------

        public override Object Data()
        {
            MerchantJobLocationJSON location = new MerchantJobLocationJSON();
            location.country_id = this._country_state._country.CurrentId;
            location.state_id = this._country_state._state.CurrentId;
            location.state = this._country_state._state.Text;
            return location;
        }
    }

    //-----------------------------------------------------

    class MerchantPage : ContentPage
    {
        private AppDelegate _delegate;
        private StackLayout _layout;
        private StackLayout _waiting_layout;
        private C2Switch _first_job;
        private C2Switch _scholarship;
        private C2Switch _ong;
        private C2BirthDate _availability;
        private C2Switch _morning;
        private C2Switch _evening;
        private C2Switch _fulltime;
        private C2Switch _any_position;
        private C2Table _position_table;
        private C2Switch _any_sector;
        private C2Table _sector_table;
        private C2Switch _mycountry;
        private C2Switch _everywhere;
        private C2Table _location_table;
        private C2Switch _nonpaid;

        //-----------------------------------------------------

        public MerchantPage(AppDelegate _delegate) : base()
        {
            Device.OnPlatform(Android: () => NavigationPage.SetTitleIcon(this, "no_image.png"));
            this.Title = Local.txtMerchant00;
            this.Style = AppStyle.Page;
            this._delegate = _delegate;
            this._layout = new StackLayout();
            this._layout.VerticalOptions = LayoutOptions.Start;
            this._layout.Spacing = AppStyle.Layout_stack_long_spacing;

            {
                StackLayout search_layout;
                search_layout = new StackLayout();
                search_layout.Spacing = AppStyle.Layout_stack_spacing;
                this._first_job = new C2Switch(Local.txtMerchant02, null);
                this._scholarship = new C2Switch(Local.txtMerchant03, null);
                this._ong = new C2Switch(Local.txtMerchant19, null);                
                search_layout.Children.Add(new SectionHeaderLayout(Local.txtMerchant01));
                search_layout.Children.Add(this._first_job);
                search_layout.Children.Add(this._scholarship);
                search_layout.Children.Add(this._ong);
                this._layout.Children.Add(search_layout);
            }

            {
                StackLayout time_layout;
                time_layout = new StackLayout();
                time_layout.Spacing = AppStyle.Layout_stack_spacing;
                this._availability = new C2BirthDate(Local.txtMerchant20,true);
                this._morning = new C2Switch(Local.txtMerchant11, null);
                this._evening = new C2Switch(Local.txtMerchant12, null);
                this._fulltime = new C2Switch(Local.txtMerchant13, null);
                time_layout.Children.Add(new SectionHeaderLayout(Local.txtMerchant10));
                time_layout.Children.Add(this._availability);
                time_layout.Children.Add(this._morning);
                time_layout.Children.Add(this._evening);
                time_layout.Children.Add(this._fulltime);
                this._layout.Children.Add(time_layout);
            }

            {
                StackLayout position_layout;
                List<C2TableColumn> columns;
                position_layout = new StackLayout();
                position_layout.Spacing = AppStyle.Layout_stack_spacing;
                this._any_position = new C2Switch(Local.txtMerchant05, this.i_On_any_position_toggled);
                columns = new List<C2TableColumn>();
                columns.Add(new C2TableColumn(10.0));
                this._position_table = new C2Table(columns, "add2.png", "delete.png", this.i_New_position);
                position_layout.Children.Add(new SectionHeaderLayout(Local.txtMerchant04));
                position_layout.Children.Add(this._any_position);
                position_layout.Children.Add(this._position_table);
                this._layout.Children.Add(position_layout);
            }

            {
                StackLayout sector_layout;
                List<C2TableColumn> columns;
                sector_layout = new StackLayout();
                sector_layout.Spacing = AppStyle.Layout_stack_spacing;
                this._any_sector = new C2Switch(Local.txtMerchant08, this.i_On_any_sector_toggled);
                columns = new List<C2TableColumn>();
                columns.Add(new C2TableColumn(10.0));
                this._sector_table = new C2Table(columns, "add2.png", "delete.png", this.i_New_sector);
                sector_layout.Children.Add(new SectionHeaderLayout(Local.txtMerchant07));
                sector_layout.Children.Add(this._any_sector);
                sector_layout.Children.Add(this._sector_table);
                this._layout.Children.Add(sector_layout);
            }

            {
                StackLayout where_layout;
                List<C2TableColumn> columns;
                where_layout = new StackLayout();
                where_layout.Spacing = AppStyle.Layout_stack_spacing;
                this._everywhere = new C2Switch(Local.txtMerchant15, this.i_On_any_location_toggled);
                this._mycountry = new C2Switch(Local.txtMerchant16, this.i_On_any_location_toggled);
                columns = new List<C2TableColumn>();
                columns.Add(new C2TableColumn(5.0));
                columns.Add(new C2TableColumn(5.0));
                this._location_table = new C2Table(columns, "add2.png", "delete.png", this.i_New_location);
                where_layout.Children.Add(new SectionHeaderLayout(Local.txtMerchant14));
                where_layout.Children.Add(this._everywhere);
                where_layout.Children.Add(this._mycountry);
                where_layout.Children.Add(this._location_table);
                this._layout.Children.Add(where_layout);
            }

            {
                StackLayout nonpaid_layout;
                nonpaid_layout = new StackLayout();
                nonpaid_layout.Spacing = AppStyle.Layout_stack_spacing;
                this._nonpaid = new C2Switch(Local.txtMerchant28, null);
                nonpaid_layout.Children.Add(new SectionHeaderLayout(Local.txtMerchant27));
                nonpaid_layout.Children.Add(this._nonpaid);
                this._layout.Children.Add(nonpaid_layout);
            }

            {
                ButtonsLayout buttons_layout;
                buttons_layout = new ButtonsLayout(this, this.i_On_save);
                this._layout.Children.Add(buttons_layout);
            }

            this._waiting_layout = UserPage.Waiting_layout();

            Content = new ScrollView
            {
                Content = this._waiting_layout
            };
        }

        //-----------------------------------------------------

        private PositionRow i_Position_row(MerchantPositionJSON positionJSON)            
        {
            C2AssistedEntry position;
            position = new C2AssistedEntry(Local.txtMerchant06, true, false, this.i_On_position_assist_required, null);

            if (positionJSON != null)
                position.Set_text_and_id(positionJSON.name, positionJSON.pivot.id);
            
            return new PositionRow(position);
        }

        //-----------------------------------------------------

        private PositionRow i_New_position()
        {
            return i_Position_row(null);
        }

        //-----------------------------------------------------

        private SectorRow i_Sector_row(MerchantSectorJSON sectorJSON)
        {
            C2AssistedEntry sector;
            sector = new C2AssistedEntry(Local.txtMerchant09, true, false, this.i_On_sector_assist_required, null);

            if (sectorJSON != null)
                sector.Set_text_and_id(sectorJSON.name, sectorJSON.pivot.id);

            return new SectorRow(sector);
        }

        //-----------------------------------------------------

        private SectorRow i_New_sector()
        {
            return i_Sector_row(null);
        }

        //-----------------------------------------------------

        private LocationRow i_Location_row(MerchantJobLocationJSON locationJSON)
        {
            CountryState country_state;
            country_state = new CountryState(this._delegate, this, Local.txtMerchant17, Local.txtMerchant26, true, null, null);
            country_state.Load_countries();

            if (locationJSON != null)
            {
                country_state._country.CurrentId = locationJSON.country_id;
                country_state._state.Set_text_and_id(locationJSON.state, locationJSON.state_id);
            }

            return new LocationRow(country_state);
        }

        //-----------------------------------------------------

        private LocationRow i_New_location()
        {
            return i_Location_row(null);
        }

        //-----------------------------------------------------

        private void i_Set_waiting_layout()
        {
            ScrollView scroll = (ScrollView)this.Content;
            scroll.Content = this._waiting_layout;
        }

        //-----------------------------------------------------

        private void i_Set_layout()
        {
            ScrollView scroll = (ScrollView)this.Content;
            scroll.Content = this._layout;
        }

        //-----------------------------------------------------

        private void i_OnAppearing()
        {
            this.i_Set_waiting_layout();
        }

        //-----------------------------------------------------

        public async void Set_data()
        {
            this.i_OnAppearing();
            this._delegate.GA_track_screen(Local.txtGA12);

            ServerResponse<MerchantDataJSON> response;
            response = await this._delegate.On_student_merchant();
            if (response.type == ResponseType.OK_SUCCESS)
            {
                this._first_job.IsToggled = Server.i_valorbool(response.result_object.student_data.first_job);
                this._scholarship.IsToggled = Server.i_valorbool(response.result_object.student_data.scholarship);
                this._ong.IsToggled = Server.i_valorbool(response.result_object.student_data.ong);

                if (response.result_object.student_data.availability != null)
                    this._availability.yyyy_mm_dd = response.result_object.student_data.availability;
                else
                    this._availability.Date = DateTime.Now;

                this._morning.IsToggled = Server.i_valorbool(response.result_object.student_data.morning);
                this._evening.IsToggled = Server.i_valorbool(response.result_object.student_data.afternoon);
                this._fulltime.IsToggled = Server.i_valorbool(response.result_object.student_data.all_day);

                this._any_position.IsToggled = Server.i_valorbool(response.result_object.student_data.any_position);
                this._position_table.Clear();
                if (response.result_object.positions!=null) {
                    for (int i = 0; i < response.result_object.positions.Count; ++i)
                    {
                        MerchantPositionJSON positionJSON = response.result_object.positions[i];
                        PositionRow position = this.i_Position_row(positionJSON);
                        this._position_table.Add_row(position);
                    }
                }
                
                this._position_table.IsVisible = !this._any_position.IsToggled;

                this._any_sector.IsToggled = Server.i_valorbool(response.result_object.student_data.any_sector);
                this._sector_table.Clear();
                if (response.result_object.sectors != null) {
                    for (int i = 0; i < response.result_object.sectors.Count; ++i)
                    {
                        MerchantSectorJSON sectorJSON = response.result_object.sectors[i];
                        SectorRow sector = this.i_Sector_row(sectorJSON);
                        this._sector_table.Add_row(sector);
                    }
                }
                this._sector_table.IsVisible = !this._any_sector.IsToggled;

                this._mycountry.IsToggled = Server.i_valorbool(response.result_object.student_data.any_state);
                this._everywhere.IsToggled = Server.i_valorbool(response.result_object.student_data.any_country);
                this._location_table.Clear();
                if (response.result_object.locations != null) {
                    for (int i = 0; i < response.result_object.locations.Count; ++i)
                    {
                        MerchantJobLocationJSON locationJSON = response.result_object.locations[i];
                        C2TableRow location = this.i_Location_row(locationJSON);
                        this._location_table.Add_row(location);
                    }
                }
                
                this._location_table.IsVisible = !this._mycountry.IsToggled || !this._everywhere.IsToggled;

                this._nonpaid.IsToggled = Server.i_valorbool(response.result_object.student_data.updaid);
                this.i_Set_layout();
            }
            else
            {
                Alert.Show(response, this);
                await this.Navigation.PopAsync();
            }
        }

        //-----------------------------------------------------

        private void i_On_any_position_toggled(bool on_off)
        {
            this._position_table.IsVisible = !on_off;
        }

        //-----------------------------------------------------

        private void i_On_any_sector_toggled(bool on_off)
        {
            this._sector_table.IsVisible = !on_off;
        }

        //-----------------------------------------------------

        private void i_On_any_location_toggled(bool on_off)
        {
            this._location_table.IsVisible = !this._mycountry.IsToggled || !this._everywhere.IsToggled;
        }
        
        //-----------------------------------------------------

        private async Task<bool> i_On_position_assist_required(C2AssistedEntry sender, string search_text)
        {
            ServerResponse<List<PositionJSON>> response;
            response = await this._delegate.On_positions(search_text);

            if (response.type == ResponseType.OK_SUCCESS)
            {
                for (int i = 0; i < response.result_object.Count; ++i)
                    sender.Add_assisted_item(response.result_object[i].name, response.result_object[i].id);
                return true;
            }
            else
            {
                Alert.Show(response, this);
                return false;
            }
        }

        //-----------------------------------------------------

        private async Task<bool> i_On_sector_assist_required(C2AssistedEntry sender, string search_text)
        {
            ServerResponse<List<SectorJSON>> response;
            response = await this._delegate.On_sectors(search_text);

            if (response.type == ResponseType.OK_SUCCESS)
            {
                for (int i = 0; i < response.result_object.Count; ++i)
                    sender.Add_assisted_item(response.result_object[i].name, response.result_object[i].id);
                return true;
            }
            else
            {
                Alert.Show(response, this);
                return false;
            }
        }

        //-----------------------------------------------------

        private async void i_On_save(object sender, EventArgs e)
        {
            bool ok = true;
            ((Button)sender).IsEnabled = false;
            ok = ok & this._availability.Validate();
            ok = ok & this._position_table.Validate();
            ok = ok & this._sector_table.Validate();
            ok = ok & this._location_table.Validate();

            if (ok == true)
            {
                MerchantDataJSON merchant = new MerchantDataJSON();
                merchant.student_data = new StudentDataJSON();
                merchant.student_data.first_job =Server.i_bool(this._first_job.IsToggled);
                merchant.student_data.scholarship = Server.i_bool(this._scholarship.IsToggled);
                merchant.student_data.ong = Server.i_bool(this._ong.IsToggled);
                merchant.student_data.updaid = Server.i_bool(this._nonpaid.IsToggled);
                merchant.student_data.any_country = Server.i_bool(this._everywhere.IsToggled);
                merchant.student_data.any_state = Server.i_bool(this._mycountry.IsToggled);
                merchant.student_data.availability = this._availability.yyyy_mm_dd;
                merchant.student_data.morning = Server.i_bool(this._morning.IsToggled);
                merchant.student_data.afternoon = Server.i_bool(this._evening.IsToggled);
                merchant.student_data.all_day = Server.i_bool(this._fulltime.IsToggled);
                merchant.student_data.any_position = Server.i_bool(this._any_position.IsToggled);
                merchant.student_data.any_sector = Server.i_bool(this._any_sector.IsToggled);
                merchant.locations = this._location_table.CurrentData<MerchantJobLocationJSON>();
                merchant.sectors = this._sector_table.CurrentData<MerchantSectorJSON>();
                merchant.positions = this._position_table.CurrentData<MerchantPositionJSON>();

                ServerResponse<SaveDataJSON> response;
                response = await this._delegate.On_merchant_data_save(merchant);
                if (response.type == ResponseType.OK_SUCCESS)
                {
                    if (response.result_object.result == true || response.result_object.student_id != int.MinValue)
                    {
                        await this.Navigation.PopAsync();
                        //this._delegate.On_merchant_data_change();
                    }
                    else
                    {
                        await this.DisplayAlert("Developer Error", null, "Ok");
                    }
                }
                else
                {
                    Alert.Show(response, this);
                }
            }

            ((Button)sender).IsEnabled = true;
        }

        //-----------------------------------------------------


    }
}
