﻿using System;
using System.Collections.Generic;
using Xamarin.Forms;
using C2Forms;

namespace Bewanted
{
    public class KillerLayout : StackLayout
    {
        public C2Editor _response;
        public int _killer_id;

        //-----------------------------------------------------

        public KillerLayout(string title, int killer_id)
        {
            Label label;
            this.Spacing = 0.0;
            this._killer_id = killer_id;
            this.BackgroundColor = C2Style.Control_background;
            this.Padding = new Thickness(0.0, AppStyle.Layout_stack_long_spacing, 5.0, 0.0);
            label = new Label { Text = title };
            label.Style = AppStyle.Label_header;
            label.TranslationX = C2Style.Entry_padding_horizontal;
            label.VerticalOptions = LayoutOptions.Center;
            this._response = new C2Editor(Local.txtKiller03, 10, 200, 6,null);
            this.Children.Add(label);
            this.Children.Add(this._response);
        }
    }

    //-----------------------------------------------------

    public class KillerPage : ContentPage
    {
        private AppDelegate _delegate;
        private int _offer_id;
        private StackLayout _killer_layout;

        //-----------------------------------------------------

        public KillerPage(AppDelegate _delegate) : base()
        {
            StackLayout layout;
            Device.OnPlatform(Android: () => NavigationPage.SetTitleIcon(this, "no_image.png"));
            this.Title = Local.txtKiller00;
            this.Style = AppStyle.Page;
            this._delegate = _delegate;
            this._offer_id = int.MinValue;
            layout = new StackLayout();
            layout.VerticalOptions = LayoutOptions.Start;
            layout.Padding = new Thickness(0.0, AppStyle.Layout_padding_vertical);
            layout.Spacing = AppStyle.Layout_padding_vertical;

            {
                StackLayout label_layout;
                Label label;
                label_layout = new StackLayout();
                label_layout.Padding = new Thickness(AppStyle.Content_box_padding_horizontal, 0.0);
                label = new Label { Text = Local.txtKiller01 };
                label.Style = AppStyle.Label_header;
                label.FontSize = C2Style.Entry_font_size;
                label.LineBreakMode = LineBreakMode.WordWrap;
                label_layout.Children.Add(label);
                layout.Children.Add(label_layout);
            }

            this._killer_layout = new StackLayout();
            this._killer_layout.Spacing = AppStyle.Layout_stack_spacing;
            layout.Children.Add(this._killer_layout);

            {
                StackLayout buttons_layout;
                C2Button send_button;
                C2Button cancel_button;
                buttons_layout = new StackLayout();
                buttons_layout.Padding = new Thickness(AppStyle.Button_padding_horizontal, 0.0);
                buttons_layout.Spacing = AppStyle.Button_padding_vertical;
                send_button = new C2Button { Text = Local.txtKiller04 };
                send_button.Style = AppStyle.Button1_style;
                send_button.Clicked += i_On_send;
                cancel_button = new C2Button { Text = Local.txtKiller05 };
                cancel_button.Style = AppStyle.Button2_style;
                cancel_button.Clicked += i_On_cancel;
                buttons_layout.Children.Add(send_button);
                buttons_layout.Children.Add(cancel_button);
                layout.Children.Add(buttons_layout);
            }

            Content = new ScrollView
            {
                Content = layout
            };
        }

        //-----------------------------------------------------

        public void Set_data(int offer_id, List<OfferKillerDataJSON> killer_questions)
        {
            this._delegate.GA_track_screen(Local.txtGA08);
            this._offer_id = offer_id;
            for (int i = 0; i < killer_questions.Count; ++i)
            {
                string title = (i + 1).ToString() + ". " + killer_questions[i].name;
                KillerLayout layout = new KillerLayout(title, killer_questions[i].id);
                this._killer_layout.Children.Add(layout);
            }
        }

        //-----------------------------------------------------

        private async void i_On_send(object sender, EventArgs e)
        {
            bool ok = true;
            List<OfferKillerDataJSON> killer_questions = new List<OfferKillerDataJSON>();

            for (int i = 0; i < this._killer_layout.Children.Count; ++i)
            {
                KillerLayout layout = (KillerLayout)this._killer_layout.Children[i];
                OfferKillerDataJSON killer;
                killer = new OfferKillerDataJSON();
                killer.id = layout._killer_id;
                killer.answers = new OfferKillerAnswerDataJSON();
                killer.answers.response = layout._response.Text;
                ok = ok & layout._response.Validate();
                killer_questions.Add(killer);
            }

            if (ok == true)
            {
                ServerResponse<SaveDataJSON> response;
                response = await this._delegate.On_offer_accept(this._offer_id, killer_questions);
                if (response.type == ResponseType.OK_SUCCESS)
                {
                    if (response.result_object.result == true || response.result_object.id !=int.MinValue)
                    {
                        this._delegate.User_current_filter = UserCurrentFilter.kNews;
                        await this.DisplayAlert(Local.txtKiller06, null, Local.txtKiller07);
                        await this.Navigation.PopToRootAsync();
                        
                    }
                    else
                    {
                        await this.DisplayAlert("Developer Error", null, "Ok");
                    }
                }
                else
                {
                    Alert.Show(response, this);
                }
            }
        }

        //-----------------------------------------------------

        private async void i_On_cancel(object sender, EventArgs e)
        {
            if (await this.DisplayAlert(Local.txtButtons07, Local.txtButtons08, Local.txtButtons05, Local.txtButtons06) == true)
                await this.Navigation.PopAsync();
        }

        //-----------------------------------------------------

    }
}
