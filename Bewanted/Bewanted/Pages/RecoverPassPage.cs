﻿using System;
using Xamarin.Forms;
using C2Forms;

namespace Bewanted
{
    class RecoverPassPage : ContentPage
    {
        private AppDelegate _delegate;
        private C2MailEntry _email;

        //-----------------------------------------------------

        public RecoverPassPage(AppDelegate _delegate) : base()
        {
            StackLayout layout;
            Device.OnPlatform(Android: () => NavigationPage.SetTitleIcon(this, "no_image.png"));
            this.Title = Local.txtRecover00;
            this.Style = AppStyle.Page;
            this._delegate = _delegate;
            this._delegate.GA_track_screen(Local.txtGA20);
            layout = new StackLayout();
            layout.Padding = new Thickness(0.0, AppStyle.Layout_padding_vertical);
            layout.VerticalOptions = LayoutOptions.Start;
            layout.Spacing = 0.0;
            this._email = new C2MailEntry(Local.txtRecover05);
            layout.Children.Add(this._email);

            {
                StackLayout buttons_layout;
                C2Button button;
                buttons_layout = new StackLayout();
                buttons_layout.Padding = new Thickness(AppStyle.Button_padding_horizontal, AppStyle.Layout_padding_vertical);
                buttons_layout.Spacing = AppStyle.Button_padding_vertical;
                button = new C2Button { Text = Local.txtRecover02 };
                button.Style = AppStyle.Button1_style;
                button.Clicked += i_On_send;
                buttons_layout.Children.Add(button);
                layout.Children.Add(buttons_layout);
            }

            Content = new ScrollView
            {
                Content = layout
            };
        }

        //-----------------------------------------------------

        private async void i_On_send(object sender, EventArgs e)
        {
            bool ok = this._email.Validate();
            if (ok == true)
            {
                ServerResponse<SaveDataJSON> response;
                response = await this._delegate.On_recovery_password(this._email.Text);
                if (response.type == ResponseType.OK_SUCCESS)
                {
                    if (response.result_object.result == true || response.result_object.details=="The instructions to recover the password have been sent to the student's email")
                    {
                        await this.DisplayAlert(null, Local.txtRecover01, Local.txtRecover04);
                        await this.Navigation.PopAsync();
                    }
                    else
                    {
                        await this.DisplayAlert("Developer Error", null, "Ok");
                    }
                }
                else
                {
                    Alert.Show(response, this);
                }
            }
        }

        //-----------------------------------------------------


    }
}
