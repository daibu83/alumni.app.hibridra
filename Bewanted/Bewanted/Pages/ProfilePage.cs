﻿using C2Core;
using C2Forms;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace Bewanted
{
    class SectionView : ContentView
    {
        public Image _plus_image;
        public Image _minus_image;
        public StackLayout _title_layout;
        public StackLayout _content_layout;
        
        //-----------------------------------------------------

        public SectionView(string name, EventHandler handler) : base()
        {
            StackLayout layout;
            this.Style = C2Style.Control_style;
            layout = new StackLayout();
            layout.Spacing = 0;

            {
                Label label;
                TapGestureRecognizer gesture;
                this._title_layout = new StackLayout();
                this._title_layout.Orientation = StackOrientation.Horizontal;
                this._title_layout.Padding = new Thickness(10);
                label = new Label { Text = name, Style = AppStyle.Label_header };
                label.FontSize = C2Style.Entry_font_size;
                label.FontAttributes = FontAttributes.Bold;
                label.HorizontalOptions = LayoutOptions.FillAndExpand;
                this._plus_image = new Image { Source = "plus.png" };
                this._plus_image.HorizontalOptions = LayoutOptions.End;
                this._plus_image.WidthRequest = 24;
                this._minus_image = new Image { Source = "minus.png" };
                this._minus_image.HorizontalOptions = LayoutOptions.End;
                this._minus_image.HeightRequest = 31;
                gesture = new TapGestureRecognizer();
                gesture.Tapped += handler;
                this._title_layout.Children.Add(label);
                this._title_layout.Children.Add(this._plus_image);
                this._title_layout.Children.Add(this._minus_image);
                this._title_layout.GestureRecognizers.Add(gesture);
                layout.Children.Add(this._title_layout);
            }

            {
                this._content_layout = new StackLayout();
                this._content_layout.Spacing = 0;
                this._content_layout.Padding = new Thickness(0);
                this._content_layout.IsVisible = true;
                this._minus_image.IsVisible = false;
                layout.Children.Add(this._content_layout);
            }

            this.Content = layout;
        }
    }

    //-----------------------------------------------------

    class TextLayout : StackLayout
    {
        private Label _title;
        private Label _detail1;
        private Label _detail2;
        private Label _detail3;


        //-----------------------------------------------------

        public TextLayout() : base()
        {
            this.Spacing = 0;
            this._title = new Label { Text = "", Style = AppStyle.Label_header };
            this._title.FontAttributes = FontAttributes.Bold;
            this.Children.Add(this._title);
            this._detail1 = new Label { Text = "", Style = AppStyle.Label };
            this.Children.Add(this._detail1);
            this._detail2 = new Label { Text = "", Style = AppStyle.Label };
            this.Children.Add(this._detail2);
            this._detail3 = new Label { Text = "", Style = AppStyle.Label };
            this.Children.Add(this._detail3);
        }

        //-----------------------------------------------------

        private void i_Update_label(Label label, string text)
        {
            if (string.IsNullOrEmpty(text) == true)
            {
                label.IsVisible = false;
            }
            else
            {
                label.Text = text;
                label.IsVisible = true;
            }
        }

        //-----------------------------------------------------

        public void Set_data(string title, string detail1, string detail2, string detail3)
        {
            this.i_Update_label(this._title, title);
            this.i_Update_label(this._detail1, detail1);
            this.i_Update_label(this._detail2, detail2);
            this.i_Update_label(this._detail3, detail3);
        }

    }

    //-----------------------------------------------------

    public class ProfilePage : ContentPage, ModalDialog
    {
        PopupLayout _popup = new PopupLayout();
        private ScrollView scroll;
        private AppDelegate _delegate;
        private CVDataJSON _cv_json;
        private Label _percent;
        private ContentView view_msg;
        private ContentView view_popup_msg;
        private Guid _id_msg;
        private Label _label;
        private StackLayout _layoutmsg;
        private int _percentcompletado;
        private StackLayout _waiting_layout;
        private ImageLayout _personal_layout;
        private ContentView _personal_view;
        private StackLayout _sections_layout;
        private PersonalPage _personal_page;
        private AcademicPage _academic_page;
        private MerchantPage _merchant_page;
        private LanguagePage _language_page;
        private InternshipPage _internship_page;
        private ExperiencePage _experience_page;
        private VoluntaryPage _voluntary_page;
        private OtherPage _other_page;
        private ProfileSection _profile_selection;
        private bool _is_first_appearing;
        private bool _is_image_plus;
        private bool _is_image_minus_child;
        private bool _is_image_minus;
        private bool _launch_personal_page;
        private C2PageDialog objModalPage;
        #region IModalHost implementation

        public Task DisplayPageModal(Page page)
        {
            var displayEvent = DisplayPageModalRequested;

            Task completion = null;
            if (displayEvent != null)
            {
                var eventArgs = new DisplayPageModalRequestedEventArgs(page);
                displayEvent(this, eventArgs);
                completion = eventArgs.DisplayingPageTask;
            }

            // If there is not task, just create a new completed one
            return completion ?? Task.FromResult<object>(null);
        }

        #endregion

        public event EventHandler<DisplayPageModalRequestedEventArgs> DisplayPageModalRequested;

        public sealed class DisplayPageModalRequestedEventArgs : EventArgs
        {
            public Task DisplayingPageTask { get; set; }

            public Page PageToDisplay { get; }

            public DisplayPageModalRequestedEventArgs(Page modalPage)
            {
                PageToDisplay = modalPage;
            }
        }

        //-----------------------------------------------------

        private ContentView i_Profile_bar_view()
        {
            ContentView view;
            StackLayout layout;
            Image image;
            Label label1;
            Label label2;
            view = new ContentView();
            view.Style = AppStyle.Profile_background;
            view.Padding = new Thickness(10, 5, 10, 5);
            layout = new StackLayout();
            layout.Orientation = StackOrientation.Horizontal;
            layout.Spacing = 0.0;
            image = new Image { Source = "profile.png" };
            image.WidthRequest = 24;
            label1 = new Label { Text = Local.txtProfile01 };
            label1.TranslationX = 5;
            label1.Style = AppStyle.Filter_label_strong;
            label1.VerticalOptions = LayoutOptions.Center;
            this._percent = new Label();
            this._percent.Style = AppStyle.Filter_label_strong;
            this._percent.FontAttributes = FontAttributes.Bold;
            this._percent.TranslationX = 5;
            this._percent.VerticalOptions = LayoutOptions.Center;
            label2 = new Label { Text = Local.txtProfile02 };
            label2.TranslationX = 5;
            label2.Style = AppStyle.Filter_label_strong;
            label2.VerticalOptions = LayoutOptions.Center;
            layout.Children.Add(image);
            layout.Children.Add(label1);
            layout.Children.Add(this._percent);
            layout.Children.Add(label2);
            view.Content = layout;
            return view;
        }




        //mod001:mensaje de inicio de rellenado de datos
        private ContentView i_Personal_msg_view_popup()
        {

            C2Dialog layout;

            this.view_popup_msg = new ContentView();
            this.view_popup_msg.BackgroundColor = Color.White;

            var fstitle = new FormattedString();
            fstitle.Spans.Add(new Span { Text = Local.txtProfile26, FontSize = C2Style.Label_font_size_title, FontAttributes = FontAttributes.Bold });

            var fs = new FormattedString();
            fs.Spans.Add(new Span { Text = Local.txtProfile23, FontSize = C2Style.Label_font_size_popmsg });
            fs.Spans.Add(new Span { Text = Local.txtProfile24, FontSize = C2Style.Label_font_size_popmsg, FontAttributes = FontAttributes.Bold });
            fs.Spans.Add(new Span { Text = Local.txtProfile25, FontSize = C2Style.Label_font_size_popmsg });

            layout = new C2Dialog(fstitle, fs, true, false, i_On_close);
            layout.Orientation = StackOrientation.Vertical;
            layout.Spacing = 10.0;

            this.view_popup_msg.Content = layout;
            this.view_popup_msg.WidthRequest = this.Width - 30;

            return this.view_popup_msg;





        }

        //-----------------------------------------------------

        private ContentView i_Perfil_msg_view()
        {


            Image image;


            this.view_msg = new ContentView();
            this.view_msg.Style = AppStyle.Msg_label_background;
            this.view_msg.Padding = new Thickness(10, 12, 18, 13);
            this.view_msg.IsVisible = false;

            this._layoutmsg = new StackLayout();
            this._layoutmsg.Orientation = StackOrientation.Horizontal;
            this._layoutmsg.Spacing = 0.0;
            image = new Image { Source = "profile.png" };
            image.WidthRequest = 18;
            image.TranslationX = 3;


            this._label = new Label();
            _label.TranslationX = 16;
            this._layoutmsg.Children.Add(image);
            this._layoutmsg.Children.Add(this._label);
            this._id_msg = this._layoutmsg.Id;
            this.view_msg.Content = this._layoutmsg;
            return this.view_msg;
        }
        //-----------------------------------------------------

        private StackLayout i_Sections_layout()
        {
            this._sections_layout = new StackLayout();
            this._sections_layout.Spacing = AppStyle.Layout_stack_long_spacing;
            this._sections_layout.Padding = new Thickness(0, AppStyle.Layout_stack_long_spacing, 0, 2 * C2Style.Entry_padding_vertical);
            this._sections_layout.Children.Add(new SectionView(Local.txtAcademic00, this.i_On_academic_tap));
            this._sections_layout.Children.Add(new SectionView(Local.txtMerchant00, this.i_On_merchant_tap));
            this._sections_layout.Children.Add(new SectionView(Local.txtLanguage00, this.i_On_language_tap));
            this._sections_layout.Children.Add(new SectionView(Local.txtInternship00, this.i_On_internship_tap));
            this._sections_layout.Children.Add(new SectionView(Local.txtExperience00, this.i_On_experience_tap));
            this._sections_layout.Children.Add(new SectionView(Local.txtVoluntary00, this.i_On_voluntray_tap));
            this._sections_layout.Children.Add(new SectionView(Local.txtOther00, this.i_On_other_tap));
            return this._sections_layout;
        }

        //-----------------------------------------------------

        private StackLayout i_Title_layout(int index)
        {
            SectionView view = (SectionView)this._sections_layout.Children[index];
            return view._title_layout;
        }

        //-----------------------------------------------------

        private StackLayout i_Content_layout(int index)
        {
            SectionView view = (SectionView)this._sections_layout.Children[index];
            return view._content_layout;
        }

        //-----------------------------------------------------

        private void i_Set_personal_data()
        {
            StudentDataJSON personal = this._cv_json.personal;
            string str = "";
            string img_url = "";
            List<string> icons = new List<string>();
            List<string> urls = new List<string>();
            //revisar las img como viene la url de source
            
            if (personal.sites != null) {
                for (int i = 0; i < personal.sites.Count; ++i)
                {
                    if (personal.sites[i].pivot.url != "") {
                        icons.Add(Server._URL_IMAGES_API_SOCIAL + personal.sites[i].image);
                        urls.Add(personal.sites[i].pivot.url);
                    }
                    
                }
            }
            
            if (personal.location != null) {
                if (personal.location.city != null)
                {
                    str = personal.location.city.label;
                }
                if (personal.location.country != null) {
                    if (personal.location.country.name != "")
                    {
                        if (str != "") { str = str + ", "; }
                        str = str + personal.location.country.name;
                    }
                }

            }
            if (str == "") { str = "Sin definir"; }
            if (personal.image_url != null && personal.image_url != "")
            {
                img_url = personal.image_url;
            }
            else {
                //user
                img_url = Server._URL_IMAGES_API +  "user.jpg";
            }
            
            this._personal_layout.Set_data(img_url, personal.name + " " + personal.last_name, str, personal.email, null, null, null);
            this._personal_layout.Set_icons(icons, urls);
            if (personal.phone != "")
            {
                AppDelegate._is_personal_data_complete = true;
            }
            else {
                AppDelegate._is_personal_data_complete = false;
            }

            this._delegate.On_personal_data_change(personal.name, personal.last_name, img_url);
        }

        //-----------------------------------------------------

        private void i_Set_detail_data(StackLayout layout, int index, string title, string detail1, string detail2, string detail3, EventHandler handler, Boolean blnVerimg)
        {
            TextLayout text_layout = null;
            if (layout.Children.Count > index)
            {
                ContentView view = (ContentView)layout.Children[index];
                text_layout = (TextLayout)UserPage.Right_arrow_layout(view, blnVerimg, null);
            }
            else
            {
                ContentView view;
                text_layout = new TextLayout();
                view = UserPage.Right_arrow_view(text_layout, handler, blnVerimg, null);
                layout.Children.Add(view);
            }

            text_layout.Set_data(title, detail1, detail2, detail3);
        }

        //-----------------------------------------------------

        private void i_Remove_detail_children(StackLayout layout, int remove_from)
        {
            while (layout.Children.Count > remove_from)
                layout.Children.RemoveAt(remove_from);
        }

        //-----------------------------------------------------

        private void i_Set_academic_data()
        {
            List<AcademicDataJSON> academic = this._cv_json.academic;
            StackLayout layout = this.i_Content_layout(0);
            for (int i = 0; i < academic.Count; ++i)
            {
                string calification;
                string study="";
                string university = "";
                if (academic[i].calification > 10.0f)
                    calification = String.Format("{0}: {1}", Local.txtAcademic10, Local.txtAcademic13);
                else
                    calification = String.Format("{0}: {1:0.00}", Local.txtAcademic10, academic[i].calification);
                if (academic[i].study != null)
                {
                    study = academic[i].study.study_level_name + "-" + academic[i].study.name;
                }
                if (academic[i].university != null)
                {
                    university = academic[i].university.name;
                }
                this.i_Set_detail_data(layout, i, study, academic[i].started_at, university, calification, i_On_academic_tap, true);
            }

            // this.i_Set_detail_data(layout, academic.Count, null, Local.txtAcademic17, null, null, i_On_academic_tap);
            i_Remove_detail_children(layout, academic.Count/* + 1*/);
            if (academic.Count > 0) {
                AppDelegate._is_academic_data = true;
            } else {
                AppDelegate._is_academic_data = false;
            }
        }

        //-----------------------------------------------------
        public void cambia_plus_arrow() {
            SectionView view;
            //SectionView viewResto;
            if (this._is_image_minus == true)
            {
                _is_image_minus_child = false;
            }
            else
            {
                view = (SectionView)this._sections_layout.Children[1];
                view._plus_image.Source = "arrow_right.png";
                this._is_image_minus = true;
                this._is_image_plus = false;
                _is_image_minus_child = false;
            }

        }

        public void cambia_arrow_plus()
        {
            SectionView view;

            if (this._is_image_plus == true) {
                _is_image_minus_child = true;
            }
            else
            {
                view = (SectionView)this._sections_layout.Children[1];
                view._plus_image.Source = "plus.png";
                this._is_image_plus = true;
                this._is_image_minus = false;
                _is_image_minus_child = true;
            }
        }

        private void i_Set_merchant_data()
        {
            MerchantDataJSON merchant = this._cv_json.merchant;
            StackLayout layout = this.i_Content_layout(1);
            if (this._cv_json.merchant.searching != null && this._cv_json.merchant.searching!="")
            {
                this.cambia_plus_arrow();
                AppDelegate._is_merchant_data = true;
                layout.IsVisible = true;

            }
            else
            {
                this.cambia_arrow_plus();
                AppDelegate._is_merchant_data = false;
                layout.IsVisible = false;
            }
            //REVISAR -FALNTA DATOS DE SECTORES Y POSICIONES

            this.i_Set_detail_data(layout, 0, Local.txtProfile10, merchant.searching, null, null, this.i_On_searching_tap, _is_image_minus_child);
            this.i_Set_detail_data(layout, 1, Local.txtProfile14, merchant.where, null, null, this.i_On_where_tap, _is_image_minus_child);
            this.i_Set_detail_data(layout, 2, Local.txtProfile15, merchant.availability, null, null, this.i_On_availability_tap, _is_image_minus_child);
            this.i_Set_detail_data(layout, 3, Local.txtProfile13, merchant.hours, null, null, this.i_On_hours_tap, _is_image_minus_child);
            this.i_Set_detail_data(layout, 4, Local.txtProfile11, merchant.desc_positions, null, null, this.i_On_position_tap, _is_image_minus_child);
            this.i_Set_detail_data(layout, 5, Local.txtProfile12, merchant.desc_sectors, null, null, this.i_On_sector_tap, _is_image_minus_child);


        }

        //-----------------------------------------------------

        private void i_Set_language_data()
        {
            List<LanguageDataJSON> languages = this._cv_json.languages;
            StackLayout layout = this.i_Content_layout(2);
            string levellang = "";
            string certlang = "";
            for (int i = 0; i < languages.Count; ++i) {
                levellang = "";
                certlang = "";
                if (languages[i].level != null) {
                    levellang = languages[i].level.name;
                }
                if (languages[i].certificate != null)
                {
                    certlang = languages[i].certificate.name;
                }
                this.i_Set_detail_data(layout, i, languages[i].language.name, levellang, certlang, null, i_On_language_tap, true);
            }
            //this.i_Set_detail_data(layout, languages.Count, null, Local.txtLanguage07, null, null, i_On_language_tap);
            i_Remove_detail_children(layout, languages.Count/* + 1*/);
        }

        //-----------------------------------------------------

        private void i_Set_internship_data()
        {
            List<InternshipDataJSON> internships = this._cv_json.internships;
            StackLayout layout = this.i_Content_layout(3);
            for (int i = 0; i < internships.Count; ++i)
                this.i_Set_detail_data(layout, i, internships[i].description, internships[i].country.name, internships[i].year.ToString(), null, i_On_internship_tap, true);

            //this.i_Set_detail_data(layout, internships.Count, null, Local.txtInternship10, null, null, i_On_internship_tap);
            i_Remove_detail_children(layout, internships.Count/* + 1*/);
        }

        //-----------------------------------------------------

        private void i_Set_experience_data()
        {
            List<ExperienceDataJSON> experience = this._cv_json.experience;
            StackLayout layout = this.i_Content_layout(4);
            for (int i = 0; i < experience.Count; ++i) {
                string where = "";
                where = experience[i].state.label;
                this.i_Set_detail_data(layout, i, experience[i].position.name, experience[i].company.name, where, null, i_On_experience_tap, true);
            }


            //this.i_Set_detail_data(layout, experience.Count, null, Local.txtExperience12, null, null, i_On_experience_tap);
            i_Remove_detail_children(layout, experience.Count/* + 1*/);
        }

        //-----------------------------------------------------

        private void i_Set_voluntary_data()
        {
            List<VoluntaryDataJSON> voluntary = this._cv_json.voluntary;
            StackLayout layout = this.i_Content_layout(5);
            for (int i = 0; i < voluntary.Count; ++i) {
                string where = "";
                where = voluntary[i].state.label;
                this.i_Set_detail_data(layout, i, voluntary[i].ong, where, voluntary[i].started_at, null, i_On_voluntray_tap, true);
            }
                

            //this.i_Set_detail_data(layout, voluntary.Count, null, Local.txtVoluntary05, null, null, i_On_voluntray_tap);
            i_Remove_detail_children(layout, voluntary.Count/* + 1*/);
        }

        //-----------------------------------------------------

        private void i_Set_other_info_data()
        {
            SectionView view;
            string other = this._cv_json.other;
            StackLayout layout = this.i_Content_layout(6);
            this.i_Set_detail_data(layout, 0, null, other, null, null, i_On_other_tap, false);
            if (other != null && other != "")
            {
                view = (SectionView)this._sections_layout.Children[6];
                view._plus_image.Source = "arrow_right.png";

            }
            else {
                view = (SectionView)this._sections_layout.Children[6];
                view._plus_image.Source = "plus.png";
            }
        }

        //-----------------------------------------------------

        public ProfilePage(AppDelegate _delegate, bool launch_personal_page) : base()
        {
            StackLayout layout;
            this.Style = AppStyle.Page;
            this._delegate = _delegate;
            this.Title = Local.txtProfile00;
            Device.OnPlatform(Android: () => NavigationPage.SetTitleIcon(this, "no_image.png"));
            layout = new StackLayout();
            layout.Spacing = 0.0;
            this._cv_json = new CVDataJSON();
            this._percent = null;
            this._waiting_layout = null;
            this._personal_layout = null;
            this._personal_view = null;
            this._sections_layout = null;
            this._personal_page = null;
            this._academic_page = null;
            this._merchant_page = null;
            this._language_page = null;
            this._internship_page = null;
            this._experience_page = null;
            this._voluntary_page = null;
            this._other_page = null;
            this._profile_selection = ProfileSection.kAll;
            this._is_first_appearing = true;
            this._launch_personal_page = launch_personal_page;

            layout.Children.Add(i_Perfil_msg_view());

            Content = new ScrollView
            {
                Content = layout
            };
        }

        private void muestraPopup()
        {
            this.Title = "Editar perfil ";


            int android_api = DependencyService.Get<IDeviceService>().AndroidAPILevel();
            if (android_api >= 1 && android_api <= 20)
            {
                this.Icon = "menu_marginpop.png";

            }
            else
            {
                this.Icon = "menupop.png";
            }

            var frame = new Frame
            {
                WidthRequest = Width - 80,
                Padding = new Thickness(15, 20, 15, 20),
                Content = i_Personal_msg_view_popup(),
                OutlineColor = Color.Silver,
                BackgroundColor = Color.White,
                VerticalOptions = LayoutOptions.CenterAndExpand,
                HorizontalOptions = LayoutOptions.CenterAndExpand
            };

            var PopUp = new StackLayout
            {
                VerticalOptions = LayoutOptions.CenterAndExpand,
                HorizontalOptions = LayoutOptions.CenterAndExpand,
                WidthRequest = Width,
                HeightRequest = Height,
                BackgroundColor = new Color(0, 0, 0, 0.7),
            };

            PopUp.Children.Add(frame);
            NavigationPage.SetHasBackButton(this, false);
            _delegate.Disable_Menu();
            this.scroll = (ScrollView)this.Content;
            _popup.Content = scroll;
            Content = _popup;
            _popup.ShowPopup(PopUp);


        }

        //-----------------------------------------------------

        private async void i_Update_percentage()
        {
            ServerResponse<StudentDataJSON> response;
            response = await this._delegate.On_profile_percentaje();
            if (response.type == ResponseType.OK_SUCCESS) {
                this._percentcompletado = (int)response.result_object.percentaje;
                var fs = new FormattedString();

                if (AppDelegate._is_personal_data_complete == false)
                {
                    //Si no estan completados los datos personales al 100% mostrar aviso
                    fs.Spans.Add(new Span { Text = Local.txtProfile17, ForegroundColor = AppStyle.Label_white_text, FontSize = C2Style.Label_font_size });
                    fs.Spans.Add(new Span { Text = Local.txtProfile18, ForegroundColor = AppStyle.Label_white_text, FontSize = C2Style.Label_font_size, FontAttributes = FontAttributes.Bold });
                    fs.Spans.Add(new Span { Text = Local.txtProfile19, ForegroundColor = AppStyle.Label_white_text, FontSize = C2Style.Label_font_size });
                    this._label.FormattedText = fs;
                    this.view_msg.IsVisible = true;
                    AppDelegate._showperfilcompleto = true;
                    AppDelegate._is_perfil_data_min = 2;

                }
                else if (AppDelegate._is_academic_data == true && AppDelegate._is_merchant_data == true)
                {
                    if (AppDelegate._showperfilcompleto == true)
                    {
                        //mostrar Datos minimmos conseguidos
                        AppDelegate._showperfilcompleto = false; //No volver a mostrar este mensaje
                        AppDelegate._is_perfil_data_min = 1;
                        objModalPage = new C2PageDialog(this.Height, this.Width, i_Personal_msg_view_popup());
                        Device.OnPlatform(iOS: () => DisplayPageModal(objModalPage), Android: () => muestraPopup());
                        this.cambia_plus_arrow();
                    }
                }
                else {
                    //Si no tiene completo el perfil, mostrar aviso-mostrar mensaje 2-faltan dastos minimos
                    fs.Spans.Add(new Span { Text = Local.txtProfile20, ForegroundColor = AppStyle.Label_white_text, FontSize = C2Style.Label_font_size });
                    fs.Spans.Add(new Span { Text = Local.txtProfile21, ForegroundColor = AppStyle.Label_white_text, FontSize = C2Style.Label_font_size, FontAttributes = FontAttributes.Bold });
                    fs.Spans.Add(new Span { Text = Local.txtProfile22, ForegroundColor = AppStyle.Label_white_text, FontSize = C2Style.Label_font_size });
                    this._label.FormattedText = fs;
                    this.view_msg.IsVisible = true;
                    AppDelegate._showperfilcompleto = true;
                    AppDelegate._is_perfil_data_min = 2;

                }

            }

        }

        //-----------------------------------------------------
        private void i_Remove_msg_layout_children()
        {
            ScrollView scroll = (ScrollView)this.Content;
            StackLayout layout = (StackLayout)scroll.Content;
            try
            {
                layout.Children.RemoveAt(layout.Children.IndexOf(view_msg));
            }
            catch
            {
            }

        }


        private StackLayout i_Remove_layout_children()
        {
            ScrollView scroll = (ScrollView)this.Content;
            StackLayout layout = (StackLayout)scroll.Content;
            while (layout.Children.Count > 1)
                layout.Children.RemoveAt(1);
            return layout;

        }

        //-----------------------------------------------------

        private void i_Set_waiting_layout()
        {
            StackLayout layout = this.i_Remove_layout_children();
            if (this._waiting_layout == null)
                this._waiting_layout = UserPage.Waiting_layout();
            layout.Children.Add(this._waiting_layout);
        }

        //-----------------------------------------------------
        private void personal_layout() {
            if (this._personal_layout == null)
            {
                this._personal_layout = new ImageLayout(AppStyle.Image_size_big, false,false);
                this._personal_view = UserPage.Right_arrow_view(this._personal_layout, this.i_On_personal_tap, true, null);

                this._personal_view.Padding = new Thickness(0, 13, 0, 13);
                this._sections_layout = this.i_Sections_layout();
            }
        }
        private void refresh_layout(bool is_first_appearing) {
            this.i_Update_percentage();
            this._delegate.On_profile_data_change();
            if (is_first_appearing == true)
            {
                StackLayout layout = this.i_Remove_layout_children();
                layout.Children.Add(this._personal_view);
                layout.Children.Add(this._sections_layout);
            }
        }
        private async Task<bool> i_Update_cv_data_personal(bool is_first_appearing) {
            ServerResponse<StudentDataJSON> responsepersonal;
            responsepersonal = await this._delegate.On_personal_data();
            if (responsepersonal.type == ResponseType.OK_SUCCESS)
            {
                
                this._cv_json.personal = responsepersonal.result_object;
                this.i_Set_personal_data();
            }
            
            return true;
        }
        private async Task<bool> i_Update_cv_data_languages(bool is_first_appearing)
        {
            ServerResponse<List<LanguageDataJSON>> responselang;
            responselang = await this._delegate.On_student_Languages();
            if (responselang.type == ResponseType.OK_SUCCESS)
            {
                
                this._cv_json.languages = responselang.result_object;
                this.i_Set_language_data();
            }
            
            return true;
        }
        private async Task<bool> i_Update_cv_data_experiencies(bool is_first_appearing) {
            ServerResponse<List<ExperienceDataJSON>> responseexpe;
            responseexpe = await this._delegate.On_student_experience();
            if (responseexpe.type == ResponseType.OK_SUCCESS)
            {
                
                this._cv_json.experience = responseexpe.result_object;
                this.i_Set_experience_data();
            }
            
            return true;
        }

        private async Task<bool> i_Update_cv_data_academics(bool is_first_appearing) {
            ServerResponse<List<AcademicDataJSON>> responseacademic;
            responseacademic = await this._delegate.On_student_academics();
            if (responseacademic.type == ResponseType.OK_SUCCESS)
            {
                
                this._cv_json.academic = responseacademic.result_object;
                this.i_Set_academic_data();
            }
            
            return true;
        }
        private async Task<bool> i_Update_cv_data_internships(bool is_first_appearing)
        {
            ServerResponse<List<InternshipDataJSON>> responseinter;
            responseinter = await this._delegate.On_student_internships();
            if (responseinter.type == ResponseType.OK_SUCCESS)
            {
                
                this._cv_json.internships = responseinter.result_object;
                this.i_Set_internship_data();
            }
            

            return true;
        }
        private async Task<bool> i_Update_cv_data_voluntary(bool is_first_appearing)
        {
            ServerResponse<List<VoluntaryDataJSON>> responsevol;
            responsevol = await this._delegate.On_student_voluntary();
            if (responsevol.type == ResponseType.OK_SUCCESS)
            {
                
                this._cv_json.voluntary = responsevol.result_object;
                this.i_Set_voluntary_data();
            }
            

            return true;
        }

        private async Task<bool> i_Update_cv_data_merchant(bool is_first_appearing)
        {
            ServerResponse<MerchantDataJSON> responsemer;
            responsemer = await this._delegate.On_student_merchant();
            if (responsemer.type == ResponseType.OK_SUCCESS)
            {
                
                this._cv_json.merchant = responsemer.result_object;
                this.i_Set_merchant_data();
            }
            

            return true;
        }

        private async Task<bool> i_Update_cv_data_other(bool is_first_appearing)
        {
            ServerResponse<StudentDataJSON> responseother;
            responseother = await this._delegate.On_personal_data();
            if (responseother.type == ResponseType.OK_SUCCESS)
            {
                
                this._cv_json.other = responseother.result_object.data;
                this.i_Set_other_info_data();
            }
            
            return true;
        }

        private async Task<bool> i_Update_cv_data_all(bool is_first_appearing)
        {
            bool ok = true;
            ok = ok & await i_Update_cv_data_personal(is_first_appearing);
            ok = ok & await i_Update_cv_data_languages(is_first_appearing);
            ok = ok & await i_Update_cv_data_academics(is_first_appearing);
            ok = ok & await i_Update_cv_data_experiencies(is_first_appearing);
            ok = ok & await i_Update_cv_data_internships(is_first_appearing);
            ok = ok & await i_Update_cv_data_voluntary(is_first_appearing);
            ok = ok & await i_Update_cv_data_merchant(is_first_appearing);
            ok = ok & await i_Update_cv_data_other(is_first_appearing);
            return ok;
        }


        private async Task<bool> i_Update_cv_data(bool is_first_appearing)
        {

            bool ok = true;
            this._delegate._blncargando = true;
            if (is_first_appearing == true)
                this.i_Set_waiting_layout();

            personal_layout();
            switch (this._profile_selection) {
                case ProfileSection.kPersonal:
                    ok= ok &  await i_Update_cv_data_personal(is_first_appearing);
                    break;
                case ProfileSection.kLanguages:
                    ok = ok & await i_Update_cv_data_languages(is_first_appearing);
                    break;
                case ProfileSection.kAcademic:
                    ok = ok & await i_Update_cv_data_academics(is_first_appearing);
                    break;
                case ProfileSection.kExperience:
                    ok = ok & await i_Update_cv_data_experiencies(is_first_appearing);
                    break;
                case ProfileSection.kInternship:
                    ok = ok & await i_Update_cv_data_internships(is_first_appearing);
                    break;
                case ProfileSection.kVoluntary:
                    ok = ok & await i_Update_cv_data_voluntary(is_first_appearing);
                    break;
                case ProfileSection.kMerchant:
                    ok = ok & await i_Update_cv_data_merchant(is_first_appearing);
                    break;
                case ProfileSection.kOther:
                    ok = ok & await i_Update_cv_data_other(is_first_appearing);
                    break;
                case ProfileSection.kAll:
                    ok = ok & await i_Update_cv_data_all(is_first_appearing);
                    break;
                default:
                    break;
            }
            

            refresh_layout(is_first_appearing);
            if (this._launch_personal_page == false){
                this._delegate._blncargando = false;
            }
            
            return ok;

            
        }

        //-----------------------------------------------------

        protected override async void OnAppearing()
        {

            this._delegate.GA_track_screen(Local.txtGA19);
            if (await this.i_Update_cv_data(this._is_first_appearing) == true)
            {
                this._is_first_appearing = false;
                if (this._launch_personal_page == true)
                {
                    this.i_On_personal_tap(null, null);
                    this._launch_personal_page = false;
                }
            }
        }

        //-----------------------------------------------------
        private void restableceMenu()
        {
            this.Title = "Editar perfil  ";
            
            
            _popup.DismissPopup();
            Content = this.scroll;
            _delegate.Enable_Menu();
        }

        private void i_On_close(object sender, EventArgs e)
        {

            Device.OnPlatform(iOS: () => objModalPage.Close(), Android: () => restableceMenu());
            if (this.view_msg != null)
            {
                if (this.view_msg.IsVisible)
                {
                    this.view_msg.IsVisible = false;
                }
            }
        }

        /*
    private void i_On_section_tap(object sender, EventArgs e)
    {
        for (int i = 0; i < this._sections_layout.Children.Count; ++i)
        {
            SectionView view = (SectionView)this._sections_layout.Children[i];
            if (view._title_layout == sender)
            {
                view._content_layout.IsVisible = !view._content_layout.IsVisible;
                view._plus_image.IsVisible = !view._content_layout.IsVisible;
                view._minus_image.IsVisible = view._content_layout.IsVisible;
                return;
            }
        }

        C2Assert.Error(false);
    }*/

        //-----------------------------------------------------

        private int i_Sender_index(object sender, int section_index)
        {
            StackLayout title_layout;
            StackLayout content_layout;
            title_layout = this.i_Title_layout(section_index);
            content_layout = this.i_Content_layout(section_index);

            if (title_layout == sender)
                return content_layout.Children.Count;

            for (int i = 0; i < content_layout.Children.Count; ++i)
            {
                if (content_layout.Children[i] == sender)
                    return i;
            }

            C2Assert.Error(false);
            return 0;
        }

        //-----------------------------------------------------

        private async void i_On_personal_tap(object sender, EventArgs e)
        {

            this._delegate._blncargando = true;
            this._personal_page = new PersonalPage(this._delegate);

            this._profile_selection = ProfileSection.kPersonal;
            await this.Navigation.PushAsync(this._personal_page);
            //Server._URL_IMAGES_API + this._cv_json.personal.image_url + ".jpg";
            string img_url;
            if (this._cv_json.personal.image_url != null && this._cv_json.personal.image_url != "")
            {
                img_url =this._cv_json.personal.image_url;
            }
            else
            {
                //user
                img_url = Server._URL_IMAGES_API + "user.jpg";
            }
            this._personal_page.Set_data(img_url);
            this._delegate._blncargando = false;
        }

        //-----------------------------------------------------

        private async void i_On_academic_tap(object sender, EventArgs e)
        {
            if (AppDelegate._is_personal_data_complete)
            {
                
                this._academic_page = new AcademicPage(this._delegate);

                this._profile_selection = ProfileSection.kAcademic;
                await this.Navigation.PushAsync(this._academic_page);

                int i = this.i_Sender_index(sender, 0);
                AcademicDataJSON academic;
                C2Assert.Error(i <= this._cv_json.academic.Count);
                if (i < this._cv_json.academic.Count)
                    academic = this._cv_json.academic[i];
                else
                    academic = null;

                this._academic_page.Set_data(academic);
            }
            else {
                await this.DisplayAlert(null, Local.txtProfile28, Local.txtAlertOk);
            }
            
        }

        //-----------------------------------------------------

        private async void i_On_merchant_tap(object sender, EventArgs e)
        {
            if (this._cv_json.academic.Count > 0)
            {
                this._merchant_page = new MerchantPage(this._delegate);

                this._profile_selection = ProfileSection.kMerchant;
                await this.Navigation.PushAsync(this._merchant_page);
                this._merchant_page.Set_data();
            }
            else
            {
                await this.DisplayAlert(null, Local.txtProfile16, Local.txtAlertOk);
            }
        }

        //-----------------------------------------------------

        private void i_On_searching_tap(object sender, EventArgs e)
        {
            this.i_On_merchant_tap(sender, e);
        }

        //-----------------------------------------------------

        private void i_On_where_tap(object sender, EventArgs e)
        {
            this.i_On_merchant_tap(sender, e);
        }

        //-----------------------------------------------------

        private void i_On_availability_tap(object sender, EventArgs e)
        {
            this.i_On_merchant_tap(sender, e);
        }

        //-----------------------------------------------------

        private void i_On_hours_tap(object sender, EventArgs e)
        {
            this.i_On_merchant_tap(sender, e);
        }

        //-----------------------------------------------------

        private void i_On_position_tap(object sender, EventArgs e)
        {
            this.i_On_merchant_tap(sender, e);
        }

        //-----------------------------------------------------

        private void i_On_sector_tap(object sender, EventArgs e)
        {
            this.i_On_merchant_tap(sender, e);
        }

        //-----------------------------------------------------

        private async void i_On_language_tap(object sender, EventArgs e)
        {
            
            this._language_page = new LanguagePage(this._delegate);

            this._profile_selection = ProfileSection.kLanguages;
            await this.Navigation.PushAsync(this._language_page);

            int i = this.i_Sender_index(sender, 2);
            LanguageDataJSON language;
            C2Assert.Error(i <= this._cv_json.languages.Count);
            if (i < this._cv_json.languages.Count)
                language = this._cv_json.languages[i];
            else
                language = null;

            this._language_page.Set_data(language);
        }

        //-----------------------------------------------------

        private async void i_On_internship_tap(object sender, EventArgs e)
        {
            
            this._internship_page = new InternshipPage(this._delegate);

            this._profile_selection = ProfileSection.kInternship;
            await this.Navigation.PushAsync(this._internship_page);

            int i = this.i_Sender_index(sender, 3);
            InternshipDataJSON internship;
            C2Assert.Error(i <= this._cv_json.internships.Count);
            if (i < this._cv_json.internships.Count)
                internship = this._cv_json.internships[i];
            else
                internship = null;

            this._internship_page.Set_data(internship);
        }

        //-----------------------------------------------------

        private async void i_On_experience_tap(object sender, EventArgs e)
        {
            
            this._experience_page = new ExperiencePage(this._delegate);

            this._profile_selection = ProfileSection.kExperience;
            await this.Navigation.PushAsync(this._experience_page);

            int i = this.i_Sender_index(sender, 4);
            ExperienceDataJSON experience;
            C2Assert.Error(i <= this._cv_json.experience.Count);
            if (i < this._cv_json.experience.Count)
                experience = this._cv_json.experience[i];
            else
                experience = null;

            this._experience_page.Set_data(experience);
        }

        //-----------------------------------------------------

        private async void i_On_voluntray_tap(object sender, EventArgs e)
        {
         
            this._voluntary_page = new VoluntaryPage(this._delegate);

            this._profile_selection = ProfileSection.kVoluntary;
            await this.Navigation.PushAsync(this._voluntary_page);

            int i = this.i_Sender_index(sender, 5);
            VoluntaryDataJSON voluntary;
            C2Assert.Error(i <= this._cv_json.voluntary.Count);
            if (i < this._cv_json.voluntary.Count)
                voluntary = this._cv_json.voluntary[i];
            else
                voluntary = null;

            this._voluntary_page.Set_data(voluntary);
        }

        //-----------------------------------------------------

        private async void i_On_other_tap(object sender, EventArgs e)
        {
            
            this._other_page = new OtherPage(this._delegate);
            this._profile_selection = ProfileSection.kOther;
            this._other_page.Set_data(this._cv_json.other);
            await this.Navigation.PushAsync(this._other_page);
        }

        protected override void OnDisappearing()
        {


            base.OnDisappearing();
        }
        //-----------------------------------------------------
        protected override bool OnBackButtonPressed()
        {
            // If you want to continue going back

            if (_popup.IsPopupActive)
            {
                return true;
            }
            else
            {
                base.OnBackButtonPressed();
                return false;
            }
        }

    }
}
