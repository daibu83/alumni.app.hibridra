﻿using System;
using Xamarin.Forms;
using C2Forms;


namespace Bewanted
{
    public class CreateAccountPage : ContentPage
    {
        private AppDelegate _delegate;
        private C2MailEntry _email;
        private C2PasswordEntry _password;
        private UserData _user_data;

        //-----------------------------------------------------

        public CreateAccountPage(AppDelegate _delegate) : base()
        {
            StackLayout layout;
            Device.OnPlatform(Android: () => NavigationPage.SetTitleIcon(this, "no_image.png"));
            this.Title = Local.txtCreate03;
            this.Style = AppStyle.Page;
            this._delegate = _delegate;
            layout = new StackLayout();
            layout.Padding = new Thickness(0.0, AppStyle.Layout_padding_vertical);
            layout.VerticalOptions = LayoutOptions.Start;
            layout.Spacing = AppStyle.Layout_stack_spacing;

            {
                this._email = new C2MailEntry(Local.txtEmail);
                this._password = new C2PasswordEntry(Local.txtCreate04, 6, 50);
                this._user_data = new UserData(_delegate, this,false);
                
                layout.Children.Add(this._email);
                layout.Children.Add(this._password);
                //this._user_data.Add_to_layout(layout);
                this._user_data.Add_to_layout_registro_nuevo_usuario(layout);
            }

            {
                StackLayout inner_layout;
                inner_layout = new StackLayout();
                inner_layout.Padding = new Thickness(AppStyle.Button_padding_horizontal, AppStyle.Button_padding_vertical);
                inner_layout.Spacing = AppStyle.Button_padding_vertical;

                {
                    StackLayout text_layout;
                    Label label;
                    TapGestureRecognizer gesture;
                    text_layout = new StackLayout();
                    text_layout.Spacing = 0.0;


                    label = new Label();
                    FormattedString fs = new FormattedString();
                    fs.Spans.Add(new Span { Text = Local.txtCreate00, ForegroundColor = AppStyle.Label_text, FontSize = C2Style.Label_font_size});
                    fs.Spans.Add(new Span { Text = " " + Local.txtCreate01 + " ", ForegroundColor = AppStyle.Label_text, FontSize = C2Style.Label_font_size, FontAttributes = FontAttributes.Bold });
                    fs.Spans.Add(new Span { Text = Local.txtCreate02, ForegroundColor = AppStyle.Label_text, FontSize = C2Style.Label_font_size });
                    label.FormattedText = fs;
                    gesture = new TapGestureRecognizer();
                    gesture.Tapped += i_On_legal;
                    label.GestureRecognizers.Add(gesture);
                    text_layout.Children.Add(label);

                    /*
                    {
                        Label label;
                        label = new Label { Text = Local.txtCreate00, Style = AppStyle.Label };
                        text_layout.Children.Add(label);
                    }*/

                    /*
                    {
                        StackLayout htext_layout;
                        Label legal_label;
                        Label label;
                        TapGestureRecognizer gesture;
                        htext_layout = new StackLayout();
                        htext_layout.Spacing = 0.0;
                        htext_layout.Orientation = StackOrientation.Horizontal;
                        htext_layout.HorizontalOptions = LayoutOptions.Start;
                        legal_label = new Label { Text = Local.txtCreate01, Style = AppStyle.Label, FontAttributes = FontAttributes.Bold };
                        legal_label.HorizontalOptions = LayoutOptions.Start;
                        label = new Label { Text = " " + Local.txtCreate02, Style = AppStyle.Label };
                        
                        label.HorizontalOptions = LayoutOptions.EndAndExpand;
                        gesture = new TapGestureRecognizer();
                        gesture.Tapped += i_On_legal;
                        legal_label.GestureRecognizers.Add(gesture);
                        htext_layout.Children.Add(legal_label);
                        htext_layout.Children.Add(label);
                        text_layout.Children.Add(htext_layout);
                    }*/

                    inner_layout.Children.Add(text_layout);
                }

                {
                    C2Button create;
                    create = new C2Button { Text = Local.txtCreate03 };
                    create.Style = AppStyle.Button1_style;
                    create.Clicked += i_On_create;
                    inner_layout.Children.Add(create);
                }

                /*  {
                      Button fill;
                      Button clear;
                      fill = new Button { Text = "Fill Data" };
                      fill.Clicked += i_On_fill;
                      clear = new Button { Text = "Clear Data" };
                      clear.Clicked += i_On_clear;
                      inner_layout.Children.Add(fill);
                      inner_layout.Children.Add(clear);
                  }*/

                layout.Children.Add(inner_layout);
            }

            Content = new ScrollView
            {
                Content = layout
            };
        }

        //-----------------------------------------------------

        protected override void OnAppearing()
        {
            this._delegate.GA_track_screen(Local.txtGA04);
            //Ya no se cargan los campos de picckers
           // this._user_data.Load_pickers_data();
        }

        //-----------------------------------------------------

        

        private async void i_On_create(object sender, EventArgs e)
        {
            this.IsEnabled = false;
            bool ok = true;
            ((Button)sender).IsEnabled = false;
            ok = ok & this._email.Validate();
            ok = ok & this._password.Validate();
            ok = ok & this._user_data.Validate();
            this.UpdateChildrenLayout();
            
            if (ok == true)
            {
                ok= await this._delegate.On_Check_Email(this._email.Text);
                if (ok == true)
                {
                    //    this._delegate.On_register(_email.Text, _password.Text, this._user_data._name.Text, this._user_data._last_name.Text, this._user_data._phone.Text, this._user_data._nationality.CurrentId, this._user_data._country_state._country.CurrentId, this._user_data._country_state._state.CurrentId, this._user_data._city.CurrentId, this._user_data._birthdate.yyyy_mm_dd, this);
                    await this._delegate.On_register(this._email.Text, this._password.Text, this._user_data._name.Text, this._user_data._last_name.Text, this);
                    this.IsEnabled = true;
                    ((Button)sender).IsEnabled = true;
                }
                else {
                    this._email.Err_Validate_Text = "El Email no esta disponible. Ya existe.";
                    this._email.Show_error();
                    this.IsEnabled = true;
                    ((Button)sender).IsEnabled = true;
                }
                
            }
            else {
                this.IsEnabled = true;
                ((Button)sender).IsEnabled = true;
            }
            
        }

        //-----------------------------------------------------

        private async void i_On_legal(object sender, EventArgs e)
        {
            LegalPage page;
            page = new LegalPage(this._delegate, Server.Legal_page());
            await this.Navigation.PushAsync(page);
        }

        //-----------------------------------------------------

        private void i_On_fill(object sender, EventArgs e)
        {
            this._email.Text = "ja.martin@cubecut.com";
            this._password.Text = "becubecut";
            this._user_data._name.Text = "Juan A.";
            this._user_data._last_name.Text = "Martín Martínez";
         //   this._user_data._phone.Text = "+34652217007";
        }

        //-----------------------------------------------------

        private void i_On_clear(object sender, EventArgs e)
        {
            this._email.Text = "";
            this._password.Text = "";
            this._user_data._name.Text = "";
            this._user_data._last_name.Text = "";
            this._user_data._phone.Text = "";
        }

        //-----------------------------------------------------


    }
}