﻿using System;
using Xamarin.Forms;
using C2Forms;

namespace Bewanted
{
    class SettingsPage : ContentPage
    {
        private AppDelegate _delegate;
        private C2Switch _public_profile;
        private C2Switch _newsletter;
        private C2Switch _notify;
        private C2PasswordEntry _password;
        private C2PasswordEntry _new_password;
        private C2PasswordEntry _repeat_password;

        //-----------------------------------------------------

        public SettingsPage(AppDelegate _delegate, bool public_profile, bool newsletter,bool notify) : base()
        {
            StackLayout layout;
            Device.OnPlatform(Android: () => NavigationPage.SetTitleIcon(this, "no_image.png"));
            this.Title = Local.txtSettings00;
            this.Style = AppStyle.Page;
            this._delegate = _delegate;
            this._delegate.GA_track_screen(Local.txtGA21);
            layout = new StackLayout();
            layout.VerticalOptions = LayoutOptions.Start;
            layout.Spacing = AppStyle.Layout_stack_long_spacing;

            {
                StackLayout support_layout;
                C2ControlLabel label;
                TapGestureRecognizer gesture;
                support_layout = new StackLayout();
                support_layout.Spacing = AppStyle.Layout_stack_spacing;
                label = new C2ControlLabel { Text = Local.txtSettings03 };
                label.TextColor = C2Style.Control_text;
                gesture = new TapGestureRecognizer();
                gesture.Tapped += this.i_On_legal;
                label.GestureRecognizers.Add(gesture);
                support_layout.Children.Add(new SectionHeaderLayout(Local.txtSettings02));
                support_layout.Children.Add(label);
                layout.Children.Add(support_layout);
            }

            {
                StackLayout config_layout;
                config_layout = new StackLayout();
                config_layout.Spacing = AppStyle.Layout_stack_spacing;
                this._public_profile = new C2Switch(Local.txtSettings05, null);
                this._public_profile.IsToggled = public_profile;

                this._newsletter = new C2Switch(Local.txtSettings06, null);
                this._newsletter.IsToggled = newsletter;

                this._notify = new C2Switch(Local.txtSettings17, null);
                this._notify.IsToggled = notify;

                config_layout.Children.Add(new SectionHeaderLayout(Local.txtSettings04));
                config_layout.Children.Add(this._public_profile);
                config_layout.Children.Add(this._newsletter);
                config_layout.Children.Add(this._notify);
                layout.Children.Add(config_layout);
            }

            {
                StackLayout pass_layout;
                pass_layout = new StackLayout();
                pass_layout.Spacing = AppStyle.Layout_stack_spacing;
                this._password = new C2PasswordEntry(Local.txtSettings08, 6, 50);
                pass_layout.Children.Add(new SectionHeaderLayout(Local.txtSettings07));
                pass_layout.Children.Add(this._password);
                layout.Children.Add(pass_layout);
            }

            { 
                StackLayout new_pass_layout;
                new_pass_layout = new StackLayout();
                new_pass_layout.Spacing = AppStyle.Layout_stack_spacing;
                this._new_password = new C2PasswordEntry(Local.txtSettings09, 6, 50);
                this._repeat_password = new C2PasswordEntry(Local.txtSettings10, 6, 50);
                new_pass_layout.Children.Add(this._new_password);
                new_pass_layout.Children.Add(this._repeat_password);
                layout.Children.Add(new_pass_layout);
            }

            {
                StackLayout buttons_layout;
                C2Button save_button;
                C2Button close_button;
                buttons_layout = new StackLayout();
                buttons_layout.Padding = new Thickness(AppStyle.Button_padding_horizontal, AppStyle.Button_padding_vertical);
                buttons_layout.Spacing = AppStyle.Button_padding_vertical;
                save_button = new C2Button { Text = Local.txtSettings11 };
                save_button.Style = AppStyle.Button1_style;
                save_button.Clicked += i_On_save;
                close_button = new C2Button { Text = Local.txtSettings01 };
                close_button.Style = AppStyle.Button3_style;
                close_button.Clicked += i_On_close;
                buttons_layout.Children.Add(save_button);
                buttons_layout.Children.Add(close_button);
                layout.Children.Add(buttons_layout);
            }

            {
                StackLayout credits_layout;
                Label company;
                Label version;
                credits_layout = new StackLayout();
                credits_layout.Spacing = 0.0;
                credits_layout.Padding = new Thickness(AppStyle.Content_box_padding_horizontal, 0.0, AppStyle.Content_box_padding_horizontal, AppStyle.Content_box_padding_vertical);
                company = new Label { Text = Local.txtSettings12, Style = AppStyle.Label };
                company.HorizontalOptions = LayoutOptions.Center;
                company.TextColor = C2Style.Control_text;
                version = new Label { Text = Local.txtSettings13, Style = AppStyle.Label };
                version.HorizontalOptions = LayoutOptions.Center;
                credits_layout.Children.Add(company);
                credits_layout.Children.Add(version);
                layout.Children.Add(credits_layout);
            }

            Content = new ScrollView
            {
                Content = layout
            };
        }

        //-----------------------------------------------------

        private async void i_On_legal(object sender, EventArgs e)
        {
            LegalPage page;
            page = new LegalPage(this._delegate, Server.Legal_page());
            await this.Navigation.PushAsync(page);
        }

        //-----------------------------------------------------

        private async void i_On_save(object sender, EventArgs e)
        {
            bool change_password = false;
            bool ok = true;

            this._password.Clear_error();
            this._new_password.Clear_error();
            this._repeat_password.Clear_error();

            if (String.IsNullOrEmpty(this._password.Text) == false)
                change_password = true;

            if (String.IsNullOrEmpty(this._new_password.Text) == false)
                change_password = true;

            if (String.IsNullOrEmpty(this._repeat_password.Text) == false)
                change_password = true;

            if (change_password == true)
            {
                ok = ok & this._password.Validate();
                ok = ok & this._new_password.Validate();
                ok = ok & this._repeat_password.Validate();

                if (ok == true)
                {
                    if (this._new_password.Text.CompareTo(this._repeat_password.Text) != 0)
                    {
                        await this.DisplayAlert(null, Local.txtSettings14, Local.txtSettings15);
                        ok = false;
                    }
                }
            }

            if (ok == true)
            {
                ServerResponse<SaveDataJSON> response;
                response = await this._delegate.On_settings_save(this._public_profile.IsToggled, this._newsletter.IsToggled, this._notify.IsToggled, this._password.Text, this._new_password.Text);
                if (response.type == ResponseType.OK_SUCCESS)
                {
                    if (response.result_object.result == true || response.result_object.id !=int.MinValue)
                    {
                        await this.DisplayAlert(null, Local.txtSettings16, Local.txtSettings15);
                    }
                    else
                    {
                        await this.DisplayAlert("Developer Error", null, "Ok");
                    }
                }
                else
                {
                    Alert.Show(response, this);
                }
            }
        }

        //-----------------------------------------------------

        private async void i_On_close(object sender, EventArgs e)
        {
            await this._delegate.On_close_session();
            Application.Current.Properties["email"] = "";
            Application.Current.Properties["password"] = "";
            await Application.Current.SavePropertiesAsync();
        }

        //-----------------------------------------------------


    }
}
