﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using C2Core;
using C2Forms;
using System.Net.Http.Headers;

namespace Bewanted
{
    public enum ResponseType : short
    {
        OK_SUCCESS = 0,
        NO_INTERNET_CONEXION,
        SERVER_ERROR,
        AUTHENTICATION_ERROR,
        SERVICE_ERROR,
        URL_ERROR
    }

    public enum MethodHTTPType : short
    {
        GET = 0,
        MULTIPART,
        POST,
        PUT,
        DELETE

    }

    //public enum StateOfferType
    //{
    //    [Description("NUEVA")]
    //    NUEVA = 1,
    //    [Description("ACEPTADA")]
    //    ACEPTADA = 3,
    //    [Description("RECHAZADA")]
    //    RECHAZADA=2,
    //    [Description("DESCARTADA")]
    //    DESCARTADA =8
    //}
    //-----------------------------------------------------

    public struct ServerResponse
    {
        public ResponseType type;
        public string message;
    }

    //-----------------------------------------------------

    public struct ServerResponse<T> where T : class
    {
        public ResponseType type;
        public string message;
        public T result_object;
    }

    //-----------------------------------------------------

    public class TimeStamp
    {
        public TimeStamp()
        {
            this.milliseconds = 0;
        }

        public long milliseconds;
    }

    //-----------------------------------------------------

    public static class Server
    {
        public static readonly string _APP_VERSION = "1.1";
        public static string device_id;
        //url del API test-entorno pruebas:
        //public static readonly string _URL_BASE_API = "http://pre.api.bewanted.com/";
        //url de producción
        public static readonly string _URL_BASE_API = "https://api.bewanted.com/";
        //metodos del API
        private static readonly string _AUTH_SERVICE_API = "oauth/token";
        //parametros para autentificarse
        private static readonly string _AUTH_GRANT_TYPE_API = "client_credentials";
        private static readonly string _AUTH_CLIENT_ID_API = "2";
        private static readonly string _AUTH_CLIENT_SECRET_API = "2NyjgSacLaKMJg7mwcOoyQgfAp0oCkX4gRCgtWKH";
        //API
        public static string _access_token_API;
        public static DateTime _token_expire_date_API;

        public static readonly string _LOGIN_SERVICE_API = "v1/students/transactions/log-in";
        private static readonly string _FORGOT_PASSWORD_SERVICE_API = "v1/students/transactions/password-recovery";
        private static readonly string _CHANGE_PASSWORD_SERVICE_API = "/change-password";
        ///students/{student_id}/change-password
        private static readonly string _CHECK_EMAIL_SERVICE_API = "v1/students/transactions/check-email";
        private static readonly string _REGISTER_SERVICE_API = "v1/students/transactions/sign-in";


        //url ing_profile
        //https://www.bewanted.com/assets/files/students/user.jpg
        //public static readonly string _URL_IMAGES_API_SOCIAL = "https://www.bewanted.com/assets/images-students/mobile-sites/";
        public static readonly string _URL_IMAGES_API_SOCIAL = "";
        public static readonly string _URL_IMAGES_API = "https://www.bewanted.com/assets/files/students/";
        //public static readonly string _URL_IMAGES_COMPANIES_API = "https://www.bewanted.com/assets/files/companies/";
        //public static readonly string _URL_IMAGES_COMPANIES_API_SOCIAL = "https://www.bewanted.com/assets/images-companies/";
        public static readonly string _URL_IMAGES_COMPANIES_API_SOCIAL = "";


        public static string _CULTURE_COUNTRY_API = "es";
        public static string _LOCALE_API = "/es";

        //ESTUDIANTE
        
        public static readonly string _VIEW_STUDENT_SERVICE_API = "v1/students/";
        public static readonly string _IMG_STUDENT_PROFILE_SERVICE_API = "/profile-img";
        public static readonly string _VIEW_STUDENT_PROFILE_SERVICE_API = "/profile";
        ///students/{student_id}
        //UPDATE A STUDENT-PUT/students/{student_id}
        ///students/{student_id}/profile

        public static readonly string _LOCATION_STUDENT_SERVICE_API = "/location";
        ///students/{student_id}/location LOCALIZACION
        
        public static readonly string _SOCIALS_STUDENT_SERVICE_API = "/social-networks";
        ///students/{student_id}/social-networks


        private static readonly string _INVITATIONS_LIST_SERVICE_API = "/invitations/active";
        ///students/{student_id}/invitations/active
        ///
        //public static readonly string _INVITATION_STUDENT_SERVICE_API = "/invitations/active";
        ///students/{student_id}/invitations/active

        public static readonly string _SOCIALS_SERVICE_API = "v1/students/social-networks";

        public static readonly string _LAGUAGES_STUDENT_SERVICE_API = "/languages";
        ///students/{student_id}/languages
        //DELETE /students/{student_id}/languages/{language_id}
        
        public static readonly string _ACADEMIC_STUDENT_SERVICE_API = "/academic-data";
        ///students/{student_id}/academic-data
        ////DELETE students/{student_id}/academic-data/{academicdata_id}
        
        public static readonly string _EXPERIENCE_STUDENT_SERVICE_API = "/experience";
        ///students/{student_id}/experience

        public static readonly string _COMPANY_STUDENT_SERVICE_API = "v1/student-companies/";
        //////student-companies/{student_company_id}
        public static readonly string _CREATE_COMPANY_STUDENT_SERVICE_API = "v1/student-companies";

        public static readonly string _INTERNSHIPS_STUDENT_SERVICE_API = "/internships";
        ///students/{student_id}/internships

        public static readonly string _VOLUNTARY_STUDENT_SERVICE_API = "/voluntary-services";
        ////students/{student_id}/voluntary-services

        

        public static readonly string _JOBLOCATIONS_STUDENT_SERVICE_API = "/job-locations";
        //students/{student_id}/job-locations
    
        public static readonly string _JOBSECTIONS_STUDENT_SERVICE_API = "/sectors";
        ///students/{student_id}/sectors

        public static readonly string _JOBPOSITIONS_STUDENT_SERVICE_API = "/positions";
        ///students/{student_id}/positions
        //NO DEFINIDO TODAVIA EN EL API-SUSTITUIR PO RL CORRECTO CUANDO NOS LO PASEN


        public static readonly string _OFFER_NOT_READED_API = "/notifications";
        ///students/{student_id}/notifications
        public static readonly string _UNREADED_SERVICE_API = "v1/messaging/conversations/student/";
        //messaging/conversations/student/{student_id}
        
        private static readonly string _OFFER_LIST_SERVICE_API = "v1/messaging/conversations/student/";
        private static readonly string _STUDENT_OFFER_LIST_SERVICE_API = "/invitations/active";
        ///students/{student_id}/invitations/active
        //messaging/conversations/student/{student_id}


        //mensajes de una invitacion

        private static readonly string _INVITATION_MSG_SERVICE_API = "v1/messaging/conversations/invitation/";
        //View a Conversation        GET messaging/conversations/invitation/{invitation_id}
        private static readonly string _INVITATION_MARK_MSG_READ_SERVICE_API = "v1/messaging/conversations/";
        private static readonly string _INVITATION_MARK_MSG_READ_SERVICE_API_2 = "/student-read";
        //PUT messaging/conversations/{invitation_id}/student-read



        private static readonly string _OFFER_MSG_CREATE_SERVICE_API = "v1/messaging";
        //para crear mensaje POST messaging

        


        private static readonly string _OFFERS_SERVICE_API = "v1/offers/";
        private static readonly string _OFFER_STATUS_SERVICE_API = "status/";
        private static readonly string _OFFER_KILLER_SERVICE_API = "/killer-questions/students/";
        private static readonly string _OFFER_ACCEPT_SERVICE_API = "/student-accepts";
        private static readonly string _OFFER_REJECT_SERVICE_API = "/student-rejects";
        public static readonly string _INVITATION_SERVICE_API = "/invitations/";
        private static readonly string _OFFER_KILLER_UPDATE_SERVICE_API = "/killer-questions";
        //offers/status/{id}
        ///offers/{offer_id}/killer-questions/students/{student_id}
        ////offers/{offer_id}/invitations/{student_id}/student-accepts
        ///offers/{offer_id}/invitations/{student_id}/student-rejects
        /////offers/{offer_id}/killer-questions/{killer_question_id}/students/{student_id}
        ///offers/{offer_id}/killer-questions

        //GENERICAS

        private static readonly string _POSITION_SERVICE_API = "v1/positions";
        //positions/{position_id}/{locale}
        //positions/active/{locale}


        private static readonly string _NATIONALITIES_SERVICE_API = "v1/nationalities";
        private static readonly string _COUNTRIES_SERVICE_API = "v1/countries";

        private static readonly string _COUNTRY_SERVICE_API = "v1/countries/";
        ///countries/{country_id}/{locale}

        private static readonly string _STATE_CREATE_SERVICE_API = "v1/states";
        private static readonly string _STATE_SERVICE_API = "v1/states/";
        private static readonly string _STATES_SERVICE_API = "v1/states/active/country/";
        ///states/{state_id}
        ///states/active/country/{country_id}

        private static readonly string _CITY_SERVICE_API = "v1/cities/";
        private static readonly string _CITIES_SERVICE_API = "v1/cities/active/state/";
        //cities/active/state/{state_id}
        ///cities/{city_id}

        
        private static readonly string _UNIVERSITY_SERVICE_API = "v1/universities";
        ///POST universities
        ///universities/{university_id}
        ///universities/active/country/{country_id}[/state/{state_id}]

        private static readonly string _STUDY_SERVICE_API = "v1/studies";
        ///studies/{study_id}
        ///POST /studies
        private static readonly string _STUDY_LEVEL_SERVICE_API = "v1/study-levels";
        //GET /study-levels/{locale}

        private static readonly string _GROUPS_STUDY_SERVICE_API = "v1/group-studies/active";
        ///group-studies/active/{locale}

        private static readonly string _SUBJECTS_STUDY_SERVICE_API = "v1/subjects";
        //subjects/active/study/{study_id}

        private static readonly string _SECTORS__ACTIVE_SERVICE_API = "v1/sectors/active";
        private static readonly string _SECTORS_SERVICE_API = "v1/sectors/";
        ///sectors/active/{locale}
        ///sectors/{sector_id}/{locale}

        private static readonly string _COMPANY_LIST_SERVICE_API = "v1/companies";
        private static readonly string _COMPANY_LIST_ORDERED_SERVICE_API = "v1/companies/list";
        private static readonly string _SOCIALS_COMPANY_SERVICE_API = "/social-networks";
        private static readonly string _USER_COMPANY_SERVICE_API = "/user/";
        private static readonly string _COMPANY_SECTORS_SERVICE_API = "/sectors";
        //companies/ordered
        //companies/{id}/sectors/{locale}-List all Sectors of a Company
        //companies/{id}-View company
        //companies/{id}/social-networks
        //companies/{id}/user/{user_id}


        private static readonly string _LANGUAGES_SERVICE_API = "v1/languages";
        private static readonly string _LANGUAGES_LEVEL_SERVICE_API = "v1/language-levels";
        ///languages/{language_id}/{locale}
        ///language-levels/{locale}
        ///language-levels/{language_level_id}/{locale}
        private static readonly string _LANGUAGES_CERTTIFICATES_SERVICE_API = "v1/certificates/language/";
        private static readonly string _LANGUAGES_CERTTIFICATE_SERVICE_API = "v1/certificates";
        ///certificates/{certificate_id}-view certificate
        ///certificates/language/{language_id}-list certificates by language


        


        //url del entorno de produccion:
        public static readonly string _URL_BASE = "https://www.bewanted.com/";
        //url del entorno de pruebas:
        //public static readonly string _URL_BASE = "http://alumniv2.f2fbeta.com/";
             

        private static readonly string _AUTH_SERVICE = "oauth/access_token";
        private static readonly string _AUTH_GRANT_TYPE = "client_credentials";
        private static readonly string _AUTH_CLIENT_ID = "1001";
        private static readonly string _AUTH_CLIENT_SECRET = "4e86c28983aacc5eb95cf88c851a16c9";
        private static readonly string _AUTH_SCOPE = "servermobile";
        public static readonly string _SUFFIX_MD5 = "Alumni_2016";
        //private static readonly string _LOGIN_SERVICE = "services/student/login";
        //private static readonly string _LOGIN_SERVICE = "services/student/login2";
        //private static readonly string _LOGIN_SERVICE = "services/student/login3";
        //private static readonly string _FORGOT_PASSWORD_SERVICE = "services/student/forgot-password";
        //private static readonly string _NATIONALITIES_SERVICE = "services/common/nationalities";
        //private static readonly string _COUNTRIES_SERVICE = "services/common/countries";
        //private static readonly string _STATES_SERVICE = "services/common/states";
        //private static readonly string _CITIES_SERVICE = "services/common/cities";
        //private static readonly string _GROUPS_STUDY_SERVICE = "services/common/group-studies";
        //private static readonly string _STUDIES_SERVICE = "services/common/studies";
        //private static readonly string _SUBJECTS_SERVICE = "services/common/subjects";
        //private static readonly string _UNIVERSITIES_SERVICE = "services/common/universities";
        //private static readonly string _POSITIONS_SERVICE = "services/common/positions";
        //private static readonly string _SECTORS_SERVICE = "services/common/sectors";
        //private static readonly string _COMPANY_SECTORS_SERVICE = "services/common/company-sectors";
        //private static readonly string _LANGUAGES_SERVICE = "services/common/languages";
        //private static readonly string _LEVELS_SERVICE = "services/common/language-levels";
        //private static readonly string _CERTIFICATES_SERVICE = "services/common/certificates";
        //private static readonly string _COMPANIES_SERVICE = "services/common/companies";
        //private static readonly string _REGISTER_SERVICE = "services/student/register";
        //private static readonly string _REGISTER_SERVICE = "services/student/register2";
        //public static readonly string _UNREADED_SERVICE = "services/offer/get-not-readed-messages";
        
        //private static readonly string _OFFER_LIST_SERVICE = "services/offer/list";
        //private static readonly string _OFFER_LIST_SERVICE = "services/offer/list2";
        //private static readonly string _OFFER_DETAIL_SERVICE = "services/offer/detail";
        //private static readonly string _OFFER_ACTION_SERVICE = "services/offer/offer-action";
        //private static readonly string _OFFER_MESSAGE_SERVICE = "services/offer/send-message";
        //private static readonly string _OFFER_MARK_SERVICE = "services/offer/mark-as-readed";
        //public static readonly string _OFFER_NOT_READED = "services/offer/get-not-readed-offers";
        //private static readonly string _PROFILE_CV_SERVICE = "services/profile/cv";
        //private static readonly string _PROFILE_PERCENTAJE_SERVICE = "services/profile/percentaje";
        //private static readonly string _PROFILE_GET_SERVICE = "services/profile/get";
        //private static readonly string _PROFILE_SAVE_SERVICE = "services/profile/save";
        //private static readonly string _PROFILE_DELETE_SERVICE = "services/profile/delete";
        //private static readonly string _COMPANY_LIST_SERVICE = "services/company/list";
        //private static readonly string _COMPANY_LIST_SERVICE = "services/company/list2";
        //private static readonly string _COMPANY_DETAIL_SERVICE = "services/company/detail";
        //private static readonly string _SETTINGS_SERVICE = "services/settings/update-settings";
        //private static readonly string _SETTINGS_SERVICE = "services/settings/update-settings2";
        private static readonly string _LEGAL_PAGE = "legals-app";
        //public static readonly string _NOTIFICATIONS_SERVICES = "services/student/notifications";
        //public static readonly string _READ_NOTIFICATIONS_SERVICES = "services/student/read-notification";

        
        private static TimeStamp _TIME_STAMP = new TimeStamp();

        //-----------------------------------------------------

        public static string i_bool(bool value)
        {
            if (value == true)
                return "1";
            else
                return "0";
        }

        public static bool i_valorbool(string value)
        {
            if (value == "" || value == null || value == "0" || value == "false")
                return false;
            else {
                if (value == "1" || value == "true")
                    return true;
                else
                    return false;
            } 
        }
        //-----------------------------------------------------

        static string i_float(float value)
        {
            string str = value.ToString();
            return str.Replace(',', '.');
        }

        //-----------------------------------------------------

        private static FormUrlEncodedContent i_Get_content(List<string> key_list, List<string> value_list)
        {
            List<KeyValuePair<string, string>> list;
            FormUrlEncodedContent content = null;
            C2Assert.Error(key_list != null && value_list != null && key_list.Count == value_list.Count);

            list = new List<KeyValuePair<string, string>>();

            for (int i = 0; i < key_list.Count; i++)
                list.Add(new KeyValuePair<string, string>(key_list[i], value_list[i]));


            try
            {
                content = new FormUrlEncodedContent(list);
            }
            catch (Exception e)
            {
                content = null;
            }

            return content;
        }

        //private static MultipartFormDataContent i_Get_content_multipart(List<string> key_list, List<string> value_list)
        //{
        //    List<KeyValuePair<string, string>> list;
        //    MultipartFormDataContent content = null;
        //    FormUrlEncodedContent contenthtpp = null;
        //    content = new MultipartFormDataContent();
        //    C2Assert.Error(key_list != null && value_list != null && key_list.Count == value_list.Count);

        //    list = new List<KeyValuePair<string, string>>();

        //    for (int i = 0; i < key_list.Count; i++)
        //        list.Add(new KeyValuePair<string, string>(key_list[i], value_list[i]));


        //    try
        //    {
        //        contenthtpp = new FormUrlEncodedContent(list);
        //        content.Add(contenthtpp);
        //    }
        //    catch (Exception e)
        //    {
        //        content = null;
        //    }

        //    return content;
        //}

        private static string i_Get_content_QueryString(List<string> key_list, List<string> value_list)
        {
            string query;
            List<KeyValuePair<string, string>> list;
            FormUrlEncodedContent content = null;
            C2Assert.Error(key_list != null && value_list != null && key_list.Count == value_list.Count);

            list = new List<KeyValuePair<string, string>>();

            for (int i = 0; i < key_list.Count; i++)
                list.Add(new KeyValuePair<string, string>(key_list[i], value_list[i]));


            try
            {
                content = new FormUrlEncodedContent(list);
            }
            catch (Exception e)
            {
                content = null;
            }
            query = content.ReadAsStringAsync().Result;
            return query;
        }


        private static StringContent i_Get_content_JSONString(List<string> key_list, List<string> value_list)
        {
            
            C2Assert.Error(key_list != null && value_list != null && key_list.Count == value_list.Count);

            StringContent content;
            StringBuilder stream;
            stream = new StringBuilder();
            stream.Append("{");
            for (int i = 0; i < key_list.Count; i++) {
            {
                    stream.Append("\""+ key_list[i] +"\":");
                    if (value_list[i].Contains("[") ||  value_list[i].Contains(":{"))
                    {
                        stream.Append(value_list[i]);
                    }
                    else {
                        stream.Append("\"" + value_list[i] + "\"");
                    }
                    
                    if (i < key_list.Count - 1)
                        stream.Append(",");
                    
                }
            }
            stream.Append("}");
            
            try
            {
             //   var data = JsonConvert.SerializeObject(list);
                content = new StringContent(stream.ToString(), Encoding.UTF8, "application/json");

            }
            catch (Exception e)
            {
                content = null;
            }
            
            return content;
        }

        //-----------------------------------------------------
        private static T i_ParseJSONString<T>(string json_string) where T : class
        {
            try
            {
                System.Runtime.Serialization.Json.DataContractJsonSerializer json;
                MemoryStream stream;
                T obj;
                //json_string.Replace("\"https:\\", "\"");
                json = new System.Runtime.Serialization.Json.DataContractJsonSerializer(typeof(T));
                stream = new MemoryStream(System.Text.Encoding.UTF8.GetBytes(json_string));
                obj = (T)json.ReadObject(stream);
                return obj;
            }
            catch (Exception e)
            {
                return null;
            }
        }

        //HttpResponseMessage
        //private static T i_ParseJSON<T>(string json_string) where T : class
        private static async Task<T> i_ParseJSON<T>(HttpResponseMessage json_string) where T : class
        {
            try
            {
                System.Runtime.Serialization.Json.DataContractJsonSerializer json;
                //MemoryStream stream;
                T obj;
                json = new System.Runtime.Serialization.Json.DataContractJsonSerializer(typeof(T));
                
                //stream = new MemoryStream(System.Text.Encoding.UTF8.GetBytes(json_string));
                //obj = (T)json.ReadObject(stream);
                obj = (T)json.ReadObject(await json_string.Content.ReadAsStreamAsync());
                return obj;
            }
            catch (Exception e)
            {
                return null;
            }
        }

        //-----------------------------------------------------

        private static ResponseType i_Service_error_code(long code)
        {
            return ResponseType.SERVICE_ERROR;
        }

        //-----------------------------------------------------

        private static ServerResponse<T> i_ParseErrorJSON<T>(string json_string) where T : class
        {
            ServerResponse<T> result;

            try
            {
                System.Runtime.Serialization.Json.DataContractJsonSerializer json;
                MemoryStream stream;
                ServerErrorJSON obj;
                json = new System.Runtime.Serialization.Json.DataContractJsonSerializer(typeof(ServerErrorJSON));

                stream = new MemoryStream(System.Text.Encoding.UTF8.GetBytes(json_string));
                obj = (ServerErrorJSON)json.ReadObject(stream);
                result.type = i_Service_error_code(obj.error.code);
                result.message = obj.error.message;
            }
            catch (Exception e)
            {
                result.type = ResponseType.SERVICE_ERROR;
                result.message = e.Message;
            }

            result.result_object = null;
            return result;
        }

                
        //-----------------------------------------------------
        public static async Task<ServerResponse<T>> i_Get_server_data_API<T>(string url, List<string> key_list, List<string> value_list, MethodHTTPType metodoHTTP) where T : class
        {
            ServerResponse<T> response;
            string request_string_debug;
            HttpResponseMessage http_response=null;
            try
            {
                //Uri baseAddress;
                Uri uri;
                string query;
                HttpContent content;
                

                HttpClient http_client;
                uri = new Uri(url);
                http_client = new HttpClient();
                content = null;
                response.type = ResponseType.URL_ERROR;
                response.message = "";
                response.result_object = null;
                
        
                http_client.DefaultRequestHeaders.Add("user-agent", "Mozilla/5.0 (compatible; MSIE 10.0; Windows NT 6.2; WOW64; Trident/6.0)");

                if (url.Contains(_AUTH_SERVICE_API)==false){
                    http_client.DefaultRequestHeaders.Add("Authorization", "Bearer " + _access_token_API);
                }

                switch (metodoHTTP)
                {
                    case MethodHTTPType.GET:
                        if (key_list != null)
                        {
                            query = Server.i_Get_content_QueryString(key_list, value_list);
                            request_string_debug = query;
                            http_response = await http_client.GetAsync(url+"?"+query);
                        }
                        else {
                            http_response = await http_client.GetAsync(uri);
                        }
                        
                        http_response.EnsureSuccessStatusCode();
                        response.type = ResponseType.OK_SUCCESS;
                        response.message = await http_response.Content.ReadAsStringAsync();
                        break;
                    
                    case MethodHTTPType.POST:
                        if (key_list != null)
                        {
                            content = Server.i_Get_content(key_list, value_list);
                            request_string_debug = await content.ReadAsStringAsync();
                        }
                        http_response = await http_client.PostAsync(uri, content);
                        http_response.EnsureSuccessStatusCode();
                        response.type = ResponseType.OK_SUCCESS;
                        response.message = await http_response.Content.ReadAsStringAsync();
                        break;
                    case MethodHTTPType.PUT:
                        if (key_list != null)
                        {
                            //content = Server.i_Get_content(key_list, value_list);
                            content = Server.i_Get_content_JSONString(key_list, value_list);
                            request_string_debug = await content.ReadAsStringAsync();
                        }
                        http_response = await http_client.PutAsync(uri, content);
                        http_response.EnsureSuccessStatusCode();
                        response.type = ResponseType.OK_SUCCESS;
                        response.message = await http_response.Content.ReadAsStringAsync();
                        break;
                    case MethodHTTPType.DELETE:
                        http_response = await http_client.DeleteAsync(uri);
                        http_response.EnsureSuccessStatusCode();
                        response.type = ResponseType.OK_SUCCESS;
                        response.message = await http_response.Content.ReadAsStringAsync();
                        break;
                    default:
                        http_response = await http_client.GetAsync(uri);
                        http_response.EnsureSuccessStatusCode();
                        response.type = ResponseType.OK_SUCCESS;
                        response.message = await http_response.Content.ReadAsStringAsync();
                        break;
                }
                response.result_object = null;

            }
            catch (System.Net.WebException e)
            {
                response.type = ResponseType.NO_INTERNET_CONEXION;
                response.message = (e != null) ? e.Message : "";
                if (response.message == "" || response.message == null)
                {
                    response.message = (http_response.ReasonPhrase != null) ? e.Message : "";
                }
                response.result_object = null;
            }
            catch (Exception e)
            {
                response.type = ResponseType.SERVER_ERROR;
                response.message = (e != null) ? e.Message : "";
                if (response.message == "" || response.message == null) {
                    response.message = (http_response.ReasonPhrase != null) ? e.Message : "";
                }
                
                response.result_object = null;
            }

            if (response.type == ResponseType.OK_SUCCESS)
            {
                //response.result_object = await i_ParseJSON<T>(http_response);
                //response.result_object = i_ParseJSONString<T>(response.message);

                string[] splitstring = response.message.Split(new string[] { "<!DOCTYPE html>" }, StringSplitOptions.None);
                if (splitstring.Length > 0)
                {
                    response.result_object = i_ParseJSONString<T>(splitstring[0]);
                }
                else
                {
                    response.result_object = i_ParseJSONString<T>(response.message);
                }

                if (response.result_object == null)

                    response = i_ParseErrorJSON<T>(response.message);
            }

            return response;
        }


        public static async Task<ServerResponse<T>> i_Get_server_img_data_API<T>(string url, List<string> key_list, List<string> value_list, byte[] imagebytes64) where T : class
        {
            ServerResponse<T> response;
            string request_string_debug;
            HttpResponseMessage http_response = null;
            try
            {
                
                Uri uri;
                MultipartFormDataContent contentmultipart;
                HttpClient http_client;
                uri = new Uri(url);
                http_client = new HttpClient();
                response.type = ResponseType.URL_ERROR;
                response.message = "";
                response.result_object = null;


                http_client.DefaultRequestHeaders.Add("user-agent", "Mozilla/5.0 (compatible; MSIE 10.0; Windows NT 6.2; WOW64; Trident/6.0)");

                if (url.Contains(_AUTH_SERVICE_API) == false)
                {
                    http_client.DefaultRequestHeaders.Add("Authorization", "Bearer " + _access_token_API);
                }
                
                http_client.DefaultRequestHeaders.TransferEncodingChunked = true;
                contentmultipart = new MultipartFormDataContent();
                
                var fileContent = new ByteArrayContent(imagebytes64);
                

                fileContent.Headers.ContentType = MediaTypeHeaderValue.Parse("image/jpeg");
                fileContent.Headers.ContentDisposition = new ContentDispositionHeaderValue("form-data")
                {
                    Name = "file",
                    FileName= "my_uploaded_image.jpg"
                };


                for (int i = 0; i < key_list.Count; i++)
                {
                    contentmultipart.Add(new StringContent(value_list[i]),
                        String.Format("\"{0}\"", key_list[i]));
                }

                
                contentmultipart.Add(fileContent, String.Format("\"{0}\"", "file"), "my_uploaded_image.jpg");

                
                request_string_debug = await contentmultipart.ReadAsStringAsync();
                http_response = await http_client.PostAsync(uri, contentmultipart);
                http_response.EnsureSuccessStatusCode();
                response.type = ResponseType.OK_SUCCESS;
                response.message = await http_response.Content.ReadAsStringAsync();

                response.result_object = null;

            }
            catch (System.Net.WebException e)
            {
                response.type = ResponseType.NO_INTERNET_CONEXION;
                response.message = (e != null) ? e.Message : "";
                if (response.message == "" || response.message == null)
                {
                    response.message = (http_response.ReasonPhrase != null) ? e.Message : "";
                }
                response.result_object = null;
            }
            catch (Exception e)
            {
                response.type = ResponseType.SERVER_ERROR;
                response.message = (e != null) ? e.Message : "";
                if (response.message == "" || response.message == null)
                {
                    response.message = (http_response.ReasonPhrase != null) ? e.Message : "";
                }

                response.result_object = null;
            }

            if (response.type == ResponseType.OK_SUCCESS)
            {
                //response.result_object = await i_ParseJSON<T>(http_response);
                //response.result_object = i_ParseJSONString<T>(response.message);

                string[] splitstring = response.message.Split(new string[] { "<!DOCTYPE html>" }, StringSplitOptions.None);
                if (splitstring.Length > 0)
                {
                    response.result_object = i_ParseJSONString<T>(splitstring[0]);
                }
                else
                {
                    response.result_object = i_ParseJSONString<T>(response.message);
                }

                if (response.result_object == null)

                    response = i_ParseErrorJSON<T>(response.message);
            }

            return response;
        }

        public static async Task<ServerResponse<AuthJSON>> Authentication_request_API()
        {
            List<string> key_list, value_list;
            key_list = new List<string>();
            value_list = new List<string>();
            key_list.Add("grant_type");
            value_list.Add(_AUTH_GRANT_TYPE);
            key_list.Add("client_id");
            value_list.Add(_AUTH_CLIENT_ID_API);
            key_list.Add("client_secret");
            value_list.Add(_AUTH_CLIENT_SECRET_API);
            return await Server.i_Get_server_data_API<AuthJSON>(_URL_BASE_API + _AUTH_SERVICE_API, key_list, value_list,MethodHTTPType.POST);
        }

        //-----------------------------------------------------

        public static long i_Date_to_unix_milliseconds(DateTime date)
        {
            long milliseconds;
            var epoch = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
            milliseconds = Convert.ToInt64((date.ToUniversalTime() - epoch).TotalSeconds) * 1000;

            lock(Server._TIME_STAMP)
            {
                if (Server._TIME_STAMP.milliseconds >= milliseconds)
                    Server._TIME_STAMP.milliseconds += 1;
                else
                    Server._TIME_STAMP.milliseconds = milliseconds;
            }

            return Server._TIME_STAMP.milliseconds;
        }
        public static void i_Get_common_request_params_API(long user_id, ref List<string> key_list, ref List<string> value_list)
        {
        
            C2Assert.Is_null(key_list);
            C2Assert.Is_null(value_list);
            key_list = new List<string>();
            value_list = new List<string>();
            key_list.Add("referrer_student_id");
            value_list.Add(user_id.ToString());
        }

        //-----------------------------------------------------
        public static async Task<ServerResponse<LoginDataJSON>> Login_request(string access_token, string email, string password, long user_id)
        {
            
            List<string> key_list = null;
            List<string> value_list = null;
            _access_token_API = access_token;
            device_id = Xamarin.Forms.DependencyService.Get<IDeviceService>().DeviceId();
            string device_type = Xamarin.Forms.Device.OnPlatform("iphone", "android", "win");
            key_list = new List<string>();
            value_list = new List<string>();
            key_list.Add("email");
            value_list.Add(email);
            key_list.Add("password");
            value_list.Add(password);
            key_list.Add("device_token");
            value_list.Add(AppDelegate._device_token == null ? "" : AppDelegate._device_token);
            key_list.Add("device_type");
            value_list.Add(device_type);
            key_list.Add("device_id");
            value_list.Add(device_id);

            return await Server.i_Get_server_data_API<LoginDataJSON>(_URL_BASE_API + _LOGIN_SERVICE_API, key_list, value_list,MethodHTTPType.POST);
        }

        public static async Task<ServerResponse<StudentDataJSON>> Student_request(string access_token, long user_id)
        {
            
            List<string> key_list = null;
            List<string> value_list = null;
            _access_token_API = access_token;
            return await Server.i_Get_server_data_API<StudentDataJSON>(_URL_BASE_API + _VIEW_STUDENT_SERVICE_API + user_id.ToString(), key_list, value_list, MethodHTTPType.GET);
        }

        public static async Task<ServerResponse<StudentDataJSON>> Student_profile_request(string access_token, long user_id)
        {
            List<string> key_list = null;
            List<string> value_list = null;
            _access_token_API = access_token;
            return await Server.i_Get_server_data_API<StudentDataJSON>(_URL_BASE_API + _VIEW_STUDENT_SERVICE_API + user_id.ToString()+ _VIEW_STUDENT_PROFILE_SERVICE_API, key_list, value_list, MethodHTTPType.GET);
        }

        public static async Task<ServerResponse<List<MerchantJobLocationJSON>>> student_merchant_job_location_request(string access_token, long user_id)
        {

            List<string> key_list = null;
            List<string> value_list = null;
            _access_token_API = access_token;
            return await Server.i_Get_server_data_API<List<MerchantJobLocationJSON>>(_URL_BASE_API + _VIEW_STUDENT_SERVICE_API + user_id.ToString() + _JOBLOCATIONS_STUDENT_SERVICE_API, key_list, value_list, MethodHTTPType.GET);
        }

        public static async Task<ServerResponse<List<MerchantSectorJSON>>> student_merchant_job_sections_request(string access_token, long user_id)
        {

            List<string> key_list = null;
            List<string> value_list = null;
            _access_token_API = access_token;
            return await Server.i_Get_server_data_API<List<MerchantSectorJSON>>(_URL_BASE_API + _VIEW_STUDENT_SERVICE_API + user_id.ToString() + _JOBSECTIONS_STUDENT_SERVICE_API, key_list, value_list, MethodHTTPType.GET);
        }

        public static async Task<ServerResponse<List<MerchantPositionJSON>>> student_merchant_job_positions_request(string access_token, long user_id)
        {

            List<string> key_list = null;
            List<string> value_list = null;
            _access_token_API = access_token;
            return await Server.i_Get_server_data_API<List<MerchantPositionJSON>>(_URL_BASE_API + _VIEW_STUDENT_SERVICE_API + user_id.ToString() + _JOBPOSITIONS_STUDENT_SERVICE_API, key_list, value_list, MethodHTTPType.GET);
        }

        public static async Task<ServerResponse<StudentDataJSON>> Percentaje_request(string access_token, long user_id)
        {
            List<string> key_list = null;
            List<string> value_list = null;
            _access_token_API = access_token;
            return await Server.i_Get_server_data_API<StudentDataJSON>(_URL_BASE_API + _VIEW_STUDENT_SERVICE_API + user_id.ToString(), key_list, value_list,MethodHTTPType.GET);
        }

        //student_location_json
        public static async Task<ServerResponse<StudentLocationJSON>> student_location_request(string access_token, long user_id)
        {

            List<string> key_list = null;
            List<string> value_list = null;
            _access_token_API = access_token;
            return await Server.i_Get_server_data_API<StudentLocationJSON>(_URL_BASE_API + _VIEW_STUDENT_SERVICE_API + user_id.ToString()+_LOCATION_STUDENT_SERVICE_API, key_list, value_list, MethodHTTPType.GET);
        }

        public static async Task<ServerResponse<List<SocialJSON>>> social_request(string access_token)
        {

            List<string> key_list = null;
            List<string> value_list = null;
            _access_token_API = access_token;
            key_list = new List<string>();
            value_list = new List<string>();
            key_list.Add("column");
            value_list.Add("order");
            key_list.Add("order");
            value_list.Add("asc");
            return await Server.i_Get_server_data_API<List<SocialJSON>>(_URL_BASE_API + _SOCIALS_SERVICE_API, key_list, value_list, MethodHTTPType.GET);
        }

        
        //social_company_request-companies/{id}/social-networks
        public static async Task<ServerResponse<List<CompanySocialJSON>>> company_social_request(string access_token, string company_id)
        {
            List<string> key_list = null;
            List<string> value_list = null;
            _access_token_API = access_token;
            return await Server.i_Get_server_data_API<List<CompanySocialJSON>>(_URL_BASE_API + _COMPANY_LIST_SERVICE_API + "/"+company_id + _SOCIALS_COMPANY_SERVICE_API, key_list, value_list, MethodHTTPType.GET);
        }
        ////companies/{id}/user/{user_id}
        public static async Task<ServerResponse<OfferUserDataJSON>> user_company_request(string access_token, string company_id, string user_id)
        {
            List<string> key_list = null;
            List<string> value_list = null;
            _access_token_API = access_token;
            return await Server.i_Get_server_data_API<OfferUserDataJSON>(_URL_BASE_API + _COMPANY_LIST_SERVICE_API + "/" + company_id + _USER_COMPANY_SERVICE_API+user_id, key_list, value_list, MethodHTTPType.GET);
        }
        public static async Task<ServerResponse<List<SocialJSON>>> student_social_request(string access_token, long user_id)
        {

            List<string> key_list = null;
            List<string> value_list = null;
            _access_token_API = access_token;
            return await Server.i_Get_server_data_API<List<SocialJSON>>(_URL_BASE_API + _VIEW_STUDENT_SERVICE_API+user_id.ToString()+ _SOCIALS_STUDENT_SERVICE_API, key_list, value_list, MethodHTTPType.GET);
        }

        //-----------------------------------------------------
        //check_email_request
        public static async Task<ServerResponse<CheckEmailJSON>> check_email_request(string access_token, string email)
        {
            List<string> key_list = null;
            List<string> value_list = null;
            key_list = new List<string>();
            value_list = new List<string>();
            _access_token_API = access_token;
            key_list.Add("email");
            value_list.Add(email);
            return await Server.i_Get_server_data_API<CheckEmailJSON>(_URL_BASE_API + _CHECK_EMAIL_SERVICE_API, key_list, value_list, MethodHTTPType.POST);
        }

        //public static async Task<ServerResponse<RegisterJSON>> Register_request(string access_token, string email, string password, string name, string last_name, string phone, long nationality_id, long country_id, long state_id, long city_id, string birthdate)
        public static async Task<ServerResponse<RegisterJSON>> Register_request(string access_token, string email, string password, string name, string last_name)
        {
            List<string> key_list = null;
            List<string> value_list = null;
            key_list = new List<string>();
            value_list = new List<string>();
            _access_token_API = access_token;
            key_list.Add("email");
            value_list.Add(email);
            key_list.Add("password");
            value_list.Add(password);
            key_list.Add("first_name");
            value_list.Add(name);
            key_list.Add("last_name");
            value_list.Add(last_name);
            return await Server.i_Get_server_data_API<RegisterJSON>(_URL_BASE_API + _REGISTER_SERVICE_API, key_list, value_list,MethodHTTPType.POST);
        }

        //-----------------------------------------------------

        
        public static async Task<ServerResponse<UnreadedJSON>> Unreaded_request(string access_token, long user_id)
        {
            List<string> key_list = null;
            List<string> value_list = null;
            _access_token_API = access_token;
            return await Server.i_Get_server_data_API<UnreadedJSON>(_URL_BASE_API + _UNREADED_SERVICE_API +user_id.ToString(), key_list, value_list,MethodHTTPType.GET);
        }

        //-----------------------------------------------------

        public static async Task<ServerResponse<SaveDataJSON>> Recovery_password_request(string access_token, string email)
        {
            
            List<string> key_list = null;
            List<string> value_list = null;
            key_list = new List<string>();
            value_list = new List<string>();
            key_list.Add("email");
            value_list.Add(email);
            _access_token_API = access_token;
            return await Server.i_Get_server_data_API<SaveDataJSON>(_URL_BASE_API + _FORGOT_PASSWORD_SERVICE_API, key_list, value_list, MethodHTTPType.POST);
        }

        //-----------------------------------------------------

        public static async Task<ServerResponse<List<CountryJSON>>> Nationalities_request(string access_token)
        {
            List<string> key_list = null;
            List<string> value_list = null;
            _access_token_API=access_token;
            key_list = new List<string>();
            value_list = new List<string>();
            key_list.Add("pagination");
            value_list.Add("false");
            key_list.Add("column");
            value_list.Add("orden");
            key_list.Add("order");
            value_list.Add("asc");
            return await Server.i_Get_server_data_API<List<CountryJSON>>(_URL_BASE_API + _NATIONALITIES_SERVICE_API+_LOCALE_API, key_list, value_list, MethodHTTPType.GET);
            

        }
        //-----------------------------------------------------

        public static async Task<ServerResponse<List<CountryJSON>>> Countries_request(string access_token)
        {
            List<string> key_list = null;
            List<string> value_list = null;
            key_list = new List<string>();
            value_list = new List<string>();
            key_list.Add("pagination");
            value_list.Add("false");
            key_list.Add("column");
            value_list.Add("orden");
            key_list.Add("order");
            value_list.Add("asc");
            _access_token_API = access_token;
            return  await Server.i_Get_server_data_API<List<CountryJSON>>(_URL_BASE_API + _COUNTRIES_SERVICE_API+_LOCALE_API, key_list, value_list, MethodHTTPType.GET);
        }

        //-----------------------------------------------------
        ///positions/active/{locale}
        public static async Task<ServerResponse<List<PositionJSON>>> Positions_request(string access_token, long user_id, string search_text)
        {
            List<string> key_list = null;
            List<string> value_list = null;
            string pathurl = _POSITION_SERVICE_API + "/active"+_LOCALE_API;
            _access_token_API = access_token;
            key_list = new List<string>();
            value_list = new List<string>();
            key_list.Add("pagination");
            value_list.Add("false");
            key_list.Add("search[name]");
            value_list.Add(search_text);
            return  await Server.i_Get_server_data_API<List<PositionJSON>>(_URL_BASE_API + pathurl, key_list, value_list, MethodHTTPType.GET);
           
        }

        //-----------------------------------------------------

        public static async Task<ServerResponse<List<GroupStudyJSON>>> Groups_study_request(string access_token, long user_id)
        {
            List<string> key_list = null;
            List<string> value_list = null;
            _access_token_API = access_token;
            key_list = new List<string>();
            value_list = new List<string>();
            key_list.Add("pagination");
            value_list.Add("false");
            key_list.Add("column");
            value_list.Add("name");
            key_list.Add("order");
            value_list.Add("asc");
            return await Server.i_Get_server_data_API<List<GroupStudyJSON>>(_URL_BASE_API + _GROUPS_STUDY_SERVICE_API + _LOCALE_API, key_list, value_list, MethodHTTPType.GET);
         }




        //-----------------------------------------------------
        ///subjects/active
        ///subjects/active/study/{study_id}
        public static async Task<ServerResponse<List<SubjectJSON>>> Subjects_request(string access_token, long user_id, int study_id, string search_text)
        {
            List<string> key_list = null;
            List<string> value_list = null;
            string pathurl = _SUBJECTS_STUDY_SERVICE_API + "/active";
            _access_token_API = access_token;
            key_list = new List<string>();
            value_list = new List<string>();
            key_list.Add("pagination");
            value_list.Add("false");
            key_list.Add("search[name]");
            value_list.Add(search_text);
            if (study_id.ToString() != "")
            {
                pathurl = pathurl + "/study/" + study_id.ToString();
            }
            return await Server.i_Get_server_data_API<List<SubjectJSON>>(_URL_BASE_API + pathurl, key_list, value_list, MethodHTTPType.GET);

            
        }


        //-----------------------------------------------------
        ////studies/active
        ////studies/active/university/{university_id}
        ////studies/active/group-study/{group_study_id}
        ///studies/active/university/{university_id}/group-study/{group_study_id}
        public static async Task<ServerResponse<List<StudyJSON>>> Studies_request(string access_token, long user_id, int university_id, int group_study_id, string search_text)
        {
            List<string> key_list = null;
            List<string> value_list = null;
            string pathurl = _STUDY_SERVICE_API + "/active";
            _access_token_API = access_token;
            key_list = new List<string>();
            value_list = new List<string>();
            key_list.Add("pagination");
            value_list.Add("false");
            key_list.Add("search[name]");
            value_list.Add(search_text);
            if (university_id.ToString() != "")
            {
                pathurl = pathurl + "/university/" + university_id.ToString();
            }
            if (group_study_id.ToString() != "")
            {
                pathurl = pathurl + "/group-study/" + group_study_id.ToString();
            }

            return await Server.i_Get_server_data_API<List<StudyJSON>>(_URL_BASE_API + pathurl, key_list, value_list,MethodHTTPType.GET);

        }
        //-----------------------------------------------------
        //universities/active/country/{country_id} [/state/{state_id}]
        ///universities/active
        public static async Task<ServerResponse<List<UniversityJSON>>> Universities_request(string access_token, long country_id, long state_id, string search_text)
        {
            
            List<string> key_list = null;
            List<string> value_list = null;
            string pathurl = _UNIVERSITY_SERVICE_API + "/active";
            _access_token_API = access_token;
            key_list = new List<string>();
            value_list = new List<string>();
            key_list.Add("pagination");
            value_list.Add("false");
            key_list.Add("search[name]");
            value_list.Add(search_text);
            if (country_id.ToString() != "")
            {
                pathurl = pathurl + "/country/" + country_id.ToString();
                if (state_id.ToString() != "")
                {
                    pathurl = pathurl + "/state/" + state_id.ToString();
                }
            }
            return await Server.i_Get_server_data_API<List<UniversityJSON>>(_URL_BASE_API + pathurl, key_list, value_list, MethodHTTPType.GET);
        }

        public static async Task<ServerResponse<CountryJSON>> Country_request(string access_token, string country_id)
        {
            List<string> key_list = null;
            List<string> value_list = null;
            _access_token_API = access_token;
            return await Server.i_Get_server_data_API<CountryJSON>(_URL_BASE_API + _COUNTRY_SERVICE_API + country_id + _LOCALE_API, key_list, value_list, MethodHTTPType.GET); ;

        }

        public static async Task<ServerResponse<List<StateJSON>>> States_request(string access_token, long country_id, string search_text)
        {

            List<string> key_list = null;
            List<string> value_list = null;
            _access_token_API = access_token;
            key_list = new List<string>();
            value_list = new List<string>();
            key_list.Add("pagination");
            value_list.Add("false");
            key_list.Add("search[name]");
            value_list.Add(search_text);
            return await Server.i_Get_server_data_API<List<StateJSON>>(_URL_BASE_API + _STATES_SERVICE_API + country_id.ToString(), key_list, value_list, MethodHTTPType.GET);
        }

        public static async Task<ServerResponse<StateJSON>> State_request(string access_token, string state_id)
        {
            List<string> key_list = null;
            List<string> value_list = null;
            _access_token_API = access_token;
            return await Server.i_Get_server_data_API<StateJSON>(_URL_BASE_API + _STATE_SERVICE_API + state_id , key_list, value_list, MethodHTTPType.GET); ;

        }

        //-----------------------------------------------------

        public static async Task<ServerResponse<List<CityJSON>>> Cities_request(string access_token, long state_id, string search_text)
        {

            List<string> key_list = null;
            List<string> value_list = null;
            _access_token_API = access_token;
            key_list = new List<string>();
            value_list = new List<string>();
            key_list.Add("pagination");
            value_list.Add("false");
            key_list.Add("search[name]");
            value_list.Add(search_text);
            return await Server.i_Get_server_data_API<List<CityJSON>>(_URL_BASE_API + _CITIES_SERVICE_API + state_id.ToString(), key_list, value_list, MethodHTTPType.GET);
        }

        public static async Task<ServerResponse<CityJSON>> City_request(string access_token, string city_id)
        {
            List<string> key_list = null;
            List<string> value_list = null;
            _access_token_API = access_token;
            return await Server.i_Get_server_data_API<CityJSON>(_URL_BASE_API + _CITY_SERVICE_API + city_id, key_list, value_list, MethodHTTPType.GET); ;
        }
        //-----------------------------------------------------



        //-----------------------------------------------------
        //offers/status/{id}
        public static async Task<ServerResponse<OfferJSON>> Offers_status_request(string access_token, long user_id, int job_offer_status)
        {
            List<string> key_list = null;
            List<string> value_list = null;
            _access_token_API = access_token;
            return await Server.i_Get_server_data_API<OfferJSON>(_URL_BASE_API + _OFFERS_SERVICE_API + _OFFER_STATUS_SERVICE_API + job_offer_status.ToString(), key_list, value_list, MethodHTTPType.GET);
        }


        
        ///students/{student_id}/invitations/active
        public static async Task<ServerResponse<List<OfferJSON>>> Offers_request(string access_token, long user_id, int job_offer_status)
        {
            List<string> key_list = null;
            List<string> value_list = null;
            _access_token_API = access_token;
            
            if (job_offer_status != int.MinValue && job_offer_status != 0) {
                key_list = new List<string>();
                value_list = new List<string>();
                key_list.Add("filter[job_offers_status_id]");
                value_list.Add(job_offer_status.ToString());
            }
            
            return await Server.i_Get_server_data_API<List<OfferJSON>>(_URL_BASE_API + _VIEW_STUDENT_SERVICE_API + user_id.ToString()+ _STUDENT_OFFER_LIST_SERVICE_API, key_list, value_list, MethodHTTPType.GET);
            //messaging/conversations/student/{student_id}
            //return await Server.i_Get_server_data_API<OffersJSON>(_URL_BASE_API + _OFFER_LIST_SERVICE_API + user_id.ToString(), key_list, value_list, MethodHTTPType.GET);
        }


        public static async Task<ServerResponse<OffersJSON>> Offer_request_conversation(string access_token, long user_id, int job_offer_id)
        {
            List<string> key_list = null;
            List<string> value_list = null;
            _access_token_API = access_token;

            if (job_offer_id != int.MinValue && job_offer_id != 0)
            {
                key_list = new List<string>();
                value_list = new List<string>();
                key_list.Add("filter[job_offer_id]");
                value_list.Add(job_offer_id.ToString());
            }

            //messaging/conversations/student/{student_id}
            return await Server.i_Get_server_data_API<OffersJSON>(_URL_BASE_API + _OFFER_LIST_SERVICE_API + user_id.ToString(), key_list, value_list, MethodHTTPType.GET);
        }

        //messaging/conversations/invitation/{invitation_id}
        public static async Task<ServerResponse<OfferMsgDataJSON>> Messages_request(string access_token, int invitation_id)
        {
            List<string> key_list = null;
            List<string> value_list = null;
            _access_token_API = access_token;
            return await Server.i_Get_server_data_API<OfferMsgDataJSON>(_URL_BASE_API + _INVITATION_MSG_SERVICE_API + invitation_id.ToString(), key_list, value_list, MethodHTTPType.GET);
        }
        //-----------------------------------------------------

        public static async Task<ServerResponse<OfferDetailDataJSON>> Offer_detail_request(string access_token, long user_id, int offer_id)
        {
            List<string> key_list = null;
            List<string> value_list = null;
            _access_token_API = access_token;
            return await Server.i_Get_server_data_API<OfferDetailDataJSON>(_URL_BASE_API + _OFFERS_SERVICE_API + offer_id.ToString() + _LOCALE_API, key_list, value_list, MethodHTTPType.GET);
        }


        public static async Task<ServerResponse<OfferDetailDataJSON>> Offer_detail_request_name(string access_token, int offer_id)
        {
            List<string> key_list = null;
            List<string> value_list = null;
            _access_token_API = access_token;
            return await Server.i_Get_server_data_API<OfferDetailDataJSON>(_URL_BASE_API + _OFFERS_SERVICE_API + offer_id.ToString()+_LOCALE_API, key_list, value_list, MethodHTTPType.GET);
        }
        /////offers/{offer_id}/killer-questions/students/{student_id}
        public static async Task<ServerResponse<List<OfferKillerDataJSON>>> Offer_killer_detail_request(string access_token, long user_id, int offer_id)
        {
            List<string> key_list = null;
            List<string> value_list = null;
            _access_token_API = access_token;
            return await Server.i_Get_server_data_API<List<OfferKillerDataJSON>>(_URL_BASE_API + _OFFERS_SERVICE_API + offer_id.ToString() + _OFFER_KILLER_SERVICE_API+user_id.ToString(), key_list, value_list, MethodHTTPType.GET);
        }

        public static async Task<ServerResponse<List<OfferKillerDataJSON>>> Offer_killer_request(string access_token, long user_id, int offer_id)
        {
            List<string> key_list = null;
            List<string> value_list = null;
            _access_token_API = access_token;
            return await Server.i_Get_server_data_API<List<OfferKillerDataJSON>>(_URL_BASE_API + _OFFERS_SERVICE_API + offer_id.ToString() + _OFFER_KILLER_UPDATE_SERVICE_API, key_list, value_list, MethodHTTPType.GET);
        }
        //////Luego tenemos dos endpoints:
        //////http://alumniv2.f2fbeta.com/services/student/notifications
        //////Tiene los parámetros 'student_id' y 'all'. Devuelve todas las notificaciones que tiene un estudiante.Por defecto manda solo las que están sin leer pero si pones 'all' a true, entonces te devuelve todas.
        //public static async Task<ServerResponse<NotificationJSON>> notification_request(string access_token, long user_id, Boolean _all)
        //{
        //    List<string> key_list = null;
        //    List<string> value_list = null;
        //    Server.i_Get_common_request_params(user_id, access_token, ref key_list, ref value_list);
        //    //key_list = new List<string>();
        //    //value_list = new List<string>();
        //    //key_list.Add("student_id");
        //    //value_list.Add(user_id.ToString());
        //    key_list.Add("all");
        //    value_list.Add("0"); //para false 
        //    //value_list.Add(_all.ToString());
        //    //value_list.Add("1"); //para true ver todas
        //    return await Server.i_Get_server_data<NotificationJSON>(_URL_BASE + _NOTIFICATIONS_SERVICES, key_list, value_list);
        //}

        ////Duplicada en FICHERO:Droid HelperService.cs
        ////////http://alumniv2.f2fbeta.com/services/student/read-notification
        ////////Tiene el parámetro 'notification_id'. Simplemente la marca como leída.
        ////Duplicada en FICHERO:Droid HelperService.cs
        //public static async Task<ServerResponse<SaveDataJSON>> save_read_notification(string access_token, long user_id,string notification_id )
        //{
        //    List<string> key_list = null;
        //    List<string> value_list = null;
        //    Server.i_Get_common_request_params(user_id, access_token, ref key_list, ref value_list);
        //    ////key_list.Add("student_id");
        //    ////value_list.Add(user_id.ToString());
        //    key_list.Add("notification_id");
        //    value_list.Add(notification_id);
        //    return await Server.i_Get_server_data<SaveDataJSON>(_URL_BASE + _READ_NOTIFICATIONS_SERVICES, key_list, value_list);
        //}
        //-----------------------------------------------------

        ////offers/{offer_id}/invitations/{student_id}/student-accepts
        ///offers/{offer_id}/invitations/{student_id}/student-rejects
        public static async Task<ServerResponse<SaveDataJSON>> Offer_action_request(string access_token, long user_id, int offer_id, string action, List<OfferKillerDataJSON> killer_questions)
        {
            List<string> key_list = null;
            List<string> value_list = null;
            ServerResponse<SaveDataJSON> killer_json;
            _access_token_API = access_token;
            bool regok;
            string path = _URL_BASE_API + _OFFERS_SERVICE_API + offer_id.ToString()+_INVITATION_SERVICE_API+user_id.ToString();
            string pathkiller;
            switch (action.ToUpper())
            {
                case "ACCEPT":
                    path = path + _OFFER_ACCEPT_SERVICE_API;
                    break;
                case "DECLINE":
                    path = path + _OFFER_REJECT_SERVICE_API;
                    break;
                default:
                    break;
            }

            regok = true;
            if (killer_questions != null && killer_questions.Count > 0)
            {
                ////offers/{offer_id}/killer-questions/{killer_question_id}/students/{student_id}
                
                for (int i = 0; i < killer_questions.Count; ++i)
                {
                    pathkiller = _URL_BASE_API + _OFFERS_SERVICE_API + offer_id.ToString() + _OFFER_KILLER_UPDATE_SERVICE_API + "/"+ killer_questions[i].id.ToString() + "/students/" + user_id.ToString();
                    key_list = new List<string>();
                    value_list = new List<string>();
                    key_list.Add("answer");
                    value_list.Add(killer_questions[i].answers.response);
                    killer_json =await Server.i_Get_server_data_API<SaveDataJSON>(pathkiller, key_list, value_list,MethodHTTPType.POST);
                    if (killer_json.type == ResponseType.OK_SUCCESS)
                    {
                        //continuamos con la siguiente respuesta
                    }
                    else {
                        regok = false;
                    }
                }
                
            }
            if (regok == true)
            {
                return await Server.i_Get_server_data_API<SaveDataJSON>(path, key_list, value_list,MethodHTTPType.POST);
            }
            else {
                ServerResponse<SaveDataJSON> response = new ServerResponse<SaveDataJSON>();
                response.result_object = new SaveDataJSON();
                response.result_object.result = false;
                response.message = "";
                response.type = ResponseType.SERVICE_ERROR;
                return response;

            }
            
        }

        //-----------------------------------------------------

        public static async Task<ServerResponse<SaveDataJSON>> Offer_message_save(string access_token, long user_id, int invitation_id, int company_user_id, string message)
        {
            List<string> key_list = null;
            List<string> value_list = null;
            _access_token_API = access_token;
            key_list = new List<string>();
            value_list = new List<string>();
            //"company_user_id" : "1","message" : "1","transmitter" : "c","invitation_id" : "34","read" : "0"
            key_list.Add("company_user_id");
            value_list.Add(company_user_id.ToString());
            key_list.Add("message");
            value_list.Add(message);
            key_list.Add("transmitter");
            //descomentar c para simular de company y comentar s que es de student
            //value_list.Add("c");
            value_list.Add("s");
            key_list.Add("invitation_id");
            value_list.Add(invitation_id.ToString());
            key_list.Add("read");
            value_list.Add("0");


            return await Server.i_Get_server_data_API<SaveDataJSON>(_URL_BASE_API + _OFFER_MSG_CREATE_SERVICE_API, key_list, value_list,MethodHTTPType.POST);
        }

        //-----------------------------------------------------

        
        //PUT messaging/conversations/{invitation_id}/student-read
        public static async Task<ServerResponse<SaveDataJSON>> Offer_mark_messages_as_readed(string access_token, long user_id, int invitation_id)
        {
            List<string> key_list = null;
            List<string> value_list = null;
            _access_token_API = access_token;
            return await Server.i_Get_server_data_API<SaveDataJSON>(_URL_BASE_API + _INVITATION_MARK_MSG_READ_SERVICE_API+invitation_id.ToString()+ _INVITATION_MARK_MSG_READ_SERVICE_API_2, key_list, value_list,MethodHTTPType.PUT);
        }

        //-----------------------------------------------------

        public static async Task<ServerResponse<OfferReadedJSON>> Offer_get_not_readed(string access_token, long user_id)
        {
            ServerResponse<OfferReadedJSON> response = new ServerResponse<OfferReadedJSON>();
            
            List<string> key_list = null;
            List<string> value_list = null;
            _access_token_API = access_token;
            //response.result_object= new OfferReadedJSON();
            //key_list = new List<string>();
            //value_list = new List<string>();
            //key_list.Add("filter[job_offers_status_id]");
            //value_list.Add("1");
            //ServerResponse<List<OfferJSON>> offers_news= await Server.i_Get_server_data_API<List<OfferJSON>>(_URL_BASE_API + _VIEW_STUDENT_SERVICE_API + user_id.ToString() + _STUDENT_OFFER_LIST_SERVICE_API, key_list, value_list, MethodHTTPType.GET);
            //if (offers_news.type == ResponseType.OK_SUCCESS)
            //{
            //    response.result_object.offers_not_readed = offers_news.result_object.Count;
            //}
            //else {
            //    response.result_object.offers_not_readed = 0;
            //}
            //response.type = offers_news.type;
            //response.message = offers_news.message;
            //return response;
            return await Server.i_Get_server_data_API<OfferReadedJSON>(_URL_BASE_API + _VIEW_STUDENT_SERVICE_API + user_id.ToString() + _OFFER_NOT_READED_API, key_list, value_list, MethodHTTPType.GET);
        }


        //-----------------------------------------------------
        public static async Task<string> Personal_img_data_save(string access_token, long user_id,  byte[] imagebytes64) {
            List<string> key_list = null;
            List<string> value_list = null;
            _access_token_API = access_token;
            string regOk = "";
            key_list = new List<string>();
            value_list = new List<string>();
            key_list.Add("extension");
            value_list.Add("jpg");
            key_list.Add("mimetype");
            value_list.Add("image/jpeg");
            key_list.Add("size");
            value_list.Add(imagebytes64.Length.ToString());
            key_list.Add("originalName");
            value_list.Add("my_uploaded_image.jpg");
            ServerResponse<SaveDataJSON> consUser_json = await Server.i_Get_server_img_data_API<SaveDataJSON>(_URL_BASE_API + _VIEW_STUDENT_SERVICE_API + user_id.ToString()+_IMG_STUDENT_PROFILE_SERVICE_API, key_list, value_list, imagebytes64);
            if (consUser_json.type == ResponseType.OK_SUCCESS)
            {
                regOk=consUser_json.result_object.url;

            }
            
            return regOk;
        }

        public static async Task<bool> Personal_data_save(string access_token, long user_id, PersonalDataJSON personal, byte[] imageBytes64)
        {
            List<string> key_list = null;
            List<string> value_list = null;
            _access_token_API = access_token;
            bool regOk = false;
            string url = "";
            key_list = new List<string>();
            value_list = new List<string>();
            key_list.Add("first_name");
            value_list.Add(personal.name);
            key_list.Add("last_name");
            value_list.Add(personal.last_name);
            key_list.Add("telephone");
            value_list.Add(personal.phone);
            key_list.Add("nationality");
            value_list.Add(personal.nationality_id.ToString());
            key_list.Add("birth_date");
            value_list.Add(personal.birth_date);
            if (imageBytes64 != null)
            {
                //key_list.Add("img_profile");
                //value_list.Add(new string(image64));
                url = await Personal_img_data_save(access_token, user_id, imageBytes64);
                if (url != "") { regOk = true; }
            }
            else
            {
                regOk = true;
            }

            if (regOk == true)
            {
                //primero put /students/{student_id}
                ServerResponse<SaveDataJSON> consUser_json = await Server.i_Get_server_data_API<SaveDataJSON>(_URL_BASE_API + _VIEW_STUDENT_SERVICE_API + user_id.ToString(), key_list, value_list, MethodHTTPType.PUT);
                if (consUser_json.type == ResponseType.OK_SUCCESS)
                {
                    //continuamos con la localizacion PUT /students/{student_id}/location
                    key_list = new List<string>();
                    value_list = new List<string>();
                    key_list.Add("country_id");
                    value_list.Add(personal.location.country_id.ToString());
                    key_list.Add("state_id");
                    value_list.Add(personal.location.state.id.ToString());
                    key_list.Add("city_id");
                    value_list.Add(personal.location.city.id.ToString());
                    ServerResponse<SaveDataJSON> consUserLoc_json = await Server.i_Get_server_data_API<SaveDataJSON>(_URL_BASE_API + _VIEW_STUDENT_SERVICE_API + user_id.ToString() + _LOCATION_STUDENT_SERVICE_API, key_list, value_list, MethodHTTPType.PUT);
                    if (consUser_json.type == ResponseType.OK_SUCCESS)
                    {
                        if (personal.sites.Count > 0)
                        {
                            key_list = new List<string>();
                            value_list = new List<string>();
                            //continuamos con las redes sociales PUT students/{student_id}/social-networks
                            {
                                StringBuilder stream;
                                key_list.Add("socialNetworks");

                                stream = new StringBuilder();
                                stream.Append("{");
                                for (int i = 0; i < personal.sites.Count; ++i)
                                {

                                    stream.Append("\"" + personal.sites[i].id + "\":");
                                    stream.Append("{");
                                    stream.Append("\"url\":\"");
                                    stream.Append(personal.sites[i].url);
                                    stream.Append("\"}");
                                    if (i < personal.sites.Count - 1)
                                        stream.Append(",");
                                }
                                stream.Append("}");

                                value_list.Add(stream.ToString());
                            }
                            ServerResponse<SaveDataJSON> consUserSocial_json = await Server.i_Get_server_data_API<SaveDataJSON>(_URL_BASE_API + _VIEW_STUDENT_SERVICE_API + user_id.ToString() + _SOCIALS_STUDENT_SERVICE_API, key_list, value_list, MethodHTTPType.PUT);
                            if (consUser_json.type == ResponseType.OK_SUCCESS)
                            {
                                regOk = true;
                            }
                            else regOk = false;

                        }
                        else regOk = true;


                    }
                    else regOk = false;


                }
                else regOk = false;
            }
            //if (datpersonalescompletos == false) {
            //    key_list.Add("first_employ");
            //    value_list.Add(i_bool(false));
            //    key_list.Add("trainee");
            //    value_list.Add(i_bool(false));
            //    key_list.Add("non_profit");
            //    value_list.Add(i_bool(false));
            //}
            

            return regOk;

            //return await Server.i_Get_server_data<SaveDataJSON>(_URL_BASE + _PROFILE_SAVE_SERVICE, key_list, value_list);
        }

        //-----------------------------------------------------


        public static async Task<bool> Personal_merchant_save(string access_token, long user_id)
        {
            List<string> key_list = null;
            List<string> value_list = null;
            _access_token_API = access_token;
            bool regOk = false;
            key_list = new List<string>();
            value_list = new List<string>();
            key_list.Add("first_employ");
            value_list.Add(i_bool(false));
            key_list.Add("trainee");
            value_list.Add(i_bool(false));
            key_list.Add("non_profit");
            value_list.Add(i_bool(false));
            ServerResponse<SaveDataJSON> consUser_json = await Server.i_Get_server_data_API<SaveDataJSON>(_URL_BASE_API + _VIEW_STUDENT_SERVICE_API + user_id.ToString(), key_list, value_list, MethodHTTPType.PUT);
            if (consUser_json.type == ResponseType.OK_SUCCESS)
            {
                if (consUser_json.type == ResponseType.OK_SUCCESS)
                {
                    regOk = true;

                }
                else regOk = false;
            }
            else regOk = false;

            return regOk;

            //return await Server.i_Get_server_data<SaveDataJSON>(_URL_BASE + _PROFILE_SAVE_SERVICE, key_list, value_list);
        }


        public static async Task<ServerResponse<SaveDataJSON>> Academic_data_save(string access_token, long user_id, AcademicDataJSON academic)
        {
            List<string> key_list = null;
            List<string> value_list = null;
            List<string> key_list_Parcial = null;
            List<string> value_list_Parcial = null;
            key_list = new List<string>();
            value_list = new List<string>();
            string university="";
            string study="";
            string subject="";
            bool addcoma = false;
            ///students/{student_id}/academic-data[/{academicdata_id}]
            string pathurl = _URL_BASE_API + _VIEW_STUDENT_SERVICE_API + user_id.ToString() + _ACADEMIC_STUDENT_SERVICE_API;

            _access_token_API = access_token;

            if (academic.id != int.MinValue)
            {
                //update
                pathurl = pathurl + "/" + academic.id;
            }

            if (academic.university.id != int.MinValue)
            {
                university = academic.university.id.ToString();
            }
            else {
                //crear university
                key_list_Parcial = new List<string>();
                value_list_Parcial = new List<string>();

                key_list_Parcial.Add("name");
                value_list_Parcial.Add(academic.university.name);
                key_list_Parcial.Add("country_id");
                value_list_Parcial.Add(academic.university.country_id.ToString());
                key_list_Parcial.Add("state_id");
                value_list_Parcial.Add(academic.university.state_id.ToString());
                ServerResponse<SaveDataJSON> uni_json= await Server.i_Get_server_data_API<SaveDataJSON>(_URL_BASE_API+ _UNIVERSITY_SERVICE_API, key_list_Parcial, value_list_Parcial, MethodHTTPType.POST);
                if (uni_json.type == ResponseType.OK_SUCCESS) {
                    university = uni_json.result_object.id.ToString();
                }
                
            }
            key_list.Add("university_id");
            value_list.Add(university);
            key_list.Add("group_study_id");
            value_list.Add(academic.group_id.ToString());
            if (academic.study.id != int.MinValue)
            {
                study = academic.study.id.ToString();
                
            }
            else
            {
                //crear study
                
                key_list_Parcial = new List<string>();
                value_list_Parcial = new List<string>();
                key_list_Parcial.Add("forceCreation");
                value_list_Parcial.Add("1");
                key_list_Parcial.Add("name");
                value_list_Parcial.Add(academic.study.name);
                key_list_Parcial.Add("study_level_id");
                value_list_Parcial.Add(academic.study.study_level_id.ToString());
                key_list_Parcial.Add("group_study_id");
                value_list_Parcial.Add(academic.group_id.ToString());
                key_list_Parcial.Add("university_id");
                value_list_Parcial.Add(university);
                
                //study_level??
                ServerResponse<SaveDataJSON> study_json = await Server.i_Get_server_data_API<SaveDataJSON>(_URL_BASE_API + _STUDY_SERVICE_API, key_list_Parcial, value_list_Parcial, MethodHTTPType.POST);
                if (study_json.type == ResponseType.OK_SUCCESS)
                {
                    study=study_json.result_object.id.ToString();
                }
            }
            key_list.Add("study_id");
            value_list.Add(study);
            key_list.Add("average_calification");
            value_list.Add(i_float(academic.calification));
            key_list.Add("started_at");
            value_list.Add(String.Format("{0}-{1}-{2}", academic.year_start.ToString("D4"), academic.month_start.ToString("D2"),"01"));
            key_list.Add("finished_at");
            value_list.Add(academic.year_end == 0 ? "" : String.Format("{0}-{1}-{2}", academic.year_end.ToString("D4"), academic.month_end.ToString("D2"),"01"));



            
            if (academic.subjects != null) {
                {
                    if (academic.subjects.Count > 0)
                    {
                        if (academic.subjects[0].pivot.qualification > 0)
                        {
                            StringBuilder stream;
                            key_list.Add("subjects");

                            stream = new StringBuilder();
                            stream.Append("[");
                            for (int i = 0; i < academic.subjects.Count; ++i)
                            {
                                if (academic.subjects[i].pivot.qualification > 0)
                                {
                                    if (addcoma == true)
                                        stream.Append(",");

                                    stream.Append("{");


                                    if (academic.subjects[i].id != int.MinValue)
                                    {
                                        subject = academic.subjects[i].id.ToString(); 
                                    }
                                    else
                                    {
                                        //crear subject
                                        key_list_Parcial = new List<string>();
                                        value_list_Parcial = new List<string>();

                                        key_list_Parcial.Add("name");
                                        value_list_Parcial.Add(academic.subjects[i].name);
                                        key_list_Parcial.Add("study_id");
                                        value_list_Parcial.Add(study);
                                        ServerResponse<SaveDataJSON> subject_json = await Server.i_Get_server_data_API<SaveDataJSON>(_URL_BASE_API + _SUBJECTS_STUDY_SERVICE_API, key_list_Parcial, value_list_Parcial, MethodHTTPType.POST);
                                        if (subject_json.type == ResponseType.OK_SUCCESS)
                                        {
                                            subject = subject_json.result_object.id.ToString();
                                        }


                                    }
                                    stream.Append("\"id\":");
                                    stream.Append("\"" + subject + "\"");
                                    stream.Append(",\"qualification\":");
                                    stream.Append("\"" + i_float(academic.subjects[i].pivot.qualification) + "\"");
                                    stream.Append("}");
                                    addcoma = true;

                                }

                            }
                            stream.Append("]");
                            value_list.Add(stream.ToString());
                        }
                    }

                }

            }

            return await Server.i_Get_server_data_API<SaveDataJSON>(pathurl, key_list, value_list,MethodHTTPType.PUT);
        }

        
        //-----------------------------------------------------
        //DELETE /students/{student_id}/academic-data/{academicdata_id}
        public static async Task<ServerResponse<SaveDataJSON>> Academic_data_delete(string access_token, long user_id, long academic_id)
        {
            List<string> key_list = null;
            List<string> value_list = null;
            _access_token_API = access_token;
            return await Server.i_Get_server_data_API<SaveDataJSON>(_URL_BASE_API + _VIEW_STUDENT_SERVICE_API + user_id.ToString() + _ACADEMIC_STUDENT_SERVICE_API + "/" + academic_id.ToString(), key_list, value_list, MethodHTTPType.DELETE);
        }

        

        //-----------------------------------------------------
        public static async Task<ServerResponse<SaveDataJSON>> Merchant_positions_data_save(string access_token, long user_id, List<MerchantPositionJSON> positions)
        {
            ServerResponse<SaveDataJSON> positions_json = new ServerResponse<SaveDataJSON>();
            positions_json.type = ResponseType.OK_SUCCESS;
            positions_json.result_object = new SaveDataJSON();
            positions_json.result_object.result = true;
            positions_json.message = "";
            List<string> key_list = null;
            List<string> value_list = null;

            _access_token_API = access_token;

            int count = positions.Count;
            if (count > 0)
            {
                {
                    key_list = new List<string>();
                    value_list = new List<string>();
                    StringBuilder stream;
                    key_list.Add("positions");
                    stream = new StringBuilder();
                    stream.Append("[");
                    for (int i = 0; i < count; ++i)
                    {
                        stream.Append("{");
                        stream.Append("\"position_id\":");
                        stream.Append(positions[i].pivot.id);
                        stream.Append("}");
                        if (i < count - 1)
                            stream.Append(",");
                    }
                    stream.Append("]");

                    value_list.Add(stream.ToString());
                }
                positions_json = await Server.i_Get_server_data_API<SaveDataJSON>(_URL_BASE_API + _VIEW_STUDENT_SERVICE_API + user_id.ToString() + _JOBPOSITIONS_STUDENT_SERVICE_API, key_list, value_list, MethodHTTPType.PUT);

            }
            else {
                //vaciamos
                {
                    key_list = new List<string>();
                    value_list = new List<string>();
                    StringBuilder stream;
                    key_list.Add("positions");
                    stream = new StringBuilder();
                    stream.Append("[]");
                    value_list.Add(stream.ToString());
                }
                positions_json = await Server.i_Get_server_data_API<SaveDataJSON>(_URL_BASE_API + _VIEW_STUDENT_SERVICE_API + user_id.ToString() + _JOBPOSITIONS_STUDENT_SERVICE_API, key_list, value_list, MethodHTTPType.PUT);
            }
            return positions_json;

        }

        public static async Task<ServerResponse<SaveDataJSON>> Merchant_sectors_data_save(string access_token, long user_id, List<MerchantSectorJSON> sectors)
        {
            ServerResponse<SaveDataJSON> sectors_json = new ServerResponse<SaveDataJSON>();
            sectors_json.type = ResponseType.OK_SUCCESS;
            sectors_json.result_object = new SaveDataJSON();
            sectors_json.result_object.result = true;
            sectors_json.message = "";
            List<string> key_list = null;
            List<string> value_list = null;
           
            _access_token_API = access_token;

            int count = sectors.Count;
            if (count > 0)
            {
                {
                    key_list = new List<string>();
                    value_list = new List<string>();
                    StringBuilder stream;

                    key_list.Add("sectors");
                    stream = new StringBuilder();
                    stream.Append("[");
                    for (int i = 0; i < count; ++i)
                    {
                        stream.Append(sectors[i].pivot.id);
                        if (i < count - 1)
                            stream.Append(",");
                    }
                    stream.Append("]");

                    value_list.Add(stream.ToString());
                    sectors_json = await Server.i_Get_server_data_API<SaveDataJSON>(_URL_BASE_API + _VIEW_STUDENT_SERVICE_API + user_id.ToString() + _JOBSECTIONS_STUDENT_SERVICE_API, key_list, value_list, MethodHTTPType.PUT);
                }

            }
            else {
                //vaciamaos
                {
                    key_list = new List<string>();
                    value_list = new List<string>();
                    StringBuilder stream;

                    key_list.Add("sectors");
                    stream = new StringBuilder();
                    stream.Append("[]");
                    value_list.Add(stream.ToString());
                    sectors_json = await Server.i_Get_server_data_API<SaveDataJSON>(_URL_BASE_API + _VIEW_STUDENT_SERVICE_API + user_id.ToString() + _JOBSECTIONS_STUDENT_SERVICE_API, key_list, value_list, MethodHTTPType.PUT);
                }
            }
            return sectors_json;

        }

        public static async Task<ServerResponse<SaveDataJSON>> Merchant_location_data_save(string access_token, long user_id, List<MerchantJobLocationJSON> locations)
        {
            ServerResponse<SaveDataJSON> jobloc_json = new ServerResponse<SaveDataJSON>();
            jobloc_json.type = ResponseType.OK_SUCCESS;
            jobloc_json.result_object = new SaveDataJSON();
            jobloc_json.result_object.result = true;
            jobloc_json.message = "";

            List<string> key_list = null;
            List<string> value_list = null;
            List<string> key_list_Parcial = null;
            List<string> value_list_Parcial = null;
            bool regOk = true;
            _access_token_API = access_token;
            int count = locations.Count;
            if (count > 0)
            {
                {
                    key_list = new List<string>();
                    value_list = new List<string>();
                    StringBuilder stream;

                    key_list.Add("jobLocations");
                    stream = new StringBuilder();
                    stream.Append("[");
                    for (int i = 0; i < count; ++i)
                    {
                        stream.Append("{");
                        stream.Append("\"country_id\":\"");
                        stream.Append(locations[i].country_id);
                        stream.Append("\",\"state_id\":\"");
                        if (locations[i].state_id != int.MinValue)
                        {
                            stream.Append(locations[i].state_id);
                        }
                        else
                        {
                            //crear state
                            key_list_Parcial = new List<string>();
                            value_list_Parcial = new List<string>();

                            key_list_Parcial.Add("name");
                            value_list_Parcial.Add(locations[i].state);
                            key_list_Parcial.Add("country_id");
                            value_list_Parcial.Add(locations[i].country_id.ToString());
                            ServerResponse<SaveDataJSON> state_json = await Server.i_Get_server_data_API<SaveDataJSON>(_URL_BASE_API + _STATE_CREATE_SERVICE_API, key_list_Parcial, value_list_Parcial, MethodHTTPType.POST);
                            if (state_json.type == ResponseType.OK_SUCCESS)
                            {
                                stream.Append(state_json.result_object.id.ToString());
                            }
                            else
                            {
                                regOk = false;
                                jobloc_json.type = ResponseType.SERVICE_ERROR;
                            }

                        }
                        stream.Append("\"}");
                        if (i < count - 1)
                            stream.Append(",");
                    }
                    stream.Append("]");

                    value_list.Add(stream.ToString());
                }

                if (regOk == true)
                {
                    jobloc_json = await Server.i_Get_server_data_API<SaveDataJSON>(_URL_BASE_API + _VIEW_STUDENT_SERVICE_API + user_id.ToString() + _JOBLOCATIONS_STUDENT_SERVICE_API, key_list, value_list, MethodHTTPType.PUT);
                }
            }
            else {
                //vaciamaos
                {
                    key_list = new List<string>();
                    value_list = new List<string>();
                    StringBuilder stream;

                    key_list.Add("jobLocations");
                    stream = new StringBuilder();
                    stream.Append("[]");
                    value_list.Add(stream.ToString());
                    jobloc_json = await Server.i_Get_server_data_API<SaveDataJSON>(_URL_BASE_API + _VIEW_STUDENT_SERVICE_API + user_id.ToString() + _JOBLOCATIONS_STUDENT_SERVICE_API, key_list, value_list, MethodHTTPType.PUT);
                }
            }
            return jobloc_json;

        }

        public static async Task<ServerResponse<SaveDataJSON>> Merchant_data_save(string access_token, long user_id, MerchantDataJSON merchant)
        {
            ServerResponse<SaveDataJSON> merchant_json=new ServerResponse<SaveDataJSON>();
            merchant_json.type = ResponseType.SERVICE_ERROR;
            merchant_json.result_object = new SaveDataJSON();
            merchant_json.result_object.result = true;
            merchant_json.message = "";
            List<string> key_list = null;
            List<string> value_list = null;
            key_list = new List<string>();
            value_list = new List<string>();
            bool regOk = true;
            _access_token_API = access_token;

            
            key_list.Add("first_employ");
            value_list.Add(i_bool(i_valorbool(merchant.student_data.first_job)));        
            key_list.Add("trainee");
            value_list.Add(i_bool(i_valorbool(merchant.student_data.scholarship)));
            key_list.Add("non_profit");
            value_list.Add(i_bool(i_valorbool(merchant.student_data.ong)));
            key_list.Add("unpaid_job");
            value_list.Add(i_bool(i_valorbool(merchant.student_data.updaid)));
            key_list.Add("any_country");
            value_list.Add(i_bool(i_valorbool(merchant.student_data.any_country)));
            key_list.Add("any_state");
            value_list.Add(i_bool(i_valorbool(merchant.student_data.any_state)));
            key_list.Add("availability");
            value_list.Add(merchant.student_data.availability);
            key_list.Add("morning");
            value_list.Add(i_bool(i_valorbool(merchant.student_data.morning)));
            key_list.Add("afternoon");
            value_list.Add(i_bool(i_valorbool(merchant.student_data.afternoon)));
            key_list.Add("all_day");
            value_list.Add(i_bool(i_valorbool(merchant.student_data.all_day)));
            key_list.Add("any_position");
            value_list.Add(i_bool(i_valorbool(merchant.student_data.any_position)));
            key_list.Add("any_sector");
            value_list.Add(i_bool(i_valorbool(merchant.student_data.any_sector)));
            merchant_json = await Server.i_Get_server_data_API<SaveDataJSON>(_URL_BASE_API + _VIEW_STUDENT_SERVICE_API + user_id.ToString(), key_list, value_list, MethodHTTPType.PUT);
            if (merchant_json.type != ResponseType.OK_SUCCESS){
                regOk = false;
            }
            merchant_json = await Server.Merchant_location_data_save(_access_token_API, user_id, merchant.locations);
            if (merchant_json.type != ResponseType.OK_SUCCESS)
            {
                regOk = false;
            }
            merchant_json = await Server.Merchant_sectors_data_save(_access_token_API, user_id, merchant.sectors);
            if (merchant_json.type != ResponseType.OK_SUCCESS)
            {
                regOk = false;
            }
            merchant_json = await Server.Merchant_positions_data_save(_access_token_API, user_id, merchant.positions);
            if (merchant_json.type != ResponseType.OK_SUCCESS)
            {
                regOk = false;
            }
            if (regOk == false) {
                merchant_json.type = ResponseType.SERVICE_ERROR;
                merchant_json.result_object = new SaveDataJSON();
                merchant_json.result_object.result = true;
                merchant_json.message = "";
            }
            return merchant_json;
        }

        
        
        //-----------------------------------------------------
        
        public static async Task<ServerResponse<List<SectorJSON>>> sectors_request(string access_token, long user_id, string search_text)
        {
            List<string> key_list = null;
            List<string> value_list = null;
            _access_token_API = access_token;
            key_list = new List<string>();
            value_list = new List<string>();
            key_list.Add("pagination");
            value_list.Add("false");
            key_list.Add("search[name]");
            value_list.Add(search_text);
            key_list.Add("column");
            value_list.Add("name");
            key_list.Add("order");
            value_list.Add("asc");
            return await Server.i_Get_server_data_API<List<SectorJSON>>(_URL_BASE_API + _SECTORS__ACTIVE_SERVICE_API + _LOCALE_API, key_list, value_list, MethodHTTPType.GET);
        }

        public static async Task<ServerResponse<SectorJSON>> sector_request(string access_token, long user_id,int sector_id)
        {
            List<string> key_list = null;
            List<string> value_list = null;
            _access_token_API = access_token;
            return await Server.i_Get_server_data_API<SectorJSON>(_URL_BASE_API + _SECTORS_SERVICE_API + sector_id.ToString() + _LOCALE_API, key_list, value_list, MethodHTTPType.GET);
        }

        //-----------------------------------------------------
        public static async Task<ServerResponse<List<VoluntaryDataJSON>>> student_voluntary_data_request(string access_token, long user_id)
        {
            List<string> key_list = null;
            List<string> value_list = null;
            _access_token_API = access_token;
            return await Server.i_Get_server_data_API<List<VoluntaryDataJSON>>(_URL_BASE_API + _VIEW_STUDENT_SERVICE_API + user_id.ToString() + _VOLUNTARY_STUDENT_SERVICE_API, key_list, value_list, MethodHTTPType.GET);
        }

        //-----------------------------------------------------
       

        public static async Task<ServerResponse<List<InternshipDataJSON>>> student_interships_data_request(string access_token, long user_id)
        {
            List<string> key_list = null;
            List<string> value_list = null;
            _access_token_API = access_token;
            return await Server.i_Get_server_data_API<List<InternshipDataJSON>>(_URL_BASE_API + _VIEW_STUDENT_SERVICE_API + user_id.ToString() + _INTERNSHIPS_STUDENT_SERVICE_API, key_list, value_list, MethodHTTPType.GET);
        }

        //-----------------------------------------------------
        public static async Task<ServerResponse<List<ExperienceDataJSON>>> student_experince_data_request(string access_token, long user_id)
        {
            List<string> key_list = null;
            List<string> value_list = null;
            _access_token_API = access_token;
            return await Server.i_Get_server_data_API<List<ExperienceDataJSON>>(_URL_BASE_API + _VIEW_STUDENT_SERVICE_API + user_id.ToString() + _EXPERIENCE_STUDENT_SERVICE_API, key_list, value_list, MethodHTTPType.GET);
        }
        public static async Task<ServerResponse<PositionJSON>> position_student_request(string access_token, int position_id)
        {
            List<string> key_list = null;
            List<string> value_list = null;
            _access_token_API = access_token;
            return await Server.i_Get_server_data_API<PositionJSON>(_URL_BASE_API + _POSITION_SERVICE_API + "/" + position_id.ToString()+_LOCALE_API, key_list, value_list, MethodHTTPType.GET);
        }

        public static async Task<ServerResponse<CompanyJSON>> company_student_request(string access_token, int company_id)
        {
            List<string> key_list = null;
            List<string> value_list = null;
            _access_token_API = access_token;
            return await Server.i_Get_server_data_API<CompanyJSON>(_URL_BASE_API + _COMPANY_STUDENT_SERVICE_API + company_id.ToString() , key_list, value_list, MethodHTTPType.GET);
        }

        public static async Task<ServerResponse<CompanyJSON>> company_request(string access_token, int company_id)
        {
            List<string> key_list = null;
            List<string> value_list = null;
            _access_token_API = access_token;
            return await Server.i_Get_server_data_API<CompanyJSON>(_URL_BASE_API + _COMPANY_LIST_SERVICE_API + "/" + company_id.ToString(), key_list, value_list, MethodHTTPType.GET);
        }


        //-----------------------------------------------------
        public static async Task<ServerResponse<VoluntaryDataJSON>> Voluntary_data_request(string access_token, long user_id, long voluntary_id)
        {
            List<string> key_list = null;
            List<string> value_list = null;
            key_list = new List<string>();
            value_list = new List<string>();
            key_list.Add("filter[id]");
            value_list.Add(voluntary_id.ToString());
            _access_token_API = access_token;
            return await Server.i_Get_server_data_API<VoluntaryDataJSON>(_URL_BASE_API + _VIEW_STUDENT_SERVICE_API + user_id.ToString() + _VOLUNTARY_STUDENT_SERVICE_API, key_list, value_list, MethodHTTPType.GET);
        }

        

        public static async Task<ServerResponse<InternshipDataJSON>> Internship_data_request(string access_token, long user_id, long internship_id)
        {
            
            List<string> key_list = null;
            List<string> value_list = null;
            key_list = new List<string>();
            value_list = new List<string>();
            key_list.Add("filter[id]");
            value_list.Add(internship_id.ToString());
            _access_token_API = access_token;
            return await Server.i_Get_server_data_API<InternshipDataJSON>(_URL_BASE_API + _VIEW_STUDENT_SERVICE_API + user_id.ToString() + _INTERNSHIPS_STUDENT_SERVICE_API, key_list, value_list, MethodHTTPType.GET);
        }

        //-----------------------------------------------------
        public static async Task<ServerResponse<AcademicDataJSON>> Academic_data_request(string access_token, long user_id, long academic_id)
        {
            List<string> key_list = null;
            List<string> value_list = null;
            _access_token_API = access_token;
            key_list = new List<string>();
            value_list = new List<string>();
            key_list.Add("filter[id]");
            value_list.Add(academic_id.ToString());
            return await Server.i_Get_server_data_API<AcademicDataJSON>(_URL_BASE_API + _VIEW_STUDENT_SERVICE_API + user_id.ToString() + _ACADEMIC_STUDENT_SERVICE_API, key_list, value_list, MethodHTTPType.GET);
        }

        public static async Task<ServerResponse<ExperienceDataJSON>> Experience_data_request(string access_token, long user_id, long experience_id)
        {
            List<string> key_list = null;
            List<string> value_list = null;
            _access_token_API = access_token;
            key_list = new List<string>();
            value_list = new List<string>();
            key_list.Add("filter[id]");
            value_list.Add(experience_id.ToString());
            return await Server.i_Get_server_data_API<ExperienceDataJSON>(_URL_BASE_API + _VIEW_STUDENT_SERVICE_API + user_id.ToString() + _EXPERIENCE_STUDENT_SERVICE_API, key_list, value_list, MethodHTTPType.GET);
        }

        //-----------------------------------------------------
        //-----------------------------------------------------

        public static async Task<ServerResponse<List<AcademicDataJSON>>> student_academic_data_request(string access_token, long user_id)
        {
            List<string> key_list = null;
            List<string> value_list = null;
            _access_token_API = access_token;
            return await Server.i_Get_server_data_API<List<AcademicDataJSON>>(_URL_BASE_API + _VIEW_STUDENT_SERVICE_API + user_id.ToString() + _ACADEMIC_STUDENT_SERVICE_API, key_list, value_list, MethodHTTPType.GET);

        }
        public static async Task<ServerResponse<UniversityJSON>> university_request(string access_token, string university_id)
        {
            List<string> key_list = null;
            List<string> value_list = null;
            _access_token_API = access_token;
            return await Server.i_Get_server_data_API<UniversityJSON>(_URL_BASE_API + _UNIVERSITY_SERVICE_API + "/" + university_id , key_list, value_list, MethodHTTPType.GET);
        }

        public static async Task<ServerResponse<AcademicStudyJSON>> study_request(string access_token, string study_id)
        {
            List<string> key_list = null;
            List<string> value_list = null;
            _access_token_API = access_token;
            
            return await Server.i_Get_server_data_API<AcademicStudyJSON>(_URL_BASE_API + _STUDY_SERVICE_API + "/" + study_id, key_list, value_list, MethodHTTPType.GET);
        }

        public static async Task<ServerResponse<List<LanguageDataJSON>>> student_Languages_data_request(string access_token, long user_id)
        {
            List<string> key_list = null;
            List<string> value_list = null;
            _access_token_API = access_token;
            return  await Server.i_Get_server_data_API<List<LanguageDataJSON>>(_URL_BASE_API + _VIEW_STUDENT_SERVICE_API + user_id.ToString() + _LAGUAGES_STUDENT_SERVICE_API, key_list, value_list, MethodHTTPType.GET);
            
        }

        

        public static async Task<ServerResponse<LanguageJSON>> Language_data_request(string access_token, long user_id, string language_id)
        {
            List<string> key_list = null;
            List<string> value_list = null;
            _access_token_API = access_token;
            LanguageJSON _lang=new LanguageJSON();
            ServerResponse<LanguageJSON> language=new ServerResponse<LanguageJSON>();
            ServerResponse<List<LanguageDataJSON>> languages_json = await Server.i_Get_server_data_API<List<LanguageDataJSON>>(_URL_BASE_API + _VIEW_STUDENT_SERVICE_API + user_id.ToString() + _LAGUAGES_STUDENT_SERVICE_API, key_list, value_list, MethodHTTPType.GET);
            if (languages_json.type == ResponseType.OK_SUCCESS) {
                for (int i = 0; i < languages_json.result_object.Count; ++i)

                {
                    LanguageDataJSON langl_ = languages_json.result_object[i];
                    if (langl_.language_id == language_id) {
                        language.type = ResponseType.OK_SUCCESS;
                        _lang.data = langl_;
                        language.result_object = _lang;
                        break;
                    }

                }
            }

            return language;
                //return await Server.i_Get_server_data_API<LanguageJSON>(_URL_BASE_API + _VIEW_STUDENT_SERVICE_API + user_id.ToString() + _LAGUAGES_STUDENT_SERVICE_API, key_list, value_list, MethodHTTPType.GET);
        }

        
        //-----------------------------------------------------

        public static async Task<ServerResponse<SaveDataJSON>> Language_data_save(string access_token, long user_id, LanguageDataJSON language)
        {
            List<string> key_list = null;
            List<string> value_list = null;
            _access_token_API = access_token;
            string cert_id = "";
            ///students/{student_id}/languages
            //GUARDAR PRIMERO EL CERTIFICADO POST /certificates
            if (language.certificate.name != null && language.certificate.name != "") {
                key_list = new List<string>();
                value_list = new List<string>();

                key_list.Add("name");
                value_list.Add(language.certificate.name);
                key_list.Add("language_id");
                value_list.Add(language.language.id.ToString());
                ServerResponse<SaveDataJSON> cert_json = await Server.i_Get_server_data_API<SaveDataJSON>(_URL_BASE_API + _LANGUAGES_CERTTIFICATE_SERVICE_API, key_list, value_list, MethodHTTPType.POST);
                if (cert_json.type == ResponseType.OK_SUCCESS) {
                    if (cert_json.result_object.id != int.MinValue) {
                        cert_id = cert_json.result_object.id.ToString();
                    }
                }
            }

            key_list = new List<string>();
            value_list = new List<string>();
            key_list.Add("language_id");
            value_list.Add(language.language.id.ToString());
            if (cert_id != "") {
                key_list.Add("certificate_id");
                value_list.Add(cert_id);
            }
            key_list.Add("language_level_id");
            value_list.Add(language.level.id.ToString());
            return await Server.i_Get_server_data_API<SaveDataJSON>(_URL_BASE_API + _VIEW_STUDENT_SERVICE_API + user_id.ToString() + _LAGUAGES_STUDENT_SERVICE_API, key_list, value_list, MethodHTTPType.PUT);

            

            
        }

        //-----------------------------------------------------

        //DELETE /students/{student_id}/languages/{language_id}
        public static async Task<ServerResponse<SaveDataJSON>> Language_data_delete(string access_token, long user_id, string language_id)
        {
            List<string> key_list = null;
            List<string> value_list = null;
            _access_token_API = access_token;
            return await Server.i_Get_server_data_API<SaveDataJSON>(_URL_BASE_API+ _VIEW_STUDENT_SERVICE_API + user_id.ToString() + _LAGUAGES_STUDENT_SERVICE_API + "/" + language_id, key_list, value_list,MethodHTTPType.DELETE);
        }

        //-----------------------------------------------------


        

        public static async Task<ServerResponse<List<LanguageItemJSON>>> Language_items_request(string access_token)
        {
            List<string> key_list = null;
            List<string> value_list = null;
            _access_token_API = access_token;
            key_list = new List<string>();
            value_list = new List<string>();
            key_list.Add("pagination");
            value_list.Add("false");
            return  await Server.i_Get_server_data_API<List<LanguageItemJSON>>(_URL_BASE_API + _LANGUAGES_SERVICE_API+_LOCALE_API, key_list, value_list,MethodHTTPType.GET);
            
        }

        public static async Task<ServerResponse<LanguageDescriptionJSON>> language_request(string access_token, string language_id)
        {
            List<string> key_list = null;
            List<string> value_list = null;
            _access_token_API = access_token;
            return await Server.i_Get_server_data_API<LanguageDescriptionJSON>(_URL_BASE_API + _LANGUAGES_SERVICE_API +"/"+ language_id.ToString() + _LOCALE_API, key_list, value_list, MethodHTTPType.GET);
        }

        public static async Task<ServerResponse<LanguageLevelJSON>> level_language_request(string access_token, string level_id)
        {
            List<string> key_list = null;
            List<string> value_list = null;
            _access_token_API = access_token;
            return await Server.i_Get_server_data_API<LanguageLevelJSON>(_URL_BASE_API + _LANGUAGES_LEVEL_SERVICE_API + "/" + level_id.ToString() + _LOCALE_API, key_list, value_list, MethodHTTPType.GET);
        }

        public static async Task<ServerResponse<LanguageCertificateJSON>> certificate_language_request(string access_token, string certificate_id)
        {
            List<string> key_list = null;
            List<string> value_list = null;
            _access_token_API = access_token;
            return await Server.i_Get_server_data_API<LanguageCertificateJSON>(_URL_BASE_API + _LANGUAGES_CERTTIFICATE_SERVICE_API + "/" + certificate_id.ToString(), key_list, value_list, MethodHTTPType.GET);
        }

        //-----------------------------------------------------

        public static async Task<ServerResponse<List<LevelJSON>>> Levels_request(string access_token, long user_id)
        {
            
            List<string> key_list = null;
            List<string> value_list = null;
            _access_token_API = access_token;
            key_list = new List<string>();
            value_list = new List<string>();
            key_list.Add("pagination");
            value_list.Add("false");
            return await Server.i_Get_server_data_API<List<LevelJSON>>(_URL_BASE_API + _LANGUAGES_LEVEL_SERVICE_API + _LOCALE_API, key_list, value_list, MethodHTTPType.GET);
        }

        public static async Task<ServerResponse<List<LevelJSON>>> Levels_study_request(string access_token, long user_id,string search_text)
        {

            List<string> key_list = null;
            List<string> value_list = null;
            _access_token_API = access_token;
            key_list = new List<string>();
            value_list = new List<string>();
            key_list.Add("pagination");
            value_list.Add("false");
            if (search_text != null && search_text != "") {
                key_list.Add("search[name]");
                value_list.Add(search_text);
            }
            return await Server.i_Get_server_data_API<List<LevelJSON>>(_URL_BASE_API + _STUDY_LEVEL_SERVICE_API + _LOCALE_API, key_list, value_list, MethodHTTPType.GET);
        }
        ///study-levels/{study_level_id}/{locale}
        public static async Task<ServerResponse<LevelJSON>> Level_study_request(string access_token, string study_level_id)
        {

            List<string> key_list = null;
            List<string> value_list = null;
            _access_token_API = access_token;
            return await Server.i_Get_server_data_API<LevelJSON>(_URL_BASE_API + _STUDY_LEVEL_SERVICE_API + "/"+study_level_id +_LOCALE_API, key_list, value_list, MethodHTTPType.GET);
        }
        //-----------------------------------------------------

        public static async Task<ServerResponse<List<CertificateJSON>>> Certificates_request(string access_token, long user_id, int language_id, string search_text)
        {
            
            List<string> key_list = null;
            List<string> value_list = null;
            key_list = new List<string>();
            value_list = new List<string>();
            key_list.Add("pagination");
            value_list.Add("false");
            key_list.Add("search[name]");
            value_list.Add(search_text);
            _access_token_API = access_token;
            return await Server.i_Get_server_data_API<List<CertificateJSON>>(_URL_BASE_API + _LANGUAGES_CERTTIFICATES_SERVICE_API + language_id, key_list, value_list, MethodHTTPType.GET);
        }

        //-----------------------------------------------------

        ///students/{student_id}/internships[/{internship_id}]
        public static async Task<ServerResponse<SaveDataJSON>> Internship_data_save(string access_token, long user_id, InternshipDataJSON internship)
        {
            List<string> key_list = null;
            List<string> value_list = null;
            string intership_id = internship.id != int.MinValue ? internship.id.ToString() : "";
            string pathurl = _URL_BASE_API + _VIEW_STUDENT_SERVICE_API + user_id.ToString() + _INTERNSHIPS_STUDENT_SERVICE_API;
            if (intership_id != "") {
                //update
                pathurl = pathurl + "/" + intership_id;
            }
            _access_token_API = access_token;
            key_list = new List<string>();
            value_list = new List<string>();
            key_list.Add("country_id");
            value_list.Add(internship.country_id.ToString());
            key_list.Add("description");
            value_list.Add(internship.description);
            key_list.Add("year");
            value_list.Add(internship.year.ToString("D4"));
            return await Server.i_Get_server_data_API<SaveDataJSON>(pathurl, key_list, value_list, MethodHTTPType.PUT);
        }

        //-----------------------------------------------------
        //DELETE /students/{student_id}/internships/{internship_id}
        public static async Task<ServerResponse<SaveDataJSON>> Internship_data_delete(string access_token, long user_id, long internship_id)
        {
            List<string> key_list = null;
            List<string> value_list = null;
            _access_token_API = access_token;
            return await Server.i_Get_server_data_API<SaveDataJSON>(_URL_BASE_API + _VIEW_STUDENT_SERVICE_API + user_id.ToString() + _INTERNSHIPS_STUDENT_SERVICE_API + "/" + internship_id.ToString(), key_list, value_list, MethodHTTPType.DELETE);
        }

        //-----------------------------------------------------


        ///students/{student_id}/experience[/{experience_id}]
        public static async Task<ServerResponse<SaveDataJSON>> Experience_data_save(string access_token, long user_id, ExperienceDataJSON experience)
        {
            List<string> key_list = null;
            List<string> value_list = null;
            _access_token_API = access_token;
            string company = "";
            string puesto = "";
            string pathurl = _URL_BASE_API + _VIEW_STUDENT_SERVICE_API + user_id.ToString() + _EXPERIENCE_STUDENT_SERVICE_API;
            if (experience.id != int.MinValue)
            {
                //update
                pathurl = pathurl+ "/" + experience.id.ToString();
            }
            if (experience.company.id != int.MinValue && experience.company.id != 0)
            {
                company = experience.company.id.ToString();
            }
            else {
                //grabamos la company 
                key_list = new List<string>();
                value_list = new List<string>();
                key_list.Add("forceCreation");
                value_list.Add("1");
                key_list.Add("name");
                value_list.Add(experience.company.name);
                ServerResponse<SaveDataJSON> company_json = await Server.i_Get_server_data_API<SaveDataJSON>(_URL_BASE_API + _CREATE_COMPANY_STUDENT_SERVICE_API, key_list, value_list, MethodHTTPType.POST);
                if (company_json.type == ResponseType.OK_SUCCESS)
                {
                    company = company_json.result_object.id.ToString();
                }
            }

            
                        
            if (experience.position.id != int.MinValue)
            {
                puesto = experience.position.id.ToString();
            }
            else {
                //crear posiicon
                key_list = new List<string>();
                value_list = new List<string>();
                key_list.Add("name");
                value_list.Add(experience.position.name);
                ServerResponse<SaveDataJSON> pos_json = await Server.i_Get_server_data_API<SaveDataJSON>(_URL_BASE_API + _POSITION_SERVICE_API, key_list, value_list, MethodHTTPType.POST);
                if (pos_json.type == ResponseType.OK_SUCCESS)
                {
                    puesto = pos_json.result_object.id.ToString();
                }
            }

            key_list = new List<string>();
            value_list = new List<string>();
            key_list.Add("company_id");
            value_list.Add(company);
            key_list.Add("position_id");
            value_list.Add(puesto);
            key_list.Add("started_at");
            value_list.Add(String.Format("{0}-{1}-{2}", experience.year_start.ToString("D4"), experience.month_start.ToString("D2"),"01"));
            key_list.Add("finished_at");
            value_list.Add(experience.year_end == 0 ? "" : String.Format("{0}-{1}-{2}", experience.year_end.ToString("D4"), experience.month_end.ToString("D2"), "01"));
            
            //key_list.Add("actually");
            //value_list.Add(experience.year_end == 0 ? "1" : "0");
            key_list.Add("country_id");
            value_list.Add(experience.country_id.ToString());
            key_list.Add("state_id");
            value_list.Add(experience.state.id.ToString());
            return await Server.i_Get_server_data_API<SaveDataJSON>(pathurl, key_list, value_list, MethodHTTPType.PUT);
        }

        //-----------------------------------------------------
        ///students/{student_id}/experience/{experience_id}
        public static async Task<ServerResponse<SaveDataJSON>> Experience_data_delete(string access_token, long user_id, long experience_id)
        {
            List<string> key_list = null;
            List<string> value_list = null;
            _access_token_API = access_token;
            return await Server.i_Get_server_data_API<SaveDataJSON>(_URL_BASE_API + _VIEW_STUDENT_SERVICE_API + user_id.ToString() + _EXPERIENCE_STUDENT_SERVICE_API + "/" + experience_id.ToString(), key_list, value_list, MethodHTTPType.DELETE);
        }


        
        ///students/{student_id}/voluntary_services[/{voluntary_service_id}]
        public static async Task<ServerResponse<SaveDataJSON>> Voluntary_data_save(string access_token, long user_id, VoluntaryDataJSON voluntary)
        {
            List<string> key_list = null;
            List<string> value_list = null;
            string voluntary_id = voluntary.id != int.MinValue ? voluntary.id.ToString() : "";
            string pathurl = _URL_BASE_API + _VIEW_STUDENT_SERVICE_API + user_id.ToString() + _VOLUNTARY_STUDENT_SERVICE_API;
            _access_token_API = access_token;
            key_list = new List<string>();
            value_list = new List<string>();
            if (voluntary_id != "") {
                //update
                pathurl = pathurl + "/" + voluntary_id;
            }
            key_list.Add("ong");
            value_list.Add(voluntary.ong);
            key_list.Add("started_at");
            value_list.Add(String.Format("{0}-{1}-{2}", voluntary.year_start.ToString("D4"), voluntary.month_start.ToString("D2"), "01"));
            key_list.Add("finished_at");
            value_list.Add(voluntary.year_end == 0 ? "" : String.Format("{0}-{1}-{2}", voluntary.year_end.ToString("D4"), voluntary.month_end.ToString("D2"), "01"));
            //key_list.Add("actually");
            //value_list.Add(voluntary.year_end == 0 ? "1" : "0");
            key_list.Add("country_id");
            value_list.Add(voluntary.country_id.ToString());
            key_list.Add("state_id");
            value_list.Add(voluntary.state.id.ToString());
            return await Server.i_Get_server_data_API<SaveDataJSON>(pathurl, key_list, value_list, MethodHTTPType.PUT);
        }

        //-----------------------------------------------------
        ///students/{student_id}/voluntary_services/{voluntary_service_id}
        public static async Task<ServerResponse<SaveDataJSON>> Voluntary_data_delete(string access_token, long user_id, long voluntary_id)
        {
            List<string> key_list = null;
            List<string> value_list = null;
            _access_token_API = access_token;
            return await Server.i_Get_server_data_API<SaveDataJSON>(_URL_BASE_API + _VIEW_STUDENT_SERVICE_API + user_id.ToString() + _VOLUNTARY_STUDENT_SERVICE_API + "/" + voluntary_id.ToString(), key_list, value_list, MethodHTTPType.DELETE);
        }

        //-----------------------------------------------------

        //public static async Task<ServerResponse<OtherDataJSON>> Other_data_request(string access_token, long user_id)
        //{
        //    List<string> key_list = null;
        //    List<string> value_list = null;
        //    Server.i_Get_common_request_params(user_id, access_token, ref key_list, ref value_list);
        //    key_list.Add("section");
        //    value_list.Add("other_data");
        //    key_list.Add("item_id");
        //    value_list.Add("1");
        //    return await Server.i_Get_server_data<OtherDataJSON>(_URL_BASE + _PROFILE_GET_SERVICE, key_list, value_list);
        //}

        //-----------------------------------------------------

        public static async Task<ServerResponse<SaveDataJSON>> Other_data_save(string access_token, long user_id, StudentDataJSON other)
        {
            List<string> key_list = null;
            List<string> value_list = null;
            _access_token_API = access_token;
            key_list = new List<string>();
            value_list = new List<string>();
            key_list.Add("other_data");
            value_list.Add(other.data);
            return await Server.i_Get_server_data_API<SaveDataJSON>(_URL_BASE_API + _VIEW_STUDENT_SERVICE_API+ user_id.ToString(), key_list, value_list,MethodHTTPType.PUT);
        }

        //-----------------------------------------------------

        //public static async Task<ServerResponse<CompaniesJSON>> Companies_request(string access_token, long user_id, int sector_id, int page,string search_text)
        //{
        //    List<string> key_list = null;
        //    List<string> value_list = null;
        //    Server.i_Get_common_request_params(user_id, access_token, ref key_list, ref value_list);
        //    key_list.Add("sector_id");
        //    value_list.Add(sector_id.ToString());
        //    key_list.Add("page");
        //    value_list.Add(page.ToString());
        //    key_list.Add("search_text");
        //    if (search_text != null)
        //    {
        //        value_list.Add(search_text);

        //    }
        //    else {
        //        value_list.Add("");
        //    }
            

        //    return await Server.i_Get_server_data<CompaniesJSON>(_URL_BASE + _COMPANY_LIST_SERVICE, key_list, value_list);
        //}

        public static async Task<ServerResponse<CompaniesJSON>> Companies_request(string access_token, long user_id, int sector_id, int page, string search_text)
        {
            List<string> key_list = null;
            List<string> value_list = null;
            _access_token_API = access_token;
            key_list = new List<string>();
            value_list = new List<string>();
            if (search_text != null)
            {
                if (search_text != "")
                {
                    //key_list.Add("search[name]");
                    //value_list.Add(search_text);
                    key_list.Add("free_search");
                    value_list.Add(search_text);
                }

            }

            if (sector_id > 0)
            {
                key_list.Add("sector_id");
                value_list.Add(sector_id.ToString());
            }

            
            key_list.Add("page");
            value_list.Add(page.ToString());
            key_list.Add("per_page");
            value_list.Add("5");
            //////if (search_text != null)
            //////{
            //////    if (search_text != "")
            //////    {
            //////        key_list.Add("search[name]");
            //////        value_list.Add(search_text);
            //////    }

            //////}

            ////string query = Server.i_Get_content_QueryString(key_list, value_list);
            ////key_list = new List<string>();
            ////value_list = new List<string>();

            //key_list.Add("country");
            //value_list.Add(_CULTURE_COUNTRY_API);
            
            
            //ServerResponse<CompaniesSingleJSON> prueba= await Server.i_Get_server_data_API<CompaniesSingleJSON>(_URL_BASE_API + _COMPANY_LIST_ORDERED_SERVICE_API + "?" + query, key_list, value_list, MethodHTTPType.PUT);
            //if (prueba.type == ResponseType.OK_SUCCESS) {

            //}
            ///return await Server.i_Get_server_data_API<CompaniesJSON>(_URL_BASE_API + _COMPANY_LIST_ORDERED_SERVICE_API+"?"+query, key_list, value_list, MethodHTTPType.GET);
            return await Server.i_Get_server_data_API<CompaniesJSON>(_URL_BASE_API + _COMPANY_LIST_ORDERED_SERVICE_API, key_list, value_list, MethodHTTPType.GET);
        }


        public static async Task<ServerResponse<List<CompanySectorJSON>>> company_sectors_request(string access_token, string company_id)
        {
            List<string> key_list = null;
            List<string> value_list = null;
            _access_token_API = access_token;
            return await Server.i_Get_server_data_API<List<CompanySectorJSON>>(_URL_BASE_API + _COMPANY_LIST_SERVICE_API +"/"+ company_id + _COMPANY_SECTORS_SERVICE_API +_LOCALE_API, key_list, value_list, MethodHTTPType.GET); ;

        }


        //-----------------------------------------------------

        //public static async Task<ServerResponse<CompanyDetailJSON>> Company_detail_request(string access_token, long user_id, int company_id)
        //{
        //    List<string> key_list = null;
        //    List<string> value_list = null;
        //    Server.i_Get_common_request_params(user_id, access_token, ref key_list, ref value_list);
        //    key_list.Add("company_id");
        //    value_list.Add(company_id.ToString());
        //    return await Server.i_Get_server_data<CompanyDetailJSON>(_URL_BASE + _COMPANY_DETAIL_SERVICE, key_list, value_list);
        //}

        //-----------------------------------------------------

        public static async Task<ServerResponse<SaveDataJSON>> Settings_save(string access_token, long user_id, bool public_profile, bool newsletter, string old_password, string new_password,bool notify)
        {
            ServerResponse<SaveDataJSON> pass_json = new ServerResponse<SaveDataJSON>();
            pass_json.type = ResponseType.OK_SUCCESS;
            pass_json.result_object = new SaveDataJSON();
            pass_json.result_object.result = true;
            pass_json.message = "";

            List<string> key_list = null;
            List<string> value_list = null;
            List<string> key_list_Parcial = null;
            List<string> value_list_Parcial = null;
            bool regOk = true;
            
            _access_token_API = access_token;

            
            regOk = true;
            if (String.IsNullOrEmpty(old_password) == false)
            {
                //key_list.Add("change_password");
                //value_list.Add(i_bool(true));
                key_list_Parcial = new List<string>();
                value_list_Parcial = new List<string>();
                key_list_Parcial.Add("oldPassword");
                value_list_Parcial.Add(old_password);
                key_list_Parcial.Add("newPassword");
                value_list_Parcial.Add(new_password);
                pass_json = await Server.i_Get_server_data_API<SaveDataJSON>(_URL_BASE_API + _VIEW_STUDENT_SERVICE_API + user_id.ToString()+_CHANGE_PASSWORD_SERVICE_API, key_list_Parcial, value_list_Parcial, MethodHTTPType.POST);
                if (pass_json.type == ResponseType.OK_SUCCESS)
                {
                    regOk = true;
                    pass_json.result_object.result = true;
                    pass_json.message = "";
                }
                else
                {
                    regOk = false;
                    pass_json.type = ResponseType.SERVICE_ERROR;
                    pass_json.result_object.result = false;
                    pass_json.message = Local.txtSettings19;
                    return pass_json;
                }
            }
            //else
            //{
            //    key_list.Add("change_password");
            //    value_list.Add(i_bool(false));
            //}
            key_list = new List<string>();
            value_list = new List<string>();
            key_list.Add("wants_offers");
            value_list.Add(i_bool(public_profile));
            key_list.Add("newsletter");
            value_list.Add(i_bool(newsletter));
            key_list.Add("notify");
            value_list.Add(i_bool(notify));
            device_id = Xamarin.Forms.DependencyService.Get<IDeviceService>().DeviceId();
            string device_type = Xamarin.Forms.Device.OnPlatform("iphone", "android", "win");
            key_list.Add("device_token");
            value_list.Add(AppDelegate._device_token==null ? "": AppDelegate._device_token);
            
            key_list.Add("device_type");
            value_list.Add(device_type);

            key_list.Add("device_id");
            value_list.Add(device_id);

            return await Server.i_Get_server_data_API<SaveDataJSON>(_URL_BASE_API + _VIEW_STUDENT_SERVICE_API+user_id.ToString(), key_list, value_list,MethodHTTPType.PUT);
        }

        //-----------------------------------------------------

        public static string Legal_page()
        {
            return Server._URL_BASE + Server._LEGAL_PAGE;
        }

    }
}
