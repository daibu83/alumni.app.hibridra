﻿namespace Bewanted
{
    class Local
    {
        // Common
        public static string txtCommon00 = "Campo obligatorio.";
        public static string txtCommon01 = "Mínimo {0} caracteres.";
        public static string txtCommon02 = "Máximo {0} caracteres.";
        public static string txtCommon03 = "Sin coincidencia '{0}'.";
        public static string txtCommon04 = "No es un número.";
        public static string txtCommon05 = "El mínimo es ";
        public static string txtCommon06 = "El máximo es ";
        public static string txtCommon07 = "Aplicación ocupada..., por favor, espere.";
        public static string[] txtMonth = new string[] { "Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre" };
        public static string txtEmail = "Email";
        public static string txtEmailError = "Por favor, introduce una dirección de email válida.";
        public static string txtPass = "Contraseña";
        

        // Alert
        public static string txtAlert00 = "Sin Internet";
        public static string txtAlert01 = "Revisa tu conexión e inténtalo de nuevo.";   
        public static string txtAlert02 = "Sin Servicio";
        public static string txtAlert03 = "No se puede conectar con el servidor de Bewanted (auth).";
        public static string txtAlert04 = "Sin Servicio";
        public static string txtAlert05 = "No se puede conectar con el servidor de Bewanted (server).";
        public static string txtAlert06 = "Sin Servicio";
        public static string txtAlert07 = "No se puede conectar con el servidor de Bewanted (url).";
        public static string txtAlert08 = "Error al grabar";
        public static string txtAlert09 = "No se pudieron grabar lso cambios.";
        public static string txtAlertOk = "Ok";
        public static string txtErrServer = "Error al acceder al servicio";
        //txtErrServer
        // LoginPage
        public static string txtLogin00 = "¿Olvidaste tu contraseña?";
        public static string txtLogin01 = "Haz click aquí";
        public static string txtLogin02 = "Entrar";

        // UserDataPage
        public static string txtUser00 = "Nombre";
        public static string txtUser01 = "Apellidos";
        public static string txtUser02 = "Teléfono";
        public static string txtUser03 = "Nacionalidad";
        public static string txtUser04 = "País de residencia";
        public static string txtUser05 = "Provincia";
        public static string txtUser06 = "Ciudad";
        public static string txtUser07 = "Fecha de nacimiento";

        // CreateAccountPage
        public static string txtCreate00 = "Creando una cuenta estás aceptando nuestro";
        public static string txtCreate01 = "Aviso legal";
        public static string txtCreate02 = "y recibir comunicaciones de Bewanted";
        public static string txtCreate03 = "Crear cuenta";
        public static string txtCreate04 = "Contraseña * (mín. 6 caract.)";

        // RecoverPage
        public static string txtRecover00 = "Recordar contraseña";
        public static string txtRecover01 = "Te mandaremos un email con las instrucciones para restablecer tu contraseña.";
        public static string txtRecover02 = "Restablecer contraseña";
        public static string txtRecover04 = "Ok";
        public static string txtRecover05 = "Introduce tu e-mail";

        // LegalPage
        public static string txtLegal00 = "Aviso legal";

        // PanelPage
        public static string txtPanel00 = "Mi panel";
        public static string txtPanel01 = "Ver";
        public static string txtPanel02 = "Todas";
        public static string txtPanel03 = "Nuevas";
        public static string txtPanel04 = "Aceptadas";
        public static string txtPanel05 = "Rechazadas";
        public static string txtPanel06 = "Descartadas";

        public static string txtPanel07 = "Ninguna oferta disponible";
        public static string txtPanel08 = "Actualiza tu currículum para que";
        public static string txtPanel09 = "las empresas te encuentren";
        public static string txtPanel10 = "y te inviten a procesos";

        public static string txtPanel11 = "Mensajes";
        public static string txtPanel12 = "Completado";
        public static string txtPanel13 = " Incompleto ";

        public static string txtPanel14 = "Completa tu perfil y";
        public static string txtPanel15 = " tus datos personales para que";
        public static string txtPanel16 = " las empresas te encuentren";
        public static string txtPanel17 = " y te inviten a procesos";

        public static string txtPanel18 = "Mensaje";

        // OfferPage
        public static string txtOffer00 = "Oferta";
        public static string txtOffer01 = "Descripción";
        public static string txtOffer18 = "Compañía";
        public static string txtOffer19 = "Candidato";
        public static string txtOffer02 = "Contacto";
        public static string txtOffer03 = "Puesto";
        public static string txtOffer04 = "Vacantes";
        public static string txtOffer05 = "Tipo de contrato";
        public static string txtOffer06 = "Horario";
        public static string txtOffer07 = "Mañana";
        public static string txtOffer08 = "Tarde";
        public static string txtOffer09 = "Jornada completa";
        public static string txtOffer11 = "Información adicional";
        public static string txtOffer12 = "Aceptar";
        public static string txtOffer13 = "Rechazar";
        public static string txtOffer14 = "Abandonar proceso";
        public static string txtOffer15 = "Enviar mensaje";
        public static string txtOffer16 = "Oferta aceptada";
        public static string txtOffer17 = "Salario";
        public static string txtOffer20 = "Práctica";
        public static string txtOffer21 = "Beca";
        public static string txtOffer22 = "Voluntariado";
        public static string txtOffer23 = "Contrato";
        public static string txtOffer24 = "Oferta aceptada";
        public static string txtOffer25 = "Ok";
        public static string txtOffer26 = "Oferta rechazada";
        public static string txtOffer27 = "Abandonado el proceso de selección";
        public static string txtOffer28 = "Mensajes relacionados con esta oferta";

        // KillerPage
        public static string txtKiller00 = "Killers Questions";
        public static string txtKiller01 = "Responde a las preguntas para participar en el proceso";
        public static string txtKiller03 = "Escribe tu respuesta";
        public static string txtKiller04 = "Enviar";
        public static string txtKiller05 = "Cancelar";
        public static string txtKiller06 = "Killer questions enviadas correctamente";
        public static string txtKiller07 = "Ok";

        // MessagesPage
        public static string txtMessages00 = "Mensajes";
        public static string txtMessages01 = "Nuevo mensaje";
        public static string txtMessages02 = "Para";
        //public static string txtMessages03 = "Asunto";
        public static string txtMessages04 = "Escribe tu mensaje";
        public static string txtMessages05 = "Enviar";
        public static string txtMessages06 = "Cancelar";
        public static string txtMessages07 = "Mensaje enviado correctamente";
        public static string txtMessages08 = "Ok";
        public static string txtMessages09 = "Yo";
        public static string txtMessages10 = "Empresa";
        public static string txtMessages11 = "Respuesta enviada";

        // ButtonsLayout
        public static string txtButtons01 = "Guardar";
        public static string txtButtons02 = "Cancelar";
        public static string txtButtons03 = "Eliminar";
        public static string txtButtons05 = "Si";
        public static string txtButtons06 = "No";
        public static string txtButtons07 = "¿Desea continuar?";
        public static string txtButtons08 = "Se perderán los cambios";

        // ProfilePage
        public static string txtProfile00 = "Editar perfil";
        public static string txtProfile01 = "Perfil  ";
        public static string txtProfile02 = "  completado";
        public static string txtProfile10 = "Busco";
        public static string txtProfile11 = "Puesto";
        public static string txtProfile12 = "Sector";
        public static string txtProfile13 = "Horario";
        public static string txtProfile14 = "Preferencias geográficas";
        public static string txtProfile15 = "Fecha disponible";
        public static string txtProfile16 = "Debes introducir al menos un estudio para poder acceder al mercado.";

        public static string txtProfile17 = "¡Recuerda completar tus ";
        public static string txtProfile18 = "datos personales";
        public static string txtProfile19 = " para empezar a recibir ofertas!";

        public static string txtProfile20 = "¡Aún no has completado los ";
        public static string txtProfile21 = "datos mínimos de tu perfil";
        public static string txtProfile22 = " y no puedes recibir ofertas!";

        public static string txtProfile26 = "Felicidades!";
        public static string txtProfile23 = "Ya tienes completos los ";
        public static string txtProfile24 = "datos mínimos";
        public static string txtProfile25 = " de tu perfil. Cuanto más completo tengas tu perfil, más posibilidades tendrás de recibir una oferta.";

        public static string txtProfile27 = "¡Recuerda completar tus datos personales!";
        public static string txtProfile28 = "Debes completar tus datos personales para poder acceder a estudios.";

        public static string txtProfile29 = "Primer empleo";
        public static string txtProfile30 = "Prácticas";
        public static string txtProfile31 = "Voluntariado";

        public static string txtProfile32 = "Cualquier parte del mundo";
        public static string txtProfile33 = "Cualquier lugar de tu país";

        public static string txtProfile34 = "Mañana";
        public static string txtProfile35 = "Tarde";
        public static string txtProfile36 = "Jornada completa";
        public static string txtProfile37 = "No definido";

        public static string txtProfile38 = "Cualquier posición";
        public static string txtProfile39 = "Cualquier sector";

        // IntervalLayout
        public static string txtInterval00 = "Fecha inicio";
        public static string txtInterval01 = "Fecha fin";
        public static string txtInterval02 = "Mes";
        public static string txtInterval03 = "Año";

        // PersonalPage
        public static string txtPersonal00 = "Datos personales";    
        public static string txtPersonal01 = "Cambiar la imagen de perfil";
        public static string txtPersonal02 = "Selecciona una imagen";
        public static string txtPersonal04 = "Url";
        public static string txtPersonal05 = "Imagen no válida";
        public static string txtPersonal06 = "Tamaño recomendado 237x237px";
        public static string txtPersonal07 = "Tamaño recomendado 1:1";

        public static string txtPersonal08 = "¡Completa tu ";
        public static string txtPersonal09 = "perfil y datos personales"; //bold
        public static string txtPersonal10 = " para poder recibir ofertas!";

        public static string txtPersonal11 = "¡Enhorabuena!";
        public static string txtPersonal12 = "Ya tienes completados tus datos personales."; //bold

        public static string txtPersonal13 = "Datos personales actualizados";
        public static string txtPersonal14 = "Completa tu perfil para empezar a recibir ofertas."; //bold


        public static string txtPersonal15 = "Datos personales";
        public static string txtPersonal16 = "Actualizados correctamente."; 


        // AcademicPage
        public static string txtAcademic00 = "Estudios y lugar de estudio";
        public static string txtAcademic01 = "País";
        public static string txtAcademic02 = "Provincia";
        public static string txtAcademic03 = "Universidad / Centro de estudios";
        public static string txtAcademic04 = "Título y carrera";
        public static string txtAcademic09 = "Estudio aquí actualmente";
        public static string txtAcademic10 = "Nota media";
        public static string txtAcademic13 = "MH";
        public static string txtAcademic14 = "Rama de estudios";
        public static string txtAcademic15 = "Asignatura";
        public static string txtAcademic16 = "Nota";
        public static string txtAcademic17 = "Añadir un nuevo estudio";
        public static string txtAcademic19 = "(MH Matrícula de Honor)";
        public static string txtAcademic20 = "¿Seguro que deseas eliminar este estudio?";
        public static string txtAcademic21 = "Tipo de estudios";

        // MerchantPage
        public static string txtMerchant00 = "Mercado";
        public static string txtMerchant01 = "¿Que buscas?";
        public static string txtMerchant02 = "Primer empleo";
        public static string txtMerchant03 = "Beca";
        public static string txtMerchant19 = "Ong";
        public static string txtMerchant10 = "Horario";
        public static string txtMerchant20 = "Fecha disponible";
        public static string txtMerchant11 = "Mañana";
        public static string txtMerchant12 = "Tarde";
        public static string txtMerchant13 = "Jornada completa";
        public static string txtMerchant04 = "Puesto";
        public static string txtMerchant05 = "Cualquier posición";
        public static string txtMerchant06 = "Posición";
        public static string txtMerchant07 = "Sector";
        public static string txtMerchant08 = "Cualquier sector";
        public static string txtMerchant09 = "Sector";
        public static string txtMerchant14 = "¿Dónde estás buscando?";
        public static string txtMerchant16 = "Cualquier lugar de tu país";
        public static string txtMerchant15 = "Cualquier parte del mundo";
        public static string txtMerchant17 = "País";
        public static string txtMerchant26 = "Provincia";
        public static string txtMerchant27 = "Empleo no remunerado";
        public static string txtMerchant28 = "Recibir ofertas";

        // LanguagePage
        public static string txtLanguage00 = "Idiomas";
        public static string txtLanguage01 = "Idioma";
        public static string txtLanguage02 = "Nivel";
        public static string txtLanguage03 = "Certificado";
        public static string txtLanguage07 = "Añadir un nuevo idioma";
        public static string txtLanguage08 = "¿Seguro que deseas eliminar este idioma?";

        // InternshipPage
        public static string txtInternship00 = "Programa de intercambio";
        public static string txtInternship01 = "Programa de intercambio";
        public static string txtInternship02 = "Año";
        public static string txtInternship03 = "País";
        public static string txtInternship10 = "Añadir un nuevo intercambio";
        public static string txtInternship11 = "¿Seguro que deseas eliminar este intercambio?";

        // ExperiencePage
        public static string txtExperience00 = "Experiencia";
        public static string txtExperience01 = "Empresa";
        public static string txtExperience02 = "Posición";
        public static string txtExperience03 = "Trabajo aquí actualmente";
        public static string txtExperience04 = "País";
        public static string txtExperience05 = "Provincia";
        public static string txtExperience12 = "Añadir una nueva experiencia";
        public static string txtExperience13 = "¿Seguro que deseas eliminar esta experiencia?";

        // VoluntaryPage
        public static string txtVoluntary00 = "Voluntariado";
        public static string txtVoluntary01 = "Ong";
        public static string txtVoluntary02 = "Soy voluntario aquí actualmente";
        public static string txtVoluntary03 = "País";
        public static string txtVoluntary04 = "Provincia";
        public static string txtVoluntary05 = "Añadir un nuevo voluntariado";
        public static string txtVoluntary06 = "¿Seguro que deseas eliminar este voluntariado?";

        // OtherPage
        public static string txtOther00 = "Información adicional";
        public static string txtOther01 = "Deportes, aficiones, cursos, viajes, etc.";
        public static string txtOther02 = "Borrar texto";

        // CompanyPage
        public static string txtCompany00 = "Empresas";
        public static string txtCompany01 = "Filtrar por ";
        public static string txtCompany02 = "Sector";
        public static string txtCompany03 = "Ninguna empresa que mostrar";
        public static string txtCompany04 = "Selecciona otro filtro";
        public static string txtCompany05 = "Todas";
        public static string txtCompany06 = "Descripción";
        public static string txtCompany07 = "Dirección";

        // HowToPage
        public static string txtHowto00 = "Cómo funciona";
        public static string txtHowto01 = "Cómo funciona Bewanted";
        public static string txtHowto02 = "Gracias por registrarte";
        public static string txtHowto03 = "¡Entendido!";
        public static string txtHowto04 = "Completa tu perfil";
        public static string txtHowto05 = "Valida tu email de validación que te hemos enviado y completa tu perfil para que las empresas te encuentren.";
        public static string txtHowto06 = "Las empresas buscan";
        public static string txtHowto07 = "Crean procesos de selección y buscan perfiles que se adapten a sus necesidades entre todos los registrados.";
        public static string txtHowto08 = "Te invitan a procesos";
        public static string txtHowto09 = "La empresa te invitará a participar en su proceso de selección a través de la plataforma y con un mail.";
        public static string txtHowto10 = "Gestiona tus procesos";
        public static string txtHowto11 = "Gestiona las ofertas desde tu panel. Así podrás ver la información o el estado y contestar a la invitación.";
        public static string txtHowto12 = "Gracias por registrarte";
        public static string txtHowto13 = "¡Entendido!";

        // SettingsPage
        public static string txtSettings00 = "Ajustes";
        public static string txtSettings01 = "Cerrar sesión";
        public static string txtSettings02 = "Soporte";
        public static string txtSettings03 = "Aviso legal";
        public static string txtSettings04 = "Configuración";
        public static string txtSettings05 = "Recibir ofertas de trabajo";
        public static string txtSettings06 = "Recibir comunicaciones Bewanted";
        public static string txtSettings07 = "Cambiar contraseña";
        public static string txtSettings08 = "Introduce la contraseña actual";
        public static string txtSettings09 = "Introduce nueva contraseña";
        public static string txtSettings10 = "Repite contraseña";
        public static string txtSettings11 = "Guardar";
        public static string txtSettings12 = "Bewanted";
        public static string txtSettings13 = "Versión 1.1.0";
        public static string txtSettings14 = "'Repetir contraseña' no coincide con la nueva contraseña.";
        public static string txtSettings15 = "Ok";
        public static string txtSettings16 = "Ajustes guardados correctamente";
        public static string txtSettings17 = "Permitir notificaciones";
        public static string txtSettings18 = "beWanted podrá enviarle Notificaciones, active/desactive la opción que le interese desde 'Ajustes'";
        public static string txtSettings19 = "Error al cambiar pass";

        // Google Analytics
        public static string txtGA00 = "Login";
        public static string txtGA01 = "Estudios";
        public static string txtGA02 = "Listado Empresas";
        public static string txtGA03 = "Detalle Empresa";
        public static string txtGA04 = "Registro";
        public static string txtGA05 = "Experiencia";
        public static string txtGA06 = "Cómo Funciona";
        public static string txtGA07 = "Programa de Intercambio";
        public static string txtGA08 = "Killer Questions";
        public static string txtGA09 = "Idiomas";
        public static string txtGA10 = "Aviso Legal";
        public static string txtGA11 = "Menú";
        public static string txtGA12 = "Mercado";
        public static string txtGA13 = "Mensajes";
        public static string txtGA14 = "Nuevo Mensaje";
        public static string txtGA15 = "Detalle Oferta";
        public static string txtGA16 = "Información Adicional";
        public static string txtGA17 = "Listado Ofertas";
        public static string txtGA18 = "Datos Personales";
        public static string txtGA19 = "Editar Perfil";
        public static string txtGA20 = "Recordar Contraseña";
        public static string txtGA21 = "Ajustes";
        public static string txtGA22 = "Voluntariado";
    }
}

