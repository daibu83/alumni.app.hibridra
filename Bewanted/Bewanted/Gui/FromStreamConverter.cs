﻿using System;
using System.Globalization;
using System.IO;
using Xamarin.Forms;

namespace Bewanted
{
    public class FromStreamConveter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {

            //if (Device.OS == TargetPlatform.Android)
            //{
            //    var image = value as byte[];
            //    if (image == null)
            //        return null;
            //    return ImageSource.FromStream(() => new MemoryStream(image));
            //}
            //else {
            //    return null;
            //}

            var image = value as byte[];
            if (image == null)
                return null;
            return ImageSource.FromStream(() => new MemoryStream(image));


        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}