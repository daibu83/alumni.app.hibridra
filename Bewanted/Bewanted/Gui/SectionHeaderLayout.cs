﻿using Xamarin.Forms;
using C2Forms;

namespace Bewanted
{
    public class SectionHeaderLayout : StackLayout
    {
        public SectionHeaderLayout(string title) : base()
        {
            Label label;
            this.Padding = new Thickness(C2Style.Entry_padding_horizontal, AppStyle.Layout_stack_long_spacing, 0, AppStyle.Layout_stack_long_spacing);
            label = new Label { Text = title, Style = AppStyle.Label_header };
            label.FontSize = C2Style.Entry_font_size;
            this.Children.Add(label);
        }

        //-----------------------------------------------------


    }
}
