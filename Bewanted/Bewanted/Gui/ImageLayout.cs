﻿using System.Collections.Generic;
using Xamarin.Forms;
using C2Forms;
using System;

namespace Bewanted
{
    
    

    public class ImageItem
    {
        private string _image;
        private byte[] _imageUrl;
        private string _title;
        private string _detail1;
        private string _detail2;
        private bool _detail2visible;
        private string _detail3;
        private bool _detail3visible;
        private string _linklabel;
        private bool _linklabelvisible;
        private string _estadoOferta;
        private string _msgOferta;
        private Color _estadoOfertaColor;
        private Color _estadoOfertaBorderColor;
        //private string _noleidos;
        public int _index;
        public int _id;
        public bool _empresa;


         //-----------------------------------------------------

        public string Image
        {
            get { return this._image; }
            set { this._image = value; }
        }

        public byte[] ImageUrl
        {
            get { return this._imageUrl; }
            set { this._imageUrl = value; }
        }

        //-----------------------------------------------------

        public string Title
        {
            get { return this._title; }
            set { this._title = value; }
        }

        //-----------------------------------------------------

        public string Detail1
        {
            get { return this._detail1; }
            set { this._detail1 = value; }
        }

        //-----------------------------------------------------

        public string Detail2
        {
            get { return this._detail2; }
            set { this._detail2 = value; }
        }
        public bool Detail2Visible
        {
            get { return this._detail2visible; }
            set { this._detail2visible = value; }
        }

        public string LinkLabel
        {
            get { return this._linklabel; }
            set { this._linklabel = value; }
        }
        public bool LinkLabelVisible
        {
            get { return this._linklabelvisible; }
            set { this._linklabelvisible = value; }
        }
        //-----------------------------------------------------

        public string Detail3
        {
            get { return this._detail3; }
            set { this._detail3 = value; }
        }
        public bool Detail3Visible
        {
            get { return this._detail3visible; }
            set { this._detail3visible = value; }
        }
        public string EstadoOferta
        {
            get { return this._estadoOferta; }
            set { this._estadoOferta = value; }
        }
        public string MsgOferta
        {
            get { return this._msgOferta; }
            set { this._msgOferta = value; }
        }

        public Color EstadoOfertaColor
        {
            get { return this._estadoOfertaColor; }
            set { this._estadoOfertaColor = value; }
        }

        public Color EstadoOfertaBorderColor
        {
            get { return this._estadoOfertaBorderColor; }
            set { this._estadoOfertaBorderColor = value; }
        }
        //public string NoLeidos
        //{
        //    get { return this._noleidos; }
        //    set { this._noleidos = value; }
        //}

    }


    public class ImageItemEmpresa
    {
        private string _image;
        private byte[] _imageUrl;
        private string _title;
        private string _detail1;
        private string _detail2;
        private bool _detail2visible;


        public int _index;
        public int _id;
        public bool _empresa;


        //-----------------------------------------------------

        public string Image
        {
            get { return this._image; }
            set { this._image = value; }
        }

        public byte[] ImageUrl
        {
            get { return this._imageUrl; }
            set { this._imageUrl = value; }
        }

        //-----------------------------------------------------

        public string Title
        {
            get { return this._title; }
            set { this._title = value; }
        }

        //-----------------------------------------------------

        public string Detail1
        {
            get { return this._detail1; }
            set { this._detail1 = value; }
        }

        //-----------------------------------------------------

        public string Detail2
        {
            get { return this._detail2; }
            set { this._detail2 = value; }
        }

        public bool Detail2Visible
        {
            get { return this._detail2visible; }
            set { this._detail2visible = value; }
        }


    }


    //-----------------------------------------------------

    public class ImageCell : ViewCell
    {
        public ImageLayout layout;
        public ImageCell()
        {

            this.layout = new ImageLayout(AppStyle.Image_size, false, true);
            this.View = this.layout.Right_arrow_view(layout);
        }

        public ImageCell(bool bolamsg)
        {

            this.layout = new ImageLayout(AppStyle.Image_size,false,bolamsg);
            this.View = UserPage.Right_arrow_view(layout, null,true,null);
        }

        //-----------------------------------------------------

            /*
        protected override void OnBindingContextChanged()
        {
            base.OnBindingContextChanged ();
            this.Height = Device.OnPlatform(100.0, -1.0, 0.0);
        }*/
    }

    public class ImageCellEmpresa : ViewCell
    {
        public ImageCellEmpresa()
        {

            ImageLayout layout = new ImageLayout(AppStyle.Image_size, true,false);
            this.View = UserPage.Right_arrow_view(layout, null,true,null);
        }

        //-----------------------------------------------------

        /*
    protected override void OnBindingContextChanged()
    {
        base.OnBindingContextChanged ();
        this.Height = Device.OnPlatform(100.0, -1.0, 0.0);
    }*/
    }

    //-----------------------------------------------------

    public class ImageLayout : StackLayout
    {
        private Image _image;
        private Label _title;
        private Label _detail1;
        private Label _detail2;
        private Label _detail3;
        private C2HyperLinkLabel _linklabel;
        private C2Badge _badge1;
        private C2Badge2 _estadoOferta;
        //private C2Badge _noleidos;
        private StackLayout _icons_layout;
        private static readonly Color _NEW_OFERT = Color.FromHex("bfd3a1");
        private static readonly Color _ACCEPT_OFERT = Color.FromHex("8ecee9");
        private static readonly Color _REFUSE_OFERT = Color.FromHex("b4b9bf");
        private static readonly Color _DISCARD_OFERT = Color.FromHex("b4b9bf");

        //-----------------------------------------------------

        public ImageLayout(double image_size,bool empresa,bool bolamsg)
        {
            StackLayout text_layout;
            StackLayout detail_layout;
            
            Grid msg;
            if (bolamsg==false)
            {
                msg = new Grid
                {
                    //VerticalOptions = LayoutOptions.FillAndExpand,
                    RowDefinitions =
                    {
                        new RowDefinition { Height =new GridLength(1, GridUnitType.Star)},

                    },
                    ColumnDefinitions =
                    {

                        new ColumnDefinition { Width = new GridLength(image_size, GridUnitType.Absolute) },
                        new ColumnDefinition { Width = new GridLength(1, GridUnitType.Star) }
                    }
                };

            }
            else {
                msg = new Grid
                {

                    RowDefinitions =
                    {
                        new RowDefinition { Height =new GridLength(1, GridUnitType.Star)},

                    },
                    //VerticalOptions = LayoutOptions.FillAndExpand,
                    ColumnDefinitions =
                    {

                    //new ColumnDefinition { Width = new GridLength(.3, GridUnitType.Star) },
                    new ColumnDefinition { Width = new GridLength(image_size, GridUnitType.Absolute) },
                    new ColumnDefinition { Width = new GridLength(1, GridUnitType.Star) }
                    //new ColumnDefinition { Width = new GridLength(1, GridUnitType.Star) }
                        //new ColumnDefinition { Width = new GridLength(.6, GridUnitType.Star) },
                        //new ColumnDefinition { Width = new GridLength(.1, GridUnitType.Star) }
                    }
                };

            }




            this.Orientation = StackOrientation.Horizontal;
            this.Spacing = 10;
            this.VerticalOptions = LayoutOptions.StartAndExpand;
            this.HorizontalOptions = LayoutOptions.StartAndExpand;
            this._image = new Image { Aspect = Aspect.AspectFit } ;
            this._image.WidthRequest = image_size;
            this._image.HeightRequest = image_size;
            msg.Children.Add(this._image, 0, 0);
            //this.Children.Add(this._image);
            text_layout = new StackLayout();
            
            text_layout.Spacing = 5;
            text_layout.Padding = new Thickness(5, 0, 0, 0);
            text_layout.VerticalOptions = LayoutOptions.StartAndExpand;
            text_layout.HorizontalOptions = LayoutOptions.FillAndExpand;

            detail_layout = new StackLayout();
            if (empresa) { detail_layout.Spacing = 2; } else { detail_layout.Spacing = 2; }
            
            detail_layout.VerticalOptions = LayoutOptions.StartAndExpand;
            detail_layout.HorizontalOptions = LayoutOptions.Start;
            this._title = new Label { Text = "", Style = AppStyle.Label_header };
            
            this._title.FontSize = C2Style.Entry_font_size;
            this._title.FontAttributes = FontAttributes.Bold;
            //this._title.LineBreakMode = LineBreakMode.TailTruncation;
            this._detail1 = new Label { Text = "", Style = AppStyle.Label };
            this._detail2 = new Label { Text = "", Style = AppStyle.Label };
            this._detail3 = new Label { Text = "", Style = AppStyle.Label };
            //this._image.SetBinding(C2Image.TextUrlProperty, "ImageUrl");
            //this._image.SetBinding(Image.SourceProperty, "Image");
            this._image.SetBinding(Image.SourceProperty, "ImageUrl", BindingMode.OneWay, converter: new FromStreamConveter());
            //if (Device.OS == TargetPlatform.Android)
            //{
            //    this._image.SetBinding(Image.SourceProperty, "ImageUrl", BindingMode.OneWay, converter: new FromStreamConveter());
            //}
            //else {
            //    this._image.SetBinding(Image.SourceProperty, "Image");
            //}
            
            this._title.SetBinding(Label.TextProperty, "Title");
            this._detail1.SetBinding(Label.TextProperty, "Detail1");
            this._detail2.SetBinding(Label.TextProperty, "Detail2");
            this._detail3.SetBinding(Label.TextProperty, "Detail3");
            this._detail2.SetBinding(Label.IsVisibleProperty, "Detail2Visible");
            this._detail3.SetBinding(Label.IsVisibleProperty, "Detail3Visible");
            this._linklabel = new C2HyperLinkLabel { Text = "", Style = AppStyle.Label_link };
            //this._linklabel.TextColor = Color.Blue;
            this._linklabel.IsUnderline = true;
            this._linklabel.SetBinding(Label.TextProperty, "LinkLabel");
            this._linklabel.SetBinding(Label.IsVisibleProperty, "LinkLabelVisible");
            this._linklabel.Subject = "enlace";


            


            this._estadoOferta = new C2Badge2(Color.White, Color.White);

            //del marco
            this._estadoOferta.WidthRequest = 80;
            this._estadoOferta.HorizontalOptions = LayoutOptions.Start;
            this._estadoOferta.VerticalOptions = LayoutOptions.Center;
            this._estadoOferta.Espacios = new Thickness(3, 3, 2, 3);
            this._estadoOferta.BorderColor = Color.White;
            this._estadoOferta.BorderWidth = 2;
            this._estadoOferta.Radious = 5;

            //del texto
            this._estadoOferta.FontSize = Device.GetNamedSize(NamedSize.Micro, typeof(Label)); ;
            this._estadoOferta.Horizontal = LayoutOptions.Center;
            this._estadoOferta.Vertical = LayoutOptions.Center;

            this._estadoOferta.SetBinding(C2Badge2.TextProperty, "EstadoOferta");
            this._estadoOferta.SetBinding(C2Badge2.TextColorProperty, "EstadoOfertaColor");
            this._estadoOferta.SetBinding(C2Badge2.BorderColorProperty, "EstadoOfertaBorderColor");


            text_layout.Children.Add(this._title);
            detail_layout.Children.Add(this._detail1);
            detail_layout.Children.Add(this._detail2);
            if (empresa == false)
            {
                detail_layout.Children.Add(this._linklabel);
                detail_layout.Children.Add(this._detail3);
                detail_layout.Children.Add(this._estadoOferta);
            }
            

            this._icons_layout = new StackLayout();
            this._icons_layout.Orientation = StackOrientation.Horizontal;
            this._icons_layout.Spacing = 2;
            this._icons_layout.IsVisible = false;
            

            text_layout.Children.Add(detail_layout);
            text_layout.Children.Add(this._icons_layout);
            msg.Children.Add(text_layout, 1, 0);



            //this.Children.Add(text_layout);
            //if (bolamsg == true) {
            //    double size;
            //    double font_size;
            //    font_size = Device.GetNamedSize(NamedSize.Small, typeof(Label));
            //    size = 1.6 * font_size;
            //    this._noleidos = new C2Badge(size, size, font_size);
            //    this._noleidos.BoxColor = AppStyle.Mensajes_panel_label1;
            //    //_badge1.HeightRequest = 31;

            //    //this._noleidos.HorizontalOptions = LayoutOptions.End;
            //    //this._noleidos.VerticalOptions = LayoutOptions.Center;
            //    this._noleidos.VerticalOptions = LayoutOptions.CenterAndExpand;
            //    this._noleidos.HorizontalOptions = LayoutOptions.End;
            //    this._noleidos.SetBinding(C2Badge.TextProperty, "NoLeidos");
            //    this._noleidos.IsVisible = false;
            //    //this._noleidos.TranslationX = 5;
            //    StackLayout layout = new StackLayout();
            //    layout.Orientation = StackOrientation.Vertical;
            //    layout.VerticalOptions = LayoutOptions.FillAndExpand;
            //    layout.HorizontalOptions = LayoutOptions.EndAndExpand;
            //    //layout.Children.Add(this._noleidos);
                
            //    //msg.Children.Add(layout, 2, 0);

            //}
            //this.Children.Add(this._noleidos);
            this.Children.Add(msg);



        }


        public ContentView Right_arrow_view(Layout content_layout)
        {
            ContentView view;
            StackLayout layout;
            //Image image;
            Image image = new Image();
            view = new ContentView();
            view.Style = C2Style.Control_style;

            layout = new StackLayout();
            layout.Orientation = StackOrientation.Horizontal;
            layout.Padding = new Thickness(10);
            layout.VerticalOptions = LayoutOptions.Center;
            layout.HorizontalOptions = LayoutOptions.FillAndExpand;

            Grid msg;
            msg = new Grid
            {
                //VerticalOptions = LayoutOptions.Center,
                //HorizontalOptions = LayoutOptions.FillAndExpand,
                RowDefinitions =
                {
                new RowDefinition { Height =new GridLength(1, GridUnitType.Star)},

                },
                ColumnDefinitions =
                {
                    new ColumnDefinition { Width = new GridLength(8, GridUnitType.Star) },
                    new ColumnDefinition { Width = new GridLength(2, GridUnitType.Star) }
                }
            };





            //this._message_view.Content = layout;


            layout.Children.Add(content_layout);
            ////msg.Children.Add(layout, 0, 0);
            //empty_view = new ContentView();
            //empty_view.HorizontalOptions = LayoutOptions.FillAndExpand;
            ////msg.Children.Add(empty_view, 1, 0);
            //layout.Children.Add(empty_view);

            msg.Children.Add(layout, 0, 0);
            layout = new StackLayout();
            //layout.Spacing = 5;
            layout.Orientation = StackOrientation.Vertical;
            layout.Padding = new Thickness(0, 0, 15, 0);
            
            layout.VerticalOptions = LayoutOptions.FillAndExpand;
            layout.HorizontalOptions = LayoutOptions.EndAndExpand;

            double size;
            double font_size;
            font_size = Device.GetNamedSize(NamedSize.Small, typeof(Label));
            size = 1.6 * font_size;
            this._badge1 = new C2Badge(size, size, font_size);
            this._badge1.BoxColor = AppStyle.Mensajes_panel_label1;
            this._badge1.HorizontalOptions = LayoutOptions.End;
            this._badge1.VerticalOptions = LayoutOptions.CenterAndExpand;
            this._badge1.SetBinding(C2Badge.TextProperty, "MsgOferta");
            layout.Children.Add(this._badge1);

            msg.Children.Add(layout, 1, 0);
            //view.Content = layout;
            view.Content = msg;
            return view;
        }

        //-----------------------------------------------------

        private void i_Update_label(Label label, string text)
        {
            if (string.IsNullOrEmpty(text) == true)
            {
                label.IsVisible = false;
            }
            else
            {
                label.Text = text;
                label.IsVisible = true;
            }
        }

        //private void i_Update_Noleiedos(C2Badge noleidos, string text)
        //{
        //    if (string.IsNullOrEmpty(text) == true)
        //    {
        //        noleidos.IsVisible = false;
        //    }
        //    else
        //    {
                
        //        noleidos.Text = text;
        //        noleidos.IsVisible = true;
        //    }
        //}
        

        private void i_Update_text_estado(C2Badge2 estado, string text)
        {
            if (string.IsNullOrEmpty(text) == true)
            {
                estado.IsVisible = false;
            }
            else
            {
                
                switch (text.ToUpper())
                {
                    case "NUEVA INVITACIÓN":
                    case "NUEVA OFERTA":
                    case "1":
                        estado.Text = "Nueva oferta";
                        estado.TextColor = AppStyle.Nueva_Oferta;
                        estado.BorderColor = AppStyle.Nueva_Oferta;
                        break;
                    case "ACEPTADA":
                    case "3":
                        estado.Text = "Aceptada";
                        estado.TextColor = AppStyle.Aceptada_Oferta;
                        estado.BorderColor = AppStyle.Aceptada_Oferta;
                        break;
                    case "RECHAZADA":
                    case "2":
                        estado.Text = "Rechazada";
                        estado.TextColor = AppStyle.Rechazada_Oferta;
                        estado.BorderColor = AppStyle.Rechazada_Oferta;
                        break;
                    case "DESCARTADO":
                    case "DESCARTADA":
                    case "8":
                        estado.Text = "Descartada";
                        estado.TextColor = AppStyle.Descartada_Oferta;
                        estado.BorderColor = AppStyle.Descartada_Oferta;
                        break;
                    default:
                        estado.Text = "Aceptada";
                        estado.TextColor = AppStyle.Aceptada_Oferta;
                        estado.BorderColor = AppStyle.Aceptada_Oferta;
                        break;
                }


                estado.IsVisible = true;
            }
        }

        private void i_Update_Linklabel(C2HyperLinkLabel label, string text)
        {
            if (string.IsNullOrEmpty(text) == true)
            {
                label.IsVisible = false;
            }
            else
            {
                label.Text = text;
                string enlace;
                enlace = text;
                if (enlace.Contains("https://")) {
                    this._linklabel.NavigateUri = enlace;
                    enlace = enlace.Replace("https://", "");
                }
                else if (enlace.Contains("http://")) {
                    this._linklabel.NavigateUri = enlace;
                    enlace = enlace.Replace("http://", "");
                }
                else  {
                    this._linklabel.NavigateUri = "http://" +enlace;
                }
                label.Text = enlace;
                label.IsVisible = true;
            }
        }
        //-----------------------------------------------------

        public void Set_data(string image_source, string title, string detail1, string detail2, string detail3,string linklabel,string estado)
        {
            try { this._image.Source = image_source; } catch { }
            this.i_Update_label(this._title, title);
            this.i_Update_label(this._detail1, detail1);
            this.i_Update_label(this._detail2, detail2);
            this.i_Update_Linklabel(this._linklabel, linklabel);
            this.i_Update_label(this._detail3, detail3);
            this.i_Update_text_estado(this._estadoOferta, estado);
        }

        //public void set_Noleidos_text_estado(string text)
        //{
        //    this.i_Update_Noleiedos(this._noleidos, text);
        //}
        //-----------------------------------------------------

        public void Set_icons(List<string> icons, List<string> urls)
        {
            C2HyperLinkLabelAwesome image = new C2HyperLinkLabelAwesome { Text = '\uf096'.ToString() };
            string ico_code;
            if (icons != null && icons.Count > 0)
            {
                
                this._icons_layout.Children.Clear();
                for (int i = 0; i < icons.Count; ++i)
                {

                    //switch (icons[i].ToUpper())
                    //{
                    //    case "F.PNG":
                    //        image = new C2HyperLinkLabelAwesome { Text = '\uf230'.ToString() };
                    //        break;
                    //    case "T.PNG":
                    //        image = new C2HyperLinkLabelAwesome { Text = '\uf081'.ToString() };
                    //        break;
                    //    case "IN.PNG":
                    //        image = new C2HyperLinkLabelAwesome { Text = '\uf08c'.ToString() };
                    //        break;
                    //    case "P.PNG":
                    //        image = new C2HyperLinkLabelAwesome { Text = '\uf0d3'.ToString() };
                    //        break;
                    //    case "CMRA.PNG":
                    //        image = new C2HyperLinkLabelAwesome { Text = '\uf16d'.ToString() };
                    //        break;
                    //    case "AA.PNG":
                    //        image = new C2HyperLinkLabelAwesome { Text = '\uf0c8'.ToString() };
                    //        break;
                    //    case "ICO-FACEBOOK.PNG":
                    //        image = new C2HyperLinkLabelAwesome { Text = '\uf09a'.ToString() };
                    //        break;
                    //    case "ICO-TWITTER.PNG":
                    //        image = new C2HyperLinkLabelAwesome { Text = '\uf099'.ToString() };
                    //        break;
                    //    case "ICO-LINKEDIN.PNG":
                    //        image = new C2HyperLinkLabelAwesome { Text = '\uf0e1'.ToString() };
                    //        break;
                    //    case "ICO-GOOGLE.PNG":
                    //        image = new C2HyperLinkLabelAwesome { Text = '\uf1a0'.ToString() };
                    //        break;
                    //    default:
                    //         image = new C2HyperLinkLabelAwesome { Text = '\uf0c8'.ToString() };
                    //        break;
                    //}
                    ico_code= Util.icoAwesomeCode(icons[i].ToUpper());
                    image = new C2HyperLinkLabelAwesome { Text = ico_code };


                    //C2HyperLinkImage image = new C2HyperLinkImage { Source = icons[i] };
                    image.WidthRequest = 24;
                    image.HeightRequest = 24;
                    
                    image.FontSize = Device.GetNamedSize(NamedSize.Large, typeof(Label));
                    image.TextColor = Util.icoAwesomeColor(icons[i].ToUpper());
                    //image.BackgroundColor = Util.icoAwesomeBackColor(icons[i].ToUpper());
                    image.Subject = "enlace";
                    image.NavigateUri = urls[i];

                    this._icons_layout.Children.Add(image);
                }

                this._icons_layout.IsVisible = true;
            }
            else
            {
               this._icons_layout.IsVisible = false;
            }
        }

        //-----------------------------------------------------


    }
}
