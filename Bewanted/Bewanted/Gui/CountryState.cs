﻿using System.Threading.Tasks;
using Xamarin.Forms;
using C2Core;
using C2Forms;
using System.Collections.Generic;

namespace Bewanted
{
    public class CountryState
    {
        private AppDelegate _delegate;
        private Page _page;
        private C2Picker.FPtr_on_selected_changed _func_on_country_changed;
        public C2Picker _country;
        public C2AssistedEntry _state;

        //-----------------------------------------------------

        public CountryState(AppDelegate _delegate, Page page, string country_field_name, string state_filed_name, bool allow_non_match_state, C2Picker.FPtr_on_selected_changed func_on_country_changed, C2AssistedEntry.FPtr_on_id_changed func_on_state_changed) : base()
        {
            this._delegate = _delegate;
            this._page = page;
            this._country = new C2Picker(country_field_name, this.i_On_country_changed);
            this._state = new C2AssistedEntry(state_filed_name, true, allow_non_match_state, this.i_On_state_assist_required, func_on_state_changed);
            this._func_on_country_changed = func_on_country_changed;
        }

        //-----------------------------------------------------

        public async void Load_countries()
        {
            this._country.Set_waiting_state();

            if (this._country.Count == 0)
            { 
                ServerResponse<List<CountryJSON>> response = await this._delegate.On_countries();
                if (response.type == ResponseType.OK_SUCCESS)
                {
                    for (int i = 0; i < response.result_object.Count; ++i)
                        this._country.Add(response.result_object[i].name, response.result_object[i].id);
                }
                else
                {
                    Alert.Show(response, this._page);
                }
            }

            this._country.Unset_waiting_state();
        }

        //-----------------------------------------------------

        private async Task<bool> i_On_state_assist_required(C2AssistedEntry sender, string search_text)
        {
            ServerResponse<List<StateJSON>> response;
            C2Assert.Error(sender == this._state);
            response = await this._delegate.On_states(this._country.CurrentId, search_text);

            if (response.type == ResponseType.OK_SUCCESS)
            {
                for (int i = 0; i < response.result_object.Count; ++i)
                    sender.Add_assisted_item(response.result_object[i].label, response.result_object[i].id);
                return true;
            }
            else
            {
                Alert.Show(response, this._page);
                return false;
            }
        }

        //-----------------------------------------------------

        private void i_On_country_changed(C2Picker picker)
        {
            this._state.Clear();
            if (this._func_on_country_changed != null)
                this._func_on_country_changed(picker);
        }

        //-----------------------------------------------------

        public bool Validate()
        {
            bool ok = true;
            ok = ok & this._country.Validate();
            ok = ok & this._state.Validate();
            return ok;
        }

        //-----------------------------------------------------

        public void Clear()
        {
            this._country.Clear(false);
            this._state.Clear();
        }

        //-----------------------------------------------------


    }
}