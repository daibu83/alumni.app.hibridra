﻿using System;
using Xamarin.Forms;
using C2Forms;

namespace Bewanted
{
    public class IntervalLayout : StackLayout
    {
        private C2Picker _month_start;
        private C2Picker _year_start;
        private C2Picker _month_end;
        private C2Picker _year_end;
        private C2Switch _current_layout;
        protected Label _label;
        protected Grid grid_layout;
        //-----------------------------------------------------

        public IntervalLayout(string current_text) : base()
        {
            
            C2ControlLabel label1;
            C2ControlLabel label2;
            this.Spacing = AppStyle.Layout_stack_spacing;
            grid_layout = new Grid();
            grid_layout.VerticalOptions = LayoutOptions.FillAndExpand;
            grid_layout.RowSpacing = AppStyle.Layout_stack_spacing;
            grid_layout.ColumnSpacing = AppStyle.Layout_stack_spacing;
            grid_layout.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(0.34, GridUnitType.Star) });
            grid_layout.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(0.33, GridUnitType.Star) });
            grid_layout.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(0.33, GridUnitType.Star) });
            label1 = new C2ControlLabel { Text = Local.txtInterval00 };
            label1.VerticalOptions = LayoutOptions.Start;
            label2 = new C2ControlLabel { Text = Local.txtInterval01 };
            label2.VerticalOptions = LayoutOptions.Start;
            this._month_start = new C2MonthPicker(Local.txtInterval02, Local.txtMonth, null);
            this._month_start.VerticalOptions = LayoutOptions.Start;
            this._year_start = new C2YearPicker(Local.txtInterval03, null, 1971, DateTime.Now.Year);
            this._year_start.VerticalOptions = LayoutOptions.Start;
            this._month_end = new C2MonthPicker(Local.txtInterval02, Local.txtMonth, null);
            this._month_end.VerticalOptions = LayoutOptions.Start;
            this._year_end = new C2YearPicker(Local.txtInterval03, null, 1971, DateTime.Now.Year);
            this._year_end.VerticalOptions = LayoutOptions.Start;
            this._label = new Label();
            this._label.VerticalOptions = LayoutOptions.Start;
            this._label.Style = C2Style.Error_label_style;
            //this.Children.Add(this._horizontal_layout);
            
            

            grid_layout.Children.Add(label1, 0, 0);
            grid_layout.Children.Add(this._month_start, 1, 0);
            grid_layout.Children.Add(this._year_start, 2, 0);
            grid_layout.Children.Add(label2, 0, 1);
            grid_layout.Children.Add(this._month_end, 1, 1);
            grid_layout.Children.Add(this._year_end, 2, 1);
            
            
            this._current_layout = new C2Switch(current_text, this.i_On_current_change);
            this.Children.Add(grid_layout);
            this.Children.Add(this._current_layout);
            this.i_Show_error_label(false);
        }


        protected void i_Show_error_label(bool show)
        {
            if (show == true)
            {
                
                grid_layout.Children.Add(_label, 0,3, 2,3);
            }
            else
            {
                grid_layout.Children.Remove(_label);
            }
            this.InvalidateMeasure();
        }
        //-----------------------------------------------------

        public void Set_data(int mstart, int ystart, int mend, int yend)
        {
            this._month_start.CurrentId = mstart;
            this._year_start.CurrentId = ystart;

            this._month_end.IsEnabled = true;
            this._year_end.IsEnabled = true;
            if (mend != 0)
            {
                this._month_end.CurrentId = mend;
                this._year_end.CurrentId = yend;
                this._current_layout.IsToggled = false;
            }
            else
            {
                this._month_end.IsEnabled = false;
                this._year_end.IsEnabled = false;
                this._month_end.Clear(false);
                this._year_end.Clear(false);
                this._current_layout.IsToggled = true;
            }
        }

        //-----------------------------------------------------
        
        public void Get_data(out int mstart, out int ystart, out int mend, out int yend)
        {
            mstart = this._month_start.CurrentId;
            ystart = this._year_start.CurrentId;

            if (this._current_layout.IsToggled == true)
            {
                mend = 0;
                yend = 0;
            }
            else
            {
                mend = this._month_end.CurrentId;
                yend = this._year_end.CurrentId;
            }
        }

        //-----------------------------------------------------

        public void Clear()
        {
            this._month_start.Clear(false);
            this._year_start.Clear(false);
            this._month_end.Clear(false);
            this._year_end.Clear(false);
            this._month_end.IsEnabled = true;
            this._year_end.IsEnabled = true;
            this._current_layout.IsToggled = false;
        }

        //-----------------------------------------------------

        public bool Validate()
        {
            bool ok = true;
            ok = ok & this._month_start.Validate();
            ok = ok & this._year_start.Validate();
            if (this._current_layout.IsToggled == false)
            {
                ok = ok & this._month_end.Validate();
                ok = ok & this._year_end.Validate();
                if (ok == true) {

                    if (this._year_end.CurrentId >= this._year_start.CurrentId)
                    {
                        if (this._year_end.CurrentId == this._year_start.CurrentId) {
                            if (this._month_end.CurrentId <= this._month_start.CurrentId) {
                                ok = false;
                            }
                        }
                    }
                    else {
                        ok = false;
                    }
                }
            }
            if (ok == false) {
                this._label.Text = string.Format("{0}: {1}", "Fechas", "Fechas incorrectas");
            }
            this.i_Show_error_label(!ok);
            return ok;
        }

        //-----------------------------------------------------

        private void i_On_current_change(bool on_off)
        {
            this._month_end.IsEnabled = !on_off;
            this._year_end.IsEnabled = !on_off;

            if (on_off == true)
            {
                this._month_end.Clear(false);
                this._year_end.Clear(false);
            }
        }

        //-----------------------------------------------------


    }

}
