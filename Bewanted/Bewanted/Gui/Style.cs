﻿using Xamarin.Forms;
using C2Forms;

namespace Bewanted
{
    public class AppStyle
    {
        // Colors
        private static readonly Color _SPLASH_BACKGROUND = Color.FromHex("009F84");
        private static readonly Color _PAGE_BACKGROUND = Color.FromHex("F1F2F4");
        private static readonly Color _NEW_OFERT = Color.FromHex("bfd3a1");
        private static readonly Color _ACCEPT_OFERT = Color.FromHex("8ecee9");
        private static readonly Color _REFUSE_OFERT = Color.FromHex("b4b9bf");
        private static readonly Color _DISCARD_OFERT = Color.FromHex("b4b9bf");

        private static readonly Color _NAVIGATION_BAR = Color.FromHex("3A6360");
        private static readonly Color _NAVIGATION_BAR_TEXT = Color.White;
        private static readonly Color _CONTROL_TEXT = Color.FromHex("4b5966");
        private static readonly Color _CONTROL_PLACEHOLDER = Color.FromHex("7C8790");
        private static readonly Color _CONTROL_BACKGROUND = Color.White;                    
        private static readonly Color _CONTROL_ERROR_BACKGROUND = Color.FromRgb(255, 240, 240);
        private static readonly Color _LABEL_TEXT = Color.FromHex("6A7480");
        private static readonly Color _LABEL_WHITE_TEXT = Color.White;
        private static readonly Color _LINK_LABEL = Color.FromHex("307FC5");   
        private static readonly Color _HEADER_LABEL = Color.FromHex("4b5966");


        


        private static readonly Color _BUTTON1_BACKGROUND = Color.FromHex("009F84");
        private static readonly Color _BUTTON2_BACKGROUND = Color.FromHex("1D9ED3");
        private static readonly Color _BUTTON3_BACKGROUND = Color.FromHex("E55353");
        private static readonly Color _BUTTON4_BACKGROUND = Color.FromHex("575A5B");


        private static readonly Color _STRONG_TEXT = Color.Black;                           // Negro
        private static readonly Color _ERROR_TEXT = Color.FromRgb(220, 81, 81);             // Rojo
        private static readonly Color _INFO_TEXT = Color.FromRgb(130, 167, 61);             // Verde medio
        private static readonly Color _BUTTON_TEXT = Color.White;                           // Blanco
        private static readonly Color _FILTER_BACKGROUND = Color.FromHex("4B5966");      // Gris oscuro
        private static readonly Color _FILTER_TEXT = Color.FromRgb(220, 220, 220);          // Gris claro
        private static readonly Color _FILTER_STRONG_TEXT = Color.White;                    // Blanco
        private static readonly Color _MENU_BACKGROUND_SELECTED = Color.FromRgb(37, 40, 47);// Casi Negro
        private static readonly Color _MENU_BACKGROUND = Color.FromRgb(46, 49, 56);         // Negro claro
        private static readonly Color _MENU_TEXT_SELECTED = Color.White;                    // Blanco
        private static readonly Color _MENU_TEXT = Color.FromRgb(165, 165, 165);            // Gris claro
        private static readonly Color _MENU_FRAMED_LABEL1 = Color.FromRgb(226, 33, 42);     // Rojo
        private static readonly Color _MENSAJES_LABEL = Color.FromHex("009F84");     // Verde
        private static readonly Color _MENU_FRAMED_LABEL2 = Color.FromRgb(132, 167, 63);    // Verde
        private static readonly Color _MENU_FRAMED_TEXT = Color.White;                      // Blanco
        private static readonly Color _PROFILE_BACKGROUND = Color.FromHex("279ED6");    // Botón azul
        private static readonly Color _COLOR_BLUE_BOX = Color.FromHex("279ED6");    // Box azul
        //private static readonly Color _MSG_LABEL_BACKGROUND = Color.FromRgb(226, 33, 42);     // Rojo
        private static readonly Color _MSG_LABEL_BACKGROUND = Color.FromHex("E5584F");    // Box ROJO
        // Fonts
        private static readonly double _LABEL_FONT_SIZE_TITLE = Device.GetNamedSize(NamedSize.Medium, typeof(Label));
        private static readonly double _LABEL_FONT_SIZE = Device.GetNamedSize(NamedSize.Small, typeof(Label));
        private static readonly double _LABEL_FONT_SIZE_POPMSG =1.1 * Device.GetNamedSize(NamedSize.Small, typeof(Label));
        private static readonly double _LABEL2_FONT_SIZE = 0.5 * (Device.GetNamedSize(NamedSize.Small, typeof(Entry)) + Device.GetNamedSize(NamedSize.Medium, typeof(Entry)));
        private static readonly double _ENTRY_FONT_SIZE = 0.5 * (Device.GetNamedSize(NamedSize.Small, typeof(Entry)) + Device.GetNamedSize(NamedSize.Medium, typeof(Entry)));
        private static readonly double _BUTTON_FONT_SIZE = _ENTRY_FONT_SIZE;
        private static readonly double _MENU_FONT_SIZE = _ENTRY_FONT_SIZE; // Device.GetNamedSize(NamedSize.Medium, typeof(Entry));

        // Geometry
        private static readonly double _BUTTON_RADIUS = 5.0;
        private static readonly double _ENTRY_PADDING_LEFT = 8.0;
        private static readonly double _LABEL_ERROR_PADDING_LEFT = 5.0;
        private static readonly double _LABEL_INFO_PADDING_LEFT = 5.0;
        private static readonly double _LAYOUT_STACK_SPACING = 1.0;
        private static readonly double _LAYOUT_STACK_LONG_SPACING = 5.0;
        private static readonly double _LAYOUT_PADDING_VERTICAL = 15.0;
        private static readonly double _BUTTON_PADDING_HORIZONTAL = 10.0;
        private static readonly double _BUTTON_PADDING_VERTICAL = 10.0;
        private static readonly double _FILTER_PADDING_HORIZONTAL = 10.0;
        private static readonly double _FILTER_PADDING_VERTICAL = 10.0;

        private static readonly double _CONTENT_BOX_PADDING_HORIZONTAL = 10.0;
        private static readonly double _CONTENT_BOX_PADDING_VERTICAL = 10.0;

        private static readonly double _MENU_PADDING_HORIZONTAL = 10.0;
        private static readonly double _MENU_PADDING_VERTICAL = 20.0;
        private static readonly double _FILTER_IMAGE_SIZE = 20.0;
        private static readonly double _MENU_IMAGE_SIZE = 24.0;
        private static readonly double _IMAGE_SIZE = 64.0;
        private static readonly double _IMAGE_SIZE_BIG = 96.0;
        private static readonly double _TABLE_ROW_SPACING = 1.0;
        private static readonly double _TABLE_COLUMN_SPACING = 1.0;


        //-----------------------------------------------------

        private static readonly Color _FACEBOOK_BACKGROUND = Color.FromHex("3b5998");
        private static readonly Color _TWITTER_BACKGROUND = Color.FromHex("1da1f2");
        private static readonly Color _LINKEDIN_BACKGROUND = Color.FromHex("0077b5");
        private static readonly Color _PINTEREST_BACKGROUND = Color.FromHex("bd081c");
        private static readonly Color _INSTAGRAM_BACKGROUND = Color.FromHex("AD257D");
        private static readonly Color _WEB_BLOG_BACKGROUND = Color.FromHex("f18f28");

        public static void Set_C2_style()
        {
            C2Style.Table_row_spacing = _TABLE_ROW_SPACING;
            C2Style.Table_column_spacing = _TABLE_COLUMN_SPACING;
            C2Style.Button_font_size = _BUTTON_FONT_SIZE;
            C2Style.Button_text = _BUTTON_TEXT;
            C2Style.Label_error_padding_left = _LABEL_ERROR_PADDING_LEFT;
            C2Style.Label_info_padding_left = _LABEL_INFO_PADDING_LEFT;
            C2Style.Label_font_size = _LABEL_FONT_SIZE;
            C2Style.Label_font_size_popmsg = _LABEL_FONT_SIZE_POPMSG;
            C2Style.Label_font_size_title = _LABEL_FONT_SIZE_TITLE;
            C2Style.Entry_font_size = _ENTRY_FONT_SIZE;
            C2Style.Entry_padding_vertical = _ENTRY_FONT_SIZE;
            C2Style.Entry_padding_horizontal = _ENTRY_PADDING_LEFT;
            C2Style.Control_text = _CONTROL_TEXT;
            C2Style.Control_text_strong = _STRONG_TEXT;
            C2Style.Control_background = _CONTROL_BACKGROUND;
            C2Style.Control_error_background = _CONTROL_ERROR_BACKGROUND;
            C2Style.Control_placeholder = _CONTROL_PLACEHOLDER;
            C2Style.Error_text = _ERROR_TEXT;
            C2Style.Info_text = _INFO_TEXT;
        }

        //-----------------------------------------------------

        private static Style _entry_style_strong = null;
        public static Color Label_text = _LABEL_TEXT;
        public static Color Label_white_text = _LABEL_WHITE_TEXT;
        public static Color percentage_color = _COLOR_BLUE_BOX;


        //-----------------------------------------------------

        public static Style Page = new Style(typeof(Page))
        {
            Setters =
            {
                new Setter { Property = Xamarin.Forms.Page.BackgroundColorProperty, Value = _PAGE_BACKGROUND }
            }
        };

        public static Color Nueva_Oferta = _NEW_OFERT;
        public static Color Aceptada_Oferta = _ACCEPT_OFERT;
        public static Color Rechazada_Oferta = _REFUSE_OFERT;
        public static Color Descartada_Oferta = _DISCARD_OFERT;

        public static Color Page_background = _PAGE_BACKGROUND;
        public static Color Splash_background = _SPLASH_BACKGROUND;
        public static double Layout_stack_spacing = _LAYOUT_STACK_SPACING;
        public static double Layout_stack_long_spacing = _LAYOUT_STACK_LONG_SPACING;
        public static double Layout_padding_vertical = _LAYOUT_PADDING_VERTICAL;
        public static double Button_padding_horizontal = _BUTTON_PADDING_HORIZONTAL;
        public static double Button_padding_vertical = _BUTTON_PADDING_VERTICAL;
        public static double Filter_padding_horizontal = _FILTER_PADDING_HORIZONTAL;
        public static double Filter_padding_vertical = _FILTER_PADDING_VERTICAL;
        public static double Content_box_padding_horizontal = _CONTENT_BOX_PADDING_HORIZONTAL;
        public static double Content_box_padding_vertical = _CONTENT_BOX_PADDING_VERTICAL;
        public static Color Color_Facebook = _FACEBOOK_BACKGROUND;
        public static Color Color_Twitter = _TWITTER_BACKGROUND;
        public static Color Color_Linkedin = _LINKEDIN_BACKGROUND;
        public static Color Color_Pinterest = _PINTEREST_BACKGROUND;
        public static Color Color_Instagram = _INSTAGRAM_BACKGROUND;
        public static Color Color_Web_Blog = _WEB_BLOG_BACKGROUND;


        
        //-----------------------------------------------------

        public static Style Navigation_page = new Style(typeof(NavigationPage))
        {
            Setters =
            {
                new Setter { Property = NavigationPage.BarBackgroundColorProperty, Value = _NAVIGATION_BAR },
                new Setter { Property = NavigationPage.BarTextColorProperty, Value = _NAVIGATION_BAR_TEXT }
            }
        };

        //-----------------------------------------------------

        public static Style Label = new Style(typeof(Label))
        {
            Setters =
            {
                new Setter { Property = Xamarin.Forms.Label.TextColorProperty, Value = _LABEL_TEXT },
                new Setter { Property = Xamarin.Forms.Label.FontSizeProperty, Value = _LABEL_FONT_SIZE },
                new Setter { Property = Xamarin.Forms.Label.LineBreakModeProperty, Value = LineBreakMode.WordWrap }
            }
        };



        //-----------------------------------------------------

        public static Style Label_header = new Style(typeof(Label))
        {
            Setters =
            {
                new Setter { Property = Xamarin.Forms.Label.TextColorProperty, Value = _HEADER_LABEL },
                new Setter { Property = Xamarin.Forms.Label.FontSizeProperty, Value = _LABEL_FONT_SIZE },
                new Setter { Property = Xamarin.Forms.Label.LineBreakModeProperty, Value = LineBreakMode.WordWrap }
            }
        };

        //-----------------------------------------------------

        public static Style Label_link = new Style(typeof(Label))
        {
            Setters =
            {
                new Setter { Property = Xamarin.Forms.Label.TextColorProperty, Value = _LINK_LABEL },
                new Setter { Property = Xamarin.Forms.Label.FontSizeProperty, Value = _LABEL_FONT_SIZE },
                new Setter { Property = Xamarin.Forms.Label.LineBreakModeProperty, Value = LineBreakMode.WordWrap }
            }
        };

        //-----------------------------------------------------

        public static Style Content_label = new Style(typeof(Label))
        {
            Setters =
            {
                new Setter { Property = Xamarin.Forms.Label.TextColorProperty, Value = _LABEL_TEXT },
                new Setter { Property = Xamarin.Forms.Label.FontSizeProperty, Value = _LABEL_FONT_SIZE },
                new Setter { Property = Xamarin.Forms.Label.LineBreakModeProperty, Value = LineBreakMode.WordWrap }
            }
        };

        //-----------------------------------------------------

        public static Style Content_label_strong = new Style(typeof(Label))
        {
            Setters =
            {
                new Setter { Property = Xamarin.Forms.Label.TextColorProperty, Value = _HEADER_LABEL },
                new Setter { Property = Xamarin.Forms.Label.FontSizeProperty, Value = _LABEL_FONT_SIZE },
                new Setter { Property = Xamarin.Forms.Label.LineBreakModeProperty, Value = LineBreakMode.WordWrap }
            }
        };

        //-----------------------------------------------------

        public static Style Entry_style_strong
        {
            get
            {
                if (AppStyle._entry_style_strong == null)
                {
                    AppStyle._entry_style_strong = new Style(typeof(Entry))
                    {
                        Setters =
                        {
                            new Setter { Property = Entry.BackgroundColorProperty, Value = C2Style.Control_background },
                            new Setter { Property = Entry.TextColorProperty, Value = C2Style.Control_text_strong }
                        }
                    };
                }

                return AppStyle._entry_style_strong;
            }
        }

        //-----------------------------------------------------

        public static Style Button1_style = new Style(typeof(Button))
        {
            Setters =
            {
                new Setter { Property = Button.BackgroundColorProperty, Value = _BUTTON1_BACKGROUND },
                new Setter { Property = Button.BorderRadiusProperty, Value = _BUTTON_RADIUS },
            }
        };

        //-----------------------------------------------------

        public static Style Button2_style = new Style(typeof(Button))
        {
            Setters =
            {
                new Setter { Property = Button.BackgroundColorProperty, Value = _BUTTON2_BACKGROUND },
                new Setter { Property = Button.BorderRadiusProperty, Value = _BUTTON_RADIUS }
            }
        };

        //-----------------------------------------------------

        public static Style Button3_style = new Style(typeof(Button))
        {
            Setters =
            {
                new Setter { Property = Button.BackgroundColorProperty, Value = _BUTTON3_BACKGROUND },
                new Setter { Property = Button.BorderRadiusProperty, Value = _BUTTON_RADIUS }
            }
        };

        //-----------------------------------------------------

        public static Style Button4_style = new Style(typeof(Button))
        {
            Setters =
            {
                new Setter { Property = Button.BackgroundColorProperty, Value = _BUTTON4_BACKGROUND },
                new Setter { Property = Button.BorderRadiusProperty, Value = _BUTTON_RADIUS }
            }
        };

        //-----------------------------------------------------

        public static Style Filter_background = new Style(typeof(View))
        {
            Setters =
            {
                new Setter { Property = View.BackgroundColorProperty, Value = _FILTER_BACKGROUND }
            }
        };

        public static double Filter_image_size = _FILTER_IMAGE_SIZE;
        public static Color Filter_background_color = _FILTER_BACKGROUND;

        //-----------------------------------------------------

        public static Style Filter_label = new Style(typeof(Label))
        {
            Setters =
            {
                new Setter { Property = Xamarin.Forms.Label.TextColorProperty, Value = _FILTER_TEXT },
                new Setter { Property = Xamarin.Forms.Label.FontSizeProperty, Value = _LABEL_FONT_SIZE },
                new Setter { Property = Xamarin.Forms.Label.LineBreakModeProperty, Value = LineBreakMode.WordWrap }
            }
        };

        public static Color Filter_text = _FILTER_TEXT;

        //-----------------------------------------------------

        public static Style Filter_label_strong = new Style(typeof(Label))
        {
            Setters =
            {
                new Setter { Property = Xamarin.Forms.Label.TextColorProperty, Value = _FILTER_STRONG_TEXT },
                new Setter { Property = Xamarin.Forms.Label.FontSizeProperty, Value = _LABEL_FONT_SIZE },
                new Setter { Property = Xamarin.Forms.Label.LineBreakModeProperty, Value = LineBreakMode.WordWrap }
            }
        };

        //-----------------------------------------------------

        public static Style Menu_background = new Style(typeof(View))
        {
            Setters =
            {
                new Setter { Property = View.BackgroundColorProperty, Value = _MENU_BACKGROUND }
            }
        };

        //-----------------------------------------------------

        public static Style Menu_background_strong = new Style(typeof(View))
        {
            Setters =
            {
                new Setter { Property = View.BackgroundColorProperty, Value = _MENU_BACKGROUND_SELECTED }
            }
        };

        //-----------------------------------------------------

        public static Style Menu_label = new Style(typeof(Label))
        {
            Setters =
            {
                new Setter { Property = Xamarin.Forms.Label.TextColorProperty, Value = _MENU_TEXT },
                new Setter { Property = Xamarin.Forms.Label.FontSizeProperty, Value = _MENU_FONT_SIZE },
                new Setter { Property = Xamarin.Forms.Label.LineBreakModeProperty, Value = LineBreakMode.WordWrap }
            }
        };

        //-----------------------------------------------------

        public static Style Menu_label_strong = new Style(typeof(Label))
        {
            Setters =
            {
                new Setter { Property = Xamarin.Forms.Label.TextColorProperty, Value = _MENU_TEXT_SELECTED },
                new Setter { Property = Xamarin.Forms.Label.FontSizeProperty, Value = _MENU_FONT_SIZE },
                new Setter { Property = Xamarin.Forms.Label.LineBreakModeProperty, Value = LineBreakMode.WordWrap }
            }
        };

        //-----------------------------------------------------

        public static double Menu_padding_horizontal = _MENU_PADDING_HORIZONTAL;
        public static double Menu_padding_vertical = _MENU_PADDING_VERTICAL;
        public static double Menu_image_size = _MENU_IMAGE_SIZE;
        public static Color Menu_framed_label1 = _MENU_FRAMED_LABEL1;
        public static Color Mensajes_panel_label1 = _MENSAJES_LABEL;
        public static Color Menu_framed_label2 = _MENU_FRAMED_LABEL2;
        public static Color Menu_framed_text = _MENU_FRAMED_TEXT;

        //-----------------------------------------------------

        public static Style Profile_background = new Style(typeof(View))
        {
            Setters =
            {
                new Setter { Property = View.BackgroundColorProperty, Value = _PROFILE_BACKGROUND }
            }
        };

        public static Style Msg_label_background = new Style(typeof(View))
        {
            Setters =
            {
                new Setter { Property = View.BackgroundColorProperty, Value = _MSG_LABEL_BACKGROUND }
            }
        };

        //-----------------------------------------------------

        public static double Image_size = _IMAGE_SIZE;
        public static double Image_size_big = _IMAGE_SIZE_BIG;





    }
}
