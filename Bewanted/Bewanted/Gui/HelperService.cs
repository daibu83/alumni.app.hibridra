﻿using C2Core;
using C2Forms;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace Bewanted
{
    public class HelperService
    {
        public static DateTime _token_expire_date;
        public static string _access_token_API;

        

        public static async Task<ServerResponse> i_Access_token_API()
        {
            ServerResponse response;
            if (_token_expire_date > DateTime.Now)
            {
                response.type = ResponseType.OK_SUCCESS;
                response.message = "";
            }
            else
            {
                response = await i_Authenticate();
            }

            return response;
        }

        private static DateTime i_Date_from_unix_time(long seconds_from_epoch)
        {
            return new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc).AddSeconds(seconds_from_epoch);
        }

        public static async Task<ServerResponse> i_Authenticate()
        {
            ServerResponse<AuthJSON> auth_response;
            ServerResponse response;

            auth_response = await Server.Authentication_request_API();

            if (auth_response.type == ResponseType.OK_SUCCESS)
            {
                _access_token_API = auth_response.result_object.access_token;
                _token_expire_date = i_Date_from_unix_time(auth_response.result_object.expires - 3600);
                response.type = ResponseType.OK_SUCCESS;
                response.message = "";
            }
            else if (auth_response.type == ResponseType.SERVER_ERROR)
            {
                response.type = ResponseType.AUTHENTICATION_ERROR;
                response.message = auth_response.message;
            }
            else if (auth_response.type == ResponseType.SERVICE_ERROR)
            {
                response.type = ResponseType.AUTHENTICATION_ERROR;
                response.message = auth_response.message;
            }

            else if (auth_response.type == ResponseType.URL_ERROR)
            {
                response.type = ResponseType.AUTHENTICATION_ERROR;
                response.message = auth_response.message;
            }
            else
            {
                response.type = auth_response.type;
                response.message = auth_response.message;
            }

            return response;
        }


        


        public static async Task<ServerResponse<LoginDataJSON>> Login_request()
        {
            List<string> key_list = null;
            List<string> value_list = null;
            string email="";
            string password="";
            
            string device_type;
            HelperService._token_expire_date = new DateTime(1970, 1, 1);
            await HelperService.i_Access_token_API();
            string _access_token = HelperService._access_token_API;

            Device.OnPlatform(iOS: () =>
            {
                if (Application.Current.Properties.ContainsKey("email") == true)
                {
                    email = Convert.ToString(Application.Current.Properties["email"]);
                }
                if (Application.Current.Properties.ContainsKey("password") == true)
                    password = Convert.ToString(Application.Current.Properties["password"]);
            }, Android: () => {
                List<string> data = new List<string>();
                DependencyService.Get<IFileService>().Read_data(data);
                if (data.Count >= 2)
                {
                    email = data[0];
                    password = data[1];
                }
            });
            Server._access_token_API = _access_token;
            Server.device_id = Xamarin.Forms.DependencyService.Get<IDeviceService>().DeviceId();
            device_type = Xamarin.Forms.Device.OnPlatform("iphone", "android", "win");
            key_list = new List<string>();
            value_list = new List<string>();
            key_list.Add("email");
            value_list.Add(email);
            key_list.Add("password");
            value_list.Add(password);
            key_list.Add("device_token");
            value_list.Add(AppDelegate._device_token == null ? "" : AppDelegate._device_token);
            key_list.Add("device_type");
            value_list.Add(device_type);
            key_list.Add("device_id");
            value_list.Add(Server.device_id);
            return await Server.i_Get_server_data_API<LoginDataJSON>(Server._URL_BASE_API + Server._LOGIN_SERVICE_API, key_list, value_list,MethodHTTPType.POST);
        }
    }
}
