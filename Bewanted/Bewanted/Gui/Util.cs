﻿using System;
using Xamarin.Forms;

namespace Bewanted
{
    public class Util
    {
        

        private static string i_Date(string date)
        {
            try
            {
                DateTime dt = DateTime.Parse(date);
                return String.Format("{0:dd/MM/yyyy}", dt);
            }
            catch (Exception)
            {
                return "";
            }
        }
        public static string icoAwesomeCode(string urlico)
        {
            string ico_code;
            switch (urlico.ToUpper())
            {
                case "F.PNG":
                    ico_code ='\uf230'.ToString();
                    break;
                case "T.PNG":
                    ico_code ='\uf081'.ToString();
                    break;
                case "IN.PNG":
                    ico_code ='\uf08c'.ToString();
                    break;
                case "P.PNG":
                    ico_code ='\uf0d3'.ToString();
                    break;
                case "CMRA.PNG":
                    ico_code ='\uf16d'.ToString();
                    break;
                case "AA.PNG":
                    ico_code ='\uf1fa'.ToString();
                    //ico_code = '\ue900'.ToString();
                    break;
                case "ICO-FACEBOOK.PNG":
                    ico_code ='\uf09a'.ToString();
                    break;
                case "ICO-TWITTER.PNG":
                    ico_code ='\uf099'.ToString();
                    break;
                case "ICO-LINKEDIN.PNG":
                    ico_code ='\uf0e1'.ToString();
                    break;
                case "ICO-GOOGLE.PNG":
                    ico_code ='\uf1a0'.ToString();
                    break;
                default:
                    ico_code ='\uf0c8'.ToString();
                    break;
            }
            return ico_code;
        }
        public static Color icoAwesomeColor(string urlico)
        {
            Color ico_color;
            switch (urlico.ToUpper())
            {
                case "F.PNG":
                    ico_color = AppStyle.Color_Facebook;
                    break;
                case "T.PNG":
                    ico_color = AppStyle.Color_Twitter;
                    break;
                case "IN.PNG":
                    ico_color = AppStyle.Color_Linkedin;
                    break;
                case "P.PNG":
                    ico_color = AppStyle.Color_Pinterest;
                    break;
                case "CMRA.PNG":
                    ico_color = AppStyle.Color_Instagram;
                    break;
                case "AA.PNG":
                    ico_color = AppStyle.Color_Web_Blog;
                    break;
                case "ICO-FACEBOOK.PNG":
                case "ICO-TWITTER.PNG":
                case "ICO-LINKEDIN.PNG":
                case "ICO-GOOGLE.PNG":
                    ico_color = AppStyle.Splash_background;
                    break;
                default:
                    ico_color = AppStyle.Splash_background;
                    break;
            }
            return ico_color;
        }
        public static Color icoAwesomeBackColor(string urlico)
        {
            Color ico_color;
            switch (urlico.ToUpper())
            {
                case "CMRA.PNG":
                    ico_color = AppStyle.Color_Instagram;
                    break;
                case "AA.PNG":
                    ico_color = AppStyle.Color_Web_Blog;
                    break;
                default:
                    ico_color = Color.Transparent;
                    break;
            }
            return ico_color;
        }
        //-----------------------------------------------------

        public static string Date_range(string date_from, string date_to)
        {
            string date1 = i_Date(date_from);
            string date2 = i_Date(date_to);

            if (String.IsNullOrEmpty(date1) == true)
            {
                if (String.IsNullOrEmpty(date2) == true)
                    return "";
                else
                    return "- " + date2;
            }
            else
            {
                if (String.IsNullOrEmpty(date2) == true)
                    return date1;
                else
                    return date1 + " - " + date2;
            }
        }

        //-----------------------------------------------------

        public static string Date_dd_MM_yy(string date)
        {
            try
            {
                DateTime dt = DateTime.Parse(date);
                return String.Format("{0:dd/MM/yy}", dt);
            }
            catch (Exception)
            {
                return "";
            }
        }

        //-----------------------------------------------------

        public static string Location(string city, string country)
        {
            if (String.IsNullOrEmpty(city) == true)
            {
                if (String.IsNullOrEmpty(country) == true)
                    return "";
                else
                    return country;
            }

            if (String.IsNullOrEmpty(country) == true)
                return city;

            return city + ", " + country;
        }

        public static double GetCurrentMilli()
        {
            DateTime Jan1970 = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
            TimeSpan javaSpan = DateTime.UtcNow - Jan1970;
            return javaSpan.TotalMilliseconds;
        }


    }
}
