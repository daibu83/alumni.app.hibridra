﻿using System;
using Xamarin.Forms;
using C2Forms;

namespace Bewanted
{
    public class ButtonsLayout : StackLayout
    {
        private Page _page;

        //-----------------------------------------------------

        private void i_Buttons_layout(Page page, out C2Button delete_button, EventHandler On_save, EventHandler On_delete) 
        {
            C2Button save_button;
            //C2Button cancel_button;
            this._page = page;
            this.Padding = new Thickness(AppStyle.Button_padding_horizontal, AppStyle.Button_padding_vertical);
            this.Spacing = AppStyle.Button_padding_vertical;
            save_button = new C2Button { Text = Local.txtButtons01 };
            save_button.Style = AppStyle.Button1_style;
            save_button.Clicked += On_save;
            /*cancel_button = new C2Button { Text = Local.txtButtons02 };
            cancel_button.Style = AppStyle.Button2_style;
            cancel_button.Clicked += i_On_cancel;*/
            this.Children.Add(save_button);
            //this.Children.Add(cancel_button);
            if (On_delete != null)
            {
                delete_button = new C2Button { Text = Local.txtButtons03 };
                delete_button.Style = AppStyle.Button3_style;
                delete_button.Clicked += On_delete;
                this.Children.Add(delete_button);
            }
            else
            {
                delete_button = null;
            }
        }

        //-----------------------------------------------------

        public ButtonsLayout(Page page, EventHandler On_save) : base()
        {
            C2Button delete_button;
            this.i_Buttons_layout(page, out delete_button, On_save, null);
        }

        //-----------------------------------------------------

        public ButtonsLayout(Page page, out C2Button delete_button, EventHandler On_save, EventHandler On_delete) : base()
        {
            this.i_Buttons_layout(page, out delete_button, On_save, On_delete);
        }

        //-----------------------------------------------------

            /*
        private async void i_On_cancel(object sender, EventArgs e)
        {
            if (await this._page.DisplayAlert(Local.txtButtons07, Local.txtButtons08, Local.txtButtons05, Local.txtButtons06) == true)
                await this._page.Navigation.PopAsync();
        }*/

        //-----------------------------------------------------

    }
}

