﻿using System;
using System.Globalization;
using Xamarin.Forms;

namespace Bewanted
{
    public class CacheConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (!(value is string))
            {
                throw new InvalidOperationException("Target must be a url string");
            }

            return new UriImageSource
            {
                Uri = new Uri((string)value),
                CachingEnabled = true,
                CacheValidity = new TimeSpan(30, 0, 0, 0)
            };


        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}