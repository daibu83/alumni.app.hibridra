﻿using System;
using Xamarin.Forms;
namespace C2Forms
{
    public class C2Image : Image
    {
        private static string url;

        public static BindableProperty TextUrlProperty = BindableProperty.Create("Url", typeof(String), typeof(Image), "");

        public string Uri
        {
            get { return (string)GetValue(TextUrlProperty); }
            set
            {
                SetValue(TextUrlProperty, value);

            }
        }

        
        
    }
}
