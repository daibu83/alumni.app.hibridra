﻿using Xamarin.Forms;
using C2Core;

namespace Bewanted
{
    public class Alert
    {
        public static async void Show(ServerResponse response, Page page)
        {
            C2Assert.Error(response.type != ResponseType.OK_SUCCESS);

            switch (response.type)
            {
                case ResponseType.NO_INTERNET_CONEXION:
                    await page.DisplayAlert(Local.txtAlert00, Local.txtAlert01, Local.txtAlertOk);
                    break;

                case ResponseType.AUTHENTICATION_ERROR:
                    await page.DisplayAlert(Local.txtAlert02, Local.txtAlert03, Local.txtAlertOk);
                    break;

                case ResponseType.SERVER_ERROR:
                    await page.DisplayAlert(Local.txtAlert04, Local.txtAlert05, Local.txtAlertOk);
                    break;

                case ResponseType.URL_ERROR:
                    await page.DisplayAlert(Local.txtAlert06, Local.txtAlert07, Local.txtAlertOk);
                    break;

                case ResponseType.SERVICE_ERROR:
                {
                    string[] separator = { "|" };
                        if (response.message != "")
                        {
                            string[] strings = response.message.Split(separator, System.StringSplitOptions.RemoveEmptyEntries);
                            if (strings.Length > 1)
                                await page.DisplayAlert(strings[0], strings[1], Local.txtAlertOk);
                            else
                                await page.DisplayAlert(null, strings[0], Local.txtAlertOk);
                        }
                        else {
                            await page.DisplayAlert(null, Local.txtErrServer, Local.txtAlertOk);
                        }
                    
                    break;
                }
            }
        }

        //-----------------------------------------------------

        public static void Show<T>(ServerResponse<T> response, Page page) where T : class
        {
            ServerResponse _response;
            _response.type = response.type;
            _response.message = response.message;
            Alert.Show(_response, page);
        }

        //-----------------------------------------------------


    }
}
