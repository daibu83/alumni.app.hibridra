﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace C2Forms
{
    class C2ListViewExtend: ListView
    {
        public void FilterLocations(string filter, ObservableCollection<Bewanted.ImageItemEmpresa> lista)
        {
            this.BeginRefresh();

            if (string.IsNullOrWhiteSpace(filter))
            {
                this.ItemsSource = lista;
            }
            else
            {
                this.ItemsSource = lista
                    .Where(x => x.Title.ToLower()
                   .Contains(filter.ToLower()));
            }

            this.EndRefresh();
        }
    }
}
