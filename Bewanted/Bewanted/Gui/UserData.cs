﻿using System;
using System.Threading.Tasks;
using Xamarin.Forms;
using C2Core;
using C2Forms;
using System.Collections.Generic;

namespace Bewanted
{
    public class UserData
    {
        private AppDelegate _delegate;
        private Page _page;
        public C2TextEntry _name;
        public C2TextEntry _last_name;
        public C2TextEntry _phone;
        public C2Picker _nationality;
        public CountryState _country_state;
        public C2AssistedEntry _city;
        public C2BirthDate _birthdate;

        //-----------------------------------------------------

        public UserData(AppDelegate _delegate, Page page,Boolean origdatospersonales) : base()
        {
            this._delegate = _delegate;
            this._page = page;
            this._name = new C2TextEntry(Local.txtUser00, 3, 100,null);
            this._last_name = new C2TextEntry(Local.txtUser01, 3, 100,null);
            if (origdatospersonales == true)
            {
                this._phone = new C2TextEntry(Local.txtUser02, 9, 15, Keyboard.Numeric);
                this._nationality = new C2Picker(Local.txtUser03, null);
                this._country_state = new CountryState(_delegate, page, Local.txtUser04, Local.txtUser05, false, this.i_On_country_changed, this.i_On_state_changed);
                this._city = new C2AssistedEntry(Local.txtUser06, true, false, this.i_On_city_assist_required, null);
                this._birthdate = new C2BirthDate(Local.txtUser07, true);
            }
            
        }

        //-----------------------------------------------------

        public async void Load_pickers_data()
        {
            this._country_state.Load_countries();
            this._nationality.Set_waiting_state();

            if (this._nationality.Count == 0)
            {
                ServerResponse<List<CountryJSON>> response = await this._delegate.On_nationalities();
                if (response.type == ResponseType.OK_SUCCESS)
                {
                    for (int i = 0; i < response.result_object.Count; ++i)
                        this._nationality.Add(response.result_object[i].name, response.result_object[i].id);
                }
                else
                {
                    Alert.Show(response, this._page);
                }
            }

            this._nationality.Unset_waiting_state();
        }

        //-----------------------------------------------------

        public void Add_to_layout_registro_nuevo_usuario(StackLayout layout)
        {
            layout.Children.Add(this._name);
            layout.Children.Add(this._last_name);
        }

        public void Add_to_layout(StackLayout layout)
        {
            layout.Children.Add(this._name);
            layout.Children.Add(this._last_name);
            layout.Children.Add(this._phone);
            layout.Children.Add(this._nationality);
            layout.Children.Add(this._country_state._country);
            layout.Children.Add(this._country_state._state);
            layout.Children.Add(this._city);
            layout.Children.Add(this._birthdate);
        }
        //-----------------------------------------------------

        private void i_On_state_changed(Int32 selected_value)
        {
            this._city.Clear();
        }

        //-----------------------------------------------------

        private void i_On_country_changed(C2Picker picker)
        {
            this._city.Clear();
        }

        //-----------------------------------------------------

        private async Task<bool> i_On_city_assist_required(C2AssistedEntry sender, string search_text)
        {
            Int32 state_id;
            ServerResponse<List<CityJSON>> response;
            C2Assert.Error(sender == this._city);
            state_id = this._country_state._state.CurrentId;
            response = await this._delegate.On_cities(state_id, search_text);

            if (response.type == ResponseType.OK_SUCCESS)
            {
                for (int i = 0; i < response.result_object.Count; ++i)
                    sender.Add_assisted_item(response.result_object[i].label, response.result_object[i].id);
                return true;
            }
            else
            {
                Alert.Show(response, this._page);
                return false;
            }
        }

        //-----------------------------------------------------
        public bool ValidatePersonales()
        {
            bool ok = true;
            ok = ok & this._name.Validate();
            ok = ok & this._last_name.Validate();
            ok = ok & this._phone.Validate();
            ok = ok & this._nationality.Validate();
            ok = ok & this._country_state.Validate();
            ok = ok & this._city.Validate();
            ok = ok & this._birthdate.Validate();
            return ok;
        }

        public bool Validate()
        {
            bool ok = true;
            ok = ok & this._name.Validate();
            ok = ok & this._last_name.Validate();
            return ok;
        }

        //-----------------------------------------------------


    }
}