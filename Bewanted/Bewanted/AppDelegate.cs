﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Xamarin.Forms;
using C2Core;
using C2Forms;
using System.IO;

namespace Bewanted
{
    public enum UserCurrentPage : short
    {
        kPanel = 0,
        kProfile,
        kCompanies,
        kHowto,
        kSettings,
        kActual,
    }

    public enum ProfileSection : short
    {
        kPersonal = 0,
        kAcademic,
        kMerchant,
        kLanguages,
        kInternship,
        kExperience,
        kVoluntary,
        kOther,
        kAll = 1000
    }

    public enum UserCurrentView : short
    {
        kOffers = 0,
        kFilters
    }

    public enum UserCurrentFilter : short
    {
        kAll = 0,
        kNews,
        kAccepted,
        kRejected,
        kDiscarted
    }

    public class AppDelegate : C2Disposable
    {
        public static bool _is_textfilter=false;
        //Model 
        public static bool _is_personal_data_complete;
        //0:Inicial 2:No datos minimos 1:Si datos minimos
        public static int _is_perfil_data_min = 0;

        //mensaje de perfil datos minimos-Se muestra cuando se alcanzan.sólo esa vez
        

        public static bool _showperfilcompleto = false;
        public static string _googleservices;
        public static bool _showpersonalcompleto = false;


        public static bool _is_merchant_data = false;
        public static bool _is_academic_data = false;


        //API
        public static string _access_token_API;
        public static DateTime _token_expire_date_API;
        // Model (persistent data)
        public static string _access_token;
        public static string _device_token;
        public static DateTime _token_expire_date;

        public  bool _blncargando = false;

        private  string _email;
        private  string _password;
        private UserCurrentPage _user_current_page;
        private UserCurrentView _user_current_view;
        private UserCurrentFilter _user_current_filter;
        

        // Model (non-persistent data)
        public static long _user_id;
        public string _user_name;
        public string _user_last_name;
        private string _user_image;
        
        private bool _user_public_profile;
        private bool _user_newsletter;
        public static bool _refreshcont;
        public static bool _user_notify;
        public static uint _user_new_offers;
        public static long _user_unreaded_messages;
        private List<CountryJSON> _countries;

        // View (cached for data persistence)
        private UserPage _user_page;
        public App _app;

        
        //-----------------------------------------------------
        

        private void i_Persistent_data_default()
        {
            _access_token = "";
            _token_expire_date = new DateTime(1970, 1, 1);
            _access_token_API = "";
            _token_expire_date_API = new DateTime(1970, 1, 1);
            this._email = "";
            this._password = "";
            if (this._user_current_page != 0) { } else { this._user_current_page = 0; }
            this._user_current_view = 0;
            this._user_current_filter = 0;
        }

        //-----------------------------------------------------

        public AppDelegate(App app) : base()
        {
            this.i_Persistent_data_default();
            _user_id = 0;
            this._user_name = null;
            this._user_last_name = null;
            this._user_image = null;
            this._user_public_profile = false;
            this._user_newsletter = false;
            _user_new_offers = 0;
            _user_notify = true;
            _user_unreaded_messages = 0;
            this._countries = null;
            this._user_page = null;
            
            this._app = app;
        }

        //-----------------------------------------------------

        protected override void Liberate_resources(bool disposing)
        {
            C2AssertDev.Error(disposing == true);

            if (disposing == true)
            {
                this.i_Persistent_data_default();
                _user_id = 0;
                this._user_name = null;
                this._user_last_name = null;
                this._user_image = null;
                this._user_public_profile = false;
                this._user_newsletter = false;
                _user_notify = true;
                _user_new_offers = 0;
                _user_unreaded_messages = 0;
                this._app = null;
            }
        }

        //-----------------------------------------------------

        private void i_Read()
        {
            _access_token = "";
            _token_expire_date = new DateTime(1970, 1, 1);
            _access_token_API = "";
            _token_expire_date_API = new DateTime(1970, 1, 1);
            if (this._user_current_page != 0) { } else { this._user_current_page = 0; }

            this._user_current_view = 0; // (UserCurrentView)config_params["user_current_view"];
            this._user_current_filter = 0; // (UserCurrentFilter)config_params["user_current_filter"];
        }

        //-----------------------------------------------------

        private NavigationPage i_Login_page(string email, string password)
        {
            NavigationPage navigation_page;
            LoginPage login_page;
            login_page = new LoginPage(this);
            navigation_page = new NavigationPage(login_page);
            navigation_page.Style = AppStyle.Navigation_page;
            login_page.SetData(email, password);
            return navigation_page;
        }

        //-----------------------------------------------------

        private Page i_User_detail_page(bool launch_personal_page)
        {
            switch (this._user_current_page)
            {
                case UserCurrentPage.kPanel:
                    PanelPage panel= new PanelPage(this, this._user_current_view, this._user_current_filter);
                    return panel;
                case UserCurrentPage.kProfile:
                    return new ProfilePage(this, launch_personal_page);
                case UserCurrentPage.kCompanies:
                    return new CompaniesPage(this);
                case UserCurrentPage.kHowto:
                    return new HowtoPage(this, null, null);
                case UserCurrentPage.kSettings:
                    return new SettingsPage(this, this._user_public_profile, this._user_newsletter,_user_notify);
                default:
                    C2Assert.Error(false);
                    return null;
            }
        }

        //-----------------------------------------------------

        private NavigationPage i_User_navigation_page(bool launch_personal_page)
        {
            NavigationPage navigation;
            navigation = new NavigationPage(i_User_detail_page(launch_personal_page));
            navigation.Style = AppStyle.Navigation_page;
            return navigation;
        }

        //-----------------------------------------------------

        private UserPage i_User_page()
        {
            if (this._user_page == null)
            {
                MenuPage user_menu_page;
                NavigationPage navigation_page;
                
                user_menu_page = new MenuPage(this, this._user_current_page);
                if (_is_personal_data_complete == false)
                {
                    navigation_page = i_User_navigation_page(true);
                }
                else {
                    navigation_page = i_User_navigation_page(false);
                }
                
                this._user_page = new UserPage(this, user_menu_page, navigation_page);
            }

            return this._user_page;
        }

        //-----------------------------------------------------

        public void i_Update_menu_data()
        {
            _user_new_offers = 0;
            _user_unreaded_messages = 0;
            UserPage user_page = this.i_User_page();
            MenuPage menu_page = (MenuPage)user_page.Master;
            menu_page.Set_user_data(this._user_name, this._user_last_name, this._user_image);
            menu_page.Set_percentage();
            menu_page.Set_new_offers(_user_new_offers);
            menu_page.Set_num_unreaded_messages(_user_unreaded_messages);
        }


        public void Disable_Menu()
        {
            ((MenuPage)this._user_page.Master).IsVisible=false;
        }

        public void Enable_Menu()
        {
            ((MenuPage)this._user_page.Master).IsVisible = true;
        }


        public void Block_Menu()
        {
            ((MenuPage)this._user_page.Master).IsEnabled = false;
        }

        public void Desblock_Menu()
        {
            ((MenuPage)this._user_page.Master).IsEnabled = true;
        }

        public void On_profile_data_change() {
            ((MenuPage)this._user_page.Master).Set_percentage();
        }
        //-----------------------------------------------------

        public void On_personal_data_change(string user_name, string user_last_name, string user_image)
        {
            this._user_name = user_name;
            this._user_last_name = user_last_name;
            this._user_image = user_image;
            ((MenuPage)this._user_page.Master).Set_user_data(this._user_name, this._user_last_name, this._user_image);
        }

        //-----------------------------------------------------

        public void  On_new_offers_change(uint new_offers)
        {
            _user_new_offers = new_offers;
            ((MenuPage)this._user_page.Master).Set_new_offers(_user_new_offers);
        }

        //-----------------------------------------------------

        private ServerResponse i_Error_response<T>(ServerResponse<T> response) where T : class
        {
            ServerResponse result;
            result.type = response.type;
            result.message = response.message;
            return result;
        }

        //-----------------------------------------------------

        private ServerResponse<T> i_Error_response<T>(ServerResponse response) where T : class
        {
            ServerResponse<T> result;
            result.type = response.type;
            result.message = response.message;
            result.result_object = null;
            return result;
        }

        //-----------------------------------------------------

        private static DateTime i_Date_from_unix_time(long seconds_from_epoch)
        {

            DateTime fechacontrol;
            fechacontrol= DateTime.Now;
            //fechacontrol.AddSeconds(seconds_from_epoch);
            return fechacontrol.AddSeconds(seconds_from_epoch); ;
        }

        //-----------------------------------------------------
        public static async Task<ServerResponse> i_Authenticate_API()
        {
            ServerResponse<AuthJSON> auth_response;
            ServerResponse response;

            auth_response = await Server.Authentication_request_API();

            if (auth_response.type == ResponseType.OK_SUCCESS)
            {
                _access_token_API = auth_response.result_object.access_token;
                _token_expire_date_API = i_Date_from_unix_time(auth_response.result_object.expires_in - 3600);
                response.type = ResponseType.OK_SUCCESS;
                response.message = "";
            }
            else if (auth_response.type == ResponseType.SERVER_ERROR)
            {
                response.type = ResponseType.AUTHENTICATION_ERROR;
                response.message = auth_response.message;
            }
            else if (auth_response.type == ResponseType.SERVICE_ERROR)
            {
                response.type = ResponseType.AUTHENTICATION_ERROR;
                response.message = auth_response.message;
            }

            else if (auth_response.type == ResponseType.URL_ERROR)
            {
                response.type = ResponseType.AUTHENTICATION_ERROR;
                response.message = auth_response.message;
            }
            else
            {
                response.type = auth_response.type;
                response.message = auth_response.message;
            }

            return response;
        }

        //-----------------------------------------------------

        //public static async Task<ServerResponse> i_Authenticate()
        //{
        //    ServerResponse<AuthJSON> auth_response;
        //    ServerResponse response;
            
        //    auth_response = await Server.Authentication_request();

        //    if (auth_response.type == ResponseType.OK_SUCCESS)
        //    {
        //        _access_token = auth_response.result_object.access_token;
        //        _token_expire_date = i_Date_from_unix_time(auth_response.result_object.expires - 3600);
        //        response.type = ResponseType.OK_SUCCESS;
        //        response.message = "";
        //    }
        //    else if (auth_response.type == ResponseType.SERVER_ERROR)
        //    {
        //        response.type = ResponseType.AUTHENTICATION_ERROR;
        //        response.message = auth_response.message;
        //    }
        //    else if (auth_response.type == ResponseType.SERVICE_ERROR)
        //    {
        //        response.type = ResponseType.AUTHENTICATION_ERROR;
        //        response.message = auth_response.message;
        //    }

        //    else if (auth_response.type == ResponseType.URL_ERROR)
        //    {
        //        response.type = ResponseType.AUTHENTICATION_ERROR;
        //        response.message = auth_response.message;
        //    }
        //    else
        //    {
        //        response.type = auth_response.type;
        //        response.message = auth_response.message;
        //    }

        //    return response;
        //}

        //-----------------------------------------------------

        //public static async Task<ServerResponse> i_Access_token()
        //{
        //    ServerResponse response;
        //    if (_token_expire_date > DateTime.Now)
        //    {
        //        response.type = ResponseType.OK_SUCCESS;
        //        response.message = "";
        //    }
        //    else
        //    {
        //        response = await i_Authenticate();
        //    }

        //    return response;
        //}

        public static async Task<ServerResponse> i_Access_token_API()
        {
            ServerResponse response;
            if (_token_expire_date_API > DateTime.Now)
            {
                response.type = ResponseType.OK_SUCCESS;
                response.message = "";
            }
            else
            {
                response = await i_Authenticate_API();
            }

            return response;
        }


        //-----------------------------------------------------

        private async Task<ServerResponse> i_Login()
        {
            ServerResponse response;

            if (String.IsNullOrEmpty(this._email) == true || String.IsNullOrEmpty(this._password) == true)
            {
                response.type = ResponseType.SERVICE_ERROR;
                response.message = "";
            }
            else
            {
                response = await i_Access_token_API();
                if (response.type == ResponseType.OK_SUCCESS)
                {
                    ServerResponse<LoginDataJSON> login_response = await Server.Login_request(_access_token_API, this._email, this._password,_user_id);
                    if (login_response.type == ResponseType.OK_SUCCESS)
                    {
                        _user_id = login_response.result_object.student_id;
                        //ahora hay que sacar los datos del usuario que nos interesan con otra consulta
                        ServerResponse<StudentDataJSON> user_response = await On_personal_data();
                        if (user_response.type == ResponseType.OK_SUCCESS)
                        {
                            if (user_response.result_object.phone == "" || user_response.result_object.location == null)
                            {
                                //grabo as false datos mercado
                                bool regok=await Server.Personal_merchant_save(_access_token_API, _user_id);
                            }

                            this._user_name = user_response.result_object.name;
                            this._user_last_name = user_response.result_object.last_name;
                            string img_url;
                            if (user_response.result_object.image_url != null && user_response.result_object.image_url != "")
                            {
                                img_url = user_response.result_object.image_url;
                            }
                            else
                            {
                                //user
                                img_url = Server._URL_IMAGES_API + "user.jpg";
                            }
                            this._user_image = img_url;
                            //https://www.bewanted.com/assets/files/students/d453c5162e03069d4a0fd63f14fbf8d4.jpg

                            this._user_public_profile =Server.i_valorbool(user_response.result_object.public_profile);
                            this._user_newsletter = Server.i_valorbool(user_response.result_object.newsletter);
                            _user_notify = Server.i_valorbool(user_response.result_object.notify);

                            if (this._app._platform == App.Platform.Android)
                            {
                                List<string> data = new List<string>();
                                data.Add(this._email);
                                data.Add(this._password);
                                data.Add(_user_id.ToString());
                                data.Add(Server.device_id);
                                if (_user_notify)
                                {
                                    data.Add("S");
                                }
                                else
                                {
                                    data.Add("N");
                                }
                                DependencyService.Get<IFileService>().Save_data(data);
                            }
                            else
                            {
                                Application.Current.Properties["email"] = this._email;
                                Application.Current.Properties["password"] = this._password;
                                Application.Current.Properties["user_id"] = _user_id.ToString();
                                Application.Current.Properties["device_id"] = Server.device_id;
                                if (_user_notify)
                                {
                                    Application.Current.Properties["notify"] = "S";
                                }
                                else
                                {
                                    Application.Current.Properties["notify"] = "N";
                                }
                                await Application.Current.SavePropertiesAsync();
                            }
                        }

                        
                    }
                    else
                    {
                        
                        response = i_Error_response(login_response);
                    }
                }
            }

            return response;
        }

        //-----------------------------------------------------

        private void i_init_C2DLL()
        {
            C2Entry.TXT_FIELD_REQUIRED = Local.txtCommon00;
            C2Entry.TXT_MIN_CHARACTERS = Local.txtCommon01;
            C2Entry.TXT_MAX_CHARACTERS = Local.txtCommon02;
            C2MailEntry.TXT_EMAIL_INVALID = Local.txtEmailError;
            C2AssistedEntry.TXT_NO_MATCH = Local.txtCommon03;
            C2NumberEntry.TXT_NOT_A_NUMBER = Local.txtCommon04;
            C2NumberEntry.TXT_MIN_NUMBER = Local.txtCommon05;
            C2NumberEntry.TXT_MAX_NUMBER = Local.txtCommon06;
            AppStyle.Set_C2_style();
        }

        //-----------------------------------------------------

        private async Task<bool> i_Try_login()
        {
            ServerResponse response;
            response = await this.i_Login();
            if (response.type == ResponseType.OK_SUCCESS)
            {
                await datos_personales_completos();
                await datos_estudio_completos();
                await datos_mercado_completos();

                this.i_Update_menu_data();
                this._user_current_page = UserCurrentPage.kPanel;
                this._app.MainPage = i_User_page();
                if (_user_notify)
                {
                    DependencyService.Get<INotificaciones>().iniciaServicio();
                }
                else
                {
                    DependencyService.Get<INotificaciones>().paraServicio();
                }

            }
            else
            {
                this._app.MainPage = i_Login_page(this._email, this._password);
            }

            return true;
        }

        //-----------------------------------------------------

        public async Task<bool> Start_app(string email, string password)
        {
            C2Assert.No_null(this._app);
            this._email = email;
            this._password = password;
            this.i_init_C2DLL();
            this.i_Read();
            return await this.i_Try_login();
        }

        public async Task<bool> datos_personales_completos() {
            ServerResponse<StudentDataJSON> response_personal;
            response_personal = await this.On_personal_data();
            if (response_personal.type == ResponseType.OK_SUCCESS)
            {

                if (response_personal.result_object.phone == "" || response_personal.result_object.location==null)
                {
                    _is_personal_data_complete = false;
                    _showpersonalcompleto = true;
                }
                else
                {
                    if (_is_personal_data_complete == false)
                    {
                        _is_personal_data_complete = true;
                        _showpersonalcompleto = true;
                    }
                    else {
                        _showpersonalcompleto = false;
                    }
                    
                    
                }

            }
            return true;
        }

        public async Task<bool> datos_mercado_completos()
        {
            ServerResponse<MerchantDataJSON> response;
            response = await this.On_student_merchant();
            if (response.type == ResponseType.OK_SUCCESS)
            {
                MerchantDataJSON merchant;
                merchant = response.result_object;
                if (merchant.searching != null && merchant.searching != "")
                {
                    AppDelegate._is_merchant_data = true;

                }
                else
                {
                    AppDelegate._is_merchant_data = false;
                }

            }
            return true;
        }

        public async Task<bool> datos_estudio_completos()
        {
            
            ServerResponse<List<AcademicDataJSON>> responseacademic;
            responseacademic = await this.On_student_academics();
            if (responseacademic.type == ResponseType.OK_SUCCESS)
            {
                if (responseacademic.result_object.Count>0)
                {
                    AppDelegate._is_academic_data = true;

                }
                else
                {
                    AppDelegate._is_academic_data = false;
                }

            }
            return true;
        }

        //-----------------------------------------------------

        public void End_app()
        {
        }

        //-----------------------------------------------------

        public async Task<Boolean> On_login(string email, string password, LoginPage page)
        {
            this._email = email;
            this._password = password;
            ServerResponse response = await this.i_Login();
            if (response.type == ResponseType.OK_SUCCESS)
            {
                await datos_personales_completos();
                await datos_estudio_completos();
                await datos_mercado_completos();
                this._user_current_page = UserCurrentPage.kPanel;
                this.i_Update_menu_data();
                this._app.MainPage = i_User_page();
                if (_user_notify)
                {
                    DependencyService.Get<INotificaciones>().iniciaServicio();
                }
                else
                {
                    DependencyService.Get<INotificaciones>().paraServicio();
                }

                return true;
                   
            }
            else
            {
                if (response.message == "400 (Bad Request)")
                {
                    response.message = "Verifique los datos de entrada. Usuario o Pass incorrectos";
                    await page.DisplayAlert("Login incorrecto", response.message, Local.txtAlertOk);
                }
                else {
                    Alert.Show(response, page);
                }

                
                return false;
            }
        }

        //-----------------------------------------------------
        
        public async void On_login2(string email, string password)
        {
            this._email = email;
            this._password = password;
            bool ok = await this.i_Try_login();
        }

        //-----------------------------------------------------
        public async Task<Boolean> On_Check_Email(string email) {
            bool registry_ok = false;
            ServerResponse response;
            response = await i_Access_token_API();
            if (response.type == ResponseType.OK_SUCCESS)
            {
                ServerResponse<CheckEmailJSON> check_response;
                check_response = await Server.check_email_request(_access_token_API, email);
                if (check_response.type == ResponseType.OK_SUCCESS)
                {
                    if (check_response.result_object.available == 1)
                    {
                        registry_ok = true;
                    }
                    
                }
            }
            return registry_ok;
        }
        //public async void On_register(string email, string password, string name, string last_name, string phone, long nationality_id, long country_id, long state_id, long city_id, string birthdate, CreateAccountPage page)
        public async Task<Boolean> On_register(string email, string password, string name, string last_name, CreateAccountPage page)
        {
            bool registry_ok = false;
            ServerResponse response;

            this.i_Persistent_data_default();
            this._email = email;
            this._password = password;
            response = await i_Access_token_API();
            if (response.type == ResponseType.OK_SUCCESS)
            {
                ServerResponse<RegisterJSON> register_response;
                //register_response = await Server.Register_request(_access_token, email, password, name, last_name, phone, nationality_id, country_id, state_id, city_id, birthdate);
                register_response = await Server.Register_request(_access_token_API, email, password, name, last_name);
                if (register_response.type == ResponseType.OK_SUCCESS)
                {
                    registry_ok = true;
                }
                else
                {
                    Alert.Show(register_response, page);
                    this._app.MainPage = i_Login_page(this._email, this._password);
                }
            }
            else
            {
                Alert.Show(response, page);
                this._app.MainPage = i_Login_page(this._email, this._password);
            }

            if (registry_ok == true)
            {
                NavigationPage navigation;
                navigation = new NavigationPage(new HowtoPage(this, this._email, this._password));
                navigation.Style = AppStyle.Navigation_page;
                this._app.MainPage = navigation;
            }
            return true;
        }

        public static void Actcontador() {
            int cont_ofertas = (int)_user_new_offers;
            int cont_mensajes = (int)_user_unreaded_messages;
            

            DependencyService.Get<INotificaciones>().contadorIcono(cont_ofertas + cont_mensajes);
        }
        
        public async static void  notificacionOfertasMensajes() {

            bool espera;
            _user_id= Convert.ToInt64(Application.Current.Properties["user_id"]);
            Server.device_id = Application.Current.Properties["device_id"].ToString();
            espera=await ofertas();
            espera=await messages();
            Actcontador();
        }

        
        public async static Task<Boolean> ofertas() {

            ServerResponse<OfferReadedJSON> offers_readed_json = await Server.Offer_get_not_readed(_access_token_API, _user_id);
            if (offers_readed_json.type == ResponseType.OK_SUCCESS)
            {
                uint noleidas = (uint)offers_readed_json.result_object.offers_not_readed;
                _user_new_offers = noleidas;
            }
            return true;
        }
        
        public async static Task<Boolean> messages()
        {
            ServerResponse response;
            response = await i_Access_token_API();
            if (response.type == ResponseType.OK_SUCCESS)
            {
                ServerResponse<UnreadedJSON> unreaded_messages_json;
                unreaded_messages_json = await Server.Unreaded_request(_access_token_API, _user_id);
                if (unreaded_messages_json.type == ResponseType.OK_SUCCESS)
                {
                    long unreaded_messages = 0;
                    for (int i = 0; i < unreaded_messages_json.result_object.data.Count; ++i)
                    {
                        unreaded_messages = unreaded_messages + unreaded_messages_json.result_object.data[i].unreaded_messages;
                    }
                    _user_unreaded_messages = unreaded_messages;
                    //_user_unreaded_messages = unreaded_messages_json.result_object.data.unreaded_messages;
                }
            }
            return true;
        }

        
        public async void On_unreaded_messages()
        {
            ServerResponse response;
            response = await i_Access_token_API();
            if (response.type == ResponseType.OK_SUCCESS)
            {
                ServerResponse<UnreadedJSON> unreaded_messages_json;
                unreaded_messages_json = await Server.Unreaded_request(_access_token_API, _user_id);
                if (unreaded_messages_json.type == ResponseType.OK_SUCCESS)
                {
                    long unreaded_messages=0;
                    for (int i = 0; i < unreaded_messages_json.result_object.data.Count; ++i) {
                        unreaded_messages = unreaded_messages+ unreaded_messages_json.result_object.data[i].unreaded_messages;
                    }
                        _user_unreaded_messages = unreaded_messages;
                    ((MenuPage)this._user_page.Master).Set_num_unreaded_messages(_user_unreaded_messages);
                }
            }
        }

        //-----------------------------------------------------

        public async Task<ServerResponse<SaveDataJSON>> On_recovery_password(string email)
        {
            ServerResponse response;
            response = await i_Access_token_API();
            if (response.type == ResponseType.OK_SUCCESS)
                return await Server.Recovery_password_request(_access_token_API, email);
            else
                return this.i_Error_response<SaveDataJSON>(response);
        }

        //-----------------------------------------------------

        public async Task<ServerResponse<List<CountryJSON>>> On_nationalities()
        {
            
            ServerResponse responseAPI;
            responseAPI = await i_Access_token_API();
            if (responseAPI.type == ResponseType.OK_SUCCESS) {
                return  await Server.Nationalities_request(_access_token_API);
            }
            else
                return this.i_Error_response<List<CountryJSON>>(responseAPI);
        }

        //-----------------------------------------------------

        public async Task<ServerResponse<List<CountryJSON>>> On_countries()
        {
            ServerResponse<List<CountryJSON>> response;
            if (this._countries == null)
            {
                ServerResponse responseAPI;
                responseAPI = await i_Access_token_API();
                if (responseAPI.type == ResponseType.OK_SUCCESS)
                {
                    return await Server.Countries_request(_access_token_API);
                }
                else
                {
                    return this.i_Error_response<List<CountryJSON>>(responseAPI);
                }
            }
            else
            {
                
                response.type = ResponseType.OK_SUCCESS;
                response.message = "";
                response.result_object = this._countries;
                return response;
            }
        }

        public async Task<ServerResponse<List<SocialJSON>>> On_social()
        {
            ServerResponse response;
            response = await i_Access_token_API();
            if (response.type == ResponseType.OK_SUCCESS)
                return await Server.social_request(_access_token_API);
            else
                return this.i_Error_response<List<SocialJSON>>(response);
        }
        //-----------------------------------------------------

        public async Task<ServerResponse<List<StateJSON>>> On_states(long country_id, string search_text)
        {
            ServerResponse response;
            response = await i_Access_token_API();
            if (response.type == ResponseType.OK_SUCCESS)
                return await Server.States_request(_access_token_API, country_id, search_text);
            else
                return this.i_Error_response<List<StateJSON>>(response);
        }


        //-----------------------------------------------------

        public async Task<ServerResponse<List<CityJSON>>> On_cities(long state_id, string search_text)
        {
            ServerResponse response;
            response = await i_Access_token_API();
            if (response.type == ResponseType.OK_SUCCESS)
                return await Server.Cities_request(_access_token_API, state_id, search_text);
            else
                return this.i_Error_response<List<CityJSON>>(response);
        }

        //-----------------------------------------------------

        public async Task<ServerResponse<List<UniversityJSON>>> On_universities(long country_id, long state_id, string search_text)
        {
            ServerResponse response;
            response = await i_Access_token_API();
            if (response.type == ResponseType.OK_SUCCESS)
                return await Server.Universities_request(_access_token_API, country_id, state_id, search_text);
            else
                return this.i_Error_response<List<UniversityJSON>>(response);
        }
        //-----------------------------------------------------

        public async Task<ServerResponse<List<OfferJSON>>> On_offer_list(UserCurrentFilter user_current_filter)
        {
            ServerResponse response;
            this._user_current_view = UserCurrentView.kOffers;
            this._user_current_filter = user_current_filter;
            response = await i_Access_token_API();
            if (response.type == ResponseType.OK_SUCCESS)
            {
                
                ServerResponse<List<OfferJSON>> ofertas= await Server.Offers_request(_access_token_API, _user_id, i_Job_offer_status_id());
                if (ofertas.type == ResponseType.OK_SUCCESS)
                {

                    for (int j = 0; j < ofertas.result_object.Count; ++j)
                    {

                        //offers.result_object.items.Add()
                        //messaging/conversations/invitation/{invitation_id}
                        //Offer_request_conversation


                        if (ofertas.result_object[j].job_offer_id != int.MinValue)
                        {
                            //ServerResponse<OfferDetailDataJSON> offerdetail2_json = await On_offer_detail_name(ofertas.result_object[j].job_offer_id);
                            //if (offerdetail2_json.type == ResponseType.OK_SUCCESS)
                            //{
                            //    if (offerdetail2_json.result_object.name == "") { ofertas.result_object[j].name = offerdetail2_json.result_object.position; }
                            //    else { ofertas.result_object[j].name = offerdetail2_json.result_object.name; }
                                
                            //}

                            ServerResponse<OffersJSON> offerdetail_json = await Server.Offer_request_conversation(_access_token_API, _user_id, ofertas.result_object[j].job_offer_id);
                            if (offerdetail_json.type == ResponseType.OK_SUCCESS)
                            {
                                if (offerdetail_json.result_object.items != null && offerdetail_json.result_object.items.Count > 0)
                                {

                                    if (offerdetail_json.result_object.items[0].name == null || offerdetail_json.result_object.items[0].name == "")
                                    {
                                        offerdetail_json.result_object.items[0].name = offerdetail_json.result_object.items[0].position;
                                    }
                                    ofertas.result_object[j] = offerdetail_json.result_object.items[0];

                                }
                                else
                                {
                                    ServerResponse<OfferDetailDataJSON> offerdetail2_json = await On_offer_detail_name(ofertas.result_object[j].job_offer_id);
                                    if (offerdetail2_json.type == ResponseType.OK_SUCCESS)
                                    {
                                        ofertas.result_object[j].name = offerdetail2_json.result_object.name;
                                    }
                                }
                            }
                        }

                        if (ofertas.result_object[j].company_id != int.MinValue)
                        {
                            ServerResponse<CompanyJSON> companies_json = await On_company_description(ofertas.result_object[j].company_id);
                            if (companies_json.type == ResponseType.OK_SUCCESS)
                            {
                                ofertas.result_object[j].company_name = companies_json.result_object.name;
                                //Server._URL_IMAGES_COMPANIES_API + companies_json.result_object.image_source+"_logo.jpg";
                                //ofertas.result_object[j].company_image = Server._URL_IMAGES_COMPANIES_API + companies_json.result_object.image_source + "_logo.jpg";
                                ofertas.result_object[j].company_image = companies_json.result_object.image_source;
                            }
                        }
                        //if (ofertas.result_object[j].job_offer_status_id != int.MinValue)
                        //{
                        //    ServerResponse<OfferJSON> offer_json = await Server.Offers_status_request(_access_token_API, _user_id, ofertas.result_object[j].job_offer_status_id);
                        //    if (offer_json.type == ResponseType.OK_SUCCESS)
                        //    {
                        //        ofertas.result_object[j].status_name = offer_json.result_object.status_name;

                        //    }
                        //}

                    }
                    
                    
                }
                return ofertas;

            }
            else
                return this.i_Error_response<List<OfferJSON>>(response);
        }
        

        //-----------------------------------------------------

        public async Task<ServerResponse<OfferDetailDataJSON>> On_offer_detail(int offer_id,int invitation_id)
        {
            ServerResponse response;
            response = await i_Access_token_API();
            if (response.type == ResponseType.OK_SUCCESS) {
                ServerResponse<OfferDetailDataJSON> offerdetail_json = await Server.Offer_detail_request(_access_token_API, _user_id, offer_id);
                if (offerdetail_json.type == ResponseType.OK_SUCCESS) {
                    if (offerdetail_json.result_object.company_user_id != int.MinValue) {
                        ServerResponse<OfferUserDataJSON> offeruserdetail_json = await Server.user_company_request(_access_token_API, offerdetail_json.result_object.company_id.ToString(), offerdetail_json.result_object.company_user_id.ToString());
                        if (offeruserdetail_json.type == ResponseType.OK_SUCCESS)
                        {
                            offerdetail_json.result_object.user = offeruserdetail_json.result_object;
                        }
                    }

                    ServerResponse<List<OfferKillerDataJSON>> offerkillerdetail_json = await Server.Offer_killer_detail_request(_access_token_API,_user_id, offer_id);
                    if (offerkillerdetail_json.type == ResponseType.OK_SUCCESS)
                    {
                        if (offerkillerdetail_json.result_object.Count == 0)
                        {
                            offerkillerdetail_json = await Server.Offer_killer_request(_access_token_API, _user_id, offer_id);
                            if (offerkillerdetail_json.type == ResponseType.OK_SUCCESS) {
                                offerdetail_json.result_object.killer_questions = offerkillerdetail_json.result_object;
                            }
                        }
                        else {
                            offerdetail_json.result_object.killer_questions = offerkillerdetail_json.result_object;
                        }
                        
                    }

                    if (offerdetail_json.result_object.company_id != int.MinValue)
                    {
                        ServerResponse<CompanyJSON> companies_json = await On_company_description(offerdetail_json.result_object.company_id);
                        if (companies_json.type == ResponseType.OK_SUCCESS)
                        {
                            offerdetail_json.result_object.company = new OfferCompanyDataJSON();
                            offerdetail_json.result_object.company.id = offerdetail_json.result_object.company_id;
                            offerdetail_json.result_object.company.name= companies_json.result_object.name;
                            offerdetail_json.result_object.company_description = companies_json.result_object.description;
                            
                                //offerdetail_json.result_object.company.url = Server._URL_IMAGES_COMPANIES_API + companies_json.result_object.image_source + "_logo.jpg";
                            offerdetail_json.result_object.company.url =  companies_json.result_object.image_source;
                        }
                    }
                    //Messages_request
                    if (invitation_id != int.MinValue) {
                        ServerResponse<OfferMsgDataJSON> offermsgdetail_json = await Server.Messages_request(_access_token_API, invitation_id);
                        if (offermsgdetail_json.type == ResponseType.OK_SUCCESS)
                        {
                            offerdetail_json.result_object.messages = offermsgdetail_json.result_object;

                            if (offerdetail_json.result_object.messages.data.Count>0) {
                                if (offerdetail_json.result_object.messages.data[0].messages != null)
                                {
                                    
                                    if (offerdetail_json.result_object.messages.data[0].messages.Count > 0)
                                    {
                                        offerdetail_json.result_object.messages.data[0].messages_unread_number = 0;
                                        for (int j = 0; j < offerdetail_json.result_object.messages.data[0].messages.Count; ++j)
                                        {
                                            if (offerdetail_json.result_object.messages.data[0].messages[j].company_user_id != int.MinValue)
                                            {
                                                ServerResponse<OfferUserDataJSON> offerusermsgdetail_json = await Server.user_company_request(_access_token_API, offerdetail_json.result_object.company_id.ToString(), offerdetail_json.result_object.messages.data[0].messages[j].company_user_id.ToString());
                                                if (offerusermsgdetail_json.type == ResponseType.OK_SUCCESS)
                                                {
                                                    offerdetail_json.result_object.messages.data[0].messages[j].user = offerusermsgdetail_json.result_object;
                                                }
                                            }
                                            if (offerdetail_json.result_object.messages.data[0].messages[j].readed == "0" && offerdetail_json.result_object.messages.data[0].messages[j].from == "c") {
                                                offerdetail_json.result_object.messages.data[0].messages_unread_number = offerdetail_json.result_object.messages.data[0].messages_unread_number + 1;
                                            }
                                        }
                                    }
                                }
                            }
                            

                        }
                    }
                    
                }
                return offerdetail_json;
            }
            else
                return this.i_Error_response<OfferDetailDataJSON>(response);
        }


        public async Task<ServerResponse<OfferDetailDataJSON>> On_offer_detail_name(int offer_id)
        {
            ServerResponse response;
            response = await i_Access_token_API();
            if (response.type == ResponseType.OK_SUCCESS)
                return await Server.Offer_detail_request_name(_access_token_API,  offer_id);
            else
                return this.i_Error_response<OfferDetailDataJSON>(response);
        }

        //-----------------------------------------------------

            

        //-----------------------------------------------------
        public async Task<ServerResponse<SaveDataJSON>> On_offer_accept(int offer_id, List<OfferKillerDataJSON> killer_questions)
        {
            ServerResponse response;
            response = await i_Access_token_API();
            if (response.type == ResponseType.OK_SUCCESS)
                return await Server.Offer_action_request(_access_token_API, _user_id, offer_id, "accept", killer_questions);
            else
                return this.i_Error_response<SaveDataJSON>(response);
        }


        //-----------------------------------------------------

        public async Task<ServerResponse<SaveDataJSON>> On_offer_decline(int offer_id)
        {
            ServerResponse response;
            response = await i_Access_token_API();
            if (response.type == ResponseType.OK_SUCCESS)
                return await Server.Offer_action_request(_access_token_API, _user_id, offer_id, "decline", null);
            else
                return this.i_Error_response<SaveDataJSON>(response);
        }

        //-----------------------------------------------------

        public async Task<ServerResponse<SaveDataJSON>> On_offer_delete(int offer_id)
        {
            ServerResponse response;
            response = await i_Access_token_API();
            if (response.type == ResponseType.OK_SUCCESS)
                return await Server.Offer_action_request(_access_token_API, _user_id, offer_id, "decline", null);
            else
                return this.i_Error_response<SaveDataJSON>(response);
        }

        //-----------------------------------------------------

        public async Task<ServerResponse<SaveDataJSON>> On_offer_message_save(int invitation_id, int company_user_id, string message)
        {
            ServerResponse response;
            response = await i_Access_token_API();
            if (response.type == ResponseType.OK_SUCCESS)
                return await Server.Offer_message_save(_access_token_API, _user_id, invitation_id, company_user_id, message);
            else
                return this.i_Error_response<SaveDataJSON>(response);
        }

        //-----------------------------------------------------

        public async Task<ServerResponse<SaveDataJSON>> On_offer_mark_messages_as_readed(int invitation_id)
        {
            ServerResponse response;
            response = await i_Access_token_API();
            if (response.type == ResponseType.OK_SUCCESS)
                return await Server.Offer_mark_messages_as_readed(_access_token_API, _user_id, invitation_id);
            else
                return this.i_Error_response<SaveDataJSON>(response);
        }

        //-----------------------------------------------------

        public async Task<ServerResponse<OfferReadedJSON>> On_offer_get_not_readed()
        {
            ServerResponse response;
            response = await i_Access_token_API();
            if (response.type == ResponseType.OK_SUCCESS)
                return await Server.Offer_get_not_readed(_access_token_API, _user_id);
            else
                return this.i_Error_response<OfferReadedJSON>(response);
        }

        
        //-----------------------------------------------------
        public async Task<ServerResponse<StudentDataJSON>> On_profile_percentaje()
        {
            ServerResponse response;
            response = await i_Access_token_API();
            if (response.type == ResponseType.OK_SUCCESS)
            {

                return await Server.Percentaje_request(_access_token_API, _user_id);
            }

            else
                return this.i_Error_response<StudentDataJSON>(response);
        }

        public async Task<ServerResponse<StudentLocationJSON>> On_personal_location_data()
        {
            ServerResponse response;
            response = await i_Access_token_API();
            if (response.type == ResponseType.OK_SUCCESS)
            {
                return await Server.student_location_request(_access_token_API, _user_id);
            }
            else
                return this.i_Error_response<StudentLocationJSON>(response);
        }

        


        public async Task<ServerResponse<StudentDataJSON>> On_personal_data()
        {
            ServerResponse response;
            response = await i_Access_token_API();
            if (response.type == ResponseType.OK_SUCCESS) {
                ServerResponse<StudentDataJSON> student_json = await Server.Student_profile_request(_access_token_API, _user_id);
                if (student_json.type == ResponseType.OK_SUCCESS)
                {
                    if (student_json.result_object.location != null)
                    {
                        if (student_json.result_object.location.country_id != null && student_json.result_object.location.country_id != "")
                        {
                            ServerResponse<CountryJSON> country_json = await On_view_country(student_json.result_object.location.country_id);
                            if (country_json.type == ResponseType.OK_SUCCESS)
                            {
                                student_json.result_object.location.country = country_json.result_object;
                            }
                        }
                        if (student_json.result_object.location.state_id != null && student_json.result_object.location.state_id != "")
                        {
                            ServerResponse<StateJSON> state_json = await On_view_state(student_json.result_object.location.state_id);
                            if (state_json.type == ResponseType.OK_SUCCESS)
                            {
                                student_json.result_object.location.state = state_json.result_object;
                            }
                        }
                        if (student_json.result_object.location.city_id != null && student_json.result_object.location.city_id != "")
                        {
                            ServerResponse<CityJSON> city_json = await On_view_city(student_json.result_object.location.city_id);
                            if (city_json.type == ResponseType.OK_SUCCESS)
                            {
                                student_json.result_object.location.city = city_json.result_object;
                            }
                        }
                    }
                    
                }
                return student_json;
            }
            
            else
                return this.i_Error_response<StudentDataJSON>(response);
        }

        //-----------------------------------------------------
        //public async Task<bool> On_personal_data_save(PersonalDataJSON personal, char[] image64)
        public async Task<bool> On_personal_data_save(PersonalDataJSON personal, byte[] imageBytes64)
        {
            ServerResponse response;
            response = await i_Access_token_API();
            if (response.type == ResponseType.OK_SUCCESS) {
                return await Server.Personal_data_save(_access_token_API, _user_id, personal, imageBytes64);
            }
            else
                return false;
        }

        //-----------------------------------------------------

        

        public async Task<ServerResponse<SaveDataJSON>> On_academic_data_save(AcademicDataJSON academic)
        {
            ServerResponse response;
            response = await i_Access_token_API();
            if (response.type == ResponseType.OK_SUCCESS)
                return await Server.Academic_data_save(_access_token_API, _user_id, academic);
            else
                return this.i_Error_response<SaveDataJSON>(response);
        }

        //-----------------------------------------------------

        public async Task<ServerResponse<SaveDataJSON>> On_academic_data_delete(long academic_id)
        {
            ServerResponse response;
            response = await i_Access_token_API();
            if (response.type == ResponseType.OK_SUCCESS)
                return await Server.Academic_data_delete(_access_token_API, _user_id, academic_id);
            else
                return this.i_Error_response<SaveDataJSON>(response);
        }

        //-----------------------------------------------------

        public async Task<ServerResponse<List<GroupStudyJSON>>> On_groups_study()
        {
            ServerResponse response;
            response = await i_Access_token_API();
            if (response.type == ResponseType.OK_SUCCESS)
                return await Server.Groups_study_request(_access_token_API, _user_id);
            else
                return this.i_Error_response<List<GroupStudyJSON>>(response);
        }

        //-----------------------------------------------------

        public async Task<ServerResponse<List<StudyJSON>>> On_studies(int university_id, int group_study_id, string search_text)
        {
            ServerResponse response;
            response = await i_Access_token_API();
            if (response.type == ResponseType.OK_SUCCESS)
                return await Server.Studies_request(_access_token_API, _user_id, university_id, group_study_id, search_text);
            else
                return this.i_Error_response<List<StudyJSON>>(response);
        }

        //-----------------------------------------------------

        public async Task<ServerResponse<List<SubjectJSON>>> On_subjects(int study_id, string search_text)
        {
            ServerResponse response;
            response = await i_Access_token_API();
            if (response.type == ResponseType.OK_SUCCESS)
                return await Server.Subjects_request(_access_token_API, _user_id, study_id, search_text);
            else
                return this.i_Error_response<List<SubjectJSON>>(response);
        }

        //-----------------------------------------------------

        //public async Task<ServerResponse<MerchantJSON>> On_merchant_data()
        //{
        //    ServerResponse response;
        //    response = await i_Access_token_API();
        //    if (response.type == ResponseType.OK_SUCCESS)
        //        return await Server.Merchant_data_request(_access_token_API, _user_id);
        //    else
        //        return this.i_Error_response<MerchantJSON>(response);
        //}

        //-----------------------------------------------------

        public async Task<ServerResponse<SaveDataJSON>> On_merchant_data_save(MerchantDataJSON merchant)
        {
            ServerResponse response;
            response = await i_Access_token_API();
            if (response.type == ResponseType.OK_SUCCESS)
                return await Server.Merchant_data_save(_access_token_API, _user_id, merchant);
            else
                return this.i_Error_response<SaveDataJSON>(response);
        }
        //-----------------------------------------------------
        public async Task<ServerResponse<VoluntaryDataJSON>> On_voluntary_data(int voluntary_id)
        {
            ServerResponse response;
            response = await i_Access_token_API();
            if (response.type == ResponseType.OK_SUCCESS) {
                ServerResponse<VoluntaryDataJSON> voluntary_response= await Server.Voluntary_data_request(_access_token_API, _user_id, voluntary_id);
                if (voluntary_response.type == ResponseType.OK_SUCCESS) {
                    VoluntaryDataJSON vol = voluntary_response.result_object;
                    if (vol.started_at != null && vol.started_at != "")
                    {
                        DateTime dtstart = Convert.ToDateTime(vol.started_at);
                        voluntary_response.result_object.year_start = dtstart.Year;
                        voluntary_response.result_object.month_start = dtstart.Month;
                    }

                    if (vol.finished_at != null && vol.finished_at != "")
                    {
                        DateTime dtend = Convert.ToDateTime(vol.finished_at);
                        voluntary_response.result_object.year_end = dtend.Year;
                        voluntary_response.result_object.month_end = dtend.Month;
                    }

                    if (vol.state_id != int.MinValue)
                    {
                        ServerResponse<StateJSON> state_json = await On_view_state(vol.state_id.ToString());
                        if (state_json.type == ResponseType.OK_SUCCESS)
                        {
                            voluntary_response.result_object.state = state_json.result_object;
                        }
                    }
                }
                return voluntary_response;
            }
                
            else
                return this.i_Error_response<VoluntaryDataJSON>(response);
        }

        //-----------------------------------------------------
        public async Task<ServerResponse<List<VoluntaryDataJSON>>> On_student_voluntary()
        {

            ServerResponse response;
            response = await i_Access_token_API();
            if (response.type == ResponseType.OK_SUCCESS)
            {
                ServerResponse<List<VoluntaryDataJSON>> voluntary_json = await Server.student_voluntary_data_request(_access_token_API, _user_id);
                if (voluntary_json.type == ResponseType.OK_SUCCESS)
                {

                    for (int i = 0; i < voluntary_json.result_object.Count; ++i)
                    {
                        VoluntaryDataJSON vol = voluntary_json.result_object[i];
                        if (vol.started_at != null && vol.started_at != "")
                        {
                            DateTime dtstart = Convert.ToDateTime(vol.started_at);
                            voluntary_json.result_object[i].year_start = dtstart.Year;
                            voluntary_json.result_object[i].month_start = dtstart.Month;
                        }

                        if (vol.finished_at != null && vol.finished_at != "")
                        {
                            DateTime dtend = Convert.ToDateTime(vol.finished_at);
                            voluntary_json.result_object[i].year_end = dtend.Year;
                            voluntary_json.result_object[i].month_end = dtend.Month;
                        }

                        if (vol.state_id != int.MinValue)
                        {
                            ServerResponse<StateJSON> state_json = await On_view_state(vol.state_id.ToString());
                            if (state_json.type == ResponseType.OK_SUCCESS)
                            {
                                voluntary_json.result_object[i].state = state_json.result_object;
                            }
                        }

                    }
                    return voluntary_json;
                }
                else
                {
                    ServerResponse<List<VoluntaryDataJSON>> resultado;
                    resultado.type = voluntary_json.type;
                    resultado.message = voluntary_json.message;
                    resultado.result_object = null;
                    return resultado;

                }
            }
            else
                return this.i_Error_response<List<VoluntaryDataJSON>>(response);
        }
        //-----------------------------------------------------
        public async Task<ServerResponse<MerchantDataJSON>> On_student_merchant()
        {

            ServerResponse response;
            response = await i_Access_token_API();
            if (response.type == ResponseType.OK_SUCCESS)
            {
                MerchantDataJSON merchant_json = new MerchantDataJSON();

                
                ServerResponse<StudentDataJSON>  student_json= await this.On_personal_data();
                if (student_json.type == ResponseType.OK_SUCCESS)
                {
                    
                    merchant_json.student_data = student_json.result_object;
                    string search = "";
                    if (Server.i_valorbool(merchant_json.student_data.scholarship) == true) {
                        search = Local.txtProfile30;
                    }
                    if (Server.i_valorbool(merchant_json.student_data.first_job) == true)
                    {
                        if (search != "") { search = search + ", "; }
                        search = search + Local.txtProfile29;
                    }
                    if (Server.i_valorbool(merchant_json.student_data.ong) == true)
                    {
                        if (search != "") { search = search + ", "; }
                        search = search + Local.txtProfile31;
                    }
                    merchant_json.searching = search;

                    
                    string horario = "";
                    if (Server.i_valorbool(merchant_json.student_data.morning) == true)
                    {
                        horario = Local.txtProfile34;
                    }
                    if (Server.i_valorbool(merchant_json.student_data.afternoon) == true)
                    {
                        if (horario != "") { horario = horario + ", "; }
                        horario = horario + Local.txtProfile35;
                    }
                    if (Server.i_valorbool(merchant_json.student_data.all_day) == true)
                    {
                        if (horario != "") { horario = horario + ", "; }
                        horario = horario + Local.txtProfile36;
                    }
                    if (horario == "") { horario = Local.txtProfile37; }
                    merchant_json.hours = horario;

                    
                    ServerResponse<List<MerchantJobLocationJSON>> student_job_location_json = await this.On_job_location_merchant();
                    if (student_job_location_json.type == ResponseType.OK_SUCCESS) {
                        merchant_json.locations = student_job_location_json.result_object;
                        for (int i = 0; i < merchant_json.locations.Count; ++i) {
                            if (merchant_json.locations[i].country_id != int.MinValue)
                            {
                                ServerResponse<CountryJSON> country_json = await On_view_country(merchant_json.locations[i].country_id.ToString());
                                if (country_json.type == ResponseType.OK_SUCCESS)
                                {
                                    merchant_json.locations[i].country = country_json.result_object.name;
                                }
                            }
                            if (merchant_json.locations[i].state_id != int.MinValue)
                            {
                                ServerResponse<StateJSON> state_json = await On_view_state(merchant_json.locations[i].state_id.ToString());
                                if (state_json.type == ResponseType.OK_SUCCESS)
                                {
                                    merchant_json.locations[i].state = state_json.result_object.label;
                                }
                            }
                        }
                        
                    }


                    string disponibilidad = "";
                    if (merchant_json.student_data.availability != null && merchant_json.student_data.availability != "")
                    {
                        DateTime dtstart = Convert.ToDateTime(merchant_json.student_data.availability);
                        disponibilidad =  String.Format("{0} de {1} {2}", dtstart.Day.ToString("D2"), Local.txtMonth[dtstart.Month-1], dtstart.Year.ToString("D4"));
                    }
                    merchant_json.availability = disponibilidad;

                    string geolocalizaciones = "";
                    if (Server.i_valorbool(merchant_json.student_data.any_country) == true)
                    {
                        geolocalizaciones = Local.txtProfile32;
                    }
                    else
                    {
                        if (Server.i_valorbool(merchant_json.student_data.any_state) == true)
                        {
                            geolocalizaciones = Local.txtProfile33;
                        }
                        else
                        {
                            if (merchant_json.locations != null)
                            {
                                for (int i = 0; i < merchant_json.locations.Count; ++i)
                                {
                                    if (geolocalizaciones != "") { geolocalizaciones = geolocalizaciones + ", "; }
                                    geolocalizaciones = geolocalizaciones+ String.Format("{0}({1})", merchant_json.locations[i].state, merchant_json.locations[i].country);
                                }
                            }
                            else {
                                geolocalizaciones = Local.txtProfile37;
                            }


                        }
                    }
                    if (geolocalizaciones == "") { geolocalizaciones = Local.txtProfile37; }
                    merchant_json.where = geolocalizaciones;

                    //Sectores y positions???
                    ServerResponse<List<MerchantSectorJSON>> student_sections_location_json = await this.On_job_sections_merchant();
                    if (student_sections_location_json.type == ResponseType.OK_SUCCESS)
                    {
                        merchant_json.sectors = student_sections_location_json.result_object;
                    }

                    string sectores = "";
                    if (Server.i_valorbool(merchant_json.student_data.any_sector) == true)
                    {
                        sectores = Local.txtProfile39;
                    }
                    else
                    {
                        //recorrer sectores
                        if (merchant_json.sectors != null)
                        {
                            for (int i = 0; i < merchant_json.sectors.Count; ++i)
                            {
                                if (sectores != "") { sectores = sectores + ", "; }
                                sectores = sectores + merchant_json.sectors[i].name;
                            }
                        }
                        else
                        {
                            sectores = Local.txtProfile37;
                        }
                    }
                    if (sectores == "") { sectores = Local.txtProfile37; }
                    merchant_json.desc_sectors = sectores;

                    ServerResponse<List<MerchantPositionJSON>> student_possition_json = await this.On_job_positions_merchant();
                    if (student_possition_json.type == ResponseType.OK_SUCCESS)
                    {
                        merchant_json.positions = student_possition_json.result_object;
                    }

                    string puesto = "";
                    if (Server.i_valorbool(merchant_json.student_data.any_position) == true)
                    {
                        puesto = Local.txtProfile38;
                    }
                    else
                    {
                        //recorrer posiciones
                        if (merchant_json.positions != null)
                        {
                            for (int i = 0; i < merchant_json.positions.Count; ++i)
                            {
                                if (puesto != "") { puesto = puesto + ", "; }
                                puesto = puesto + merchant_json.positions[i].name;
                            }
                        }
                        else
                        {
                            puesto = Local.txtProfile37;
                        }
                    }
                    if (puesto == "") { puesto = Local.txtProfile37; }
                    merchant_json.desc_positions = puesto;
                    if (search == "") {
                        merchant_json.hours = "";
                        merchant_json.availability = "";
                        merchant_json.where = "";
                        merchant_json.desc_sectors = "";
                        merchant_json.desc_positions = "";
                        
                    }
                    
                    ServerResponse<MerchantDataJSON> resultado;
                    resultado.type = ResponseType.OK_SUCCESS;
                    resultado.message = "";
                    resultado.result_object = merchant_json;
                    return resultado;
                    
                }
                else
                {
                    ServerResponse<MerchantDataJSON> resultado;
                    resultado.type = ResponseType.SERVICE_ERROR;
                    resultado.message ="Error";
                    resultado.result_object = null;
                    return resultado;

                }
            }
            else
                return this.i_Error_response<MerchantDataJSON>(response);
        }
        public async Task<ServerResponse<List<MerchantJobLocationJSON>>> On_job_location_merchant()
        {
            ServerResponse response;
            response = await i_Access_token_API();
            if (response.type == ResponseType.OK_SUCCESS)
                return await Server.student_merchant_job_location_request(_access_token_API, _user_id);
            else
                return this.i_Error_response<List<MerchantJobLocationJSON>>(response);
        }
        public async Task<ServerResponse<List<MerchantSectorJSON>>> On_job_sections_merchant()
        {
            ServerResponse response;
            response = await i_Access_token_API();
            if (response.type == ResponseType.OK_SUCCESS) {
                ServerResponse<List<MerchantSectorJSON>> sectors_json = await Server.student_merchant_job_sections_request(_access_token_API, _user_id);
                if (sectors_json.type==ResponseType.OK_SUCCESS) {
                    if (sectors_json.result_object.Count>0) {
                        for (int i = 0; i < sectors_json.result_object.Count; ++i)
                        {
                            if (sectors_json.result_object[i].pivot.id != int.MinValue)
                            {
                                ServerResponse<SectorJSON> sector_json = await Server.sector_request(_access_token_API,_user_id,sectors_json.result_object[i].pivot.id);
                                if (sector_json.type == ResponseType.OK_SUCCESS)
                                {
                                    sectors_json.result_object[i].name = sector_json.result_object.name;
                                }
                            }
                            
                        }
                    }
                }
                return sectors_json;
            }
            else
                return this.i_Error_response<List<MerchantSectorJSON>>(response);
        }

        public async Task<ServerResponse<List<MerchantPositionJSON>>> On_job_positions_merchant()
        {
            ServerResponse response;
            response = await i_Access_token_API();
            if (response.type == ResponseType.OK_SUCCESS)
            {
                ServerResponse<List<MerchantPositionJSON>> positions_json = await Server.student_merchant_job_positions_request(_access_token_API, _user_id);
                if (positions_json.type == ResponseType.OK_SUCCESS)
                {
                    if (positions_json.result_object.Count > 0)
                    {
                        for (int i = 0; i < positions_json.result_object.Count; ++i)
                        {
                            if (positions_json.result_object[i].pivot.id != int.MinValue)
                            {
                                ServerResponse<PositionJSON> descrpos_json = await On_position_description(positions_json.result_object[i].pivot.id);
                                if (descrpos_json.type == ResponseType.OK_SUCCESS)
                                {
                                    positions_json.result_object[i].name = descrpos_json.result_object.name;
                                }
                            }

                        }
                    }
                }
                return positions_json;
            }
            else
                return this.i_Error_response<List<MerchantPositionJSON>>(response);
        }

        //-----------------------------------------------------
        public async Task<ServerResponse<InternshipDataJSON>> On_internship_data(int internship_id)
        {
            ServerResponse response;
            response = await i_Access_token_API();
            if (response.type == ResponseType.OK_SUCCESS) {
                ServerResponse<InternshipDataJSON> internships_response =await Server.Internship_data_request(_access_token_API, _user_id, internship_id);
                if (internships_response.type == ResponseType.OK_SUCCESS) {
                    InternshipDataJSON inter = internships_response.result_object;
                    if (inter.country_id != int.MinValue)
                    {
                        ServerResponse<CountryJSON> country_json = await On_view_country(inter.country_id.ToString());
                        if (country_json.type == ResponseType.OK_SUCCESS)
                        {
                            internships_response.result_object.country = country_json.result_object;
                        }
                    }
                }
                return internships_response;
            }
            else
                return this.i_Error_response<InternshipDataJSON>(response);
        }

        //-----------------------------------------------------
        //student_interships_data_request
        public async Task<ServerResponse<List<InternshipDataJSON>>> On_student_internships()
        {

            ServerResponse response;
            response = await i_Access_token_API();
            if (response.type == ResponseType.OK_SUCCESS)
            {
                ServerResponse<List<InternshipDataJSON>> internships_json = await Server.student_interships_data_request(_access_token_API, _user_id);
                if (internships_json.type == ResponseType.OK_SUCCESS)
                {

                    for (int i = 0; i < internships_json.result_object.Count; ++i)
                    {
                        InternshipDataJSON inter = internships_json.result_object[i];
                        
                        
                        if (inter.country_id != int.MinValue)
                        {
                            ServerResponse<CountryJSON> country_json = await On_view_country(inter.country_id.ToString());
                            if (country_json.type == ResponseType.OK_SUCCESS)
                            {
                                internships_json.result_object[i].country= country_json.result_object;
                            }
                        }

                    }
                    return internships_json;
                }
                else
                {
                    ServerResponse<List<InternshipDataJSON>> resultado;
                    resultado.type = internships_json.type;
                    resultado.message = internships_json.message;
                    resultado.result_object = null;
                    return resultado;

                }
            }
            else
                return this.i_Error_response<List<InternshipDataJSON>>(response);
        }

        public async Task<ServerResponse<ExperienceDataJSON>> On_experience_data(int experience_id)
        {
            ServerResponse response;
            response = await i_Access_token_API();
            if (response.type == ResponseType.OK_SUCCESS) {
                ServerResponse<ExperienceDataJSON> response_expe = await Server.Experience_data_request(_access_token_API, _user_id, experience_id);
                if (response_expe.type == ResponseType.OK_SUCCESS) {
                    ExperienceDataJSON expe_ = response_expe.result_object;
                    if (expe_.started_at != null && expe_.started_at != "")
                    {
                        DateTime dtstart = Convert.ToDateTime(expe_.started_at);
                        response_expe.result_object.year_start = dtstart.Year;
                        response_expe.result_object.month_start = dtstart.Month;
                    }

                    if (expe_.finished_at != null && expe_.finished_at != "")
                    {
                        DateTime dtend = Convert.ToDateTime(expe_.finished_at);
                        response_expe.result_object.year_end = dtend.Year;
                        response_expe.result_object.month_end = dtend.Month;
                    }

                    if (expe_.position_id != int.MinValue)
                    {

                        ServerResponse<PositionJSON> descrpos_json = await On_position_description(expe_.position_id);
                        if (descrpos_json.type == ResponseType.OK_SUCCESS)
                        {
                            response_expe.result_object.position = descrpos_json.result_object;
                        }
                    }
                    if (expe_.company_id != int.MinValue)
                    {
                        ServerResponse<CompanyJSON> company_json = await On_company_student_description(expe_.company_id);
                        if (company_json.type == ResponseType.OK_SUCCESS)
                        {
                            response_expe.result_object.company = company_json.result_object;
                        }
                    }
                    if (expe_.state_id != int.MinValue)
                    {
                        ServerResponse<StateJSON> state_json = await On_view_state(expe_.state_id.ToString());
                        if (state_json.type == ResponseType.OK_SUCCESS)
                        {
                            response_expe.result_object.state = state_json.result_object;
                        }
                    }
                }
                return response_expe;

            }
            else
                return this.i_Error_response<ExperienceDataJSON>(response);
        }
        //-----------------------------------------------------
        public async Task<ServerResponse<List<ExperienceDataJSON>>> On_student_experience()
        {

            ServerResponse response;
            response = await i_Access_token_API();
            if (response.type == ResponseType.OK_SUCCESS)
            {
                ServerResponse<List<ExperienceDataJSON>> experiencies_json = await Server.student_experince_data_request(_access_token_API, _user_id);
                if (experiencies_json.type == ResponseType.OK_SUCCESS)
                {

                    for (int i = 0; i < experiencies_json.result_object.Count; ++i)
                    {
                        ExperienceDataJSON expe_ = experiencies_json.result_object[i];
                        if (expe_.started_at != null && expe_.started_at != "")
                        {
                            DateTime dtstart = Convert.ToDateTime(expe_.started_at);
                            experiencies_json.result_object[i].year_start = dtstart.Year;
                            experiencies_json.result_object[i].month_start = dtstart.Month;
                        }

                        if (expe_.finished_at != null && expe_.finished_at != "")
                        {
                            DateTime dtend = Convert.ToDateTime(expe_.finished_at);
                            experiencies_json.result_object[i].year_end = dtend.Year;
                            experiencies_json.result_object[i].month_end = dtend.Month;
                        }

                        if (expe_.position_id != int.MinValue)
                        {

                            ServerResponse<PositionJSON> descrpos_json = await On_position_description(expe_.position_id);
                            if (descrpos_json.type == ResponseType.OK_SUCCESS)
                            {
                                experiencies_json.result_object[i].position = descrpos_json.result_object;
                            }
                        }
                        if (expe_.company_id !=int.MinValue)
                        {
                            ServerResponse<CompanyJSON> company_json = await On_company_student_description(expe_.company_id);
                            if (company_json.type == ResponseType.OK_SUCCESS)
                            {
                                experiencies_json.result_object[i].company = company_json.result_object;
                            }
                        }
                        if (expe_.state_id != int.MinValue)
                        {
                            ServerResponse<StateJSON> state_json = await On_view_state(expe_.state_id.ToString());
                            if (state_json.type == ResponseType.OK_SUCCESS)
                            {
                                experiencies_json.result_object[i].state = state_json.result_object;
                            }
                        }
                        
                    }
                    return experiencies_json;
                }
                else
                {
                    ServerResponse<List<ExperienceDataJSON>> resultado;
                    resultado.type = experiencies_json.type;
                    resultado.message = experiencies_json.message;
                    resultado.result_object = null;
                    return resultado;

                }
            }
            else
                return this.i_Error_response<List<ExperienceDataJSON>>(response);
        }

        public async Task<ServerResponse<PositionJSON>> On_position_description(int position_id)
        {
            ServerResponse response;
            response = await i_Access_token_API();
            if (response.type == ResponseType.OK_SUCCESS)
                return await Server.position_student_request(_access_token_API, position_id);
            else
                return this.i_Error_response<PositionJSON>(response);
        }

        

        //-----------------------------------------------------
        public async Task<ServerResponse<List<PositionJSON>>> On_positions(string search_text)
        {
            ServerResponse response;
            response = await i_Access_token_API();
            if (response.type == ResponseType.OK_SUCCESS)
                return await Server.Positions_request(_access_token_API, _user_id, search_text);
            else
                return this.i_Error_response<List<PositionJSON>>(response);
        }
        
        
        
        //-----------------------------------------------------

        public async Task<ServerResponse<List<SectorJSON>>> On_sectors(string search_text)
        {
            ServerResponse response;
            response = await i_Access_token_API();
            if (response.type == ResponseType.OK_SUCCESS)
                return await Server.sectors_request(_access_token_API, _user_id, search_text);
            else
                return this.i_Error_response<List<SectorJSON>>(response);
        }

        //-----------------------------------------------------
        public async Task<ServerResponse<AcademicDataJSON>> On_academic_data(long academic_id)
        {
            ServerResponse response;
            response = await i_Access_token_API();
            if (response.type == ResponseType.OK_SUCCESS) {
                ServerResponse<AcademicDataJSON> academic_response= await Server.Academic_data_request(_access_token_API, _user_id, academic_id);
                if (academic_response.type == ResponseType.OK_SUCCESS)
                {
                    AcademicDataJSON academic_ = academic_response.result_object;
                    if (academic_.started_at != null && academic_.started_at != "")
                    {
                        DateTime dtstart = Convert.ToDateTime(academic_.started_at);
                        academic_response.result_object.year_start = dtstart.Year;
                        academic_response.result_object.month_start = dtstart.Month;
                    }

                    if (academic_.finished_at != null && academic_.finished_at != "")
                    {
                        DateTime dtend = Convert.ToDateTime(academic_.finished_at);
                        academic_response.result_object.year_end = dtend.Year;
                        academic_response.result_object.month_end = dtend.Month;
                    }


                    if (academic_.study_id != null && academic_.study_id != "")
                    {

                        ServerResponse<UniversityJSON> descruni_json = await On_university_description(academic_.university_id);
                        if (descruni_json.type == ResponseType.OK_SUCCESS)
                        {
                            academic_response.result_object.university = descruni_json.result_object;
                        }
                    }
                    if (academic_.study_id != null && academic_.study_id != "")
                    {
                        ServerResponse<AcademicStudyJSON> study_json = await On_study_description(academic_.study_id);
                        if (study_json.type == ResponseType.OK_SUCCESS)
                        {
                            academic_response.result_object.study = study_json.result_object;
                        }
                    }

                }
                return academic_response;
            }
            else
                return this.i_Error_response<AcademicDataJSON>(response);
        }


        //-----------------------------------------------------
        public async Task<ServerResponse<List<AcademicDataJSON>>> On_student_academics()
        {
            
            ServerResponse response;
            response = await i_Access_token_API();
            if (response.type == ResponseType.OK_SUCCESS)
            {
                ServerResponse<List<AcademicDataJSON>> academics_json = await Server.student_academic_data_request(_access_token_API, _user_id);
                if (academics_json.type == ResponseType.OK_SUCCESS)
                {
                    
                    for (int i = 0; i < academics_json.result_object.Count; ++i)
                    {
                        AcademicDataJSON academic_ = academics_json.result_object[i];
                        if (academic_.started_at != null && academic_.started_at != "")
                        {
                            DateTime dtstart = Convert.ToDateTime(academic_.started_at);
                            academics_json.result_object[i].year_start = dtstart.Year;
                            academics_json.result_object[i].month_start = dtstart.Month;
                        }
                         
                        if (academic_.finished_at != null && academic_.finished_at != "") {
                            DateTime dtend = Convert.ToDateTime(academic_.finished_at);
                            academics_json.result_object[i].year_end = dtend.Year;
                            academics_json.result_object[i].month_end = dtend.Month;
                        }
                            

                        if (academic_.university_id != null && academic_.university_id != "")
                        {
                            
                            ServerResponse<UniversityJSON> descruni_json = await On_university_description(academic_.university_id);
                            if (descruni_json.type == ResponseType.OK_SUCCESS)
                            {
                                academics_json.result_object[i].university = descruni_json.result_object;
                            }
                        }
                        if (academic_.study_id != null && academic_.study_id != "")
                        {
                            ServerResponse<AcademicStudyJSON> study_json = await On_study_description(academic_.study_id);
                            if (study_json.type == ResponseType.OK_SUCCESS)
                            {
                                academics_json.result_object[i].study = study_json.result_object;

                                
                            }
                        }

                    }
                    return academics_json;
                }
                else
                {
                    ServerResponse<List<AcademicDataJSON>> resultado;
                    resultado.type = academics_json.type;
                    resultado.message = academics_json.message;
                    resultado.result_object = null;
                    return resultado;

                }
            }
            else
                return this.i_Error_response<List<AcademicDataJSON>>(response);
        }


        public async Task<ServerResponse<UniversityJSON>> On_university_description(string university_id)
        {
            ServerResponse response;
            response = await i_Access_token_API();
            if (response.type == ResponseType.OK_SUCCESS) {
                ServerResponse<UniversityJSON> university_json = await Server.university_request(_access_token_API, university_id);
                if (response.type == ResponseType.OK_SUCCESS)
                {
                    if (university_json.result_object.country_id != int.MinValue)
                    {
                        ServerResponse<CountryJSON> country_json = await On_view_country(university_json.result_object.country_id.ToString());
                        if (country_json.type == ResponseType.OK_SUCCESS)
                        {
                            university_json.result_object.country = country_json.result_object;
                        }
                    }
                    if (university_json.result_object.state_id != int.MinValue)
                    {
                        ServerResponse<StateJSON> state_json = await On_view_state(university_json.result_object.state_id.ToString());
                        if (state_json.type == ResponseType.OK_SUCCESS)
                        {
                            university_json.result_object.state = state_json.result_object;
                        }
                    }
                }
                return university_json;
            }
            else
                return this.i_Error_response<UniversityJSON>(response);
        }

        public async Task<ServerResponse<AcademicStudyJSON>> On_study_description(string study_id)
        {
            ServerResponse response;
            response = await i_Access_token_API();
            if (response.type == ResponseType.OK_SUCCESS) {
                ServerResponse<AcademicStudyJSON> study_json = await Server.study_request(_access_token_API, study_id);
                if (study_json.type == ResponseType.OK_SUCCESS)
                {
                    if (study_json.result_object.study_level_id != "" && study_json.result_object.study_level_id != null)
                    {
                        ServerResponse<LevelJSON> level_json = await Server.Level_study_request(_access_token_API, study_json.result_object.study_level_id);
                        if (level_json.type == ResponseType.OK_SUCCESS)
                        {
                            study_json.result_object.study_level_name = level_json.result_object.name;
                        }
                    }
                    //else {
                    //    if (study_json.result_object.academic_level != "" && study_json.result_object.academic_level != null) {
                    //        ServerResponse<List<LevelJSON>> level2_json = await Server.Levels_study_request(_access_token_API,_user_id, study_json.result_object.academic_level);
                    //        if (level2_json.type == ResponseType.OK_SUCCESS)
                    //        {
                    //            if (level2_json.result_object.Count > 0) {
                    //                study_json.result_object.study_level_name = level2_json.result_object[0].name;
                    //                study_json.result_object.study_level_id = level2_json.result_object[0].id.ToString();
                    //            }
                                
                    //        }
                    //    }
                    //}
                    
                }
                return study_json;
            }
            
            else
                return this.i_Error_response<AcademicStudyJSON>(response);
        }

        

        //-----------------------------------------------------
        public async Task<ServerResponse<List<LanguageDataJSON>>> On_student_Languages() {
            ServerResponse response;
            response = await i_Access_token_API();
            if (response.type == ResponseType.OK_SUCCESS) {
                ServerResponse<List<LanguageDataJSON>> languages_json = await Server.student_Languages_data_request(_access_token_API, _user_id);
                if (languages_json.type == ResponseType.OK_SUCCESS)
                {
                    for (int i = 0; i < languages_json.result_object.Count; ++i)
                    {
                        LanguageDataJSON langl_ = languages_json.result_object[i];
                        if (langl_.language_id != null && langl_.language_id != "")
                        {
                            ServerResponse<LanguageDescriptionJSON> descrlanguage_json = await On_language_data_description(langl_.language_id);
                            if (descrlanguage_json.type == ResponseType.OK_SUCCESS)
                            {
                                languages_json.result_object[i].language = descrlanguage_json.result_object;
                            }
                        }
                        if (langl_.language_level_id != null && langl_.language_level_id != "")
                        {
                            ServerResponse<LanguageLevelJSON> level_json = await On_language_data_level(langl_.language_level_id);
                            if (level_json.type == ResponseType.OK_SUCCESS)
                            {
                                
                                languages_json.result_object[i].level = level_json.result_object;
                            }
                        }
                        if (langl_.certificate_id != null && langl_.certificate_id != "")
                        {
                            ServerResponse<LanguageCertificateJSON> certificate_json = await On_language_data_certificate(langl_.certificate_id);
                            if (certificate_json.type == ResponseType.OK_SUCCESS)
                            {
                                languages_json.result_object[i].certificate = certificate_json.result_object;
                            }
                        }
                    }

                    
                    return languages_json;

                    
                }
                else {
                    ServerResponse<List<LanguageDataJSON>> resultado;
                    resultado.type = languages_json.type;
                    resultado.message = languages_json.message;
                    resultado.result_object = null;
                    return resultado;

                }
            }
            else
                return this.i_Error_response<List<LanguageDataJSON>> (response);
        }

        public async Task<ServerResponse<LanguageJSON>> On_language_data(string language_id)
        {
            ServerResponse response;
            response = await i_Access_token_API();
            if (response.type == ResponseType.OK_SUCCESS) {
                ServerResponse<LanguageJSON> language_json =  await Server.Language_data_request(_access_token_API, _user_id, language_id);
                if (language_json.type == ResponseType.OK_SUCCESS)
                {
                    if (language_json.result_object.data.language_id != null && language_json.result_object.data.language_id != "")
                    {
                        ServerResponse<LanguageDescriptionJSON> descrlanguage_json = await On_language_data_description(language_json.result_object.data.language_id);
                        if (descrlanguage_json.type == ResponseType.OK_SUCCESS)
                        {
                            language_json.result_object.data.language = descrlanguage_json.result_object;
                        }
                    }
                    if (language_json.result_object.data.language_level_id != null && language_json.result_object.data.language_level_id != "")
                    {
                        ServerResponse<LanguageLevelJSON> level_json = await On_language_data_level(language_json.result_object.data.language_level_id);
                        if (level_json.type == ResponseType.OK_SUCCESS)
                        {
                            language_json.result_object.data.level = level_json.result_object;
                        }
                    }
                    if (language_json.result_object.data.certificate_id != null && language_json.result_object.data.certificate_id != "")
                    {
                        ServerResponse<LanguageCertificateJSON> certificate_json = await On_language_data_certificate(language_json.result_object.data.certificate_id);
                        if (certificate_json.type == ResponseType.OK_SUCCESS)
                        {
                            language_json.result_object.data.certificate = certificate_json.result_object;
                        }
                    }
                }
                return language_json;
            }
            else
                return this.i_Error_response<LanguageJSON>(response);
        }


        public async Task<ServerResponse<LanguageDescriptionJSON>> On_language_data_description(string language_id)
        {
            ServerResponse response;
            response = await i_Access_token_API();
            if (response.type == ResponseType.OK_SUCCESS)
                return await Server.language_request(_access_token_API, language_id);
            else
                return this.i_Error_response<LanguageDescriptionJSON>(response);
        }

        public async Task<ServerResponse<LanguageLevelJSON>> On_language_data_level(string level_id)
        {
            ServerResponse response;
            response = await i_Access_token_API();
            if (response.type == ResponseType.OK_SUCCESS)
                return await Server.level_language_request(_access_token_API, level_id);
            else
                return this.i_Error_response<LanguageLevelJSON>(response);
        }
        public async Task<ServerResponse<LanguageCertificateJSON>> On_language_data_certificate(string certificate_id)
        {
            ServerResponse response;
            response = await i_Access_token_API();
            if (response.type == ResponseType.OK_SUCCESS)
                return await Server.certificate_language_request(_access_token_API, certificate_id);
            else
                return this.i_Error_response<LanguageCertificateJSON>(response);
        }
        //-----------------------------------------------------

        public async Task<ServerResponse<SaveDataJSON>> On_language_data_save(LanguageDataJSON language)
        {
            ServerResponse response;
            response = await i_Access_token_API();
            if (response.type == ResponseType.OK_SUCCESS)
                return await Server.Language_data_save(_access_token_API, _user_id, language);
            else
                return this.i_Error_response<SaveDataJSON>(response);
        }

        //-----------------------------------------------------

        public async Task<ServerResponse<SaveDataJSON>> On_language_data_delete(string language_id)
        {
            ServerResponse response;
            response = await i_Access_token_API();
            if (response.type == ResponseType.OK_SUCCESS)
                return await Server.Language_data_delete(_access_token_API, _user_id, language_id);
            else
                return this.i_Error_response<SaveDataJSON>(response);
        }

        //-----------------------------------------------------

        public async Task<ServerResponse<List<LanguageItemJSON>>> On_language_items()
        {
            ServerResponse response;
            response = await i_Access_token_API();
            if (response.type == ResponseType.OK_SUCCESS)
                return await Server.Language_items_request(_access_token_API);
            else
                return this.i_Error_response<List<LanguageItemJSON>>(response);
        }

        //-----------------------------------------------------

        public async Task<ServerResponse<List<LevelJSON>>> On_levels()
        {
            ServerResponse response;
            response = await i_Access_token_API();
            if (response.type == ResponseType.OK_SUCCESS)
                return await Server.Levels_request(_access_token_API, _user_id);
            else
                return this.i_Error_response<List<LevelJSON>>(response);
        }

        public async Task<ServerResponse<List<LevelJSON>>> On_levels_study()
        {
            ServerResponse response;
            response = await i_Access_token_API();
            if (response.type == ResponseType.OK_SUCCESS)
                return await Server.Levels_study_request(_access_token_API, _user_id,null);
            else
                return this.i_Error_response<List<LevelJSON>>(response);
        }
        //-----------------------------------------------------

        public async Task<ServerResponse<List<CertificateJSON>>> On_certificates(int language_id, string search_text)
        {
            ServerResponse response;
            response = await i_Access_token_API();
            if (response.type == ResponseType.OK_SUCCESS)
                return await Server.Certificates_request(_access_token_API, _user_id, language_id, search_text);
            else
                return this.i_Error_response<List<CertificateJSON>>(response);
        }

        //-----------------------------------------------------

        

        public async Task<ServerResponse<SaveDataJSON>> On_internship_data_save(InternshipDataJSON internship)
        {
            ServerResponse response;
            response = await i_Access_token_API();
            if (response.type == ResponseType.OK_SUCCESS)
                return await Server.Internship_data_save(_access_token_API, _user_id, internship);
            else
                return this.i_Error_response<SaveDataJSON>(response);
        }

        //-----------------------------------------------------

        public async Task<ServerResponse<SaveDataJSON>> On_internship_data_delete(long internship_id)
        {
            ServerResponse response;
            response = await i_Access_token_API();
            if (response.type == ResponseType.OK_SUCCESS)
                return await Server.Internship_data_delete(_access_token_API, _user_id, internship_id);
            else
                return this.i_Error_response<SaveDataJSON>(response);
        }

               

        //-----------------------------------------------------

        public async Task<ServerResponse<SaveDataJSON>> On_experience_data_save(ExperienceDataJSON experience)
        {
            ServerResponse response;
            response = await i_Access_token_API();
            if (response.type == ResponseType.OK_SUCCESS)
                return await Server.Experience_data_save(_access_token_API, _user_id, experience);
            else
                return this.i_Error_response<SaveDataJSON>(response);
        }

        //-----------------------------------------------------

        public async Task<ServerResponse<SaveDataJSON>> On_experience_data_delete(long experience_id)
        {
            ServerResponse response;
            response = await i_Access_token_API();
            if (response.type == ResponseType.OK_SUCCESS)
                return await Server.Experience_data_delete(_access_token_API, _user_id, experience_id);
            else
                return this.i_Error_response<SaveDataJSON>(response);
        }

        //-----------------------------------------------------

        
        

        public async Task<ServerResponse<SaveDataJSON>> On_voluntary_data_save(VoluntaryDataJSON voluntary)
        {
            ServerResponse response;
            response = await i_Access_token_API();
            if (response.type == ResponseType.OK_SUCCESS)
                return await Server.Voluntary_data_save(_access_token_API, _user_id, voluntary);
            else
                return this.i_Error_response<SaveDataJSON>(response);
        }

        //-----------------------------------------------------

        public async Task<ServerResponse<SaveDataJSON>> On_voluntary_data_delete(long voluntary_id)
        {
            ServerResponse response;
            response = await i_Access_token_API();
            if (response.type == ResponseType.OK_SUCCESS)
                return await Server.Voluntary_data_delete(_access_token_API, _user_id, voluntary_id);
            else
                return this.i_Error_response<SaveDataJSON>(response);
        }

        //-----------------------------------------------------

        //public async Task<ServerResponse<OtherDataJSON>> On_other_data()
        //{
        //    ServerResponse response;
        //    response = await i_Access_token_API();
        //    if (response.type == ResponseType.OK_SUCCESS)
        //        return await Server.Other_data_request(_access_token, _user_id);
        //    else
        //        return this.i_Error_response<OtherDataJSON>(response);
        //}

        //-----------------------------------------------------

        public async Task<ServerResponse<SaveDataJSON>> On_other_data_save(StudentDataJSON other)
        {
            ServerResponse response;
            response = await i_Access_token_API();
            if (response.type == ResponseType.OK_SUCCESS)
                return await Server.Other_data_save(_access_token_API, _user_id, other);
            else
                return this.i_Error_response<SaveDataJSON>(response);
        }

        //-----------------------------------------------------
        public async Task<ServerResponse<CompanyJSON>> On_company_description(int company_id)
        {
            ServerResponse response;
            response = await i_Access_token_API();
            if (response.type == ResponseType.OK_SUCCESS) {
                ServerResponse<CompanyJSON>  companies_json = await Server.company_request(_access_token_API, company_id);
                if (companies_json.result_object.country_id != null && companies_json.result_object.country_id != "")
                {
                    ServerResponse<CountryJSON> companies_country_json = await On_view_country(companies_json.result_object.country_id);
                    if (companies_country_json.type == ResponseType.OK_SUCCESS)
                    {
                        companies_json.result_object.country = companies_country_json.result_object;
                    }
                }
                if (companies_json.result_object.state_id != null && companies_json.result_object.state_id != "")
                {
                    ServerResponse<StateJSON> companies_state_json = await On_view_state(companies_json.result_object.state_id);
                    if (companies_state_json.type == ResponseType.OK_SUCCESS)
                    {
                        companies_json.result_object.state = companies_state_json.result_object;
                    }
                }
                if (companies_json.result_object.city_id != null && companies_json.result_object.city_id != "")
                {
                    ServerResponse<CityJSON> companies_city_json = await On_view_city(companies_json.result_object.city_id);
                    if (companies_city_json.type == ResponseType.OK_SUCCESS)
                    {
                        companies_json.result_object.city = companies_city_json.result_object;
                    }
                }
                ServerResponse<List<CompanySectorJSON>> companies_sectors_json = await On_company_list_sectors(companies_json.result_object.id.ToString());
                if (companies_sectors_json.type == ResponseType.OK_SUCCESS)
                {
                    companies_json.result_object.sectors = companies_sectors_json.result_object;
                }
                ServerResponse<List<CompanySocialJSON>> companies_social_json = await On_company_list_socials(companies_json.result_object.id.ToString());
                if (companies_social_json.type == ResponseType.OK_SUCCESS)
                {
                    companies_json.result_object.social_sites = companies_social_json.result_object;
                }
                return companies_json;
            }
                
            else
                return this.i_Error_response<CompanyJSON>(response);
        }


        public async Task<ServerResponse<CompanyJSON>> On_company_student_description(int company_id)
        {
            ServerResponse response;
            response = await i_Access_token_API();
            if (response.type == ResponseType.OK_SUCCESS)
            {
                ServerResponse<CompanyJSON> companies_json = await Server.company_student_request(_access_token_API, company_id);
                return companies_json;
            }

            else
                return this.i_Error_response<CompanyJSON>(response);
        }

        public async Task<ServerResponse<CompaniesJSON>> On_company_list(int sector_id, int page, string search_text)
        {
            ServerResponse response;
            response = await i_Access_token_API();
            if (response.type == ResponseType.OK_SUCCESS) {
                ServerResponse<CompaniesJSON> companies_json= await Server.Companies_request(_access_token_API, _user_id, sector_id, page, search_text);
                if (companies_json.type == ResponseType.OK_SUCCESS) {
                    for (int j = 0; j < companies_json.result_object.items.Count; ++j)
                    {
                        //if (companies_json.result_object.items[j].country_id != null && companies_json.result_object.items[j].country_id != "")
                        //{
                        //    ServerResponse<CountryJSON> companies_country_json = await On_view_country(companies_json.result_object.items[j].country_id);
                        //    if (companies_country_json.type == ResponseType.OK_SUCCESS)
                        //    {
                        //        companies_json.result_object.items[j].country = companies_country_json.result_object;
                        //    }
                        //}
                        //if (companies_json.result_object.items[j].state_id != null && companies_json.result_object.items[j].state_id != "")
                        //{
                        //    ServerResponse<StateJSON> companies_state_json = await On_view_state(companies_json.result_object.items[j].state_id);
                        //    if (companies_state_json.type == ResponseType.OK_SUCCESS)
                        //    {
                        //        companies_json.result_object.items[j].state = companies_state_json.result_object;
                        //    }
                        //}
                        if (companies_json.result_object.items[j].city_id != null && companies_json.result_object.items[j].city_id != "")
                        {
                            ServerResponse<CityJSON> companies_city_json = await On_view_city(companies_json.result_object.items[j].city_id);
                            if (companies_city_json.type == ResponseType.OK_SUCCESS)
                            {
                                companies_json.result_object.items[j].city = companies_city_json.result_object;
                            }
                        }
                        ServerResponse<List<CompanySectorJSON>> companies_sectors_json = await On_company_list_sectors(companies_json.result_object.items[j].orderid.ToString());
                        if (companies_sectors_json.type == ResponseType.OK_SUCCESS)
                        {
                            companies_json.result_object.items[j].sectors = companies_sectors_json.result_object;
                        }
                        //ServerResponse<List<CompanySocialJSON>> companies_social_json = await On_company_list_socials(companies_json.result_object.items[j].orderid.ToString());
                        //if (companies_social_json.type == ResponseType.OK_SUCCESS)
                        //{
                        //    companies_json.result_object.items[j].social_sites = companies_social_json.result_object;
                        //}
                    }
                }
                return companies_json;
            }
            else
                return this.i_Error_response<CompaniesJSON>(response);
        }

        //-----------------------------------------------------
        public async Task<ServerResponse<CountryJSON>> On_view_country(string country_id)
        {
            ServerResponse response;
            response = await i_Access_token_API();
            if (response.type == ResponseType.OK_SUCCESS)
                return await Server.Country_request(_access_token_API, country_id);
            else
                return this.i_Error_response<CountryJSON>(response);
        }
        //-----------------------------------------------------
        public async Task<ServerResponse<StateJSON>> On_view_state(string state_id)
        {
            ServerResponse response;
            response = await i_Access_token_API();
            if (response.type == ResponseType.OK_SUCCESS)
                return await Server.State_request(_access_token_API, state_id);
            else
                return this.i_Error_response<StateJSON>(response);
        }
        //-----------------------------------------------------
        public async Task<ServerResponse<CityJSON>> On_view_city(string city_id)
        {
            ServerResponse response;
            response = await i_Access_token_API();
            if (response.type == ResponseType.OK_SUCCESS)
                return await Server.City_request(_access_token_API, city_id);
            else
                return this.i_Error_response<CityJSON>(response);
        }
        //On_company_list_socials
        public async Task<ServerResponse<List<CompanySocialJSON>>> On_company_list_socials(string company_id)
        {
            ServerResponse response;
            response = await i_Access_token_API();
            if (response.type == ResponseType.OK_SUCCESS)
                return await Server.company_social_request(_access_token_API, company_id);
            else
                return this.i_Error_response<List<CompanySocialJSON>>(response);
        }
        //-----------------------------------------------------
        public async Task<ServerResponse<List<CompanySectorJSON>>> On_company_list_sectors(string company_id)
        {
            ServerResponse response;
            response = await i_Access_token_API();
            if (response.type == ResponseType.OK_SUCCESS)
                return await Server.company_sectors_request(_access_token_API, company_id);
            else
                return this.i_Error_response<List<CompanySectorJSON>>(response);
        }
        //-----------------------------------------------------
        //List<CompanySectorJSON>

        //public async Task<ServerResponse<CompanyDetailJSON>> On_company_detail(int company_id)
        //{
        //    ServerResponse response;
        //    response = await i_Access_token();
        //    if (response.type == ResponseType.OK_SUCCESS)
        //        return await Server.Company_detail_request(_access_token, _user_id, company_id);
        //    else
        //        return this.i_Error_response<CompanyDetailJSON>(response);
        //}

        //-----------------------------------------------------

        public async Task<ServerResponse<SaveDataJSON>> On_settings_save(bool public_profile, bool newsletter, bool notify, string old_password, string new_password)
        {
            ServerResponse response;
            response = await i_Access_token_API();
            if (response.type == ResponseType.OK_SUCCESS)
            {
                ServerResponse<SaveDataJSON> save_response = await Server.Settings_save(_access_token_API, _user_id, public_profile, newsletter, old_password, new_password,notify);
                if (save_response.type == ResponseType.OK_SUCCESS)
                {
                    this._user_public_profile = public_profile;
                    this._user_newsletter = newsletter;
                    if (_user_notify != notify) {
                        _user_notify = notify;
                        if (this._app._platform == App.Platform.Android)
                        {
                            List<string> data = new List<string>();
                            data.Add(this._email);
                            if (new_password != null)
                            {
                                data.Add(new_password);
                            }
                            else
                            {
                                data.Add(this._password);
                            }
                            data.Add(_user_id.ToString());
                            data.Add(Server.device_id);
                            if (_user_notify)
                            {
                                data.Add("S");
                            }
                            else
                            {
                                data.Add("N");
                            }
                            DependencyService.Get<IFileService>().Save_data(data);
                        }
                        else {
                            Application.Current.Properties["email"] = this._email;
                            if (new_password != null)
                            {
                                Application.Current.Properties["password"] = new_password;
                            }
                            else
                            {
                                Application.Current.Properties["password"] = this._password;
                            }
                            
                            Application.Current.Properties["user_id"] = _user_id.ToString();
                            Application.Current.Properties["device_id"] = Server.device_id;
                            if (_user_notify)
                            {
                                Application.Current.Properties["notify"] = "S";
                            }
                            else
                            {
                                Application.Current.Properties["notify"] = "N";
                            }
                            await Application.Current.SavePropertiesAsync();
                        }
                        //Si es distinto el nuevo valor con respecto al anterios Parar o Iniciar Servicio-Segun nueva opcion

                        if (_user_notify)
                        {
                            DependencyService.Get<INotificaciones>().iniciaServicio();
                        }
                        else
                        {
                            DependencyService.Get<INotificaciones>().paraServicio();
                        }



                    }


                }

                return save_response;
            }
            else
            {
                return this.i_Error_response<SaveDataJSON>(response);
            }
        }

        //-----------------------------------------------------
        public bool Control_Carga(Page page) {
            if (this._blncargando == true)
            {
                page.DisplayAlert(null, Local.txtCommon07, Local.txtAlertOk);
                return true;
            }
            else {
                return false;
            }
        }

        public void On_user_current_page_change(UserCurrentPage new_user_current_page, bool launch_personal_page)
        {
            //if (this._user_current_page != new_user_current_page)
            {
                NavigationPage navigation;
                this._user_current_page = new_user_current_page;
                navigation = this.i_User_navigation_page(launch_personal_page);
                this._user_page.Change_detail_page(navigation);
            }
        
            this._user_page.IsPresented = false;
        }

        //-----------------------------------------------------

        private int i_Job_offer_status_id()
        {
            switch (this._user_current_filter)
            {
                case UserCurrentFilter.kAll:
                    return 0;
                case UserCurrentFilter.kNews:
                    return 1;
                case UserCurrentFilter.kRejected:
                    return 2;
                case UserCurrentFilter.kAccepted:
                    return 3;
                case UserCurrentFilter.kDiscarted:
                    return 8;
                default:
                    C2Assert.Error(false);
                    return 0;
            }
        }

        //-----------------------------------------------------

        public void On_filter_button()
        {
            switch (this._user_current_view)
            {
                case UserCurrentView.kOffers:
                    this._user_current_view = UserCurrentView.kFilters;
                    break;
                case UserCurrentView.kFilters:
                    this._user_current_view = UserCurrentView.kOffers;
                    break;
            }
        }

        //-----------------------------------------------------

        public UserCurrentPage User_current_page
        {
            get { return this._user_current_page; }
            set { this._user_current_page = value; }
        }

        //-----------------------------------------------------

        public UserCurrentView User_current_view
        {
            get { return this._user_current_view; }
        }

        //-----------------------------------------------------

        public UserCurrentFilter User_current_filter
        {
            set { this._user_current_filter = value; }
            get { return this._user_current_filter; }
        }

        //-----------------------------------------------------

        public async Task<bool> On_close_session()
        {
            this.i_Persistent_data_default();
            this._user_page = null;

            if (this._app._platform == App.Platform.Android)
            {
                List<string> data = new List<string>();
                data.Add("");
                data.Add("");
                DependencyService.Get<IFileService>().Save_data(data);
            }
            else
            {
                Application.Current.Properties["email"] = "";
                Application.Current.Properties["password"] = "";
                await Task.Delay(1000);
            }

            this._app.MainPage = this.i_Login_page(this._email,this._password);
            return true;
        }

        //-----------------------------------------------------

        public void GA_track_screen(String screen_name)
        {
            IGAService service = DependencyService.Get<IGAService>();
            service.Track_screen(screen_name);
        }
    }
}