﻿using System;

namespace C2Core
{
    public abstract class C2Disposable : IDisposable
    {
        private bool _disposed;

        //-----------------------------------------------------

        public C2Disposable()
        {
            this._disposed = false;
        }

        //-----------------------------------------------------

        ~C2Disposable()
        {
            this.Dispose(false);
        }

        //-----------------------------------------------------

        protected abstract void Liberate_resources(bool disposing);

        //-----------------------------------------------------

        public void Dispose()
        {
            this.Dispose(true);
            GC.SuppressFinalize(this);
        }

        //-----------------------------------------------------

        protected virtual void Dispose(bool disposing)
        {
            C2AssertDev.Error(this._disposed == false);

            this.Liberate_resources(disposing);
            this._disposed = true;
        }

        //-----------------------------------------------------

        public bool Disposed
        {
            get
            {
                return this._disposed;
            }
        }

        //-----------------------------------------------------

        public static void Validate(C2Disposable obj)
        {
            C2Assert.Error(obj != null && obj._disposed == false);
        }

        //-----------------------------------------------------


    }
}
