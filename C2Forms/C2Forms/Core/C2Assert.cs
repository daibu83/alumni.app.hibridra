﻿using System;

namespace C2Core
{
    public static class C2Assert
    {
        public delegate void FPtr_action_on_assert();
        private static FPtr_action_on_assert _ACTION_ON_ASSERT = null;
        private static bool _EXCEPTION_ON_ASSERT = false;
        private static string _APPLICATION_IDENTIFIER = null;

        //-----------------------------------------------------

        public static void Set_application_identifier(string application_identifier)
        {
            C2Assert._APPLICATION_IDENTIFIER = application_identifier;
        }

        //-----------------------------------------------------

        public static void Exception_on_assert(bool enable)
        {
            if (C2Assert._EXCEPTION_ON_ASSERT != enable)
            {
                C2Assert._EXCEPTION_ON_ASSERT = enable;
            }
            else
            {
                C2Assert._EXCEPTION_ON_ASSERT = false;
                C2Assert.i_Manage_error(null);
            }
        }

        //-----------------------------------------------------

        public static void Error(bool boolean_condition)
        {
            if (boolean_condition == false)
                C2Assert.i_Manage_error(null);
        }

        //-----------------------------------------------------

        public static void No_null(object object_that_should_be_not_null)
        {
            if (object_that_should_be_not_null == null)
                C2Assert.i_Manage_error(null);
        }

        //-----------------------------------------------------

        public static void Is_null(object object_that_should_be_null)
        {
            if (object_that_should_be_null != null)
                C2Assert.i_Manage_error(null);
        }
    
        //-----------------------------------------------------

        public static void Default()
        {
            C2Assert.i_Manage_error(null);
        }

        //-----------------------------------------------------

        public static void Error_message(bool bolean_condition, string optional_message)
        {
            if (bolean_condition == false)
                C2Assert.i_Manage_error(optional_message);
        }

        //-----------------------------------------------------

        public static void Set_action_on_assert(C2Assert.FPtr_action_on_assert func_action_on_assert)
        {
            C2Assert._ACTION_ON_ASSERT = func_action_on_assert;
        }

        //-----------------------------------------------------

        private static void i_Action_on_assert()
        {
            if (C2Assert._ACTION_ON_ASSERT != null)
            {
                FPtr_action_on_assert current_action_on_assert;

                current_action_on_assert = C2Assert._ACTION_ON_ASSERT;
                C2Assert._ACTION_ON_ASSERT = null;

                try
                {
                    current_action_on_assert();
                }
                catch (Exception)
                {
                }

                C2Assert._ACTION_ON_ASSERT = current_action_on_assert;
            }
        }

        //-----------------------------------------------------

        private static void i_Manage_error(string optional_message)
        {
            C2Assert.i_Action_on_assert();

            if (C2Assert._EXCEPTION_ON_ASSERT == true)
            {
                if (optional_message != null)
                    throw new Exception(optional_message);
                else
                    throw new Exception();
            }
            else
            {
            }
        }

        //-----------------------------------------------------


    }
}
