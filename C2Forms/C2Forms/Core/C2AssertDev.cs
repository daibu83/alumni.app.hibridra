﻿using System.Diagnostics;

namespace C2Core
{
    public static class C2AssertDev
    {
        //-----------------------------------------------------
        [Conditional("DEBUG")]
        public static void Error(bool boolean_condition)
        {
            C2Assert.Error(boolean_condition);
        }

        //-----------------------------------------------------
        [Conditional("DEBUG")]
        public static void No_null(object object_that_should_be_not_null)
        {
            C2Assert.No_null(object_that_should_be_not_null);
        }

        //-----------------------------------------------------
        [Conditional("DEBUG")]
        public static void Is_null(object object_that_should_be_null)
        {
            C2Assert.Is_null(object_that_should_be_null);
        }

        //-----------------------------------------------------
        [Conditional("DEBUG")]
        public static void Default()
        {
            C2Assert.Default();
        }

        //-----------------------------------------------------
        [Conditional("DEBUG")]
        public static void Error_message(bool bolean_condition, string optional_message)
        {
            C2Assert.Error_message(bolean_condition, optional_message);
        }

        //-----------------------------------------------------


    }
}
