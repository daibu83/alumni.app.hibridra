﻿using System.Threading.Tasks;
using Xamarin.Forms;

namespace C2Forms
{
   
    public interface IImages
    {
        Task<byte[]> ImageFromUrl(string text);
    }
}
