﻿namespace C2Forms
{
    public interface IDeviceService
    {
        string DeviceId();
        int AndroidAPILevel();
    }
}

