﻿using Xamarin.Forms;

namespace C2Forms
{
    //-----------------------------------------------------

    public delegate void FPtr_on_image_pick(byte[] data);

    public interface IPickImageService
    {
        void Pick_image(string text, FPtr_on_image_pick func_on_image_pick);
    }

    //-----------------------------------------------------

}
