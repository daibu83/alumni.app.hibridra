﻿using System.Collections.Generic;
namespace C2Forms
{
    //-----------------------------------------------------

    public interface IFileService
    {
        void Save_data(List<string> data);
        void Read_data(List<string> data);
    }

    //-----------------------------------------------------

}

