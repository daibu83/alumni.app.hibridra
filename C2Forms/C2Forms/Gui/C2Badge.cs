﻿using System;
using Xamarin.Forms;

//
//  Badge.cs
//  Created by Alexey Kinev on 19 Jan 2015.
//
//    Licensed under The MIT License (MIT)
//    http://opensource.org/licenses/MIT
//
//    Copyright (c) 2015 Alexey Kinev <alexey.rudy@gmail.com>
//
namespace C2Forms
{
    public class C2Badge : AbsoluteLayout
    {
        //protected override void OnPropertyChanged(string propertyName = null)
        //{
        //    base.OnPropertyChanged(propertyName);

        //    if (propertyName == "Text")
        //    {
        //        Texto = Text;

        //    }
        //}

        protected C2RoundedBox Box;
        protected Label Label;

        //-----------------------------------------------------

        public static  BindableProperty TextProperty = BindableProperty.Create("Text", typeof(String), typeof(C2Badge), "");
        //public static readonly BindableProperty IsVisibleProperty = BindableProperty.Create("Visible", typeof(bool), typeof(C2Badge), true);
        public static  BindableProperty BoxColorProperty = BindableProperty.Create("BoxColor", typeof(Color), typeof(C2Badge), Color.Default);

        //-----------------------------------------------------

        public string Text
        {
            get { return (string)GetValue(TextProperty); }
            set
            {
                SetValue(TextProperty, value);

            }
        }
        //public virtual string Texto
        //{
        //    get { return this.Label.Text; }
        //    set { this.Label.Text = " " + value + " "; }
        //}

        //public bool Visible
        //{
        //    get { return (bool)GetValue(IsVisibleProperty); }
        //    set { SetValue(IsVisibleProperty, value); }
        //}
        //-----------------------------------------------------

        public Color BoxColor
        {
            get { return (Color)GetValue(BoxColorProperty); }
            set { SetValue(BoxColorProperty, value); }
        }

        //-----------------------------------------------------

        public Color BorderColor
        {
            get { return this.Box.BorderColor; }
            set { this.Box.borderColor = value; }
        }

        //-----------------------------------------------------

        public Color TextColor
        {
            get { return Label.TextColor; }
            set { Label.TextColor = value; }
        }

        //-----------------------------------------------------

        public C2Badge(double size,double wsize, double fontSize)
        {
            HeightRequest = size;
            if (wsize > 0)
            {
                WidthRequest = wsize;
            }
            else {
                WidthRequest =  HeightRequest;
            }
            
            //WidthRequest = 1.5*HeightRequest;
            Box = new C2RoundedBox();
            Box.CornerRadius = HeightRequest / 2;
            Box.SetBinding(BackgroundColorProperty, new Binding("BoxColor", source: this));
            Children.Add(Box, new Rectangle(0, 0, 1.0, 1.0), AbsoluteLayoutFlags.All);
            Label = new Label();
            Label.TextColor = Color.White;
            Label.FontSize = fontSize;
            //Label.HorizontalOptions = LayoutOptions.CenterAndExpand;
            //Label.VerticalOptions = LayoutOptions.CenterAndExpand;
            
            Label.HorizontalTextAlignment = TextAlignment.Center;
            Label.VerticalTextAlignment = TextAlignment.Center;
            Label.SetBinding(Label.TextProperty, new Binding("Text", BindingMode.OneWay, source: this));
            Children.Add(Label, new Rectangle(0, 0, 1.0, 1.0), AbsoluteLayoutFlags.All);
            Children.Add(Label);

            BoxColor = Color.Red;
            BorderColor = Color.White;
            TextColor = Color.White;

            // Auto-width
            SetBinding(WidthRequestProperty, new Binding("Text", BindingMode.OneWay, new BadgeWidthConverter(WidthRequest), source: this));

            // Hide if no value
            SetBinding(IsVisibleProperty, new Binding("Text", BindingMode.OneWay, new BadgeVisibleValueConverter(), source: this));
        }
    }




    //-----------------------------------------------------

    class BadgeVisibleValueConverter : IValueConverter
    {
        #region IValueConverter implementation

        //-----------------------------------------------------

        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            var text = value as string;
            return !String.IsNullOrEmpty(text);
        }

        //-----------------------------------------------------

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }

        #endregion
    }

    //-----------------------------------------------------

    class BadgeWidthConverter : IValueConverter
    {
        readonly double baseWidth;
        const double widthRatio = 0.33;

        //-----------------------------------------------------

        public BadgeWidthConverter(double baseWidth)
        {
            this.baseWidth = baseWidth;
        }

        //-----------------------------------------------------

        #region IValueConverter implementation

        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            var text = value as string;
            if ((text != null) && (text.Length > 1))
            {
                // We won't measure text length exactly here!
                // May be we should, but it's too tricky. So,
                // we'll just approximate new badge width as
                // linear function from text legth.

                return baseWidth * (1 + widthRatio * (text.Length - 1));
            }
            return baseWidth;
        }

        //-----------------------------------------------------

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }

        #endregion

        //-----------------------------------------------------

    }
}
