﻿using Xamarin.Forms;

namespace C2Forms
{
    public class C2RoundedBox : BoxView
    {
        public static readonly BindableProperty CornerRadiusProperty = BindableProperty.Create("CornerRadius", typeof(double), typeof(C2RoundedBox), 0.0);
        public Color borderColor;
        //-----------------------------------------------------

        public double CornerRadius
        {
            get { return (double)GetValue(CornerRadiusProperty); }
            set { SetValue(CornerRadiusProperty, value); }
        }

        //-----------------------------------------------------

        public Color BorderColor
        {
            get { return this.borderColor; }
            set { this.borderColor = value;  }
        }
    }

}
