﻿using Xamarin.Forms;

namespace C2Forms
{
    public class C2Button : Button
    {
       public static readonly BindableProperty ColorProperty =
       BindableProperty.Create<C2Button, Color>(
           p => p.Color, C2Style.Button_text);

        public Color Color
        {
            get {
                if (GetValue(ColorProperty) == null) {
                    SetValue(ColorProperty, C2Style.Button_text);
                }
                return (Color)GetValue(ColorProperty);
            }
            set { SetValue(ColorProperty, value); }
        }
    }
}
