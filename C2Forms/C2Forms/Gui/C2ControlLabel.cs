﻿using Xamarin.Forms;

namespace C2Forms
{
    public class C2LabelControl : Label
    {
      
    }

    //-----------------------------------------------------

    public class C2ControlLabel : StackLayout
    {
        private C2LabelControl _label;

        //-----------------------------------------------------

        public C2ControlLabel() : base()
        {
            this.Orientation = StackOrientation.Horizontal;
            this.HorizontalOptions = LayoutOptions.FillAndExpand;
            this.Spacing = 0.0;
            this.Padding = new Thickness(C2Style.Entry_padding_horizontal, 0.0, C2Style.Entry_padding_horizontal, 0.0);
            this.BackgroundColor = C2Style.Control_background;
            this.HeightRequest = Device.OnPlatform(C2Style.Entry_font_size + 2.0 * (C2Style.Entry_padding_vertical), -1, -1);
            this._label = new C2LabelControl();
            this._label.FontSize = C2Style.Entry_font_size;
            this._label.TextColor = C2Style.Control_text_strong;
            this._label.HorizontalOptions = LayoutOptions.FillAndExpand;
            this._label.VerticalOptions = LayoutOptions.Center;
            this.Children.Add(this._label);
        }

        //-----------------------------------------------------

        public string Text
        {
            get { return this._label.Text; }
            set { this._label.Text = value; }
        }

        //-----------------------------------------------------

        public Color TextColor
        {
            get { return this._label.TextColor; }
            set { this._label.TextColor = value; }
        }

       
    }
}
