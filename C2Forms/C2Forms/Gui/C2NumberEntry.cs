﻿using System;
using System.Collections.Generic;
using Xamarin.Forms;

namespace C2Forms
{
    public class C2NumberEntry : C2Entry
    {
        public static string TXT_NOT_A_NUMBER = "";
        public static string TXT_MIN_NUMBER = "";
        public static string TXT_MAX_NUMBER = "";
        private static string[] _FORMATS = new string[] { "{0:0}", "{0:0.0}", "{0:0.00}", "{0:0.000}", "{0:0.0000}" };
        private float _min_value;
        private float _max_value;
        private float _value;
        private uint _num_decimals;
        private struct i_Constant
        {
            public string _name;
            public float _value;
        };
        private List<i_Constant> _contants;

        //-----------------------------------------------------

        public C2NumberEntry(string field_name, float min_value, float max_value, uint num_decimals, bool align_right, Keyboard teclado) : base(field_name, 1, 100, false, true, teclado)
        {
            this._min_value = min_value;
            this._max_value = max_value;
            this._value = this._min_value;
            this._num_decimals = num_decimals <= 4 ? num_decimals : 4;

            if (align_right == true)
                this._entry.align = TextAlignment.End;

            this._contants = null;
        }

        //-----------------------------------------------------

        private string i_Text_value(float value)
        {
            return String.Format(_FORMATS[this._num_decimals], value);
        }

        //-----------------------------------------------------

        private float i_Constant_value_from_text(string text)
        {
            if (this._contants == null)
                return float.NaN;

            for (int i = 0; i < this._contants.Count; ++i)
            {
                if (this._contants[i]._name.CompareTo(text.ToLower()) == 0)
                {
                    return this._contants[i]._value;
                }
            }

            return float.NaN;
        }

        //-----------------------------------------------------

        private string i_Constant_text_from_value(float value)
        {
            if (this._contants == null)
                return null;

            for (int i = 0; i < this._contants.Count; ++i)
            {
                if (this._contants[i]._value == value)
                    return this._contants[i]._name;
            }

            return null;
        }

        //-----------------------------------------------------

        public float Value
        {
            get
            {
                return this._value;
            }

            set
            {
                string constant_text = this.i_Constant_text_from_value(value);
                if (constant_text != null)
                {
                    this._entry.Text = constant_text;
                    this._value = value;
                }
                else
                {
                    if (value < this._min_value)
                        this._value = this._min_value;
                    else if (value > this._max_value)
                        this._value = this._max_value;
                    else
                        this._value = value;
                    this._entry.Text = this.i_Text_value(this._value);
                }
            }
        }

        //-----------------------------------------------------

        public override bool Validate()
        {
            bool is_valid = base.Validate();

            if (is_valid == true)
            {
                float constant_value = this.i_Constant_value_from_text(this._entry.Text);
                if (float.IsNaN(constant_value) == false)
                {
                    this._value = constant_value;
                }
                else
                { 
                    try
                    {
                        this._value = float.Parse(this._entry.Text);
                        this._entry.Text = this.i_Text_value(this._value);

                        if (this._value < this._min_value)
                        {
                            is_valid = false;
                            this._label.Text = C2NumberEntry.TXT_MIN_NUMBER + this.i_Text_value(this._min_value);
                            this.i_Show_error_label(true);
                        }
                        else if (this._value > this._max_value)
                        {
                            is_valid = false;
                            this._label.Text = C2NumberEntry.TXT_MAX_NUMBER + this.i_Text_value(this._max_value);
                            this.i_Show_error_label(true);
                        }
                    }
                    catch (Exception)
                    {
                        is_valid = false;
                        this._label.Text = String.Format("{0}: {1}", this._field_name, C2NumberEntry.TXT_NOT_A_NUMBER);
                        this.i_Show_error_label(true);
                    }
                }
            }

            return is_valid;
        }

        //-----------------------------------------------------

        public void Add_constant(string name, float value)
        {
            i_Constant item;

            if (this._contants == null)
                this._contants = new List<i_Constant>();

            item._name = name.ToLower();
            item._value = value;
            this._contants.Add(item);
        }

        //-----------------------------------------------------

    }
}
