﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Xamarin.Forms;
using C2Core;

namespace C2Forms
{
    struct C2AssistedItem
    {
        public string text;
        public string normalized_text;
        public int id;
    }

    //-----------------------------------------------------

    public class C2AssistedEntry : C2Entry
    {
        public static string TXT_NO_MATCH = "";

        private static readonly int _NUM_CHARACTER_FOR_ASSIST = 2;
        public delegate Task<bool> FPtr_on_assist_required(C2AssistedEntry sender, string search_text);
        public delegate void FPtr_on_id_changed(Int32 id);
        private bool _allow_non_match;
        private ContentView _assist_view;
        private bool _launch_text_change_event;
        private Int32 _current_id;
        private string _predictive_str;
        private FPtr_on_assist_required _func_on_assist_required;
        private FPtr_on_id_changed _func_on_id_changed;
        private List<C2AssistedItem> _items;

        //-----------------------------------------------------

        public C2AssistedEntry(string field_name, bool is_required, bool allow_non_match, FPtr_on_assist_required func_on_assist_required, FPtr_on_id_changed func_on_id_changed) : base(field_name, (is_required == true) ? 1u : 0, 256, true, true,null)
        {
            StackLayout assist_layout;
            assist_layout = new StackLayout();
            //Para aumentar font size
            assist_layout.Padding = new Thickness(0.0, 0.0, 0.0, 20.0);
            //assist_layout.Padding = new Thickness(0.0, 0.0, 0.0, 10.0);
            assist_layout.Spacing = 10.0;
            this._allow_non_match = allow_non_match;
            this._assist_view = new ContentView();
            this._assist_view.Style = C2Style.Control_style;
            this._assist_view.Content = assist_layout;
            this.Children.Add(this._assist_view);
            this._launch_text_change_event = true;
            this._current_id = Int32.MinValue;
            this._predictive_str = "";
            this._func_on_assist_required = func_on_assist_required;
            this._func_on_id_changed = func_on_id_changed;
            this._items = new List<C2AssistedItem>();
            this._entry.Focused += this.i_On_focus;
            this._entry.TextChanged += this.i_On_text_changed;
            this.i_Show_assist_view(false);
        }

        //-----------------------------------------------------

        private void i_Show_assist_view(bool show)
        {
            this._assist_view.IsVisible = show;
        }

        //-----------------------------------------------------

        private static char i_Normalice_character(char lower_character)
        {
            int index;
            const string _ACCENT_CHARACTER_ = "áàäéèëíìïóòöúùuç";
            const string _NON_ACCENT_CHARACTER = "aaaeeeiiiooouuuc";
            index = _ACCENT_CHARACTER_.IndexOf(lower_character);
            return (index != -1) ? _NON_ACCENT_CHARACTER[index] : lower_character;
        }

        //-----------------------------------------------------

        private static string i_Normalice_string(string str)
        {
            string lower_str;
            char[] lower_normaliced;

            lower_str = str.ToLower();
            lower_normaliced = lower_str.ToCharArray();

            for (int i = 0; i < lower_normaliced.Length; i++)
                lower_normaliced[i] = C2AssistedEntry.i_Normalice_character(lower_normaliced[i]);

            return new string(lower_normaliced);
        }

        //-----------------------------------------------------

        private void i_On_id_changed()
        {
            if (this._func_on_id_changed != null)
                this._func_on_id_changed(this._current_id);
        }

        //-----------------------------------------------------

        private void i_Unset_current_id()
        {
            if (this._current_id != Int32.MinValue)
            {
                this._current_id = Int32.MinValue;
                this.i_On_id_changed();
            }
        }

        //-----------------------------------------------------

        private async void i_On_focus(object sender, EventArgs e)
        {
            ScrollView view = null;
            Element element = this._entry;
            while (element != null)
            {
                if (element is ScrollView)
                {
                    view = (ScrollView)element;
                    element = null;
                }
                else
                {
                    element = element.Parent;
                }
            }

            if (view != null)
                await view.ScrollToAsync(this._entry, ScrollToPosition.Start, true);
        }

        //-----------------------------------------------------

        private async void i_On_text_changed(object sender, TextChangedEventArgs e)
        {
            if (this._launch_text_change_event == false)
                return;

            string text = e.NewTextValue;
            int length = String.IsNullOrEmpty(text) ? 0 : text.Length;
            bool launch_predictive = false;

            if (length >= _NUM_CHARACTER_FOR_ASSIST)
            {
                string predictive_str = text.Substring(0, _NUM_CHARACTER_FOR_ASSIST);
                predictive_str = C2AssistedEntry.i_Normalice_string(predictive_str);

                if (predictive_str.CompareTo(this._predictive_str) != 0)
                {
                    this._predictive_str = predictive_str;
                    launch_predictive = true;
                }
            }

            if (launch_predictive == true)
            {
                bool ok;
                this._items.Clear();
                this.Set_waiting_state();
                ok = await this._func_on_assist_required(this, this._predictive_str);
                this.Unset_waiting_state();
                //text = this._entry.Text;
                //length = String.IsNullOrEmpty(text) ? 0 : text.Length;
            }

            if (length >= _NUM_CHARACTER_FOR_ASSIST)
            {
                StackLayout assistant = (StackLayout)this._assist_view.Content;
                string normalized_entry = C2AssistedEntry.i_Normalice_string(text);
                int index_match = Int32.MinValue;

                assistant.Children.Clear();
                for (int i = 0; i < this._items.Count; ++i)
                {
                    if (this._items[i].normalized_text.Contains(normalized_entry) == true)
                    {
                        C2TapLabel label = new C2TapLabel(this._items[i].text, this._items[i].id, this.i_On_tap_label);
                        //label.Style = C2Style.Info_label_style;

                        //Aumentar Font size a la del titulo
                        label.Style = C2Style.Info_label_Autocomplete_style;
                        
                        ((StackLayout)this._assist_view.Content).Children.Add(label);
                        index_match = i;
                    }
                }

                if (assistant.Children.Count == 0)
                {
                    this.i_Show_assist_view(false);
                    this.i_Unset_current_id();
                }
                else if (assistant.Children.Count == 1)
                {
                    if (normalized_entry.CompareTo(this._items[index_match].normalized_text) == 0)
                    {
                        this._entry.Text = this._items[index_match].text;
                        this._current_id = this._items[index_match].id;
                        this.i_On_id_changed();
                        this.i_Show_assist_view(false);
                    }
                    else
                    {
                        this.i_Show_assist_view(true);
                        this.i_Unset_current_id();
                    }
                }
                else
                {
                    this.i_Show_assist_view(true);
                    this.i_Unset_current_id();
                }
            }
            else
            {
                this.i_Show_assist_view(false);
                this.i_Unset_current_id();
            }
        }

        //-----------------------------------------------------

        public override bool Validate()
        {
            bool is_valid = base.Validate();
            this.i_Show_assist_view(false);
            
            if (is_valid == true)
            {
                string normalized_entry = C2AssistedEntry.i_Normalice_string(this._entry.Text);
                for (int i = 0; i < this._items.Count; ++i)
                {
                    if (normalized_entry.CompareTo(this._items[i].normalized_text) == 0)
                    {
                        this._entry.Text = this._items[i].text;
                        this._current_id = this._items[i].id;
                        break;
                    }
                }

                if (this._allow_non_match == true)
                    is_valid = true;
                else
                    is_valid = this._current_id != Int32.MinValue;

                if (is_valid == false)
                {
                    this._label.Text = string.Format("{0}: {1}", this._field_name, string.Format(C2AssistedEntry.TXT_NO_MATCH, this._entry.Text));
                    this.i_Show_error_label(true);
                }
            }

            return is_valid;
        }

        //-----------------------------------------------------

        public override void Clear()
        {
            this._launch_text_change_event = false;
            base.Clear();
            this._launch_text_change_event = true;
            this._current_id = Int32.MinValue;
        }

        //-----------------------------------------------------

        public Int32 CurrentId
        {
            get
            {
                return this._current_id;
            }
        }

        //-----------------------------------------------------

        public void Set_text(string text)
        {
            if (this._allow_non_match == true)
            {
                this._launch_text_change_event = false;
                base.Text = text;
                this._current_id = Int32.MinValue;
                this._launch_text_change_event = true;
            }
            else
            {
                C2Assert.Error(false);
            }
        }

        //-----------------------------------------------------

        public void Set_text_and_id(string text, Int32 id)
        {
            this._launch_text_change_event = false;
            base.Text = text;
            this._current_id = id;
            this._launch_text_change_event = true;
        }

        //-----------------------------------------------------

        private void i_On_tap_label(string text, int value)
        {
            this._entry.Text = text;
            this._current_id = value;
            this.i_On_id_changed();
            this.i_Show_assist_view(false);
            this.i_Show_error_label(false);
        }

        //-----------------------------------------------------

        public void Add_assisted_item(string text, Int32 id)
        {
            C2AssistedItem item = new C2AssistedItem();
            item.text = text;
            item.normalized_text = C2AssistedEntry.i_Normalice_string(text);
            item.id = id;
            this._items.Add(item);
        }

        //-----------------------------------------------------


    }
}

