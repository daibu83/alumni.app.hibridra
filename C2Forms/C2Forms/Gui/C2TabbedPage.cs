﻿using System.Collections.Generic;
using Xamarin.Forms;

namespace C2Forms
{
    public class C2TabbedPage : TabbedPage
    {
        public Color _background_color;
        public Color _bar_color;
       // private List<Page> _children;
       // private StackLayout _layout;

        //-----------------------------------------------------

        private ContentView i_Tab_view(string title)
        {
            ContentView view = new ContentView();
            Label label = new Label { Text = title };
            view.Padding = new Thickness(5, 5, 5, 5);
            view.BackgroundColor = Color.Blue;
            view.Content = label;
            return view;
        }

        //-----------------------------------------------------

        public C2TabbedPage(Color background_color, Color bar_color/*, ContentPage page1, ContentPage page2*/) : base()
        {

            this._background_color = background_color;
            this._bar_color = bar_color;
            //_children = new List<Page>();
            //_children.Add(page1);
            //_children.Add(page2);
            //this._layout = new StackLayout();

            /*{
                StackLayout tablayout = new StackLayout();
                tablayout.Orientation = StackOrientation.Horizontal;
                tablayout.Children.Add(this.i_Tab_view("Oferta"));
                tablayout.Children.Add(this.i_Tab_view("Mensajes"));
                tablayout.BackgroundColor = Color.Red;
                this._layout.Children.Add(tablayout);
            }

            this._layout.Children.Add(page1.Content);*/

        }
    }
}
