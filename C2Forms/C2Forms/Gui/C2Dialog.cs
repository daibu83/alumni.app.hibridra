﻿using System;
using Xamarin.Forms;


namespace C2Forms
{
    public class C2Dialog: StackLayout
    {

        
        private FormattedString _title;
        private FormattedString _text;
        private Boolean _blnAceptar;
        private Boolean _blnCancelar;

        public C2Dialog(FormattedString title, FormattedString text, Boolean aceptar, Boolean cancelar, EventHandler On_close) :base()
        {
            this._title = title;
            this._text = text;
            this._blnAceptar = aceptar;
            this._blnCancelar = cancelar;
            Label label1;
            Label _label;
            Grid botones;
             
            //this.Spacing = 10.0;

            this.Orientation = StackOrientation.Vertical;
            //this.Padding = 15;
            this.BackgroundColor = Color.White;
            //this.BackgroundColor = Color.FromRgba(0, 0, 0, 180);
            //this.VerticalOptions = LayoutOptions.FillAndExpand;
            //this.HorizontalOptions = LayoutOptions.EndAndExpand;


            label1 = new Label();
            label1.FormattedText = title;
            label1.TranslationX = 5;
            //6d7075
            label1.TextColor = Color.FromHex("6D7075");
            label1.VerticalOptions = LayoutOptions.Center;

            _label = new Label();
            _label.TranslationX = 5;
            _label.FormattedText = text;
            _label.TextColor = Color.FromHex("6D7075");
            _label.VerticalOptions = LayoutOptions.Center;

            C2Button botonAcept;
            C2Button botonCancelar;
            C2Button botonVacio;

            botonVacio = new C2Button { Text = "Vacio" };
            botonVacio.IsVisible = false;
            botonCancelar = new C2Button { Text = "CANCELAR" };
            botonCancelar.Style = C2Style.Button_style_dialog;
            botonCancelar.FontAttributes = FontAttributes.Bold;
            botonCancelar.Color = Color.FromHex("6D7075");
            botonCancelar.Clicked += On_close;

            //5e9dd2
            
            botonAcept = new C2Button { Text = "ACEPTAR" };
            botonAcept.Style = C2Style.Button_style_dialog;
            botonAcept.FontAttributes = FontAttributes.Bold;
            botonAcept.Color = Color.FromHex("5E9DD2"); ;
            botonAcept.Clicked += On_close;

            this.Children.Add(label1);
            this.Children.Add(_label);

            botones = new Grid
            {
                VerticalOptions = LayoutOptions.FillAndExpand,
                RowDefinitions =
                {
                    new RowDefinition { Height = GridLength.Auto },
                    //new RowDefinition { Height = new GridLength(100, GridUnitType.Absolute) }
                },
                ColumnDefinitions =
                {
                    new ColumnDefinition { Width = GridLength.Auto },
                    new ColumnDefinition { Width = new GridLength(1, GridUnitType.Star) },
                    new ColumnDefinition { Width = new GridLength(100, GridUnitType.Absolute) }
                }
            };

            //botones.Orientation = StackOrientation.Horizontal;
            //botones.HorizontalOptions = LayoutOptions.Center;

            botones.Children.Add(botonVacio);
            if (_blnCancelar == true) {
                botones.Children.Add(botonCancelar,1,0);
            }
            if (_blnAceptar == true) {
                botones.Children.Add(botonAcept,2,0);
            }


            ////grid.Children.Add(new Label
            ////{
            ////    Text = "Fixed 100x100",
            ////    TextColor = Color.Aqua,
            ////    BackgroundColor = Color.Red,
            ////    HorizontalTextAlignment = TextAlignment.Center,
            ////    VerticalTextAlignment = TextAlignment.Center
            ////}, 2, 3);


            this.Children.Add(botones);




        }

        public C2Dialog(FormattedString title, FormattedString text, EventHandler On_close) : this(title, text, true, true, On_close)
        {

        }


    }
}
