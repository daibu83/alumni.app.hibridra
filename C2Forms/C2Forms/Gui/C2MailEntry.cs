﻿using System.Text.RegularExpressions;
using Xamarin.Forms;

namespace C2Forms
{    
    public class C2MailEntry : C2Entry
    {
        public static string TXT_EMAIL_INVALID = "";
        
        //-----------------------------------------------------

        public C2MailEntry(string field_name) : base(field_name, 1, 256, false, false,Keyboard.Email)
        {
        }

        //-----------------------------------------------------

        public override bool Validate()
        {
            bool is_valid = base.Validate();

            if (is_valid == true)
            {
                const string _email_regex = @"^(?("")("".+?(?<!\\)""@)|(([0-9a-z]((\.(?!\.))|[-!#\$%&'\*\+/=\?\^`\{\}\|~\w])*)(?<=[0-9a-z])@))" +
                       @"(?(\[)(\[(\d{1,3}\.){3}\d{1,3}\])|(([0-9a-z][-\w]*[0-9a-z]*\.)+[a-z0-9][\-a-z0-9]{0,22}[a-z0-9]))$";


                if (Regex.IsMatch(this._entry.Text.ToLower(), _email_regex) == false)
                {
                    is_valid = false;
                    this._label.Text = C2MailEntry.TXT_EMAIL_INVALID;
                    this.i_Show_error_label(true);
                }
            }

            return is_valid;
        }

        //-----------------------------------------------------


    }
}
