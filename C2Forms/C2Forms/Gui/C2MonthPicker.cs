﻿namespace C2Forms
{
    public class C2MonthPicker : C2Picker
    {
        public C2MonthPicker(string field_name, string [] months, FPtr_on_selected_changed func_on_selected_changed) : base(field_name, func_on_selected_changed)
        {
            for (int i = 0; i < 12; ++i)
                this.Add(months[i], i + 1);
        }

        //-----------------------------------------------------


    }
}
