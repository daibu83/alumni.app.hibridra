﻿using System;
using System.Collections.Generic;
using Xamarin.Forms;
using C2Core;

namespace C2Forms
{
    public class C2TableColumn
    {
        public double _width;
        public C2TableColumn(double width)
        {
            this._width = width;
        }
    }

    public abstract class C2TableRow
    {
        public abstract View Cell(int index);
        public abstract bool Validate();
        public abstract Object Data();
    }

    //-----------------------------------------------------

    public class C2Table : Grid
    {
        public delegate C2TableRow FPtr_new_row();
        private int _column_count;
        private List<C2TableRow> _rows;
        private List<C2TapImage> _images;
        private string _add_image_source;
        private string _delete_image_source;
        private FPtr_new_row _func_new_row;

        //-----------------------------------------------------

        private void i_Swap_rows(int index1, int index2)
        {
            C2Assert.Error(index1 != index2);
            {
                C2TableRow swap_row;
                swap_row = this._rows[index1];
                this._rows[index1] = this._rows[index2];
                this._rows[index2] = swap_row;
            }

            {
                C2TapImage swap_image;
                swap_image = this._images[index1];
                this._images[index1] = this._images[index2];
                this._images[index2] = swap_image;
            }

            C2TableRow row1 = this._rows[index1];
            C2TableRow row2 = this._rows[index2];
            for (int i = 0; i < this._column_count; ++i)
            {
                Grid.SetRow(row1.Cell(i), index1);
                Grid.SetRow(row2.Cell(i), index2);
            }

            Grid.SetRow(this._images[index1], index1);
            Grid.SetRow(this._images[index2], index2);
        }

        //-----------------------------------------------------

        private void i_Add_row(C2TableRow row, C2TapImage image, bool swap_last_row)
        {
            int index = this._rows.Count;
            image.HorizontalOptions = LayoutOptions.Center;
            image.VerticalOptions = LayoutOptions.Center;
            for (int i = 0; i < this._column_count; ++i)
                this.Children.Add(row.Cell(i), i, index);
            this.Children.Add(image, this._column_count, index);
            this._rows.Add(row);
            this._images.Add(image);
            this.RowDefinitions.Add(new RowDefinition { Height = GridLength.Auto });

            // The 'add_row' is always the last one
            if (index > 0 && swap_last_row == true)
                this.i_Swap_rows(index - 1, index);
        }

        //-----------------------------------------------------

        private void i_Add_empty_row(bool swap_last_row)
        {
            C2TableRow row;
            C2TapImage image;
            row = this._func_new_row();
            image = new C2TapImage(this._add_image_source, this.i_On_add);
            this.i_Add_row(row, image, swap_last_row);
        }

        //-----------------------------------------------------

        public C2Table(List<C2TableColumn> columns, string add_image_source, string delete_image_source, FPtr_new_row func_new_row) : base()
        {
            this._column_count = columns.Count;
            this._rows = new List<C2TableRow>();
            this._images = new List<C2TapImage>();
            this._add_image_source = add_image_source;
            this._delete_image_source = delete_image_source;
            this._func_new_row = func_new_row;
            this.ColumnSpacing = C2Style.Table_column_spacing;
            this.RowSpacing = C2Style.Table_row_spacing;
            for (int i = 0; i < columns.Count; ++i)
                this.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(columns[i]._width, GridUnitType.Star) });
            this.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(1.0, GridUnitType.Star) });
            this.i_Add_empty_row(false);
        }

        //-----------------------------------------------------

        public void Add_row(C2TableRow row)
        {
            C2TapImage image;
            image = new C2TapImage(this._delete_image_source, this.i_On_delete);
            this.i_Add_row(row, image, true);
        }

        //-----------------------------------------------------

        public View Cell(int row_index, int col_index)
        {
            C2TableRow row = this._rows[row_index];
            return row.Cell(col_index);
        }

        //-----------------------------------------------------

        public void Clear()
        {
            this._rows.Clear();
            this._images.Clear();
            this.Children.Clear();
            this.i_Add_empty_row(false);
        }

        //-----------------------------------------------------

        public bool Validate()
        {
            bool ok = true;
            for (int i = 0; i < this._rows.Count - 1; ++i)
                ok = ok & this._rows[i].Validate();
            return ok;
        }

        //-----------------------------------------------------

        public List<T> CurrentData<T>() where T : class
        {
            List<T> data = new List<T>();
            for (int i = 0; i < this._rows.Count - 1; ++i)
            {
                Object item = this._rows[i].Data();
                data.Add((T)item);

                
            }
            return data;
        }

        //-----------------------------------------------------

        private void i_On_delete(C2TapImage sender)
        {
            int index = Grid.GetRow(sender);

            {
                C2TableRow row = this._rows[index];
                for (int i = 0; i < this._column_count; ++i)
                    this.Children.Remove(row.Cell(i));
            }

            this.Children.Remove(this._images[index]);

            for (int i = index + 1; i < this._rows.Count; ++i)
            {
                C2TableRow row = this._rows[i];
                for (int j = 0; j < this._column_count; ++j)
                    Grid.SetRow(row.Cell(j), i - 1);
                Grid.SetRow(this._images[i], i - 1);
            }

            this._rows.RemoveAt(index);
            this._images.RemoveAt(index);
        }

        //-----------------------------------------------------

        private void i_On_add(C2TapImage sender)
        {
            int index = Grid.GetRow(sender);
            C2TableRow row = this._rows[index];
            C2Assert.Error(index == this._rows.Count - 1);

            if (row.Validate() == true)
            {
                C2TapImage image;
                this.Children.Remove(this._images[index]);
                image = new C2TapImage("delete.png", this.i_On_delete);
                image.HorizontalOptions = LayoutOptions.Center;
                image.VerticalOptions = LayoutOptions.Center;
                this._images[index] = image;
                this.Children.Add(image, this._column_count, index);
                this.i_Add_empty_row(false);
            }
        }

        //-----------------------------------------------------

    }
}
