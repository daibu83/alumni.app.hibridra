﻿using System;
using Xamarin.Forms;

namespace C2Forms
{
    public class C2CustomSwitch : Switch
    {

    }

    //-----------------------------------------------------

    public class C2Switch : Grid
    {
        public delegate void FPtr_on_toggled(bool on_off);
        private Label _label;
        private C2CustomSwitch _switch;
        private FPtr_on_toggled _func_on_toggled;

        //-----------------------------------------------------

        public C2Switch(string text, FPtr_on_toggled func_on_toggled) : base()
        {
            StackLayout label_layout;
            StackLayout switch_layout;
            TapGestureRecognizer gesture;
            this.BackgroundColor = C2Style.Control_background;
            this.ColumnSpacing = 0.0;
            this.RowSpacing = 0.0;
            this.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(4.0, GridUnitType.Star) });
            this.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(1.0, GridUnitType.Auto) });
            label_layout = new StackLayout();
            label_layout.Spacing = 0.0;
            label_layout.Padding = new Thickness(0, C2Style.Entry_padding_vertical, C2Style.Entry_padding_horizontal, C2Style.Entry_padding_vertical);
            label_layout.VerticalOptions = LayoutOptions.Start;
            switch_layout = new StackLayout();
            switch_layout.Spacing = 0.0;
            switch_layout.Padding = new Thickness(0, 0, C2Style.Entry_padding_horizontal, 0);
            switch_layout.VerticalOptions = LayoutOptions.Center;
            this._func_on_toggled = func_on_toggled;
            this._label = new Label { Text = text };
            this._label.FontSize = C2Style.Entry_font_size;
            this._label.TextColor = C2Style.Control_text;
            this._label.HorizontalOptions = LayoutOptions.FillAndExpand;
            this._label.VerticalOptions = LayoutOptions.Start;
            this._label.TranslationX = C2Style.Entry_padding_horizontal;
            this._label.LineBreakMode = LineBreakMode.WordWrap;
            label_layout.Children.Add(this._label);
            this._switch = new C2CustomSwitch { Style = C2Style.Control_style };
            this._switch.HorizontalOptions = LayoutOptions.End;
            this._switch.VerticalOptions = LayoutOptions.Center;
            this._switch.Toggled += this.i_on_toggled;
            switch_layout.Children.Add(this._switch);
            this.Children.Add(label_layout, 0, 0);
            this.Children.Add(switch_layout, 1, 0);
            gesture = new TapGestureRecognizer();
            gesture.Tapped += i_On_layout;
            this.GestureRecognizers.Add(gesture);
        }

        //-----------------------------------------------------

        public bool IsToggled
        {
            set
            {
                this._switch.IsToggled = value;
            }

            get
            {
                return this._switch.IsToggled;
            }
        }

        //-----------------------------------------------------

        private void i_On_layout(object sender, EventArgs e)
        {
            if (this._func_on_toggled != null)
                this._func_on_toggled(this._switch.IsToggled);
            this._switch.IsToggled = !this._switch.IsToggled;
        }

        //-----------------------------------------------------

        private void i_on_toggled(object sender, EventArgs e)
        {
            if (this._func_on_toggled != null)
                this._func_on_toggled(this._switch.IsToggled);
        }

        //-----------------------------------------------------


    }
}
