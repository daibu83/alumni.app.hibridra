﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace C2Forms
{
    public class C2PageDialog: ContentPage
    {
        public C2PageDialog(double pHeight, double pWidth, View _popup):base()
        {
                       
            Content = new Frame
            {
                WidthRequest= pWidth - 80,
                Padding = new Thickness(15, 20, 15, 20),
                Content = _popup,
                OutlineColor = Color.Silver,
                BackgroundColor=Color.White,
                VerticalOptions = LayoutOptions.Center,
                HorizontalOptions = LayoutOptions.Center
            };

            this.BackgroundColor = new Color(0, 0, 0, 0.7);
        }

        public Task Close()
        {
            var displayEvent = CloseModalRequested;

            Task completion = null;
            if (displayEvent != null)
            {
                var eventArgs = new CloseModalRequestedEventArgs();
                displayEvent(this, eventArgs);
                completion = eventArgs.ClosingPageTask;
            }
            
            return completion ?? Task.FromResult<object>(null);
        }

        public event EventHandler<CloseModalRequestedEventArgs> CloseModalRequested;

        public sealed class CloseModalRequestedEventArgs : EventArgs
        {
            public Task ClosingPageTask { get; set; }
        }


    }
}
