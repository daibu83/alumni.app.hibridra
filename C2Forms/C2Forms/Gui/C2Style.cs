﻿using Xamarin.Forms;

namespace C2Forms
{
    public class C2Style
    {
        //-----------------------------------------------------

        // Should be overwritten from application
        public static double Table_row_spacing = 1.0;
        public static double Table_column_spacing = 1.0;
        public static Color Button_text = Color.Pink;
        public static double Button_font_size = 1.0;
        public static double Label_error_padding_left = 0.0;
        public static double Label_info_padding_left = 0.0;
        public static double Label_font_size = 1.0;
        public static double Label_font_size_popmsg = 1.1;
        public static double Label_font_size_title = 1.5;
        public static double Label_font_size_autocomplete = 0.5 * (Device.GetNamedSize(NamedSize.Small, typeof(Entry)) + Device.GetNamedSize(NamedSize.Medium, typeof(Entry)));
        public static double Entry_font_size = 1.0;
        public static double Entry_padding_vertical = 1.0;
        public static double Entry_padding_horizontal = 0.0;
        public static Color Control_text = Color.Pink;
        public static Color Control_text_strong = Color.Pink;
        public static Color Control_background = Color.Pink;
        public static Color Control_error_background = Color.Pink;
        public static Color Control_placeholder = Color.Pink;
        public static Color Error_text = Color.Pink;
        public static Color Info_text = Color.Pink;
        public static Color Button_text_dialog = Color.Black;
        public static Color Button_background_dialog = Color.White;

        //-----------------------------------------------------

        private static Style _error_label_style = null;
        private static Style _info_label_style = null;
        private static Style _info_label_Autocomplete_style = null;
        private static Style _entry_style = null;
        private static Style _control_style = null;
        private static Style _button_style = null;

        //-----------------------------------------------------

        public static Style Error_label_style
        {
            get
            {
                if (C2Style._error_label_style == null)
                {
                    C2Style._error_label_style = new Style(typeof(Label))
                    {
                        Setters =
                        {
                            new Setter { Property = Label.TextColorProperty, Value = C2Style.Error_text },
                            new Setter { Property = Label.FontSizeProperty, Value = C2Style.Label_font_size },
                            new Setter { Property = Label.TranslationXProperty, Value = C2Style.Label_error_padding_left },
//                            new Setter { Property = Label.LineBreakModeProperty, Value = LineBreakMode.TailTruncation }
                        }
                    };
                }

                return C2Style._error_label_style;
            }
        }

        //-----------------------------------------------------

        public static Style Info_label_style
        {
            get
            {
                if (C2Style._info_label_style == null)
                {
                    C2Style._info_label_style = new Style(typeof(Label))
                    {
                        Setters =
                        {
                            new Setter { Property = Label.TextColorProperty, Value = C2Style.Info_text },
                            new Setter { Property = Label.FontSizeProperty, Value = C2Style.Label_font_size },
                            new Setter { Property = Label.TranslationXProperty, Value = C2Style.Label_info_padding_left },
                            new Setter { Property = Label.LineBreakModeProperty, Value = LineBreakMode.TailTruncation }
                        }
                    };
                }

                return C2Style._info_label_style;
            }
        }


        public static Style Info_label_Autocomplete_style
        {
            get
            {
                if (C2Style._info_label_Autocomplete_style == null)
                {
                    C2Style._info_label_Autocomplete_style = new Style(typeof(Label))
                    {
                        Setters =
                        {
                            new Setter { Property = Label.TextColorProperty, Value = C2Style.Info_text },
                            new Setter { Property = Label.FontSizeProperty, Value = C2Style.Label_font_size_autocomplete },
                            new Setter { Property = Label.TranslationXProperty, Value = C2Style.Label_info_padding_left },
                            new Setter { Property = Label.LineBreakModeProperty, Value = LineBreakMode.WordWrap }
                        }
                    };
                }

                return C2Style._info_label_Autocomplete_style;
            }
        }

        //-----------------------------------------------------

        public static Style Entry_style
        {
            get
            { 
                if (C2Style._entry_style == null)
                {
                    C2Style._entry_style = new Style(typeof(Entry))                
                    {
                        Setters =
                        {
                            new Setter { Property = Entry.BackgroundColorProperty, Value = C2Style.Control_background },
                            new Setter { Property = Entry.TextColorProperty, Value = C2Style.Control_text }
                        }
                    };
                }

                return C2Style._entry_style;
            }
        }

        //-----------------------------------------------------

        public static Style Control_style
        {
            get
            {
                if (C2Style._control_style == null)
                {
                    C2Style._control_style = new Style(typeof(View))
                    {
                        Setters =
                        {
                           new Setter { Property = View.BackgroundColorProperty, Value = C2Style.Control_background }
                        }
                    };
                }

                return C2Style._control_style;
            }
        }


        public static Style Button_style_dialog
        {
            get
            {
                if (C2Style._button_style == null)
                {
                    C2Style._button_style = new Style(typeof(Button))
                    {
                        Setters =
                        {
                           
                           new Setter { Property = Button.TextColorProperty, Value = C2Style.Button_text_dialog },
                           new Setter { Property = Button.BackgroundColorProperty, Value = C2Style.Button_background_dialog }
                        }
                    };
                }

                return C2Style._button_style;
            }
        }
        //-----------------------------------------------------


    }
}
