using System;
using System.Windows.Input;
using Xamarin.Forms;

namespace C2Forms
{
    /// <summary>
    /// Class HyperLinkLabel.
    /// </summary>
    public class C2HyperLinkLabelAwesome : Label
    {

        //Must match the exact "Name" of the font which you can get by double clicking the TTF in Windows
        public const string Typeface = "FontAwesome";

        //-----------------------------------------------------
        //-----------------------------------------------------
        /// <summary>
        /// The is underlined property.
        /// </summary>
        public static readonly BindableProperty IsUnderlineProperty =
            BindableProperty.Create<C2HyperLinkLabelAwesome, bool>(p => p.IsUnderline, false);

        /// <summary>
        /// Gets or sets a value indicating whether the text in the label is underlined.
        /// </summary>
        /// <value>A <see cref="bool"/> indicating if the text in the label should be underlined.</value>
        public bool IsUnderline
        {
            get
            {
                return (bool)GetValue(IsUnderlineProperty);
            }
            set
            {
                SetValue(IsUnderlineProperty, value);
            }
        }

        /// <summary>
        /// The subject property
        /// </summary>
        public static readonly BindableProperty SubjectProperty = BindableProperty.Create("Subject", typeof(string),
            typeof(C2HyperLinkLabelAwesome), string.Empty, BindingMode.OneWay, null, null, null, null);

        /// <summary>
        /// The navigate URI property
        /// </summary>
        public static readonly BindableProperty NavigateUriProperty = BindableProperty.Create("NavigateUri", typeof(string),
            typeof(C2HyperLinkLabelAwesome), string.Empty, BindingMode.OneWay, null, null, null, null);

        /// <summary>
        /// The navigate command property
        /// </summary>
        public static readonly BindableProperty NavigateCommandProperty = BindableProperty.Create("NavigateCommand", typeof(ICommand),
            typeof(C2HyperLinkLabelAwesome), null, BindingMode.OneWay, null, null, null, null);

        private TapGestureRecognizer _tapGestureRecognizer;

        /// <summary>
        /// Initializes static members of the <see cref="HyperLinkLabelAwesome" /> class.
        /// </summary>
        static C2HyperLinkLabelAwesome()
        {

        }

        /// <summary>
        /// Initializes a new instance of the <see cref="HyperLinkLabelAwesome"/> class.
        /// </summary>
        public C2HyperLinkLabelAwesome()
        {
            //FontFamily = Device.OnPlatform("FontAwesome", "FontAwesome", "/Assets/fontawesome-webfont.ttf#FontAwesome");
            FontFamily = Typeface;    //iOS is happy with this, Android needs a renderer to add ".ttf"
            //Text = fontAwesomeIcon;
            NavigateCommand = new Command(() =>
            {
                try { Device.OpenUri(new Uri(NavigateUri)); }
                catch { }

                
            });

            _tapGestureRecognizer = new TapGestureRecognizer() { Command = NavigateCommand };

            GestureRecognizers.Add(_tapGestureRecognizer);
        }

        /// <summary>
        /// Gets or sets the subject.
        /// </summary>
        /// <value>The subject.</value>
        public string Subject
        {
            get { return (string)base.GetValue(SubjectProperty); }
            set { base.SetValue(SubjectProperty, value); }
        }

        /// <summary>
        /// Gets or sets the navigate URI.
        /// </summary>
        /// <value>The navigate URI.</value>
        public string NavigateUri
        {
            get { return (string)base.GetValue(NavigateUriProperty); }
            set { base.SetValue(NavigateUriProperty, value); }
        }

        /// <summary>
        /// Gets or sets the navigate command.
        /// </summary>
        /// <value>The navigate command.</value>
        public ICommand NavigateCommand
        {
            get { return (ICommand)base.GetValue(NavigateCommandProperty); }
            set { base.SetValue(NavigateCommandProperty, value); }
        }

        #region Overrides of BindableObject

        /// <param name="propertyName">The name of the property that changed.</param>
        /// <summary>
        /// Call this method from a child class to notify that a change happened on a property.
        /// </summary>
        /// <remarks>
        /// <para>
        /// A <see cref="T:Xamarin.Forms.BindableProperty"/> triggers this by itself. An inheritor only needs to call this for properties without <see cref="T:Xamarin.Forms.BindableProperty"/> as the backend store.
        /// </para>
        /// </remarks>
        protected override void OnPropertyChanged(string propertyName = null)
        {
            base.OnPropertyChanged(propertyName);

            if (propertyName == "NavigateCommand")
            {
                this.GestureRecognizers.Remove(_tapGestureRecognizer);

                _tapGestureRecognizer = new TapGestureRecognizer() { Command = NavigateCommand };

                GestureRecognizers.Add(_tapGestureRecognizer);
            }
        }

        #endregion
    }
}