﻿using Xamarin.Forms;

namespace C2Forms
{
    public class C2ControlImage : ContentView
    {
        private Image _image;

        //-----------------------------------------------------

        public C2ControlImage(string image_source, double size) : base()
        {
            this.BackgroundColor = C2Style.Control_background;
            this._image = new Image { Source = image_source };
            this._image.WidthRequest = size;
            this._image.HeightRequest = size;
            this._image.HorizontalOptions = LayoutOptions.Center;
            this._image.VerticalOptions = LayoutOptions.Center;
            this.Content = this._image;
        }
    }
}
