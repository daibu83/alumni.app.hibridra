﻿using System;
using C2Core;

namespace C2Forms
{
    public class C2YearPicker : C2Picker
    {
        public C2YearPicker(string field_name, FPtr_on_selected_changed func_on_selected_changed, int from_year, int to_year) : base(field_name, func_on_selected_changed)
        {
            C2Assert.Error(from_year <= to_year);
            //this._picker.Focused += this.i_On_focus;
            for (int i = to_year; i >= from_year; --i)
                this.Add(i.ToString(), i);
        }

        //-----------------------------------------------------

            /*
        private void i_On_focus(object sender, EventArgs e)
        {
            if (this.CurrentId == Int32.MinValue)
                this.CurrentId = System.DateTime.Now.Year;
        }*/

        //-----------------------------------------------------

    }
}