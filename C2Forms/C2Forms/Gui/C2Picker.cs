﻿using System;
using System.Collections.Generic;
using Xamarin.Forms;
using C2Core;

namespace C2Forms
{
    public class C2PickerC : Picker
    {
        public TextAlignment align;
    }

    //-----------------------------------------------------

    public class C2Picker : StackLayout
    {
        public delegate void FPtr_on_selected_changed(C2Picker picker);
        private List<Int32> _ids;
        protected C2PickerC _picker;
        private Label _label;
        private StackLayout _horizontal_layout;
        private ContentView _image;
        private string _field_name;
        private bool _is_required;
        private Int32 _waiting_id;
        private bool _launch_on_item_change;
        private FPtr_on_selected_changed _func_on_selected_changed;

        //-----------------------------------------------------

        public C2Picker(string field_name, FPtr_on_selected_changed func_on_selected_changed) : base()
        {
            this.Spacing = 0.0;
            this._ids = new List<Int32>();
            this._horizontal_layout = new StackLayout();
            this._horizontal_layout.Orientation = StackOrientation.Horizontal;
            this._horizontal_layout.HorizontalOptions = LayoutOptions.FillAndExpand;
            this._horizontal_layout.Spacing = 0.0;
            this._horizontal_layout.Padding = new Thickness(C2Style.Entry_padding_horizontal, 0.0, C2Style.Entry_padding_horizontal, 0.0);
            this._horizontal_layout.BackgroundColor = C2Style.Control_background;       
            this._picker = new C2PickerC();
            this._picker.SelectedIndexChanged += i_On_selected_changed;
            this._picker.align = TextAlignment.Start;
            this._picker.HorizontalOptions = LayoutOptions.FillAndExpand;
            this._picker.VerticalOptions = LayoutOptions.Center;
            this._picker.HeightRequest = Device.OnPlatform(C2Style.Entry_font_size + 2.0 * (C2Style.Entry_padding_vertical), -1, -1);
            this._picker.align = TextAlignment.Start;
            this._horizontal_layout.Children.Add(this._picker);

            {
                ActivityIndicator activity;
                activity = new ActivityIndicator { WidthRequest = 18, HeightRequest = 18 };
                activity.IsRunning = true;
                this._image = new ContentView();
                this._image.Padding = new Thickness(0.0, 0.0, C2Style.Entry_padding_horizontal, 0.0);
                this._image.Content = activity;
                this._image.IsVisible = false;
                this._image.VerticalOptions = LayoutOptions.Center;
                this._image.HorizontalOptions = LayoutOptions.End;
                this._horizontal_layout.Children.Add(this._image);
            }

            this._label = new Label();
            this._label.Style = C2Style.Error_label_style;
            this.Children.Add(this._horizontal_layout);
            this.Children.Add(this._label);

            this._field_name = field_name;
            this._is_required = true;
            this._waiting_id = Int32.MinValue;
            this._launch_on_item_change = true;      
            this._func_on_selected_changed = func_on_selected_changed;

            if (this._is_required == true)
                this._picker.Title = field_name + " *";
            else
                this._picker.Title = field_name;

            this.i_Show_error_label(false);
        }

        //-----------------------------------------------------

        protected void i_Show_error_label(bool show)
        {
            if (show == true)
            {
                this._picker.BackgroundColor = C2Style.Control_error_background;
                this._horizontal_layout.BackgroundColor = C2Style.Control_error_background;
                this._label.IsVisible = true;
            }
            else
            {
                this._picker.BackgroundColor = C2Style.Control_background;
                this._horizontal_layout.BackgroundColor = C2Style.Control_background;
                this._label.IsVisible = false;
            }

            this.InvalidateMeasure();
        }

        //-----------------------------------------------------

        protected void i_On_selected_changed(object sender, EventArgs e)
        {
            if (this._launch_on_item_change == true)
            {
                this.Validate();
                if (this._func_on_selected_changed != null)
                    this._func_on_selected_changed(this);
            }
        }

        //-----------------------------------------------------

        public virtual bool Validate()
        {
            bool is_valid = true;

            if (this._picker.IsEnabled == true)
            {
                if (this._is_required == true && this._picker.SelectedIndex < 0)
                {
                    is_valid = false;
                    this._label.Text = String.Format("{0}: {1}", this._field_name, C2Entry.TXT_FIELD_REQUIRED);
                }
            }

            this.i_Show_error_label(!is_valid);
            return is_valid;
        }

        //-----------------------------------------------------

        public int Count
        {
            get
            {
                return this._picker.Items.Count;
            }
        }

        //-----------------------------------------------------

        public void Clear(bool delete_items)
        {
            this._launch_on_item_change = false;
            if (delete_items == true)
            {
                this._ids.Clear();
                this._picker.Items.Clear();
            }

            this._picker.SelectedIndex = -1;
            this.i_Show_error_label(false);
            this._launch_on_item_change = true;
        }

        //-----------------------------------------------------

        public new bool IsEnabled
        {
            get
            {
                return this._picker.IsEnabled;
            }

            set
            {
                base.IsEnabled = value;
                this._picker.IsEnabled = value;
            }
        }

        //-----------------------------------------------------

        public void Add(string label, Int32 id)
        {
            this._picker.Items.Add(label);
            this._ids.Add(id);
            C2Assert.Error(this._picker.Items.Count == this._ids.Count);
        }

        //-----------------------------------------------------

        private void i_Set_current_id(Int32 id)
        {
            this._launch_on_item_change = false;
            int i;
            for (i = 0; i < this._ids.Count; ++i)
            {
                if (id == this._ids[i])
                {
                    this._picker.SelectedIndex = i;
                    break;
                }
            }

            if (i == this._ids.Count)
                this._picker.SelectedIndex = -1;

            this._launch_on_item_change = true;
        }

        //-----------------------------------------------------

        public void Set_waiting_state()
        {
            this._picker.IsEnabled = false;
            this._image.IsVisible = true;
        }

        //-----------------------------------------------------

        public void Unset_waiting_state()
        {
            this._picker.IsEnabled = true;
            this._image.IsVisible = false;
            if (this._waiting_id != Int32.MinValue)
            {
                this.i_Set_current_id(this._waiting_id);
                this._waiting_id = Int32.MinValue;
            }
        }

        //-----------------------------------------------------

        public int SelectedIndex
        {
            get
            {
                return this._picker.SelectedIndex;
            }

            set
            {
                this._picker.SelectedIndex = value;
            }
        }

        //-----------------------------------------------------

        public Int32 CurrentId
        {
            get
            {
                if (this._picker.SelectedIndex >= 0)
                    return this._ids[this._picker.SelectedIndex];
                else
                    return Int32.MinValue;
            }

            set
            {
                try
                {
                    if (this._picker.IsEnabled == true && this._picker.Items.Count > 0)
                    {
                        this.i_Set_current_id(value);
                        this._waiting_id = Int32.MinValue;
                    }
                    else
                    {
                        this._waiting_id = value;
                    }
                }
                catch(Exception e)
                {
                    string st = e.Message;
                }
            }
        }

        //-----------------------------------------------------

        public string CurrentText
        {
            get
            {
                if (this._picker.SelectedIndex >= 0)
                    return this._picker.Items[this._picker.SelectedIndex];
                else
                    return "";
            }
        }

        //-----------------------------------------------------


    }
}