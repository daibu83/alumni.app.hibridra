﻿using System;
using Xamarin.Forms;

namespace C2Forms
{
    public class C2EditorC : Editor
    {
        public string _placeholder;

        public static readonly new BindableProperty KeyboardProperty =
           BindableProperty.Create<C2EditorC, Keyboard>(p => p.Keyboard, Keyboard.Default);


        public bool TipoKeyboard
        {
            get
            {
                return (bool)GetValue(KeyboardProperty);
            }
            set
            {
                SetValue(KeyboardProperty, value);
            }
        }
    }

    //-----------------------------------------------------

    public class C2Editor : StackLayout
    {
        protected C2EditorC _editor;
        protected Label _label;
        private uint _min_size;
        private uint _max_size;

        //-----------------------------------------------------

        public C2Editor(string placeholder, uint min_size, uint max_size, uint num_files_height,Keyboard tipoTeclado) : base()
        {
            this.Spacing = 0.0;
            this.Padding = new Thickness(C2Style.Entry_padding_horizontal, C2Style.Entry_padding_vertical / 2, C2Style.Entry_padding_horizontal, C2Style.Entry_padding_vertical / 2);
            this.BackgroundColor = C2Style.Control_background;
            this.HorizontalOptions = LayoutOptions.FillAndExpand;
            this.VerticalOptions = LayoutOptions.FillAndExpand;
            this._editor = new C2EditorC();
            if (tipoTeclado != null)
            {
                this._editor.Keyboard = tipoTeclado;
            }

            this._editor._placeholder = placeholder;
            this._editor.Completed += On_completed;
            this._editor.HorizontalOptions = LayoutOptions.FillAndExpand;
            this._editor.VerticalOptions = LayoutOptions.FillAndExpand;
            this._editor.Focused += this.i_On_focused;
            this._editor.HeightRequest = (double)(num_files_height + 1) * C2Style.Entry_font_size;
            this._label = new Label();
            this._label.Style = C2Style.Error_label_style;
            this.Children.Add(this._editor);
            this.Children.Add(this._label);
            this._min_size = min_size;
            this._max_size = max_size;
            this.i_Show_error_label(false);
        }

        //-----------------------------------------------------

        protected void i_Show_error_label(bool show)
        {
            if (show == true)
            {
                this._editor.BackgroundColor = C2Style.Control_error_background;
                this.BackgroundColor = C2Style.Control_error_background;
            }
            else
            {
                this._editor.BackgroundColor = C2Style.Control_background;
                this.BackgroundColor = C2Style.Control_background;
            }

            this._label.IsVisible = show;
        }

        //-----------------------------------------------------

        public virtual bool Validate()
        {
            bool is_valid = true;

            if (this._min_size > 0)
            {
                if (String.IsNullOrEmpty(this._editor.Text) == true)
                {
                    is_valid = false;
                    this._label.Text = C2Entry.TXT_FIELD_REQUIRED;
                }
                else if (this._editor.Text.Length < this._min_size)
                {
                    is_valid = false;
                    this._label.Text = String.Format(C2Entry.TXT_MIN_CHARACTERS, this._min_size);
                }
            }

            if (String.IsNullOrEmpty(this._editor.Text) == false && is_valid == true)
            {
                if (this._editor.Text.Length > this._max_size)
                {
                    is_valid = false;
                    this._label.Text = String.Format(C2Entry.TXT_MAX_CHARACTERS, this._max_size);
                }
            }

            this.i_Show_error_label(!is_valid);
            return is_valid;
        }

        //-----------------------------------------------------

        public virtual string Text
        {
            get
            {
                return this._editor.Text;
            }

            set
            {
                this._editor.Text = value;
            }
        }

        //-----------------------------------------------------

        private void i_On_focused(object sender, EventArgs e)
        {
            this.i_Show_error_label(false);
        }

        //-----------------------------------------------------

        public void Clear()
        {
            this._editor.Text = "";
            this.i_Show_error_label(false);
        }

        //-----------------------------------------------------

        protected void On_completed(object sender, EventArgs e)
        {
            this.Validate();
        }

        //-----------------------------------------------------


    }
}
