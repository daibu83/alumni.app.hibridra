﻿using System;
using Xamarin.Forms;

namespace C2Forms
{
    public class C2TapImage : Image
    {
        public delegate void FPtr_on_tap(C2TapImage sender);
        private FPtr_on_tap _func_on_tap;

        //-----------------------------------------------------

        public C2TapImage(ImageSource source, FPtr_on_tap func_on_tap) : base()
        {
            TapGestureRecognizer gesture;
            this.Source = source;
            this._func_on_tap = func_on_tap;
            gesture = new TapGestureRecognizer();
            gesture.Tapped += this.i_On_tap;
            this.GestureRecognizers.Add(gesture);
        }

        //-----------------------------------------------------

        private void i_On_tap(object sender, EventArgs e)
        {
            if (this._func_on_tap != null)
                this._func_on_tap(this);
        }

        //-----------------------------------------------------

    }
}
