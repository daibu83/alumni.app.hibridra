﻿using System;
using Xamarin.Forms;
using C2Core;

namespace C2Forms
{
    

    public class C2EntryC : Entry
    {
        public TextAlignment align;
        public bool autocapitalized;
    
        


        public static readonly new BindableProperty KeyboardProperty =
            BindableProperty.Create<C2EntryC, Keyboard>(p => p.Keyboard, Keyboard.Default);


        public bool TipoKeyboard
        {
            get
            {
                return (bool)GetValue(KeyboardProperty);
            }
            set
            {
                SetValue(KeyboardProperty, value);
            }
        }
    }

    //-----------------------------------------------------

    public abstract class C2Entry : StackLayout
    {
        public static string TXT_FIELD_REQUIRED = "";
        public static string TXT_MIN_CHARACTERS = "";
        public static string TXT_MAX_CHARACTERS = "";

        protected C2EntryC _entry;
        protected Label _label;
        //private StackLayout _horizontal_layout;
        private ContentView _image;
        protected string _field_name;
        private uint _min_size;
        private uint _max_size;

        //-----------------------------------------------------
        
        public C2Entry(string field_name, uint min_size, uint max_size, bool with_syncro_support, bool autocapitalized, Keyboard tipoTeclado) : base()
        {
            this.Spacing = 0.0;
            this.Padding = new Thickness(C2Style.Entry_padding_horizontal, 0.0, with_syncro_support == true ? 0.0 : C2Style.Entry_padding_horizontal, 0.0);
            this.BackgroundColor = C2Style.Control_background;
            /*this._horizontal_layout = new StackLayout();
            this._horizontal_layout.Orientation = StackOrientation.Horizontal;
            this._horizontal_layout.HorizontalOptions = LayoutOptions.FillAndExpand;
            this._horizontal_layout.Spacing = 0.0;
            this._horizontal_layout.Padding = new Thickness(C2Style.Entry_padding_horizontal, 0.0, with_syncro_support == true ? 0.0 : C2Style.Entry_padding_horizontal, 0.0);
            this._horizontal_layout.BackgroundColor = C2Style.Control_background;*/
            this._entry = new C2EntryC();
            if (tipoTeclado != null) {
                this._entry.Keyboard = tipoTeclado;
            }
        
            this._entry.TextColor = C2Style.Control_text;
            this._entry.TextChanged += this.i_On_text_changed;
            this._entry.Completed += this.i_On_completed;
            this._entry.Unfocused += this.i_On_completed;
            this._entry.Focused += this.i_On_focused;
            this._entry.HorizontalOptions = LayoutOptions.FillAndExpand;
            this._entry.VerticalOptions = LayoutOptions.Center;
            this._entry.HeightRequest = Device.OnPlatform(C2Style.Entry_font_size + 2.0 * (C2Style.Entry_padding_vertical), -1 , -1);
            this._entry.align = TextAlignment.Start;
            this._entry.autocapitalized = autocapitalized;
            //this._horizontal_layout.Children.Add(this._entry);

            /*if (with_syncro_support == true)
            {
                ActivityIndicator activity;
                activity = new ActivityIndicator { WidthRequest = 18, HeightRequest = 18 };
                activity.IsRunning = true;
                this._image = new ContentView();
                this._image.Padding = new Thickness(0.0, 0.0, C2Style.Entry_padding_horizontal, 0.0);
                this._image.Content = activity;
                this._image.IsVisible = false;
                this._image.VerticalOptions = LayoutOptions.Center;
                this._image.HorizontalOptions = LayoutOptions.End;
                this._horizontal_layout.Children.Add(this._image);
            }
            else*/
            {
                this._image = null;
            }

            this._label = new Label();
            this._label.Style = C2Style.Error_label_style;
            //this.Children.Add(this._horizontal_layout);
            this.Children.Add(this._entry);
            this.Children.Add(this._label);

            this._field_name = field_name;
            this._min_size = min_size;
            this._max_size = max_size;

            if (this._min_size > 0)
                if (field_name.Contains("*")) { this._entry.Placeholder = field_name; }
                else { this._entry.Placeholder = field_name + " *"; }
                
            else
                this._entry.Placeholder = field_name;

            this.i_Show_error_label(false);
        }

        //-----------------------------------------------------

        public String Placeholder
        {
            get
            {
                return this._entry.Placeholder;
            }

            set
            {
                this._entry.Placeholder = value;
            }
        }

        //-----------------------------------------------------

        protected void i_Show_error_label(bool show)
        {
            if (show == true)
            {
                this._entry.BackgroundColor = C2Style.Control_error_background;
                //this._horizontal_layout.BackgroundColor = C2Style.Control_error_background;
                this._label.IsVisible = true;
            }
            else
            {
                this._entry.BackgroundColor = C2Style.Control_background;
                //this._horizontal_layout.BackgroundColor = C2Style.Control_background;
                this._label.IsVisible = false;
            }

            this.InvalidateMeasure();
        }

        //-----------------------------------------------------

        public virtual bool Validate()
        {
            bool is_valid = true;
            C2Assert.Error(this.Children.Count == 1 || this.Children.Count == 2);

            if (this._min_size > 0)
            {
                if (String.IsNullOrEmpty(this._entry.Text) == true)
                {
                    is_valid = false;
                    this._label.Text = String.Format("{0}: {1}", this._field_name, C2Entry.TXT_FIELD_REQUIRED);
                }
                else if (this._entry.Text.Length < this._min_size)
                {
                    String msg;
                    is_valid = false;
                    msg = String.Format(C2Entry.TXT_MIN_CHARACTERS, this._min_size);
                    this._label.Text = String.Format("{0}: {1}", this._field_name, msg);
                }
            }

            if (String.IsNullOrEmpty(this._entry.Text) == false && is_valid == true)
            {
                if (this._entry.Text.Length > this._max_size)
                {
                    String msg;
                    is_valid = false;
                    msg = String.Format(C2Entry.TXT_MAX_CHARACTERS, this._max_size);
                    this._label.Text = String.Format("{0}: {1}", this._field_name, msg);
                }
            }

            this.i_Show_error_label(!is_valid);

            return is_valid;
        }

        //-----------------------------------------------------

        public virtual string Text
        {
            get
            {
                return this._entry.Text;
            }

            set
            {
                this._entry.Text = value;
            }
        }


        public virtual string Err_Validate_Text
        {
            get
            {
                return this._label.Text;
            }

            set
            {
                this._label.Text = value;
            }
        }

        //-----------------------------------------------------

        public virtual void Clear()
        {
            this._entry.Text = "";
            this.i_Show_error_label(false);
        }

        //-----------------------------------------------------

        public virtual void Clear_error()
        {
            this.i_Show_error_label(false);
        }

        public virtual void Show_error()
        {
            this.i_Show_error_label(true);
        }
        //-----------------------------------------------------

        public new bool IsEnabled
        {
            get
            {
                return this._entry.IsEnabled;
            }

            set
            {
                base.IsEnabled = value;
                this._entry.IsEnabled = value;
            }
        }

        //-----------------------------------------------------

        private void i_On_text_changed(object sender, EventArgs e)
        {
            if (String.IsNullOrEmpty(this._entry.Text) == true)
                this.i_Show_error_label(false);
        }

        //-----------------------------------------------------

        private void i_On_completed(object sender, EventArgs e)
        {
            this.Validate();
        }

        //-----------------------------------------------------

        private void i_On_focused(object sender, EventArgs e)
        {
            this.i_Show_error_label(false);
        }
        
        //-----------------------------------------------------

        protected void Set_waiting_state()
        {
            if (this._image != null)
            {
                //this._entry.IsEnabled = false;
                this._image.IsVisible = true;
            }
            /*else
            {
                C2Assert.Error(false);
            }*/
        }

        //-----------------------------------------------------

        protected void Unset_waiting_state()
        {
            if (this._image != null)
            {
                //this._entry.IsEnabled = true;
                this._image.IsVisible = false;
            }
            /*else
            {
                C2Assert.Error(false);
            }*/
        }

        //-----------------------------------------------------

            /*
        protected bool Is_waiting_state()
        {
            if (this._image != null)
            {
                return this._image.IsVisible;
            }
            else
            {
                return false;
            }
        }*/

        //-----------------------------------------------------

    }
}
