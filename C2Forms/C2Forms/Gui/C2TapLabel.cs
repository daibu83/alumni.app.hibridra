﻿using System;
using Xamarin.Forms;

namespace C2Forms
{
    class C2TapLabel : Label
    {
        public delegate void FPtr_on_tap(string text, Int32 id);
        private Int32 _id;
        private FPtr_on_tap _func_on_tap;

        //-----------------------------------------------------

        public C2TapLabel(string text, Int32 id, FPtr_on_tap func_on_tap) : base()
        {
            TapGestureRecognizer gesture;
            this.Text = text;
            this._id = id;
            this._func_on_tap = func_on_tap;
            gesture = new TapGestureRecognizer();
            gesture.Tapped += this.i_On_tap;
            this.GestureRecognizers.Add(gesture);
        }

        //-----------------------------------------------------

        public Int32 CurrentId
        {
            get
            {
                return this._id;
            }
        }

        //-----------------------------------------------------

        private void i_On_tap(object sender, EventArgs e)
        {
            if (this._func_on_tap != null)
                this._func_on_tap(this.Text, this._id);
        }

        //-----------------------------------------------------


    }
}
