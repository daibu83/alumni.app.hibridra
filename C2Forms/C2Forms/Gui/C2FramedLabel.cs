﻿using System;
using Xamarin.Forms;

namespace C2Forms
{
    public class C2Badge2 : Frame
    {
        private Label _label;
        //private Color _color;
        private int _width;
        private float _radious;

        //-----------------------------------------------------
        public static  BindableProperty TextProperty = BindableProperty.Create("Text", typeof(string), typeof(C2Badge2), "");

        public static  BindableProperty TextColorProperty = BindableProperty.Create("TextColor", typeof(Color), typeof(C2Badge2), Color.Blue);


        public string Text
        {
            get { return (string)GetValue(TextProperty); }
            set
            {
                SetValue(TextProperty, value);
                
            }
        }
        public Color TextColor
        {
            get { return (Color)GetValue(TextColorProperty); }
            set
            {
                SetValue(TextColorProperty, value);
            }
        }



        //public virtual string Texto
        //{
        //    get { return this._label.Text; }
        //    set { this._label.Text = " " + value + " "; }
        //}

        public static  BindableProperty BorderColorProperty = BindableProperty.Create("BorderColor", typeof(Color), typeof(C2Badge2), Color.White);

        public C2Badge2(Color background_color, Color text_color)
        {
            this.BackgroundColor = background_color;
            //this.OutlineColor = text_color;
            this.WidthRequest = 100;       
            this.Padding = new Thickness(0, 0, 0, 0);
            this.HorizontalOptions = LayoutOptions.Center;
            this._label = new Label();
            this._label.FontSize = C2Style.Label_font_size;
            this._label.TextColor = text_color;
            this._label.HorizontalOptions = LayoutOptions.Center;
            this._label.VerticalOptions = LayoutOptions.Center;
            this._label.SetBinding(Label.TextProperty, new Binding("Text", BindingMode.OneWay, source: this));
            this._label.SetBinding(Label.TextColorProperty, new Binding("TextColor", BindingMode.OneWay, source: this));
            this.Content = this._label;
        }

        //-----------------------------------------------------

        public virtual LayoutOptions Horizontal
        {
            get
            {
                return this._label.HorizontalOptions;
            }

            set
            {
                this._label.HorizontalOptions = value;
            }
        }

        public virtual LayoutOptions Vertical
        {
            get
            {
                return this._label.VerticalOptions;
            }

            set
            {
                this._label.VerticalOptions = value;
            }
        }

        public virtual Thickness Espacios {
            get
            {
                return this.Padding;
            }

            set
            {
                this.Padding = value;
            }
        }


        public virtual double FontSize
        {
            get
            {
                return this._label.FontSize;
            }

            set
            {
                this._label.FontSize =  value;
            }
        }


        //public virtual String Texto
        //{
        //    get
        //    {
        //        return this._label.Text;
        //    }

        //    set
        //    {
        //        this._label.Text = " " + value + " ";
        //    }
        //}

        //public  Color TextoColor
        //{
        //    get
        //    {
        //        return this._label.TextColor;
        //    }

        //    set
        //    {
        //        //this.OutlineColor = value;
        //        this._label.TextColor = value;
        //    }
        //}

        public virtual int BorderWidth
        {
            get
            {
                return this._width;
            }

            set
            {
                this._width = value;
            }
        }

        public virtual float Radious
        {
            get
            {
                return this._radious;
            }

            set
            {
                this._radious = value;
            }
        }

        public  Color BorderColor
        {
            /*get
            {
                return this._color;
            }

            set
            {
                this._color = value ;
                
            }*/
            get { return (Color)GetValue(BorderColorProperty); }
            set { SetValue(BorderColorProperty, value); }
        }

        //protected override void OnPropertyChanged(string propertyName = null)
        //{
        //    base.OnPropertyChanged(propertyName);

        //    if (propertyName == "Text")
        //    {
        //        Texto=Text;

                
        //    }
        //    else if(propertyName == "TextColor")
        //    {
        //        TextoColor = TextColor;
        //    }
        //}
        //-----------------------------------------------------

    }
}
