﻿namespace C2Forms
{
    public class C2PasswordEntry : C2Entry
    {
        //-----------------------------------------------------

        public C2PasswordEntry(string field_name, uint min_size, uint max_size) : base(field_name, min_size, max_size, false, false,null)
        {
            this._entry.IsPassword = true;
        }

        //-----------------------------------------------------


    }
}
