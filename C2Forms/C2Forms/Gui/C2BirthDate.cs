﻿using System;
using Xamarin.Forms;

namespace C2Forms
{
    public class C2DatePickerC : DatePicker
    {
        public Color background_color;
        public bool with_value;
        public string place_holder;
    }

    public class C2BirthDate : StackLayout
    {
        protected C2DatePickerC _picker;
        protected Label _label;
        private StackLayout _horizontal_layout;
        private string _field_name;
        private bool _is_required;

        //-----------------------------------------------------

        public C2BirthDate(string field_name, bool is_required) : base()
        {
            this.Spacing = 0.0;
            this._horizontal_layout = new StackLayout();
            this._horizontal_layout.Orientation = StackOrientation.Horizontal;
            this._horizontal_layout.HorizontalOptions = LayoutOptions.FillAndExpand;
            this._horizontal_layout.Spacing = 0.0;
            this._horizontal_layout.Padding = new Thickness(C2Style.Entry_padding_horizontal, 0.0, 0.0, 0.0);
            this._horizontal_layout.BackgroundColor = C2Style.Control_background;
            this._picker = new C2DatePickerC();
            this._picker.Format = "dd-MM-yyyy";
            this._picker.Date = new DateTime(1980, 1, 1);
            this._picker.MinimumDate = new DateTime(1971, 1, 1);
            this._picker.MaximumDate = DateTime.Now;
            this._picker.DateSelected += this.i_On_date;
            this._picker.HorizontalOptions = LayoutOptions.FillAndExpand;
            this._picker.VerticalOptions = LayoutOptions.Center;
            this._picker.HeightRequest = Device.OnPlatform(C2Style.Entry_font_size + 2.0 * (C2Style.Entry_padding_vertical), -1, -1);
            this._picker.with_value = false;
            this._horizontal_layout.Children.Add(this._picker);
            this._label = new Label();
            this._label.Style = C2Style.Error_label_style;
            this.Children.Add(this._horizontal_layout);
            this.Children.Add(this._label);

            this._field_name = field_name;
            this._field_name = field_name;
            this._is_required = is_required;
            //this._is_required = true;

            if (this._is_required == true)
                this._picker.place_holder = field_name + " *";
            else
                this._picker.place_holder = field_name;

            this.i_Show_error_label(false);
        }

        //-----------------------------------------------------

        protected void i_Show_error_label(bool show)
        {
            if (show == true)
            {
                this._picker.background_color = C2Style.Control_error_background;
                this._picker.BackgroundColor = C2Style.Control_error_background;
                this._horizontal_layout.BackgroundColor = C2Style.Control_error_background;
                this._label.IsVisible = true;
            }
            else
            {
                this._picker.background_color = C2Style.Control_background;
                this._picker.BackgroundColor = C2Style.Control_background;
                this._horizontal_layout.BackgroundColor = C2Style.Control_background;
                this._label.IsVisible = false;
            }
        }

        //-----------------------------------------------------

        private void i_On_date(object sender, EventArgs e)
        {
            this._picker.with_value = true;
            this.Validate();
        }

        //-----------------------------------------------------

        public DateTime Date
        {
            get
            {
                return this._picker.Date;
            }

            set
            {
                this._picker.with_value = true;
                this._picker.Date = value;
            }
        }

        //-----------------------------------------------------

        public string yyyy_mm_dd
        {
            get
            {
                return String.Format("{0:yyyy-MM-dd}", this.Date);
            }

            set
            {
                try
                {
                    this.Date = DateTime.ParseExact(value, "yyyy-MM-dd", null);
                }
                catch (Exception)
                {
                    this.Date = new DateTime(1980, 1, 1);
                }
            }
        }

        //-----------------------------------------------------

        public bool Validate()
        {
            if (this._is_required == true)
            {
                if (this._picker.with_value == false)
                    this._label.Text = String.Format("{0}: {1}", this._field_name, C2Entry.TXT_FIELD_REQUIRED);

                this.i_Show_error_label(!this._picker.with_value);
                return this._picker.with_value;
            }
            else {
                return true;
            }
            
        }

        //-----------------------------------------------------


    }
}
