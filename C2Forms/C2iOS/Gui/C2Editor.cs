﻿using System;
using UIKit;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;
using C2Forms;
using C2Forms.iOS;

[assembly: ExportRenderer(typeof(C2EditorC), typeof(C2EditorRenderer))]
namespace C2Forms.iOS
{
    class C2EditorRenderer : EditorRenderer
    {
        private static UIFont _font = null;

        //-----------------------------------------------------

        private static UIFont i_font()
        {
            if (C2EditorRenderer._font == null)
                C2EditorRenderer._font = UIFont.SystemFontOfSize((nfloat)C2Style.Entry_font_size);
            return C2EditorRenderer._font;
        }

        //-----------------------------------------------------

        protected override void OnElementChanged(ElementChangedEventArgs<Editor> e)
        {
            base.OnElementChanged(e);
            if (Control != null)
            {
                Control.Font = C2EditorRenderer.i_font();
                Control.TextColor = C2Style.Control_text.ToUIColor();
            }
        }

        //-----------------------------------------------------

    }
}
