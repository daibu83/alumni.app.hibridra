
using Xamarin.Forms.Platform.iOS;
using Xamarin.Forms;
using C2Forms.iOS;
using UIKit;

[assembly: ExportRenderer(typeof(ViewCell), typeof(ExtendedC2ListViewRenderer))]
namespace C2Forms.iOS
{
    public class ExtendedC2ListViewRenderer: ViewCellRenderer
    {
        public override UITableViewCell GetCell(Cell item, UITableViewCell reusableCell, UITableView tv)
        {
            var cell = base.GetCell(item, reusableCell, tv);
            tv.SeparatorStyle = UITableViewCellSeparatorStyle.None;
            return cell;

        }
    }
}