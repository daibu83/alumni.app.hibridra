
using Xamarin.Forms.Platform.iOS;
using Xamarin.Forms;
using C2Forms.iOS;

[assembly: ExportRenderer(typeof(SearchBar), typeof(ExtendedSearchBarRenderer))]
namespace C2Forms.iOS
{
    public class ExtendedSearchBarRenderer : SearchBarRenderer
    {
        protected override void OnElementPropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            base.OnElementPropertyChanged(sender, e);
            
            if (Control != null)
            {
                Control.ShowsCancelButton = false;
            }
        }
    }
}