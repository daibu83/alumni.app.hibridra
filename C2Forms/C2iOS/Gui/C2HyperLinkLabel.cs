using System;
using UIKit;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;
using C2Forms;
using C2Forms.iOS;
using Foundation;

[assembly: ExportRenderer(typeof(C2HyperLinkLabel), typeof(C2HyperLinkLabelRenderer))]
namespace C2Forms.iOS
{
    class C2HyperLinkLabelRenderer : LabelRenderer
    {

        protected override void OnElementChanged(ElementChangedEventArgs<Label> e)
        {
            base.OnElementChanged(e);

            if (e.NewElement as C2HyperLinkLabel != null)
            {
                var view = e.NewElement as C2HyperLinkLabel;
                UpdateUi(view);
            }

            
            
        }

        /// <summary>
        /// Raises the element property changed event.
        /// </summary>
        /// <param name="sender">Sender</param>
        /// <param name="e">The event arguments</param>
        protected override void OnElementPropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            base.OnElementPropertyChanged(sender, e);

            
            if (Element as C2HyperLinkLabel != null) {
                var view = Element as C2HyperLinkLabel;
                if (view != null &&
                e.PropertyName == C2HyperLinkLabel.IsUnderlineProperty.PropertyName)
                {
                    UpdateUi(view);
                }
            }

            
        }

        /// <summary>
        /// Updates the UI.
        /// </summary>
        /// <param name="view">
        /// The view.
        /// </param>
        private void UpdateUi(C2HyperLinkLabel view)
        {
            
            var underline = view.IsUnderline ? NSUnderlineStyle.Single : NSUnderlineStyle.None;
            
            if (Control != null && view.Text!=null)
            {
                Control.AttributedText = new NSMutableAttributedString(view.Text,
                Control.Font,
                underlineStyle: underline);
                ;

            }
        }

        
    }
}
