﻿using System;
using UIKit;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;
using C2Forms;
using C2Forms.iOS;

[assembly: ExportRenderer(typeof(C2Button), typeof(C2ButtonRenderer))]
namespace C2Forms.iOS
{
    class C2ButtonRenderer : ButtonRenderer
    {
        private static UIFont _font = null;
        

        //-----------------------------------------------------

        private static UIFont i_font()
        {
            if (C2ButtonRenderer._font == null)
                C2ButtonRenderer._font = UIFont.SystemFontOfSize((nfloat)C2Style.Button_font_size);
            return C2ButtonRenderer._font;
        }


        //-----------------------------------------------------

        protected override void OnElementChanged(ElementChangedEventArgs<Xamarin.Forms.Button> e)
        {
            base.OnElementChanged(e);
            if (Control != null)
            {
                Control.Font = C2ButtonRenderer.i_font();

                //(C2Button)this.Element;
                //var labelTextField = (UILabel)Control;
                //labelTextField.TextColor = el.Color.ToUIColor();
                if ((C2Button) this.Element != null)
                {
                    C2Button elem;
                    elem = (C2Button)this.Element;
                    Control.SetTitleColor(elem.Color.ToUIColor(), UIControlState.Normal);
                }
                else {
                    Control.SetTitleColor(C2Style.Button_text.ToUIColor(), UIControlState.Normal);
                }
                
                
            }
        }

        //-----------------------------------------------------

    }
}