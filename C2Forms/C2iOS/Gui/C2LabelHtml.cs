using System;
using UIKit;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;
using C2Forms;
using C2Forms.iOS;
using Foundation;
using System.ComponentModel;

[assembly: ExportRenderer(typeof(C2LabelHtml), typeof(C2LabelHtmlRenderer))]
namespace C2Forms.iOS
{
    class C2LabelHtmlRenderer : LabelRenderer
    {
        private static void i_textHtml(UILabel control,Label element)
        {
            var attr = new NSAttributedStringDocumentAttributes();
            var nsError = new NSError();
            attr.DocumentType = NSDocumentType.HTML;

            //var myHtmlData = NSData.FromString(Element.Text, NSStringEncoding.Unicode);
            nfloat r, g, b, a;
            control.TextColor.GetRGBA(out r, out g, out b, out a);
            var textColor = string.Format("#{0:X2}{1:X2}{2:X2}", (int)(r * 255.0), (int)(g * 255.0), (int)(b * 255.0));

            var font = control.Font;
            var fontName = font.Name;
            var fontSize = font.PointSize;
            var htmlContents = "<span style=\"font-family: '" + fontName + "'; color: " + textColor + "; font-size: " + fontSize + "\">" + element.Text + "</span>";
            var myHtmlData = NSData.FromString(htmlContents, NSStringEncoding.Unicode);
            control.Lines = 0;
            control.AttributedText = new NSAttributedString(myHtmlData, attr, ref nsError);
        }
        protected override void OnElementChanged(ElementChangedEventArgs<Label> e)
        {
            base.OnElementChanged(e);

            if (Control != null && Element != null && !string.IsNullOrWhiteSpace(Element.Text))
            {
                i_textHtml(Control, Element);
            }
        }

        protected override void OnElementPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            base.OnElementPropertyChanged(sender, e);

            if (e.PropertyName == Label.TextProperty.PropertyName)
            {
                if (Control != null && Element != null && !string.IsNullOrWhiteSpace(Element.Text))
                {

                    i_textHtml(Control, Element);

                }
            }
        }
    }
}