﻿using System;
using UIKit;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;
using C2Forms;
using C2Forms.iOS;
using System.ComponentModel;

[assembly: ExportRenderer(typeof(C2EntryC), typeof(C2EntryRenderer))]
namespace C2Forms.iOS
{
    class C2EntryRenderer : EntryRenderer
    {
        private static UIFont _font = null;

        //-----------------------------------------------------

        private static UIFont i_font()
        {
            if (C2EntryRenderer._font == null)
                C2EntryRenderer._font = UIFont.SystemFontOfSize((nfloat)C2Style.Entry_font_size);
            return C2EntryRenderer._font;
        }

        //-----------------------------------------------------

        private void i_Update_control()
        {
            if (Control != null && Element != null)
            {
                Control.AttributedPlaceholder = new Foundation.NSAttributedString(Element.Placeholder, new UIStringAttributes() { ForegroundColor = C2Style.Control_placeholder.ToUIColor() });

                switch (((C2EntryC)Element).align)
                {
                    case Xamarin.Forms.TextAlignment.Center:
                        Control.TextAlignment = UIKit.UITextAlignment.Center;
                        break;
                    case Xamarin.Forms.TextAlignment.Start:
                        Control.TextAlignment = UIKit.UITextAlignment.Left;
                        break;
                    case Xamarin.Forms.TextAlignment.End:
                        Control.TextAlignment = UIKit.UITextAlignment.Right;
                        break;
                }

                if (((C2EntryC)Element).autocapitalized == true)
                    Control.AutocapitalizationType = UITextAutocapitalizationType.Sentences;
                else
                    Control.AutocapitalizationType = UITextAutocapitalizationType.None;
            }
        }

        //-----------------------------------------------------

        protected override void OnElementChanged(ElementChangedEventArgs<Entry> e)
        {
            base.OnElementChanged(e);
            if (Control != null)
            {
                Control.Font = C2EntryRenderer.i_font();
                Control.BorderStyle = UITextBorderStyle.None;
                Control.TextColor = C2Style.Control_text.ToUIColor();
                this.i_Update_control();
            }
        }

        //-----------------------------------------------------

        protected override void OnElementPropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            base.OnElementPropertyChanged(sender, e);
            
            this.i_Update_control();
        }

    }

}
