﻿using System;
using UIKit;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;
using C2Forms;
using C2Forms.iOS;

[assembly: ExportRenderer(typeof(C2PickerC), typeof(C2PickerRenderer))]
namespace C2Forms.iOS
{
    class C2PickerRenderer : PickerRenderer
    {
        private static UIFont _font = null;

        //-----------------------------------------------------

        private static UIFont i_font()
        {
            if (C2PickerRenderer._font == null)
                C2PickerRenderer._font = UIFont.SystemFontOfSize((nfloat)C2Style.Entry_font_size);
            return C2PickerRenderer._font;
        }

        //-----------------------------------------------------

        private void i_Update_control()
        {
            if (Control != null && Element != null)
            {
                Control.AttributedPlaceholder = new Foundation.NSAttributedString(Element.Title, new UIStringAttributes() { ForegroundColor = C2Style.Control_placeholder.ToUIColor() });

                switch (((C2PickerC)Element).align)
                {
                    case Xamarin.Forms.TextAlignment.Center:
                        Control.TextAlignment = UIKit.UITextAlignment.Center;
                        break;
                    case Xamarin.Forms.TextAlignment.Start:
                        Control.TextAlignment = UIKit.UITextAlignment.Left;
                        break;
                    case Xamarin.Forms.TextAlignment.End:
                        Control.TextAlignment = UIKit.UITextAlignment.Right;
                        break;
                }
            }
        }

        //-----------------------------------------------------

        protected override void OnElementChanged(ElementChangedEventArgs<Picker> e)
        {
            base.OnElementChanged(e);
            if (Control != null)
            {
                Control.Font = C2PickerRenderer.i_font();
                Control.BorderStyle = UITextBorderStyle.None;
                Control.TextColor = C2Style.Control_text.ToUIColor();
                this.i_Update_control();
            }
        }

        //-----------------------------------------------------

        protected override void OnElementPropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            base.OnElementPropertyChanged(sender, e);
            this.i_Update_control();
        }

        //-----------------------------------------------------


    }
}
