﻿using System.ComponentModel;
using UIKit;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;
using C2Forms;
using C2Forms.iOS;

[assembly: ExportRenderer(typeof(C2RoundedBox), typeof(C2RoundedBoxRenderer))]
namespace C2Forms.iOS
{
    public class C2RoundedBoxRenderer : BoxRenderer
    {
        private void i_Update_corner_radius(C2RoundedBox box)
        {
            Layer.CornerRadius = (float)box.CornerRadius;
            Layer.BorderWidth = 2.0f;
            Layer.BorderColor = box.borderColor.ToCGColor();
        }

        //-----------------------------------------------------

        protected override void OnElementChanged(ElementChangedEventArgs<BoxView> e)
        {
            base.OnElementChanged(e);

            if (Element != null)
            {
                Layer.MasksToBounds = true;
                this.i_Update_corner_radius((C2RoundedBox)Element);
            }
        }

        //-----------------------------------------------------

        protected override void OnElementPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            base.OnElementPropertyChanged(sender, e);

            if (e.PropertyName == C2RoundedBox.CornerRadiusProperty.PropertyName)
                this.i_Update_corner_radius((C2RoundedBox)Element);
        }

        //-----------------------------------------------------


    }
}

