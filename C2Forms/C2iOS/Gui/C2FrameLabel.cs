using CoreGraphics;

using UIKit;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;
using C2Forms;
using C2Forms.iOS;
using System.Drawing;

[assembly: ExportRenderer(typeof(C2Badge2), typeof(C2Badge2Renderer))]
namespace C2Forms.iOS
{
    class C2Badge2Renderer : FrameRenderer
    {
        protected override void OnElementChanged(ElementChangedEventArgs<Frame> e)
        {
            base.OnElementChanged(e);
            var elem = (C2Badge2)this.Element;
            if (elem != null)
            {
                // Border
                //this.Layer.CornerRadius = 2;
                //this.Layer.Bounds.Inset(2, 2);
                this.Layer.CornerRadius = (float)elem.Radious;
                this.Layer.Bounds.Inset((int)elem.BorderWidth, (int)elem.BorderWidth);
                this.Layer.BorderColor = elem.BorderColor.ToCGColor();
                this.Layer.BorderWidth = (float)elem.BorderWidth;


                // Shadow
                //this.Layer.ShadowColor = UIColor.DarkGray.CGColor;
                this.Layer.ShadowColor = UIColor.White.CGColor;
                //this.Layer.ShadowOpacity = 0.4f;
                //this.Layer.ShadowRadius = 2.0f;
                //this.Layer.ShadowOffset = new SizeF(0, 0); //(2.0f, 2.0f);
                //this.Layer.MasksToBounds = false;
                //Layer.BorderWidth = 2;
            }

        }
        protected override void OnElementPropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            base.OnElementPropertyChanged(sender, e);



            if (this.Element != null)
            {
                var elem = (C2Badge2)this.Element;
                if (elem != null)
                {
                    // Border
                    //this.Layer.CornerRadius = 2;
                    //this.Layer.Bounds.Inset(2, 2);
                    this.Layer.CornerRadius = (float)elem.Radious;
                    this.Layer.Bounds.Inset((int)elem.BorderWidth, (int)elem.BorderWidth);
                    this.Layer.BorderColor = elem.BorderColor.ToCGColor();
                    this.Layer.BorderWidth = (float)elem.BorderWidth;


                    // Shadow
                    //this.Layer.ShadowColor = UIColor.DarkGray.CGColor;
                    this.Layer.ShadowColor = UIColor.White.CGColor;
                    //this.Layer.ShadowOpacity = 0.4f;
                    //this.Layer.ShadowRadius = 2.0f;
                    //this.Layer.ShadowOffset = new SizeF(0, 0); //(2.0f, 2.0f);
                    //this.Layer.MasksToBounds = false;
                    //Layer.BorderWidth = 2;
                }
            }





        }

    }
}