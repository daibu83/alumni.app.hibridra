﻿using System;
using UIKit;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;
using C2Forms;
using C2Forms.iOS;

[assembly: ExportRenderer(typeof(C2DatePickerC), typeof(C2DatePickerRenderer))]
namespace C2Forms.iOS
{
    class C2DatePickerRenderer : DatePickerRenderer
    {
        private static UIFont _font = null;

        //-----------------------------------------------------

        private double i_Dp_to_pixels(double dp)
        {
            return dp;
        }

        //-----------------------------------------------------

        private static UIFont i_font()
        {
            if (C2DatePickerRenderer._font == null)
                C2DatePickerRenderer._font = UIFont.SystemFontOfSize((nfloat)C2Style.Entry_font_size);
            return C2DatePickerRenderer._font;
        }

        //-----------------------------------------------------

        private void i_Update_control()
        {
            if (Control != null && Element != null)
            {
                if (((C2DatePickerC)Element).with_value == true)
                {
                    string date_text = String.Format("{0:" + Element.Format + "}", Element.Date);
                    Control.TextColor = C2Style.Control_text.ToUIColor();
                    Control.Text = date_text;
                }
                else
                {
                    Control.TextColor = C2Style.Control_placeholder.ToUIColor();
                    Control.Text = ((C2DatePickerC)Element).place_holder;
                }
            }
        }

        //-----------------------------------------------------

        protected override void OnElementChanged(ElementChangedEventArgs<DatePicker> e)
        {
            base.OnElementChanged(e);
            if (Control != null)
            {
                Control.Font = C2DatePickerRenderer.i_font();
                Control.BorderStyle = UITextBorderStyle.None;
                Control.TextColor = C2Style.Control_text.ToUIColor();
                this.i_Update_control();
            }
        }

        //-----------------------------------------------------

        protected override void OnElementPropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            base.OnElementPropertyChanged(sender, e);
            this.i_Update_control();
        }

        //-----------------------------------------------------

    }
}


