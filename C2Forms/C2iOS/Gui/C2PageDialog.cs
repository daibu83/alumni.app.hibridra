using System;
using UIKit;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;
using C2Forms;
using C2Forms.iOS;
using CoreAnimation;
using System.Threading.Tasks;

[assembly: ExportRenderer(typeof(C2PageDialog), typeof(C2PageDialogRenderer))]
namespace C2Forms.iOS
{
    class C2PageDialogRenderer : PageRenderer
    {
        



        protected override void OnElementChanged(VisualElementChangedEventArgs e)
        {
            base.OnElementChanged(e);

            // UI settings





            //  this.ModalPresentationStyle = UIModalPresentationStyle.OverCurrentContext;
            // this.View.BackgroundColor = UIColor.Clear;
            //this.View.Alpha = 0;
            //this.View.Opaque = false;



            this.View.BackgroundColor = UIColor.FromRGBA(0, 0, 0, 0.4f);
        //    this.ModalPresentationStyle = UIModalPresentationStyle.Custom;
            this.View.Opaque = false;

            /*NativeView.BackgroundColor = new UIColor(new CGColor(0xff, 0, 0));
            this.View.Alpha = 0;
            this.View.Opaque = false;*/

            //            var popoverController = new UIPopoverController(this.ViewController);
            //          popoverController.PopoverContentSize = new SizeF(320, 180);
            //popoverController.PresentFromRect(NativeView.Frame, NativeView, UIPopoverArrowDirection.Up, true);
            //        popoverController.PresentFromRect(new RectangleF(new PointF(30, 30), new SizeF(320, 180)), NativeView, UIPopoverArrowDirection.Any, true);


            //var window = UIApplication.SharedApplication.KeyWindow;
            //PresentViewController(window.RootViewController, true, null);
            /* var window = UIApplication.SharedApplication.KeyWindow;
             var vc = window.RootViewController;
             while (vc.PresentedViewController != null)
             {
                 vc = vc.PresentedViewController;
             }

             PresentViewController(vc, true, null);*/

            // Present on vc.

            /*this.ModalPresentationStyle = UIModalPresentationStyle.CurrentContext;
            this.View.BackgroundColor = UIColor.FromRGBA(0, 0, 0, 0.4f);
            this.View.Opaque = false;

            PresentViewController(this, true, null);*/


            /* this.View.Alpha = 1.0f;
             this.View.BackgroundColor = UIColor.FromRGBA(0,0,0, 0.4f);
             this.View.Opaque = false;*/

            //labelTextField.TextColor = el.Color.ToUIColor();


        }

        /*public override void WillMoveToParentViewController(UIViewController parent)
        {
            base.WillMoveToParentViewController(parent);

            if (parent != null)
            {
                parent.ModalInPopover = true;
                parent.ModalPresentationStyle = UIModalPresentationStyle.FormSheet;
            }
        }*/

        public class SlidingHorizontalTransition : UIViewControllerTransitioningDelegate
        {
            private TransitionAnimator _animator;

            public override IUIViewControllerAnimatedTransitioning GetAnimationControllerForPresentedController(UIViewController presented, UIViewController presenting, UIViewController source)
            {
                _animator = new TransitionAnimator();
                _animator.Presenting = true;
                return _animator;
            }

            public override IUIViewControllerAnimatedTransitioning GetAnimationControllerForDismissedController(UIViewController dismissed)
            {
                _animator = new TransitionAnimator();
                return _animator;
            }

            private class TransitionAnimator : UIViewControllerAnimatedTransitioning
            {
                public bool Presenting;

                public override double TransitionDuration(IUIViewControllerContextTransitioning transitionContext)
                {
                    return 0.25f;
                }

                public override void AnimateTransition(IUIViewControllerContextTransitioning transitionContext)
                {
                    var containerView = transitionContext.ContainerView;
                    var toVC = transitionContext.GetViewControllerForKey(UITransitionContext.ToViewControllerKey);
                    var fromVC = transitionContext.GetViewControllerForKey(UITransitionContext.FromViewControllerKey);

                    if (Presenting)
                    {
                        fromVC.View.UserInteractionEnabled = false;

                        containerView.AddSubview(fromVC.View);
                        containerView.AddSubview(toVC.View);

                        var endFrame = toVC.View.Frame;
                        var frame = toVC.View.Frame;

                        frame.X = frame.Width;
                        toVC.View.Frame = frame;

                        UIView.Animate(TransitionDuration(transitionContext), 0, UIViewAnimationOptions.CurveEaseIn, () =>
                        {
                            toVC.View.Frame = endFrame;
                        },
                            () =>
                            {
                                transitionContext.CompleteTransition(true);
                            });
                    }
                    else
                    {
                        toVC.View.UserInteractionEnabled = true;

                        containerView.Add(toVC.View);
                        containerView.Add(fromVC.View);

                        var endFrame = fromVC.View.Frame;
                        endFrame.X = endFrame.Width;

                        UIView.Animate(TransitionDuration(transitionContext), 0, UIViewAnimationOptions.CurveEaseOut, async () =>
                        {
                            fromVC.View.Frame = endFrame;
                            await Task.Delay(100);
                            UIApplication.SharedApplication.KeyWindow.AddSubview(toVC.View);

                        }, () =>
                        {
                            transitionContext.CompleteTransition(true);
                        });
                    }
                }
            }
        }

        public override void WillMoveToParentViewController(UIViewController parent)
        {
            base.WillMoveToParentViewController(parent);
            if (parent != null)
            {
                parent.View.BackgroundColor = UIColor.Gray;
                parent.ModalPresentationStyle = UIKit.UIModalPresentationStyle.Custom;
                parent.TransitioningDelegate = new SlidingHorizontalTransition();

            }
        }


        public override void ViewDidLayoutSubviews()
        {
            base.ViewDidLayoutSubviews();

            // Set size manualy because page is rendered outside of Visual tree
            SetElementSize(new Size(View.Bounds.Width, View.Bounds.Height));
        }

    }

    
}