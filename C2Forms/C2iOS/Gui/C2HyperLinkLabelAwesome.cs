using System;
using UIKit;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;
using C2Forms;
using C2Forms.iOS;
using Foundation;

[assembly: ExportRenderer(typeof(C2HyperLinkLabelAwesome), typeof(C2HyperLinkLabelAwesomeRenderer))]
namespace C2Forms.iOS
{
    class C2HyperLinkLabelAwesomeRenderer : LabelRenderer
    {

        protected override void OnElementChanged(ElementChangedEventArgs<Label> e)
        {
            base.OnElementChanged(e);

            if (e.NewElement as C2HyperLinkLabelAwesome != null)
            {
                var view = e.NewElement as C2HyperLinkLabelAwesome;
                UpdateUi(view);
            }

            
            
        }

        /// <summary>
        /// Raises the element property changed event.
        /// </summary>
        /// <param name="sender">Sender</param>
        /// <param name="e">The event arguments</param>
        protected override void OnElementPropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            base.OnElementPropertyChanged(sender, e);

            
            if (Element as C2HyperLinkLabelAwesome != null) {
                var view = Element as C2HyperLinkLabelAwesome;
                if (view != null &&
                e.PropertyName == C2HyperLinkLabelAwesome.IsUnderlineProperty.PropertyName)
                {
                    UpdateUi(view);
                }
            }

            
        }

        /// <summary>
        /// Updates the UI.
        /// </summary>
        /// <param name="view">
        /// The view.
        /// </param>
        private void UpdateUi(C2HyperLinkLabelAwesome view)
        {
            
            var underline = view.IsUnderline ? NSUnderlineStyle.Single : NSUnderlineStyle.None;
            
            if (Control != null && view.Text!=null)
            {
                Control.AttributedText = new NSMutableAttributedString(view.Text,
                Control.Font,
                underlineStyle: underline);
                ;

            }
        }

        
    }
}
