﻿using UIKit;
using C2Forms.iOS;

[assembly: Xamarin.Forms.Dependency(typeof(DeviceServiceIOS))]
namespace C2Forms.iOS
{
    class DeviceServiceIOS : IDeviceService
    {
        public string DeviceId()
        {
            string str = UIDevice.CurrentDevice.IdentifierForVendor.ToString();
            return str;
        }

        //-----------------------------------------------------

        public int AndroidAPILevel()
        {
            return -1;
        }

    }
}
