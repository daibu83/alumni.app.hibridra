﻿using System;
using Foundation;
using UIKit;
using C2Forms.iOS;

[assembly: Xamarin.Forms.Dependency(typeof(PickImageServiceIOS))]
namespace C2Forms.iOS
{
    public class PickImageServiceIOS : UIViewController, IPickImageService
    {
        private UIImagePickerController _image_picker;
        private FPtr_on_image_pick _func_on_image_pick;
        public static UIApplication app = null;

        //-----------------------------------------------------

        public void Pick_image(string text, FPtr_on_image_pick func_on_image_pick)
        {
            if (PickImageServiceIOS.app != null)
            {
                UIViewController controller = PickImageServiceIOS.app.KeyWindow.RootViewController;

                if (controller == null)
                    return;

                while (controller.PresentedViewController != null)
                    controller = controller.PresentedViewController;

                this._image_picker = new UIImagePickerController();
                this._image_picker.SourceType = UIImagePickerControllerSourceType.PhotoLibrary;
                this._image_picker.MediaTypes = UIImagePickerController.AvailableMediaTypes(UIImagePickerControllerSourceType.PhotoLibrary);
                this._image_picker.FinishedPickingMedia += i_On_picking;
                this._image_picker.Canceled += i_On_cancel;
                this._func_on_image_pick = func_on_image_pick;
                controller.PresentViewController(this._image_picker, true,null);
            }
        }

        //-----------------------------------------------------

        private void i_On_cancel(object sender, EventArgs e)
        {
            if (this._func_on_image_pick != null)
                this._func_on_image_pick(null);

            this._image_picker.DismissModalViewController(true);
        }

        //-----------------------------------------------------

        private void i_On_picking(object sender, UIImagePickerMediaPickedEventArgs e)
        {
            byte[] image_data = null;
            bool isImage = false;
            switch (e.Info[UIImagePickerController.MediaType].ToString())
            {
                case "public.image":
                    isImage = true;
                    break;
                case "public.video":
                    break;
            }

            if (isImage)
            {
                UIImage image = e.Info[UIImagePickerController.OriginalImage] as UIImage;
                if (image != null)
                {
                    UIImage scaled_image = null;

                    if (image.Size.Width > 200.0f)
                    {
                        try
                        {
                            CoreGraphics.CGSize new_size = new CoreGraphics.CGSize();
                            new_size.Width = 200.0f;
                            new_size.Height = image.Size.Height * new_size.Width / image.Size.Width;
                            UIGraphics.BeginImageContext(new_size);
                            image.Draw(new CoreGraphics.CGRect(0, 0, new_size.Width, new_size.Height));
                            scaled_image = UIGraphics.GetImageFromCurrentImageContext();
                            UIGraphics.EndImageContext();
                        }
                        catch (Exception)
                        {
                            scaled_image = image;
                        }
                    }
                    else
                    {
                        scaled_image = image;
                    }

                    if (scaled_image != null)
                    {
                        NSData data = scaled_image.AsJPEG(0.8f);
                        if (data != null)
                        {
                            image_data = new byte[data.Length];
                            System.Runtime.InteropServices.Marshal.Copy(data.Bytes, image_data, 0, Convert.ToInt32(data.Length));
                        }
                    }
                }


                /*
                UIImage image = e.Info[UIImagePickerController.OriginalImage] as UIImage;
                if (image != null)
                {
                    NSData data = image.AsPNG();
                    if (data != null)
                    {
                        image_data = new byte[data.Length];
                        System.Runtime.InteropServices.Marshal.Copy(data.Bytes, image_data, 0, Convert.ToInt32(data.Length));
                    }
                }*/


            }

            if (this._func_on_image_pick != null)
                this._func_on_image_pick(image_data);

            this._image_picker.DismissModalViewController(true);
        }

        //-----------------------------------------------------


    }
}
