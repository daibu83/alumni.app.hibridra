﻿using C2Forms.iOS;
using System.Threading;

[assembly: Xamarin.Forms.Dependency(typeof(PlatformCultureInfo))]
namespace C2Forms.iOS
{
    class PlatformCultureInfo : ICultureInfo
    {

        #region ICultureInfo implementation

        public System.Globalization.CultureInfo CurrentCulture
        {
            get
            {
                return Thread.CurrentThread.CurrentCulture;
            }
            set
            {
                Thread.CurrentThread.CurrentCulture = value;
            }
        }

        public System.Globalization.CultureInfo CurrentUICulture
        {
            get
            {
                return Thread.CurrentThread.CurrentUICulture;
            }
            set
            {
                Thread.CurrentThread.CurrentUICulture = value;
            }
        }

        #endregion
    }
}
