﻿using System;
using Android.Widget;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;
using C2Forms;
using C2Forms.Droid;

[assembly: ExportRenderer(typeof(C2DatePickerC), typeof(C2DatePickerRenderer))]
namespace C2Forms.Droid
{
    class C2DatePickerRenderer : DatePickerRenderer
    {
        private int i_Dp_to_pixels(double dp)
        {
            return (int)(dp * Resources.DisplayMetrics.Density);
        }

        //-----------------------------------------------------

        private void i_Update_control()
        {
            if (Control != null && Element != null)
            {
                Control.SetBackgroundColor(((C2DatePickerC)Element).background_color.ToAndroid());

                if (((C2DatePickerC)Element).with_value == true)
                {
                    string date_text = String.Format("{0:" + Element.Format + "}", Element.Date);
                    Control.SetTextColor(C2Style.Control_text.ToAndroid());
                    Control.SetText(date_text, TextView.BufferType.Normal);
                }
                else
                {
                    Control.SetTextColor(C2Style.Control_placeholder.ToAndroid());
                    Control.SetText(((C2DatePickerC)Element).place_holder, TextView.BufferType.Normal);
                }
            }
        }

        //-----------------------------------------------------

        protected override void OnElementChanged(ElementChangedEventArgs<Xamarin.Forms.DatePicker> e)
        {
            base.OnElementChanged(e);
            if (Control != null)
            {
                int padding_vertical = i_Dp_to_pixels(C2Style.Entry_padding_vertical);
                Control.TextSize = (float)C2Style.Entry_font_size;
                Control.SetPadding(0, padding_vertical, 0, padding_vertical);
                this.i_Update_control();
            }

        }

        //-----------------------------------------------------

        protected override void OnElementPropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            base.OnElementPropertyChanged(sender, e);
            this.i_Update_control();
        }
    }
}
