using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;
using C2Forms;
using C2Forms.Droid;
using Android.Graphics;
using Android.Widget;

[assembly: ExportRenderer(typeof(C2HyperLinkLabel), typeof(C2HyperLinkLabelRenderer))]
namespace C2Forms.Droid
{
    class C2HyperLinkLabelRenderer : LabelRenderer
    {

        public C2HyperLinkLabelRenderer()
        {
        }
        //-----------------------------------------------------

        protected override void OnElementChanged(ElementChangedEventArgs<Xamarin.Forms.Label> e)
        {
            base.OnElementChanged(e);

            if (Control != null)
            {
                if ((C2HyperLinkLabel)Element != null) {
                    var view = (C2HyperLinkLabel)Element;
                    var control = Control;
                    UpdateUi(view, control);
                }
                
            }
        }

        //-----------------------------------------------------

        /// <summary>
        /// Updates the UI.
        /// </summary>
        /// <param name="view">The view.</param>
        /// <param name="control">The control.</param>
        void UpdateUi(C2HyperLinkLabel view, TextView control)
        {

            if (view.IsUnderline)
            {
                control.PaintFlags = control.PaintFlags | PaintFlags.UnderlineText;
            }

        }

    }
}

