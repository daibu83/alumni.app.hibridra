using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;
using C2Forms;
using C2Forms.Droid;
using Android.Text;
using Android.Widget;
using System.ComponentModel;
using Android.Text.Method;

[assembly: ExportRenderer(typeof(C2LabelHtml), typeof(C2LabelHtmlRenderer))]
namespace C2Forms.Droid
{
    class C2LabelHtmlRenderer: LabelRenderer
    {
        protected override void OnElementChanged(ElementChangedEventArgs<Label> e)
        {
            base.OnElementChanged(e);
            //String cdata;
            if (Control != null && Element != null)
            {
                //cdata = "<![CDATA[" + Element.Text + "]>";

                Control?.SetText(Html.FromHtml(Element.Text), TextView.BufferType.Spannable);
                // Control?.SetText(Html.FromHtml(Element.Text), TextView.BufferType.Spannable);

                //ISpanned parsed = Html.FromHtml(Control.Text);
                //var lines = parsed.ToString().Split('\n').Count();
                //Control.SetLines(lines + 10);
                //Control.Text = parsed.ToString();

                //Control.TextFormatted = new TextView(this.Context) { TextFormatted = Html.FromHtml(Control.Text) }.TextFormatted;
                //Control.MovementMethod = LinkMovementMethod.Instance;

            }
            
        }

        protected override void OnElementPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            base.OnElementPropertyChanged(sender, e);

            if (e.PropertyName == Label.TextProperty.PropertyName)
            {
                
                if (Control != null && Element != null)
                {
                    //  String cdata;
                    //cdata = "<![CDATA[" + Element.Text + "]>";


                    Control?.SetText(Html.FromHtml(Element.Text), TextView.BufferType.Spannable);

                    //ISpanned parsed = Html.FromHtml(Control.Text);
                    //var lines = parsed.ToString().Split('\n').Count();
                    //Control.SetLines(lines + 10);
                    //Control.Text = parsed.ToString();

                    //  Control.TextFormatted = new TextView(this.Context) { TextFormatted = Html.FromHtml(Control.Text) }.TextFormatted;
                    //Control.MovementMethod = LinkMovementMethod.Instance;
                }
            }
        }

        //-----------------------------------------------------
    }
}