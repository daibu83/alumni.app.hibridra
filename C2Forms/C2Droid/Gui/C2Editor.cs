﻿using System;
using Android.Runtime;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;
using C2Forms;
using C2Forms.Droid;

[assembly: ExportRenderer(typeof(C2EditorC), typeof(C2EditorRenderer))]
namespace C2Forms.Droid
{
    class C2EditorRenderer : EditorRenderer
    {
        private int i_Dp_to_pixels(double dp)
        {
            return (int)(dp * Resources.DisplayMetrics.Density);
        }

        //-----------------------------------------------------

        private void i_Update_control()
        {
            if (Control != null && Element != null)
            {
                Control.SetBackgroundColor(((C2EditorC)Element).BackgroundColor.ToAndroid());

                // http://forums.xamarin.com/discussion/42823/change-entry-cursor
                try
                {
                    IntPtr IntPtrtextViewClass = JNIEnv.FindClass(typeof(Android.Widget.TextView));
                    IntPtr mCursorDrawableResProperty = JNIEnv.GetFieldID(IntPtrtextViewClass, "mCursorDrawableRes", "I");
                    JNIEnv.SetField(Control.Handle, mCursorDrawableResProperty, 0); // replace 0 with a Resource.Drawable.my_cursor 
                }
                catch (Exception)
                {

                }
            }
        }

        //-----------------------------------------------------

        protected override void OnElementChanged(ElementChangedEventArgs<Editor> e)
        {
            base.OnElementChanged(e);
            if (Control != null)
            {
                int padding_vertical = i_Dp_to_pixels(C2Style.Entry_padding_vertical);
                Android.Content.Res.ColorStateList colors = Android.Content.Res.ColorStateList.ValueOf(C2Style.Control_placeholder.ToAndroid());
                Android.Content.Res.ColorStateList text_colors = Android.Content.Res.ColorStateList.ValueOf(C2Style.Control_text.ToAndroid());
                Control.SetHintTextColor(colors);
                Control.SetTextColor(text_colors);
                Control.TextSize = (float)C2Style.Entry_font_size;
                Control.SetPadding(0, padding_vertical, 0, padding_vertical);
                Control.SetHorizontallyScrolling(false);
                if (Element != null)
                {
                    Control.Hint = ((C2EditorC)Element)._placeholder;
                    this.i_Update_control();
                }
            }
        }

        //-----------------------------------------------------

        protected override void OnElementPropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            base.OnElementPropertyChanged(sender, e);
            this.i_Update_control();
        }
    }
}
