﻿using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;
using C2Forms;
using C2Forms.Droid;

[assembly: ExportRenderer(typeof(C2PickerC), typeof(C2PickerRenderer))]
namespace C2Forms.Droid
{
    class C2PickerRenderer : PickerRenderer
    {
        private int i_Dp_to_pixels(double dp)
        {
            return (int)(dp * Resources.DisplayMetrics.Density);
        }

        //-----------------------------------------------------

        private void i_Update_control()
        {
            if (Control != null && Element != null)
            {
                Control.SetBackgroundColor(((C2PickerC)Element).BackgroundColor.ToAndroid());

                switch (((C2PickerC)Element).align)
                {
                    case Xamarin.Forms.TextAlignment.Center:
                        Control.Gravity = Android.Views.GravityFlags.CenterVertical | Android.Views.GravityFlags.CenterHorizontal;
                        break;
                    case Xamarin.Forms.TextAlignment.Start:
                        Control.Gravity = Android.Views.GravityFlags.CenterVertical | Android.Views.GravityFlags.Left;
                        break;
                    case Xamarin.Forms.TextAlignment.End:
                        Control.Gravity = Android.Views.GravityFlags.CenterVertical | Android.Views.GravityFlags.Right;
                        break;
                }
            }
        }

        //-----------------------------------------------------

        protected override void OnElementChanged(ElementChangedEventArgs<Picker> e)
        {
            base.OnElementChanged(e);
            if (Control != null)
            {
                int padding_vertical = this.i_Dp_to_pixels(C2Style.Entry_padding_vertical);
                Android.Content.Res.ColorStateList colors = Android.Content.Res.ColorStateList.ValueOf(C2Style.Control_placeholder.ToAndroid());
                Control.SetHintTextColor(colors);
                Control.TextSize = (float)C2Style.Entry_font_size;
                Control.SetTextColor(C2Style.Control_text.ToAndroid());
                Control.SetPadding(0, padding_vertical, 0, padding_vertical);
                Control.SetCursorVisible(true);
                this.i_Update_control();
            }
        }

        //-----------------------------------------------------

        protected override void OnElementPropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            base.OnElementPropertyChanged(sender, e);
            this.i_Update_control();
        }
    }
}
