using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;
using C2Forms;
using C2Forms.Droid;
using Android.Graphics;
using Android.Widget;

[assembly: ExportRenderer(typeof(C2HyperLinkLabelAwesome), typeof(C2HyperLinkLabelAwesomeRenderer))]
namespace C2Forms.Droid
{
    class C2HyperLinkLabelAwesomeRenderer : LabelRenderer
    {

        public C2HyperLinkLabelAwesomeRenderer()
        {
        }
        //-----------------------------------------------------

        protected override void OnElementChanged(ElementChangedEventArgs<Xamarin.Forms.Label> e)
        {
            base.OnElementChanged(e);

            if (Control != null)
            {
                var label = Control;
                Typeface font;
                try
                {
                    font = Typeface.CreateFromAsset(Forms.Context.Assets, "FontAwesome.ttf");
                    label.Typeface = font;
                }
                catch (System.Exception ex)
                {
                    System.Diagnostics.Debug.WriteLine("TTF file not found. Make sure the Android project contains it at '/Assets/FontAwesome.ttf'.");
                }

                if ((C2HyperLinkLabelAwesome)Element != null) {
                    var view = (C2HyperLinkLabelAwesome)Element;
                    var control = Control;
                    UpdateUi(view, control);
                }

                
            }
        }

        //-----------------------------------------------------

        /// <summary>
        /// Updates the UI.
        /// </summary>
        /// <param name="view">The view.</param>
        /// <param name="control">The control.</param>
        void UpdateUi(C2HyperLinkLabelAwesome view, TextView control)
        {

            if (view.IsUnderline)
            {
                control.PaintFlags = control.PaintFlags | PaintFlags.UnderlineText;
            }

        }

    }
}

