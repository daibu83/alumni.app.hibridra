﻿using Android.Graphics;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;
using C2Forms;
using C2Forms.Droid;
using System.Threading.Tasks;

[assembly: ExportRenderer(typeof(C2RoundedBox), typeof(C2RoundedBoxRenderer))]
namespace C2Forms.Droid
{
    public class C2RoundedBoxRenderer : BoxRenderer
    {
        //-----------------------------------------------------

        protected override void OnElementChanged(ElementChangedEventArgs<BoxView> e)
        {
            base.OnElementChanged(e);
            this.SetWillNotDraw(false);
            this.Invalidate();
        }

        //-----------------------------------------------------

        protected override void OnElementPropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            base.OnElementPropertyChanged(sender, e);

            if (e.PropertyName == C2RoundedBox.CornerRadiusProperty.PropertyName)
            {
                this.Invalidate();
            }
        }

        //-----------------------------------------------------
        
        public override void Draw(Canvas canvas)
        {
            if (this.Element != null)
            {
                C2RoundedBox box = (C2RoundedBox)Element;
                Rect rect = new Rect();
                Paint paint = new Paint();
                paint.Color = box.BackgroundColor.ToAndroid();
                paint.StrokeWidth = 2;
                paint.AntiAlias = true;
                this.GetDrawingRect(rect);
                float radius = (float)(rect.Width() / box.Width * box.CornerRadius);
                paint.SetStyle(Paint.Style.Fill);
                canvas.DrawRoundRect(new RectF(rect), radius, radius, paint);
                paint.Color = box.borderColor.ToAndroid();
                paint.SetStyle(Paint.Style.Stroke);
                rect.Left += 1;
                rect.Right -= 1;
                rect.Top += 1;
                rect.Bottom -= 1;
                canvas.DrawRoundRect(new RectF(rect), radius, radius, paint);
            }
        }

        //-----------------------------------------------------


    }
}