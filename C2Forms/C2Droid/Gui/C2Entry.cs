﻿using System;
using Android.Runtime;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;
using C2Forms;
using C2Forms.Droid;
using System.ComponentModel;
using Android.Views.InputMethods;

[assembly: ExportRenderer(typeof(C2EntryC), typeof(C2EntryRenderer))]
namespace C2Forms.Droid
{
    class C2EntryRenderer : EntryRenderer
    {
        

        private int i_Dp_to_pixels(double dp)
        {
            return (int)(dp * Resources.DisplayMetrics.Density);
        }

        //-----------------------------------------------------

        private void i_Update_control()
        {
            if (Control != null && Element != null)
            {
                Control.SetBackgroundColor(((C2EntryC)Element).BackgroundColor.ToAndroid());

                if ( ((C2EntryC)Element).IsPassword == false)
                {
                    Control.SetHorizontallyScrolling(true);
                    Control.SetSingleLine();
                }

                // http://forums.xamarin.com/discussion/42823/change-entry-cursor
                try
                {
                    IntPtr IntPtrtextViewClass = JNIEnv.FindClass(typeof(Android.Widget.TextView));
                    IntPtr mCursorDrawableResProperty = JNIEnv.GetFieldID(IntPtrtextViewClass, "mCursorDrawableRes", "I");
                    JNIEnv.SetField(Control.Handle, mCursorDrawableResProperty, 0); // replace 0 with a Resource.Drawable.my_cursor 
                }
                catch (Exception)
                {

                }

                switch (((C2EntryC)Element).align)
                {
                    case Xamarin.Forms.TextAlignment.Center:
                        Control.Gravity = Android.Views.GravityFlags.CenterVertical | Android.Views.GravityFlags.CenterHorizontal;
                        break;
                    case Xamarin.Forms.TextAlignment.Start:
                        Control.Gravity = Android.Views.GravityFlags.CenterVertical | Android.Views.GravityFlags.Left;
                        break;
                    case Xamarin.Forms.TextAlignment.End:
                        Control.Gravity = Android.Views.GravityFlags.CenterVertical | Android.Views.GravityFlags.Right;
                        break;
                }
            }
        }

        //-----------------------------------------------------

        protected override void OnElementChanged(ElementChangedEventArgs<Entry> e)
        {
            base.OnElementChanged(e);
            if (Control != null)
            {
                

                int padding_vertical = this.i_Dp_to_pixels(C2Style.Entry_padding_vertical);
                Android.Content.Res.ColorStateList colors = Android.Content.Res.ColorStateList.ValueOf(C2Style.Control_placeholder.ToAndroid());
                Control.SetHintTextColor(colors);
                Control.TextSize = (float)C2Style.Entry_font_size;
                Control.SetPadding(0, padding_vertical, 0, padding_vertical);
                Control.SetCursorVisible(true);
                this.i_Update_control();
            }
            
        }

        //-----------------------------------------------------

        protected override void OnElementPropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            base.OnElementPropertyChanged(sender, e);
            
            
            this.i_Update_control();
        }
    }
    
}
