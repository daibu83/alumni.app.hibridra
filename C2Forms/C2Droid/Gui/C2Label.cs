﻿using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;
using C2Forms;
using C2Forms.Droid;
using Android.Graphics;
using Android.Widget;

[assembly: ExportRenderer(typeof(C2Label), typeof(C2LabelRenderer))]
namespace C2Forms.Droid
{
    class C2LabelRenderer : LabelRenderer
    {
        protected override void OnElementChanged(ElementChangedEventArgs<Xamarin.Forms.Label> e)
        {
            base.OnElementChanged(e);
            if (Control != null && Element != null) {
                Control.SetLines(((C2Label)Element)._num_lines);
            }
             
        }

        
        //-----------------------------------------------------


    }
}

