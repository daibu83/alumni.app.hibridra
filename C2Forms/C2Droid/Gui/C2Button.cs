﻿using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;
using C2Forms;
using C2Forms.Droid;

[assembly: ExportRenderer(typeof(C2Button), typeof(C2ButtonRenderer))]
namespace C2Forms.Droid
{
    class C2ButtonRenderer : ButtonRenderer
    {
        protected override void OnElementChanged(ElementChangedEventArgs<Xamarin.Forms.Button> e)
        {
            base.OnElementChanged(e);
            if (Control != null)
            {
                Android.Content.Res.ColorStateList colors;


                if ((C2Button)this.Element != null)
                {
                    C2Button elem;
                    elem = (C2Button)this.Element;
                    colors = Android.Content.Res.ColorStateList.ValueOf(elem.Color.ToAndroid());
                }
                else
                {
                    colors = Android.Content.Res.ColorStateList.ValueOf(C2Style.Button_text.ToAndroid());
                }


                Control.SetTextColor(colors);
                //Control.SetTextColor(el.Color);
                
                Control.TextSize = (float)C2Style.Button_font_size;
                Control.Gravity = Android.Views.GravityFlags.CenterVertical | Android.Views.GravityFlags.Center;
                Control.SetAllCaps(false);
            }
        }

        //-----------------------------------------------------

    }
}
