using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;
using C2Forms;
using C2Forms.Droid;
using Android.Graphics;
using Android.Widget;
using System;
using Android.Runtime;

[assembly: ExportRenderer(typeof(SearchBar), typeof(ExtendedSearchBarRenderer))]
namespace C2Forms.Droid
{
    public class ExtendedSearchBarRenderer : SearchBarRenderer
    {
        public ExtendedSearchBarRenderer(): base ()
		{ }

        public ExtendedSearchBarRenderer(IntPtr javaReference, JniHandleOwnership transfer)	: base ()
		{
        }

        protected override void OnDetachedFromWindow()
        {
            if (Element == null)
            {
                return;
            }
            base.OnDetachedFromWindow();
        }

        //protected override void OnElementPropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        //{
        //    base.OnElementPropertyChanged(sender, e);

        //    if (Control != null)
        //    {
        //        Control.ShowsCancelButton = false;
        //    }
        //}
    }
}