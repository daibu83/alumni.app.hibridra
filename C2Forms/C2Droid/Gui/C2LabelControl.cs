﻿using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;
using C2Forms;
using C2Forms.Droid;
using Android.Graphics;
using Android.Widget;

[assembly: ExportRenderer(typeof(C2LabelControl), typeof(C2LabelControlRenderer))]
namespace C2Forms.Droid
{
    class C2LabelControlRenderer : LabelRenderer
    {
        private int i_Dp_to_pixels(double dp)
        {
            return (int)(dp * Resources.DisplayMetrics.Density);
        }

        //-----------------------------------------------------

        protected override void OnElementChanged(ElementChangedEventArgs<Xamarin.Forms.Label> e)
        {
            base.OnElementChanged(e);
            if (Control != null)
            {
                int padding_vertical = i_Dp_to_pixels(C2Style.Entry_padding_vertical);
                Control.SetPadding(0, padding_vertical, 0, padding_vertical);
            }
        }

       

    }
}

