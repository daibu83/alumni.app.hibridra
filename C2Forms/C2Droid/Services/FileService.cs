﻿using System;
using System.Collections.Generic;
using System.IO;
using C2Forms.Droid;

[assembly: Xamarin.Forms.Dependency(typeof(FileServiceAndroid))]
namespace C2Forms.Droid
{
    public class FileServiceAndroid : IFileService
    {
        //-----------------------------------------------------

        public void Save_data(List<string> data)
        {
            try
            {
                System.IO.Stream stream = Android.App.Application.Context.OpenFileOutput("cache_data", Android.Content.FileCreationMode.Private);
                BinaryWriter writer = new BinaryWriter(stream);

                writer.Write(data.Count);
                for (int i = 0; i < data.Count; ++i)
                    writer.Write(data[i]);

                writer.Close();
                stream.Close();
            }
            catch (Exception e)
            {
            }
        }

        //-----------------------------------------------------

        public void Read_data(List<string> data)
        {
            try
            {
                System.IO.Stream stream = Android.App.Application.Context.OpenFileInput("cache_data");
                BinaryReader reader = new BinaryReader(stream);
                int count = reader.ReadInt32();
                for (int i = 0; i < count; ++i)
                {
                    string str;
                    str = reader.ReadString();
                    data.Add(str);
                }

                reader.Close();
                stream.Close();
            }
            catch (Exception e)
            {
            }
        }

    }
}

