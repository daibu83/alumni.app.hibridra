using System;
using Android.App;
using Xamarin.Forms;
using C2Forms.Droid;

[assembly: Xamarin.Forms.Dependency(typeof(PickImageServiceAndroid))]
namespace C2Forms.Droid
{
    public class PickImageServiceAndroid : Java.Lang.Object, IPickImageService
    {
        public static readonly int PICK_IMAGE_REQUEST_CODE = 1000;
        public static Activity activity = null;
        private static FPtr_on_image_pick _func_on_image_pick;

        //-----------------------------------------------------

        public void Pick_image(string text, FPtr_on_image_pick func_on_image_pick)
        {
            if (PickImageServiceAndroid.activity != null)
            {
                Android.Content.Intent imageIntent = new Android.Content.Intent();
                imageIntent.SetType("image/*");
                imageIntent.SetAction(Android.Content.Intent.ActionGetContent);
                PickImageServiceAndroid._func_on_image_pick = func_on_image_pick;
                PickImageServiceAndroid.activity.StartActivityForResult(Android.Content.Intent.CreateChooser(imageIntent, text), PICK_IMAGE_REQUEST_CODE);
            }
        }

        //-----------------------------------------------------

        public static void Load_image(Android.Content.Intent data)
        {
            byte[] image_data = null;

            if (PickImageServiceAndroid.activity == null)
                return;

            if (PickImageServiceAndroid._func_on_image_pick == null)
                return;

            if (data != null)
            {
                try
                {
                    Android.Net.Uri uri = data.Data;
                    if (uri != null)
                    {
                        System.IO.Stream stream = PickImageServiceAndroid.activity.ContentResolver.OpenInputStream(uri);
                        Android.Graphics.Bitmap bitmap = Android.Graphics.BitmapFactory.DecodeStream(stream);
                        Android.Graphics.Bitmap scaled_bitmap;

                        if (bitmap.Width > 200)
                        {
                            int new_width = 200;
                            int new_height = bitmap.Height * new_width / bitmap.Width;
                            scaled_bitmap = Android.Graphics.Bitmap.CreateScaledBitmap(bitmap, new_width, new_height, false);
                        }
                        else
                        {
                            scaled_bitmap = bitmap;
                        }

                        System.IO.MemoryStream jpg_stream = new System.IO.MemoryStream();
                        scaled_bitmap.Compress(Android.Graphics.Bitmap.CompressFormat.Jpeg, 90, jpg_stream);
                        image_data = jpg_stream.ToArray();

                        /*
                        long total_bytes = 0;
                        System.IO.Stream stream = PickImageServiceAndroid.activity.ContentResolver.OpenInputStream(uri);
                        Java.IO.BufferedInputStream reader = new Java.IO.BufferedInputStream(stream);
                        System.Collections.Generic.List<byte[]> ldata = new System.Collections.Generic.List<byte[]>();
                        if (stream.CanRead == true)
                        {
                            int chunk_bytes;
                            while ((chunk_bytes = reader.Available()) > 0)
                            {
                                byte[] chunk = new byte[chunk_bytes];
                                reader.Read(chunk, 0, chunk_bytes);
                                ldata.Add(chunk);
                                total_bytes += chunk_bytes;
                            }
                        }

                        stream.Close();

                        if (total_bytes > 0)
                        {
                            if (ldata.Count == 1)
                            {
                                image_data = ldata[0];
                            }
                            else
                            {
                                long offset = 0;
                                image_data = new byte[total_bytes];
                                for (int i = 0; i < ldata.Count; ++i)
                                {
                                    System.Buffer.BlockCopy(ldata[i], 0, image_data, (int)offset, ldata[i].Length);
                                    offset += ldata[i].Length;
                                }
                            }
                        }
                        */
                    }
                }
                catch (Exception)
                {

                }
            }

            PickImageServiceAndroid._func_on_image_pick(image_data);
        }
    }
}

