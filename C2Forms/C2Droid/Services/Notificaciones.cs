
using Android.App;
using Android.Content;
using Android.OS;
using C2Forms.Droid;
using Android.Graphics;
using Bewanted.Droid;
using Bewanted;

[assembly: Xamarin.Forms.Dependency(typeof(Notificaciones))]
namespace C2Forms.Droid
{
    public class Notificaciones : INotificaciones
    {
        public Notificaciones()
        {
        }

        public void contadorIcono(int contador)
        {
            
        }

        public void iniciaServicio()
        {
            Intent intent = new Intent(Application.Context, typeof(NotificationService));
            //intent.PutExtra("user_id", AppDelegate._user_id);
            //intent.PutExtra("device_id", Server.device_id);
            Application.Context.StartService(intent);

        }

        public void paraServicio()
        {
            Intent intent = new Intent(Application.Context, typeof(NotificationService));
            Application.Context.StopService(intent);
        }

        #region INotificaciones implementation

        //Duplicada en FICHERO:Droid HelperService.cs
        public void SendLocalNotification(string title, string description, string descripcion_extendida,string sumary,string id,string fecha, int iconID)
        {

            Intent intent = new Intent(Application.Context, typeof(MainActivity));
            int notificationId = 0;
            if (id != "") { notificationId = System.Convert.ToInt32(id); }
            PendingIntent pIntent = PendingIntent.GetActivity(Application.Context, notificationId, intent, PendingIntentFlags.CancelCurrent);

            Notification.BigTextStyle textStyle = new Notification.BigTextStyle();

            if (descripcion_extendida!="") { textStyle.BigText(descripcion_extendida);
            }else { textStyle.BigText(description); }


            if (sumary != "") { textStyle.SetSummaryText(sumary); }
            
            Notification.Builder builder = new Notification.Builder(Application.Context)
                .SetContentTitle(title)
                .SetContentText(description)
                .SetContentIntent(pIntent)
                .SetDefaults(NotificationDefaults.Sound)
                .SetAutoCancel(true)
                .SetSmallIcon(Bewanted.Droid.Resource.Drawable.icon_notify);

            if (descripcion_extendida != "")
            {
                builder.SetStyle(textStyle);
                Bitmap bitmap = BitmapFactory.DecodeResource(Application.Context.Resources, Bewanted.Droid.Resource.Drawable.icon);
                builder.SetLargeIcon(bitmap);
            }

            if (Build.VERSION.SdkInt >= BuildVersionCodes.Lollipop)
            {
                int whiteColorValue = Color.White;
                builder.SetColor(whiteColorValue);
                builder.SetPriority(2);
            }
            Notification notification = builder.Build();

            //para evitar con largeIcon que aparezcan los dos-se sustituye el small por el icon a mostrar en la notificacion NO Extendida
            notification.ContentView.SetImageViewResource(Android.Resource.Id.Icon, Bewanted.Droid.Resource.Drawable.icon);

            notification.Flags|=NotificationFlags.OnlyAlertOnce;
            
            NotificationManager notificationManager =
            Application.Context.GetSystemService(Context.NotificationService) as NotificationManager;
            
            
            notificationManager.Notify(notificationId, notification);

            //marcar como leida
            AppDelegate.On_Read_notification(id);
            
        }

        #endregion
    }
}

