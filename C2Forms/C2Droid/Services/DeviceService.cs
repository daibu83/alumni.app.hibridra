﻿using Android.Provider;
using C2Forms.Droid;

[assembly: Xamarin.Forms.Dependency(typeof(DeviceServiceAndroid))]
namespace C2Forms.Droid
{
    class DeviceServiceAndroid : IDeviceService
    {
        //Duplicada en FICHERO:Droid HelperService.cs-de BeWanted project
        public string DeviceId()
        {
            return Settings.Secure.GetString(Android.App.Application.Context.ContentResolver, Android.Provider.Settings.Secure.AndroidId);
        }

        //-----------------------------------------------------

        public int AndroidAPILevel()
        {
            return (int)Android.OS.Build.VERSION.SdkInt;
        }
    }
}
